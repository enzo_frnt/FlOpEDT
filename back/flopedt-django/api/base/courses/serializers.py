# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

import base.models as bm
import people.models as pm
import displayweb.models as dwm
from api.base.serializers import TrainingProgramsSerializer
from rest_framework import serializers


class PeriodSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    start_date = serializers.DateField()
    end_date = serializers.DateField()

    class Meta:
        model = bm.SchedulingPeriod
        fields = ["id", "name", "start_date", "end_date"]


class Department_TC_Serializer(serializers.Serializer):
    name = serializers.CharField()
    abbrev = serializers.CharField()
    id = serializers.IntegerField()

    class Meta:
        model = bm.Department
        fields = ["name", "abbrev", "id"]


class CourseTypeSimpleSerializer(serializers.Serializer):
    department = Department_TC_Serializer()
    name = serializers.CharField()
    id = serializers.IntegerField()

    class Meta:
        model = bm.CourseType
        fields = ["id", "name", "department"]


class ModuleDisplay_SC_Serializer(serializers.Serializer):
    color_bg = serializers.CharField()
    color_txt = serializers.CharField()

    class Meta:
        model = dwm.ModuleDisplay
        fields = ["color_bg", "color_txt"]


class Module_SC_Serializer(serializers.Serializer):
    name = serializers.CharField()
    abbrev = serializers.CharField()
    display = ModuleDisplay_SC_Serializer()
    id = serializers.IntegerField()

    class Meta:
        model = bm.Module
        fields = ["id", "name", "abbrev", "display"]


class Group_SC_Serializer(serializers.Serializer):
    id = serializers.IntegerField()
    train_prog = serializers.CharField()
    name = serializers.CharField()
    is_structural = serializers.BooleanField()

    class Meta:
        model = bm.GenericGroup
        fields = ["id", "name", "train_prog", "is_structural"]


class TrainingPrograms_M_Serializer(serializers.Serializer):
    abbrev = serializers.CharField()

    class Meta:
        model = bm.TrainingProgramme
        fields = [
            "abbrev",
        ]


class Period_M_Serializer(serializers.ModelSerializer):
    class Meta:
        model = bm.TrainingPeriod
        fields = ["periods", "name"]


class ModuleFullSerializer(serializers.ModelSerializer):
    train_prog = TrainingProgramsSerializer()
    period = Period_M_Serializer()

    class Meta:
        model = bm.Module
        fields = ["name", "abbrev", "head", "ppn", "url", "train_prog", "period", "id"]


class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.Module
        fields = ["name", "abbrev", "url", "id"]


class Department_Name_Serializer(serializers.Serializer):
    name = serializers.CharField()

    class Meta:
        model = bm.Department
        fields = ["name", "id"]


class CourseType_C_Serializer(serializers.Serializer):
    department = Department_TC_Serializer()
    name = serializers.CharField()

    class Meta:
        model = bm.CourseType
        fields = ["id", "name", "department"]


class RoomType_SC_Serializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()

    class Meta:
        model = bm.RoomType
        fields = ["id", "name"]


class RoomType_C_Serializer(serializers.Serializer):
    name = serializers.CharField()

    class Meta:
        model = bm.RoomType
        fields = ["id", "name"]


class Group_C_Serializer(serializers.Serializer):
    name = serializers.CharField()

    class Meta:
        model = bm.StructuralGroup
        fields = ["id", "name"]


class Module_C_Serializer(serializers.Serializer):
    abbrev = serializers.CharField()
    id = serializers.IntegerField()

    class Meta:
        model = bm.Module
        fields = ["id", "abbrev"]


class TutorBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = pm.User
        fields = ("username", "first_name", "last_name", "id")


class CourseTypeSerializer(serializers.ModelSerializer):
    name = serializers.CharField()

    class Meta:
        model = bm.CourseType
        fields = ["id", "name"]


class CourseTypeNameSerializer(serializers.ModelSerializer):
    name = serializers.CharField()

    class Meta:
        model = bm.CourseType
        fields = ["id", "name"]


class TutorBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = pm.User
        fields = ("username", "first_name", "last_name", "id")


class CoursesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    type = CourseTypeSimpleSerializer()
    duration = serializers.DurationField()
    room_type = RoomType_SC_Serializer()
    period = PeriodSerializer()
    groups = Group_SC_Serializer(many=True)
    tutor = TutorBasicSerializer()
    supp_tutors = TutorBasicSerializer(many=True)
    module = Module_SC_Serializer()
    modulesupp = Module_C_Serializer()
    pay_module = Module_C_Serializer()
    is_graded = serializers.BooleanField()

    class Meta:
        model = bm.Course
        fields = [
            "id",
            "period",
            "department",
            "type",
            "duration",
            "room_type",
            "tutor",
            "supp_tutors",
            "groups",
        ]
