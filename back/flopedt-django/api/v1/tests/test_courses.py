import pytest
from rest_framework import status
from rest_framework.test import APIClient

from api.v1.tests.utils import retrieve_elements, unwrap
from base.models import Course, Department
from people.models import Tutor

# pylint: disable=unused-argument


class TestCourses:
    endpoint = "/fr/api/v1/base/courses/courses/"
    courses = [
        {
            "type": {"name": "Lab", "department": None},
            "duration": "01:30:00",
            "room_type": None,
            "period": None,
            "groups": [
                {
                    "name": "CM",
                    "train_prog": {"abbrev": "tp 00"},
                    "is_structural": True,
                }
            ],
            "tutor": None,
            "supp_tutors": [],
            "module": {
                "name": "Module #00",
                "abbrev": "mod00",
                "display": None,
            },
            "modulesupp": None,
            "pay_module": None,
            "is_graded": False,
        },
        {
            "type": {"name": "Lab", "department": None},
            "duration": "01:30:00",
            "room_type": None,
            "period": None,
            "groups": [
                {
                    "name": "TD0",
                    "train_prog": {"abbrev": "tp 00"},
                    "is_structural": True,
                }
            ],
            "tutor": None,
            "supp_tutors": [],
            "module": {
                "name": "Module #00",
                "abbrev": "mod00",
                "display": None,
            },
            "modulesupp": None,
            "pay_module": None,
            "is_graded": False,
        },
    ]

    def test_courses_dept_required(self, db, client: APIClient, make_courses):
        make_courses(1)
        response = client.get(self.endpoint)
        assert not status.is_success(response.status_code)

    @pytest.mark.skip("FIXME especially module_supp")
    def test_courses_list(self, db, client: APIClient, make_courses):
        make_courses(2)

        dept = Department.objects.first()
        response = client.get(self.endpoint, {"dept": dept.abbrev})
        elements = retrieve_elements(response, n=2)
        for e in elements:
            del e["id"]
            del e["type"]["id"]
            for g in e["groups"]:
                del g["id"]
                del g["train_prog"]["id"]
            del e["module"]["id"]
        assert elements in (self.courses, self.courses[::-1])

    @pytest.mark.skip("Until course structure is stable")
    def test_courses_update_return_struct(self, db, client: APIClient, make_courses):
        make_courses(2)
        c = Course.objects.first()
        # assert len(c.groups.all()) == 1
        # gp_name = c.groups.all()[0].name

        t = Tutor.objects.create(username="bliblo")
        response = client.patch(f"{self.endpoint}{c.id}/", {"tutor_id": t.id})
        expected_course_keys = set(self.courses[0].keys())
        expected_course_keys.add("id")
        response = unwrap(response)
        assert set(response.keys()) >= expected_course_keys

        expected_module_keys = set(self.courses[0]["module"].keys())
        expected_module_keys.add("id")
        assert set(response["module"].keys()) == expected_module_keys

        assert isinstance(response["groups"], list)

        expected_group_keys = set(self.courses[0]["groups"][0].keys())
        expected_group_keys.add("id")
        for g in response["groups"]:
            assert set(g.keys()) == expected_group_keys
