import pytest


class TestScheduledCourseList:
    endpoint = "/fr/api/v1/base/courses/scheduled_courses"

    @pytest.mark.skip("Not yet implemented")
    def test_dept_filter(self, client):
        pass

    @pytest.mark.skip("Not yet implemented")
    def test_dept_and_tp_filter_fail(self, client):
        pass

    @pytest.mark.skip("Not yet implemented")
    def test_dept_and_tp_filter_success(self, client):
        pass

    @pytest.mark.skip("Not yet implemented")
    def test_perm_change_denied(self, client):
        pass
