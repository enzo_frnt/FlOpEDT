# Fixtures use to unuse arguments and variables...
# pylint: disable=unused-argument, unused-variable

import pytest

from ..factories.course import (
    CourseRRGroup,
    CourseTypeFactory,
    ModuleFactory,
    TrainingPeriodDummyFactory,
)
from ..factories.group import DepartmentFactory, TrainingProgrammeFactory


@pytest.fixture
def make_courses(db, make_classical_structural_groups):
    def make_multiple(n):
        TrainingPeriodDummyFactory.create()
        d = DepartmentFactory.create()
        tp = TrainingProgrammeFactory.create()
        groups = make_classical_structural_groups(tp)
        mod = ModuleFactory.create(train_prog=tp)
        course_type = CourseTypeFactory.create()
        return CourseRRGroup.create_batch(n)

    return make_multiple
