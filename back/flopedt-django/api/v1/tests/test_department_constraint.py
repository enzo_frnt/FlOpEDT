# disable unused-argument for fixtures

import pytest
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
)

from base.models import Department


class TestDepartmentConstraints:
    endpoint = "/fr/api/v1/departments/:dept_id/constraints/"

    def test_unknown_dept(self, db, client):  # pylint: disable=unused-argument
        assert Department.objects.count() == 0
        response = client.get(self.endpoint.replace(":dept_id", "1"))
        assert response.status_code == HTTP_404_NOT_FOUND, response.content

    @pytest.mark.skip("not yet implemented")
    def test_filter_dept(self):
        pass

    @pytest.mark.skip("not yet implemented")
    def test_permissions(self):
        # TBD; should this be locked to be only available to administrators?
        pass
