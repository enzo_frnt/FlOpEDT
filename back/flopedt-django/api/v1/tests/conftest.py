# This file is used to import all the fixtures from the other files in the same directory.
# pylint: disable=wildcard-import, unused-wildcard-import

from api.v1.tests.fixtures.availability import *
from api.v1.tests.fixtures.base import *
from api.v1.tests.fixtures.configuration import *
from api.v1.tests.fixtures.course import *
from api.v1.tests.fixtures.group import *
from api.v1.tests.fixtures.people import *
from api.v1.tests.fixtures.permission import *
from api.v1.tests.fixtures.room import *
