# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.
from typing import List

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

import base.models as bm
import people.models as pm
import displayweb.models as dwm

from api.v1.base.modification.serializers import TimetableVersionShortSerializer

#                             ------------------------------                            #
#                             ----Scheduled Courses (SC)----                            #
#                             ------------------------------                            #


class DepartmentFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.Department
        fields = ["id", "name", "abbrev"]


class CourseTypeMainSerializer(serializers.ModelSerializer):
    department = DepartmentFullSerializer()

    class Meta:
        model = bm.CourseType
        fields = ["id", "name", "department"]


class RoomTypeFullSerializer(serializers.ModelSerializer):

    class Meta:
        model = bm.RoomType
        fields = ["id", "name", "department_id"]


class SchedulingPeriodMainSerializer(serializers.ModelSerializer):

    class Meta:
        model = bm.SchedulingPeriod
        fields = ["id", "name", "start_date", "end_date"]


class TrainingProgrammeMinSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.TrainingProgramme
        fields = ["id", "abbrev"]


class GroupMainSerializer(serializers.ModelSerializer):
    train_prog = TrainingProgrammeMinSerializer()

    class Meta:
        model = bm.GenericGroup
        fields = ["id", "name", "train_prog", "is_structural"]


class TutorMainSerializer(serializers.ModelSerializer):
    class Meta:
        model = pm.User
        fields = ("id", "username", "first_name", "last_name")


class ModuleMinSerializer(serializers.ModelSerializer):

    class Meta:
        model = bm.Module
        fields = ["id", "abbrev"]


class ModuleDisplayFullSerializer(serializers.ModelSerializer):

    class Meta:
        model = dwm.ModuleDisplay
        fields = ["color_bg", "color_txt"]


class ModuleFullSerializer(serializers.ModelSerializer):
    display = ModuleDisplayFullSerializer()

    class Meta:
        model = bm.Module
        fields = ["id", "name", "abbrev", "display"]


class CourseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    period_id = serializers.IntegerField(required=False, write_only=True)
    period = SchedulingPeriodMainSerializer(read_only=True)
    type_id = serializers.IntegerField(write_only=True)
    type = CourseTypeMainSerializer(read_only=True)
    room_type_id = serializers.IntegerField(required=False, write_only=True)
    room_type = RoomTypeFullSerializer(read_only=True)
    tutor_id = serializers.IntegerField(required=False, write_only=True)
    tutor = TutorMainSerializer(read_only=True)
    supp_tutor_ids = serializers.PrimaryKeyRelatedField(
        source="supp_tutors",
        queryset=pm.Tutor.objects.all(),
        many=True,
        write_only=True,
    )
    supp_tutors = TutorMainSerializer(many=True, read_only=True)
    group_ids = serializers.PrimaryKeyRelatedField(
        source="groups", queryset=bm.GenericGroup.objects.all(), many=True
    )
    groups = GroupMainSerializer(many=True, read_only=True)
    module_id = serializers.IntegerField(write_only=True)
    module = ModuleFullSerializer(read_only=True)
    modulesupp_id = serializers.IntegerField(required=False)
    pay_module_id = serializers.IntegerField(required=False)
    suspens = serializers.BooleanField(required=False)
    is_graded = serializers.BooleanField()
    duration = serializers.DurationField()

    def create(self, validated_data):
        groups = []
        supp_tutors = []
        if "groups" in validated_data:
            groups = validated_data.pop("groups", [])
        if "supp_tutors" in validated_data:
            supp_tutors = validated_data.pop("supp_tutors", [])
        is_graded = validated_data.pop("is_graded", False)
        if "period" in validated_data:
            validated_data["period_id"] = validated_data.pop("period")["id"]
        course = bm.Course.objects.create(**validated_data)
        course.groups.set(groups)
        course.supp_tutors.set(supp_tutors)
        if is_graded:
            bm.CourseAdditional.objects.create(course=course, graded=is_graded)
        return course

    def update(self, instance, validated_data):
        if "groups" in validated_data:
            instance.groups.set(validated_data.pop("groups", []))
        if "supp_tutors" in validated_data:
            instance.supp_tutors.set(validated_data.pop("supp_tutors", []))
        if "is_graded" in validated_data:
            if hasattr(instance, "additional"):
                add_instance = instance.additional
            else:
                add_instance = bm.CourseAdditional.objects.create(course=instance)
            add_instance.graded = validated_data.pop("is_graded", False)
            add_instance.save()
        if "period" in validated_data:
            validated_data["period_id"] = validated_data.pop("period")["id"]
        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()
        return instance

    class Meta:
        model = bm.Course
        fields = [
            "id",
            "period_id",
            "period",
            "type_id",
            "type",
            "room_type_id",
            "room_type",
            "tutor_id",
            "tutor",
            "supp_tutor_ids",
            "supp_tutors",
            "group_ids",
            "groups",
            "module_id",
            "module",
            "modulesupp_id",
            "pay_module_id",
            "suspens",
            "is_graded",
            "duration",
        ]


class ScheduledCoursesSerializer(serializers.ModelSerializer):
    module_id = serializers.IntegerField(source="course.module.id")
    course_type_id = serializers.IntegerField(source="course.type.id")
    supp_tutors_ids = serializers.PrimaryKeyRelatedField(
        read_only=True, many=True, source="course.supp_tutors"
    )
    end_time = serializers.DateTimeField()
    number = serializers.IntegerField()
    train_prog_id = serializers.IntegerField(source="course.module.train_prog.id")
    group_ids = serializers.PrimaryKeyRelatedField(
        read_only=True, many=True, source="course.groups"
    )
    version = TimetableVersionShortSerializer()

    # Sructuration of the data
    class Meta:
        model = bm.ScheduledCourse
        fields = [
            "id",
            "course_id",
            "module_id",
            "course_type_id",
            "tutor_id",
            "supp_tutors_ids",
            "room_id",
            "start_time",
            "end_time",
            "train_prog_id",
            "group_ids",
            "number",
            "version",
        ]

    @classmethod
    def and_related(cls):
        return {
            "select": {"course": ["duration"], "version": ["id", "minor", "major"]},
            "prefetch": {},
        }


class RoomsSerializer(serializers.ModelSerializer):
    department_ids = serializers.SerializerMethodField()
    over_room_ids = serializers.SerializerMethodField()

    class Meta:
        model = bm.Room
        fields = ("id", "name", "over_room_ids", "department_ids")

    @extend_schema_field(List[OpenApiTypes.INT])
    def get_department_ids(self, obj):
        return [dep.id for dep in obj.departments.all()]

    @extend_schema_field(List[OpenApiTypes.INT])
    def get_over_room_ids(self, obj):
        return [over_room.id for over_room in obj.subroom_of.all()]


class ModulesSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.Module
        exclude = ("description", "ppn")


class ModulesFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.Module
        fields = (
            "id",
            "name",
            "abbrev",
            "head_id",
            "train_prog_id",
            "training_period",
            "url",
            "description",
            "ppn",
        )


class TimetableVersionSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.TimetableVersion
        fields = ("id", "period_id", "major", "minor")
