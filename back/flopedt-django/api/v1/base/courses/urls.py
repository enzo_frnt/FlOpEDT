# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.
from rest_framework import routers

from api.v1.base.courses import views as courses_views
# TODO reorganize
from api.base import views as old_views

routerCourse = routers.SimpleRouter()

routerCourse.register(
    r"courses",
    courses_views.CoursesViewSet,
    basename="courses",
)
routerCourse.register(
    r"scheduled_courses",
    courses_views.ScheduledCoursesFullParamViewSet,
    basename="scheduled_courses",
)
routerCourse.register(
    r"scheduled_courses__human",
    courses_views.ScheduledCoursesHumanParamViewSet,
    basename="scheduled_courses_human",
)
routerCourse.register(
    r"dependencies",
    old_views.DependenciesViewSet,
    basename="dependencies",
)

routerCourse.register(r"module", courses_views.ModuleViewSet, basename="module")
routerCourse.register(
    r"module-full", courses_views.ModuleFullViewSet, basename="module-full"
)
routerCourse.register(r"rooms", courses_views.RoomsViewSet, basename="rooms")
routerCourse.register(
    r"version", courses_views.TimetableVersionViewSet, basename="version"
)

