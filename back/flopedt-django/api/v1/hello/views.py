# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from drf_spectacular.utils import extend_schema
from rest_framework import views
from rest_framework.response import Response

from .serializers import HelloSerializer


# Ensure this endpoint sets CSRF cookies, so the SPA can use it to perform API calls.
@method_decorator(ensure_csrf_cookie, name="dispatch")
class HelloView(views.APIView):
    @extend_schema(responses=HelloSerializer)
    def get(self, request):
        user = request.user if request.user.id is not None else None
        data = {
            "whoami": user,
            "preferences": None,
            # TODO: User preferences relevant for the frontend (theme, configured locale, ...)
            "configuration": {
                "mandatory_authentication": False,
                # TODO: add more settings to pass to the frontend app
                # This can include any cosmetic change (e.g. the name of the organization)
                # to display, or information
                # about the capabilities of the server (whether it can send emails, etc.)
                #
                # For example:
                # 'display_name': 'IUT de Blagnac',
                # 'logo_url': 'https://www.iut-blagnac.fr/images/structure/iut-blagnac-logo.png',
            },
        }

        return Response(HelloSerializer(data).data)
