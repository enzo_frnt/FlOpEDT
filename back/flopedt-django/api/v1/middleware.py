import re
from django.http import HttpResponseNotFound
from base.models import Department


class DepartmentMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.pattern = re.compile(r'v1/departments/(?P<dept_id>\d+)')

    def __call__(self, request):
        # Check if the request path is on the departments route
        match = self.pattern.search(request.path)
        if match is not None:
            dept_id = int(match.group('dept_id'))

            try:
                request.flop_department = Department.objects.get(pk=dept_id)
            except Department.DoesNotExist:
                return HttpResponseNotFound(f"Department {dept_id} does not exist.")

        response = self.get_response(request)
        return response
