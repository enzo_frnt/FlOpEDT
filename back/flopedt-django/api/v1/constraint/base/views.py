import django_filters.rest_framework as filters
from rest_framework import permissions, viewsets, response

from TTapp.flop_constraint import FlopConstraint, all_subclasses
import base.models as bm

from . import serializers


class ConstraintTypesViewSet(viewsets.ViewSet):
    def list(self, request):
        """
        List all the constraint types.

        Return a list of all constraint types and their parameters.
        """
        types = []
        for subclass in all_subclasses(FlopConstraint):
            if subclass._meta.abstract is False:
                types.append(subclass)

        serializer = serializers.ConstraintTypeSerializer(types, many=True)
        return response.Response(serializer.data)


class CourseStartTimeFilter(filters.FilterSet):
    class Meta:
        model = bm.CourseStartTimeConstraint
        fields = ("department_id",)


class CourseStartTimeConstraintsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet to see all the courses start time constraints.

    """

    permission_classes = [permissions.AllowAny]
    queryset = bm.CourseStartTimeConstraint.objects.all()
    serializer_class = serializers.CourseStartTimeConstraintsSerializer
    filterset_class = CourseStartTimeFilter
