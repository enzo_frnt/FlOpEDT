from django.contrib.postgres.fields.array import ArrayField
from django.utils.text import capfirst
from rest_framework import serializers

import base.models as bm
from api.v1.departments.constraints.serializers import ConstraintSerializer


class ConstraintTypeSerializer(serializers.Serializer):  # pylint:disable=abstract-method
    class Meta:
        fields = ("model", "name", "description", "parameters")

    model = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    description = serializers.SerializerMethodField()
    parameters = serializers.SerializerMethodField()

    def get_model(self, obj):
        return obj.__name__

    def get_name(self, obj):
        return obj._meta.verbose_name

    def get_description(self, obj):
        return str(obj().description)

    def get_parameters(self, obj):
        parameters = []
        for field in obj._meta.get_fields():
            if field.name not in ConstraintSerializer.Meta.fields:
                typename, multiple = self._process_field(field)

                parameters.append(
                    {
                        "id": field.name,
                        "name": capfirst(field.verbose_name),
                        "description": field.help_text,
                        "type": typename,
                        "multiple": multiple,
                        "required": not field.blank,
                    }
                )

        return parameters

    def _process_field(self, field):
        multiple = False

        if not (field.many_to_one or field.many_to_many):
            typename = type(field).__name__

            if isinstance(field, ArrayField):
                multiple = True
                typename = type(field.base_field).__name__
        else:
            # <class 'xxx.(...).yyy'>; we want xxx.yyy
            mod = field.related_model
            typename_split = str(mod)[8:-2].split(".")
            typename = typename_split[0] + "." + typename_split[-1]

        return typename, multiple


class CourseStartTimeConstraintsSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.CourseStartTimeConstraint
        fields = (
            "department_id",
            "duration",
            "allowed_start_times",
        )
