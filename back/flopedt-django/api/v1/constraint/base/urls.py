from rest_framework import routers

from . import views

routerConstraintBase = routers.SimpleRouter()

routerConstraintBase.register(
    "types",
    views.ConstraintTypesViewSet,
    basename="types",
)

routerConstraintBase.register(
    "course_start_time",
    views.CourseStartTimeConstraintsViewSet,
    basename="course-start-time",
)
