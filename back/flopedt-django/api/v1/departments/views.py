from rest_framework import viewsets
from drf_spectacular.utils import extend_schema
from api.v1.permissions import DjangoModelPermissionsOrReadOnly

import base.models as bm

from .params import department_id_path
from . import serializers


@extend_schema(parameters=[department_id_path()])
class DepartmentViewSet(viewsets.ModelViewSet):
    """
    Departments of the organisation
    """

    lookup_url_kwarg = 'dept_id'
    queryset = bm.Department.objects.all()
    serializer_class = serializers.DepartmentSerializer
    permission_classes = [DjangoModelPermissionsOrReadOnly]

    def get_object(self):
        # Pull the dept from request instead of fetching it again from DB - saves one duped request
        return self.request.flop_department
