from django.urls import include, path
from rest_framework import routers

from .training_programs.urls import training_programs_router
from .groups.urls import groups_router
from .constraints.urls import constraints_router
from . import views

departments_router = routers.SimpleRouter()
departments_router.register("", views.DepartmentViewSet, basename="departments")

url_departments_patterns = [
    path("", include(departments_router.urls), name="departments"),
    path("<int:dept_id>/constraints/", include(constraints_router.urls), name="constraints"),
    path("<int:dept_id>/groups/", include(groups_router.urls), name="groups"),
    path("<int:dept_id>/training_programs/", include(training_programs_router.urls), name="training_programs"),
]
