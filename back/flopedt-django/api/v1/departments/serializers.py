from rest_framework import serializers
import base.models as bm


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.Department
        fields = "__all__"
