from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter


def department_id_path(**kwargs):
    return OpenApiParameter(
        "dept_id",
        location=OpenApiParameter.PATH,
        description="ID of the department",
        type=OpenApiTypes.INT,
        **kwargs
    )
