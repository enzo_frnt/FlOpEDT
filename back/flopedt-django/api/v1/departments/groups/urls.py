from rest_framework import routers

from . import views

groups_router = routers.SimpleRouter()

groups_router.register(
    r"structural",
    views.StructuralGroupsViewSet,
    basename="structural_groups",
)

groups_router.register(
    r"transversal",
    views.TransversalGroupsViewSet,
    basename="transversal_groups",
)
