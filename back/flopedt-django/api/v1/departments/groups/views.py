# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

from rest_framework import viewsets
from drf_spectacular.utils import extend_schema
from api.v1.permissions import DjangoModelPermissionsOrReadOnly

import base.models as bm

from ..params import department_id_path
from . import serializers


@extend_schema(parameters=[department_id_path()])
class GenericGroupsViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return self.queryset \
            .select_related("train_prog") \
            .filter(train_prog__department__id=self.kwargs["dept_id"])


class StructuralGroupsViewSet(GenericGroupsViewSet):
    """
    Structural groups
    """

    queryset = bm.StructuralGroup.objects.all() \
        .prefetch_related("parent_groups")  # TODO: no longer needed once a single parent is allowed.

    serializer_class = serializers.StructuralGroupSerializer
    permission_classes = [DjangoModelPermissionsOrReadOnly]


class TransversalGroupsViewSet(GenericGroupsViewSet):
    """
    Transversal groups
    """

    queryset = bm.TransversalGroup.objects.all() \
        .prefetch_related("conflicting_groups", "parallel_groups")

    serializer_class = serializers.TransversalGroupSerializer
    permission_classes = [DjangoModelPermissionsOrReadOnly]
