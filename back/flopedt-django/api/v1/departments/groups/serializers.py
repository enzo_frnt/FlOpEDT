# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.
from drf_spectacular.utils import extend_schema_serializer
from rest_framework import serializers

import base.models as bm


class GroupSerializer(serializers.ModelSerializer):
    department = serializers.PrimaryKeyRelatedField(source="train_prog.department", read_only=True)

    class Meta:
        model = bm.GenericGroup
        fields = ("id", "name", "full_name", "size", "type", "train_prog", "department",)


class StructuralGroupSerializer(GroupSerializer):
    parent_group = serializers.IntegerField(source="parent_group_id", read_only=True, allow_null=True)

    class Meta:
        model = bm.StructuralGroup
        fields = GroupSerializer.Meta.fields + ("basic", "parent_group",)


class TransversalGroupSerializer(GroupSerializer):
    class Meta:
        model = bm.TransversalGroup
        fields = GroupSerializer.Meta.fields + ("conflicting_groups", "parallel_groups",)
