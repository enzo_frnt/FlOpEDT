from django.contrib.postgres.fields.array import ArrayField
from rest_framework import serializers

from api.base.courses.serializers import PeriodSerializer


class ConstraintSerializer(serializers.Serializer):  # pylint: disable=abstract-method
    class Meta:
        fields = (
            "id",
            "model",
            "title",
            "comment",
            "periods",
            "weight",
            "is_active",
            "modified_at",
            "parameters",
        )

    id = serializers.IntegerField()
    model = serializers.SerializerMethodField()
    title = serializers.CharField()
    comment = serializers.CharField()
    periods = PeriodSerializer(many=True)
    weight = serializers.IntegerField()
    is_active = serializers.BooleanField()
    modified_at = serializers.DateField()
    parameters = serializers.SerializerMethodField()

    def get_model(self, obj):
        return obj.__class__.__name__

    def get_parameters(self, obj):
        parameters = {}
        for field in obj._meta.get_fields():
            if field.name not in self.Meta.fields:
                values = self._process_field(obj, field)
                parameters[field.name] = values

        return parameters

    def _process_field(self, obj, field):
        # If it's a many-to-many, we expect the query set to have prefetched the IDs to #_ids
        if field.many_to_many:
            val = getattr(obj, field.name + '_ids')
            return list(v.id for v in val)

        # If it's a many-to-one, then Django provides the #_id to get the ID directly
        if field.many_to_one:
            val = getattr(obj, field.name + '_id')
            return [val] if val is not None else []

        # It's just a "normal" field, just pull it normally
        val = getattr(obj, field.name)
        if val is None:
            return []

        # If it's not an array, turn it into one
        if not isinstance(field, ArrayField):
            val = [val]

        if "start_time" in field.name:  # Serialize *start_time* as HH:MM
            return list(st.strftime("%H:%M") for st in val)

        return val
