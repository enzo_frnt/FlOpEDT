from django.db.models import QuerySet, Prefetch


def all_with_related_ids(clazz, excluded) -> QuerySet:
    queryset = clazz.objects.all()
    for field in clazz._meta.get_fields():
        if field.name not in excluded and field.many_to_many:
            queryset = queryset.prefetch_related(
                Prefetch(field.name, queryset=field.related_model.objects.all().only("id"), to_attr=field.name + '_ids')
            )

    return queryset
