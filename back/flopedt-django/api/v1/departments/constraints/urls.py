from rest_framework import routers

from . import views

constraints_router = routers.SimpleRouter()

constraints_router.register(
    r"", views.ConstraintsOfDepartmentViewSet, basename="constraints"
)
