from drf_spectacular.utils import extend_schema
from rest_framework import response, viewsets

from api.permissions import IsAdminOrReadOnly
from api.shared.params import scheduling_period, training_period
from TTapp.flop_constraint import FlopConstraint, all_subclasses

from ..params import department_id_path
from .querysets import all_with_related_ids
from . import serializers


class ConstraintsOfDepartmentViewSet(viewsets.ViewSet):
    """
    ViewSet to see all the constraints applied in the department.

    Constraints can be filtered by scheduling or training period.
    """

    permission_classes = [IsAdminOrReadOnly]

    @extend_schema(parameters=[department_id_path(), scheduling_period(), training_period()])
    def list(self, request, **kwargs):
        dept_id = kwargs["dept_id"]
        scheduling_period_id = request.query_params.get("scheduling_period", None)
        training_period_id = request.query_params.get("training_period", None)

        constraints = []
        constraint_class_list = all_subclasses(FlopConstraint)
        for constraint_class in constraint_class_list:
            if constraint_class._meta.abstract is False:
                queryset = (
                    all_with_related_ids(constraint_class, ['periods'])
                    .prefetch_related("periods")
                    .filter(department__id=dept_id)
                )

                if scheduling_period_id is not None:
                    queryset.filter(periods__id=scheduling_period_id)

                if training_period_id is not None:
                    queryset.filter(periods__training_periods__id=training_period_id)

                for constraint in queryset:
                    constraint_serializer = serializers.ConstraintSerializer(constraint)
                    constraints.append(constraint_serializer.data)

        return response.Response(constraints)
