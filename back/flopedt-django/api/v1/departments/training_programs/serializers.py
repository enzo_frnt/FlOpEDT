from rest_framework import serializers

import base.models as bm


class TrainingProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = bm.TrainingProgramme
        fields = ("id", "name", "abbrev", "department_id")
