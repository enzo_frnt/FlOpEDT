from rest_framework import routers

from . import views

training_programs_router = routers.SimpleRouter()

training_programs_router.register(
    r"", views.TrainingProgramsViewSet, basename="training_programs"
)
