from rest_framework import viewsets
from drf_spectacular.utils import extend_schema
from api.permissions import IsAdminOrReadOnly

import base.models as bm

from ..params import department_id_path
from . import serializers


@extend_schema(parameters=[department_id_path()])
class TrainingProgramsViewSet(viewsets.ModelViewSet):
    """
    ViewSet to see all the training programs

    Can be filtered as wanted with parameter="dept"[required]
    of a TrainingProgramme object, with the function TrainingProgramsFilterSet
    """

    def get_queryset(self):
        return bm.TrainingProgramme.objects.filter(department__id=self.kwargs["dept_id"])

    serializer_class = serializers.TrainingProgramSerializer
    permission_classes = [IsAdminOrReadOnly]
