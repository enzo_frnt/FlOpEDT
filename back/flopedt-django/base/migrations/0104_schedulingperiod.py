# Generated by Django 4.2 on 2024-02-20 10:26

from django.db import migrations, models
import django.db.models.deletion

from populate.period import generate_scheduling_periods

import datetime as dt


class Migration(migrations.Migration):
    dependencies = [
        ("base", "0103_alter_holiday_day_alter_timegeneralsettings_days_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="SchedulingPeriod",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("start_date", models.DateField()),
                ("end_date", models.DateField()),
                (
                    "mode",
                    models.CharField(
                        choices=[
                            ("d", "day"),
                            ("w", "week"),
                            ("m", "month"),
                            ("y", "year"),
                            ("c", "custom"),
                        ],
                        default="w",
                        max_length=1,
                    ),
                ),
                (
                    "department",
                    models.ForeignKey(
                        blank=True,
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="base.department",
                    ),
                ),
            ],
        ),
    ]
