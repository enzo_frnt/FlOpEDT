from .base import * # pylint: disable=wildcard-import, unused-wildcard-import

DEBUG=False

if DEBUG is False:
    mddlwr_security_index = MIDDLEWARE.index(
        "django.middleware.security.SecurityMiddleware"
    )
    MIDDLEWARE.insert(
        mddlwr_security_index + 1, "whitenoise.middleware.WhiteNoiseMiddleware"
    )

STORAGES = {
    "staticfiles": {
        "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
    },
}