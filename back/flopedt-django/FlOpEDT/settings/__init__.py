import os
from importlib import import_module

env = os.getenv("ENV", "dev").lower()

try:
    # Importation dynamique du module en fonction de la valeur de 'env'
    settings_module = import_module(f".{env}", package=__package__)
    # Injection des variables du module importé dans l'espace de noms global
    globals().update(vars(settings_module))
except ImportError:
    print(f"Le module de configuration pour '{env}' n'existe pas.")
    print(f"Vérifier que le fichier {env}.py existe dans le répertoire settings.")
    exit(1)