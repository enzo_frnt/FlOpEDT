��    <      �  S   �      (     )     /  	   C     M     [     s     {     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �          	               ,     9  	   L     V     m     y     �  	   �     �     �     �     �     �     �     �     �                    -     2     7     <     Q     n     �     �     �     �     �  	   �     �  
   �     �  B  �     	     	  
   6	     A	     Q	     k	     r	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     
     )
     :
     O
     b
     h
     �
  ,   �
     �
     �
     �
     �
     �
     �
     �
     �
                5     E     T     i     n     s     x  '   �  #   �     �     �     �     �     �     �                                           %                      8   (       !       /      0       .   
       4       1   9      3            '       	          *   &          2   :   )              #                            7   $         +         6       -   5         <          ;              ,         "       Admin Author description  Category  Create a link Create or modify a link Created Date  Default week English First name  Forgot password? French Fri. Generate Header  Help Import Last name  Maximum Messages Modified Modules Mon. Move/Cancel My favorite rooms My ideal day My videoconf links Nickname  No such user as {user} Not allowed Not authorized. New link then. Preferences Preferred Quote  Rooms Sat. Schedule_banner Schedule_title Select my favorite rooms Send Set default week Sign in Sign out Signed in as  Sun. Thu. Tue. Tutor does not exist Unknown link. New link then. Update my default week Wed. Weeks button_apply button_change button_save from week iCal until week year Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Admin Descripción del autor Categoría Crear un enlace Creer o cambiar un enlace Creado Fecha Semanas predeterminada Inglés Nombre ¿Has olvidado tu contraseña? Francés Vie. Generar Título Ayuda Importar Apelido Maximum Mensajes Cambiado Módulos Lun. Mover/Cancelar Mi aula favorita Mis días preferidos Mi enlace de clase Apodo El usuario {user} no existe No permitido No tienes autorización. Nuevo enlace creado Preferencias  Favorito Cita Aulas Sab. Horario Horario Elegir mi aula favorita Mandar Configurar semana predeterminada Iniciar sesión Cerrar sesión Iniciar sesión como Dom. Jue. Mar. Tutor no existe Enlace desconocido. Nuevo enlace creado Actualizar mi semana predeterminada Mie. Semanas Applicar Cambiar Guardar Desde semana iCal hasta la semana años 