# Generated by Django 4.2 on 2024-03-13 15:42

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0120_remove_dependency_nd'),
        ('TTapp', '0093_globalmoduledependency'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsiderModuleTutorRepartitions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weight', models.PositiveSmallIntegerField(blank=True, default=None, null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(8)])),
                ('title', models.CharField(blank=True, default=None, max_length=30, null=True)),
                ('comment', models.CharField(blank=True, default=None, max_length=100, null=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Is active?')),
                ('modified_at', models.DateField(auto_now=True)),
                ('course_types', models.ManyToManyField(blank=True, to='base.coursetype')),
                ('department', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='base.department')),
                ('modules', models.ManyToManyField(blank=True, to='base.module')),
                ('periods', models.ManyToManyField(blank=True, to='base.schedulingperiod')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
