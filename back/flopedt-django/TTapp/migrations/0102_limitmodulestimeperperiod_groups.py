# Generated by Django 4.2 on 2024-04-02 07:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("base", "0131_rename_days_timegeneralsettings_weekdays"),
        ("TTapp", "0101_remove_lowerboundbusydays_lower_bound_hours_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="limitmodulestimeperperiod",
            name="groups",
            field=models.ManyToManyField(blank=True, to="base.structuralgroup"),
        ),
    ]
