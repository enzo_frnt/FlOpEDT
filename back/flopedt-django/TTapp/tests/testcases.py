from abc import abstractmethod

from django.test import TestCase

from base.models import Department, SchedulingPeriod, TimetableVersion
from TTapp.flop_constraint import FlopConstraint
from TTapp.timetable_model import TimetableModel


class ConstraintTestCase(TestCase):
    @classmethod
    def department(cls):
        return Department.objects.first()

    @classmethod
    def period(cls):
        return SchedulingPeriod.objects.get(name="W2024-1")

    @classmethod
    def version(cls, major=0):
        timetable_version, _ = TimetableVersion.objects.get_or_create(
            department=cls.department(), period=cls.period(), major=major
        )
        return timetable_version

    @classmethod
    @abstractmethod
    def relevant_constraint(cls) -> FlopConstraint: ...

    @classmethod
    @abstractmethod
    def make_constraints(cls): ...

    @classmethod
    def setUpTestData(cls) -> None:
        cls.make_constraints()
        # cls.solve_with_ttmodel()

    @classmethod
    def validate(cls, version_major=0):
        return cls.relevant_constraint().is_satisfied_for(cls.period(), cls.version(version_major))

    @classmethod
    def solve_with_ttmodel(cls, target_major):
        ttmodel = TimetableModel(
            department_abbrev=cls.department().abbrev,
            periods=[cls.period()],
            pre_assign_rooms=True,
            post_assign_rooms=False,
        )
        ttmodel.solve(target_major=target_major, solver="PULP_CBC_CMD", time_limit=60)

    @classmethod
    def validate_in_ttmodel_result(cls, target_major=1):
        cls.solve_with_ttmodel(target_major)
        return cls.validate(target_major)

    def ok(self):
        self.assertFalse(self.validate())

    def timetable_model_ok(self):
        self.assertFalse(self.validate_in_ttmodel_result())
