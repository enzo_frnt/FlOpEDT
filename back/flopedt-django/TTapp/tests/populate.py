import datetime as dt
import random

from base.models import (
    Course,
    CourseStartTimeConstraint,
    CourseType,
    Department,
    Dependency,
    GroupType,
    Mode,
    Module,
    PeriodEnum,
    Room,
    RoomType,
    ScheduledCourse,
    SchedulingPeriod,
    StructuralGroup,
    TimeGeneralSettings,
    TimetableVersion,
    TrainingPeriod,
    TrainingProgramme,
    TransversalGroup,
    UserAvailability,
    ModulePossibleTutors,
)
from people.models import FullStaff, SupplyStaff, Tutor, TutorPreference
from TTapp.slots import TimetableSlot

start_time_constraints = {
    dt.timedelta(hours=1): [dt.time(h, 0) for h in range(8, 19)],
    dt.timedelta(hours=2): [dt.time(h, 0) for h in range(8, 18, 1)],
    dt.timedelta(minutes=90): [
        (dt.datetime(1, 1, 1, hour=8) + dt.timedelta(minutes=90 * i)).time() for i in range(7)
    ],
}

allowed_weekdays = ["m", "tu", "w", "th", "f"]

course_type_names = ["CM", "TD", "TP"]

courses_dic = {
    "TP1": {
        "mod1": {"CM": (60, 1), "TD": (120, 2, "ACM"), "TP": (120, 1), "tutors": [1, 2, 3, 4, 5]},
        "mod2": {"CM": (60, 1), "TD": (90, 1), "TP": (90, 2, "ATD", "D"), "tutors": [4, 5, 6]},
        "mod3": {"CM": (60, 1), "TD": (90, 1), "TP": (90, 2, "ATD"), "tutors": [7, 8, 9]},
    },
    "TP2": {
        "mod1": {"CM": (60, 0), "TD": (60, 5), "TP": (60, 1), "tutors": [1, 3, 4, 7]},
        "mod2": {"CM": (60, 2), "TD": (90, 2), "TP": (90, 1), "tutors": [2, 4, 8]},
        "mod3": {"CM": (90, 1), "TD": (120, 2), "TP": (120, 1), "tutors": [3, 5, 9]},
    },
}


def make_dept_and_needs():
    d = Department.objects.create(name="dept_name", abbrev="abbrev")
    t, _ = TimeGeneralSettings.objects.get_or_create(department=d)
    t.weekdays = allowed_weekdays
    t.morning_end_time = dt.time(13, 0, 0)
    t.afternoon_start_time = dt.time(13, 0, 0)
    t.save()
    Mode.objects.get_or_create(department=d)
    training_period, _ = TrainingPeriod.objects.get_or_create(name="Period test")
    for p in range(1, 3):
        sched_period, _ = SchedulingPeriod.objects.get_or_create(
            name=f"W2024-{p}",
            start_date=dt.date(2024, 1, 1) + dt.timedelta(weeks=p - 1),
            end_date=dt.date(2024, 1, 7) + dt.timedelta(weeks=p - 1),
            mode=PeriodEnum.WEEK,
        )
        training_period.periods.add(sched_period)
    for t in course_type_names:
        CourseType.objects.get_or_create(department=d, name=t)
    for duration, allowed_start_times in start_time_constraints.items():
        CourseStartTimeConstraint.objects.get_or_create(
            department=d, duration=duration, allowed_start_times=allowed_start_times
        )


def make_modules():
    train_period = TrainingPeriod.objects.first()
    for tp in TrainingProgramme.objects.all():
        for i in range(1, 4):
            Module.objects.create(
                name=f"module_{i}",
                abbrev=f"mod{i}",
                train_prog=tp,
                training_period=train_period,
            )


def make_classical_groups_structure():
    department = Department.objects.first()
    cm_type = GroupType.objects.create(name="CM", department=department)
    td_type = GroupType.objects.create(name="TD", department=department)
    tp_type = GroupType.objects.create(name="TP", department=department)
    for i in range(1, 3):
        tp = TrainingProgramme.objects.create(
            name=f"TrainProg{i}", abbrev=f"TP{i}", department=Department.objects.first()
        )
        groups = []
        groups.append(
            StructuralGroup.objects.create(train_prog=tp, name="CM", type=cm_type, size=0)
        )
        for i in range(1, 3):
            group = StructuralGroup.objects.create(
                train_prog=tp, name=f"TD{i}", type=td_type, size=0
            )
            group.parent_groups.add(groups[0])
            for letter in "AB":
                subgroup = StructuralGroup.objects.create(
                    train_prog=tp, name=f"TP{i}{letter}", basic=True, type=tp_type, size=0
                )
                subgroup.parent_groups.add(group)
                groups.append(subgroup)
            groups.append(group)
        tr_12a = TransversalGroup.objects.create(train_prog=tp, name="Transversal 1&2A", size=0)
        tr_12a.conflicting_groups.add(StructuralGroup.objects.get(name="TD1", train_prog=tp))
        tr_12a.conflicting_groups.add(StructuralGroup.objects.get(name="TP2A", train_prog=tp))
        tr_12a.save()
        tr_1b2 = TransversalGroup.objects.create(train_prog=tp, name="Transversal 1B&2", size=0)
        tr_1b2.conflicting_groups.add(StructuralGroup.objects.get(name="TP1B", train_prog=tp))
        tr_1b2.conflicting_groups.add(StructuralGroup.objects.get(name="TD2", train_prog=tp))
        tr_1b2.parallel_groups.add(tr_12a)
        tr_1b2.save()


def make_rooms():

    d = Department.objects.get(abbrev="abbrev")
    n = 15
    n = max(4, n)
    q = n // 4
    a = RoomType.objects.create(name="CM", department=d)
    td = RoomType.objects.create(name="TD", department=d)
    tp = RoomType.objects.create(name="TP", department=d)
    for i in range(q):
        r = Room.objects.create(name=f"Amphi_{i}")
        r.departments.add(d)
        a.members.add(r)
    for i in range(q):
        r = Room.objects.create(name=f"TD_{i}")
        r.departments.add(d)
        td.members.add(r)
    for i in range(q):
        r = Room.objects.create(name=f"TDP_{i}")
        r.departments.add(d)
        td.members.add(r)
        tp.members.add(r)
    for i in range(n - 3 * q):
        r = Room.objects.create(name=f"TP_{i}")
        r.departments.add(d)
        tp.members.add(r)


def make_tutors_pref_and_av():
    full_staffs = []
    d = Department.objects.get(abbrev="abbrev")
    for i in range(5):
        fs = FullStaff.objects.create(username=f"full_staff_{i}")
        fs.departments.add(d)
        full_staffs.append(fs)
    supply_staffs = []
    for i in range(5):
        ss = SupplyStaff.objects.create(username=f"supply_staff_{i}")
        ss.departments.add(d)
        supply_staffs.append(ss)
    for tutor in full_staffs + supply_staffs:
        TutorPreference.objects.get_or_create(tutor=tutor)
        for day in range(1, 8):
            for hour in range(6, 23):
                UserAvailability.objects.create(
                    user=tutor,
                    start_time=dt.datetime(2024, 1, day, hour),
                    duration=dt.timedelta(hours=1),
                    value=8,
                )


def make_courses():
    print("MAKE COURSES")
    make_dept_and_needs()
    make_classical_groups_structure()
    make_rooms()
    make_tutors_pref_and_av()
    make_modules()
    period = SchedulingPeriod.objects.get(name="W2024-1")
    tutors = list(Tutor.objects.all())
    for tp_abbrev, modules_dic in courses_dic.items():
        tp = TrainingProgramme.objects.get(abbrev=tp_abbrev)
        for mod_abbrev, course_type_dic in modules_dic.items():
            m = Module.objects.get(abbrev=mod_abbrev, train_prog=tp)
            module_possible_tutors = ModulePossibleTutors.objects.create(module=m)
            considered_tutors = [tutors[i] for i in course_type_dic["tutors"]]
            module_possible_tutors.possible_tutors.set(considered_tutors)
            considered_tutors = considered_tutors + [None]
            i = 0
            for rk, course_type_name in enumerate(course_type_names):
                course_type = CourseType.objects.get(name=course_type_name)
                room_type = RoomType.objects.get(name=course_type_name)
                duration, nb, comments = (
                    random.choice([60, 90, 120]),
                    random.randint(0, 3),
                    tuple(),
                )
                if nb == 2:
                    comments = (random.choice(["D", "ND", "A" + course_type_names[rk - 1]]),)
                elif course_type_names != "CM":
                    if random.choice([True, False]):
                        comments = ("A" + course_type_names[rk - 1],)
                # duration, nb, *comments = course_type_dic[course_type_name]

                # add a course for some transversal group
                if nb == 3:
                    nb -= 1
                    trans_nb = random.randint(0, 2)
                    for tg in TransversalGroup.objects.filter(train_prog=tp)[:trans_nb]:
                        course = Course.objects.create(
                            module=m,
                            duration=dt.timedelta(minutes=duration),
                            tutor=considered_tutors[i % len(considered_tutors)],
                            type=course_type,
                            room_type=room_type,
                            period=period,
                        )
                        course.groups.add(tg)
                        i += 1 % len(considered_tutors)

                for g in StructuralGroup.objects.filter(
                    train_prog=tp, type__name=course_type_name
                ):
                    for rank in range(nb):
                        course = Course.objects.create(
                            module=m,
                            duration=dt.timedelta(minutes=duration),
                            tutor=considered_tutors[i % len(considered_tutors)],
                            type=course_type,
                            room_type=room_type,
                            period=period,
                        )
                        course.groups.add(g)
                        for comment in comments:
                            if comment.startswith("A"):
                                a_type = comment[1:]
                                firsts = Course.objects.filter(
                                    groups__in=g.ancestor_groups(), type__name=a_type
                                )
                                if firsts.exists():
                                    Dependency.objects.create(
                                        course1=firsts.first(), course2=course
                                    )
                            elif comment == "D":
                                if rank % 2 == 1:
                                    Dependency.objects.create(
                                        course1=Course.objects.get(id=course.id - 1),
                                        course2=course,
                                        successive=True,
                                    )
                            elif comment == "ND":
                                if rank % 2 == 1:
                                    Dependency.objects.create(
                                        course1=Course.objects.get(id=course.id - 1),
                                        course2=course,
                                        day_gap=1,
                                    )

                        i += 1 % len(considered_tutors)


def make_random_schedule():
    print("MAKE RANDOM SCHEDULE")
    dept = Department.objects.get(abbrev="abbrev")
    period = SchedulingPeriod.objects.get(name="W2024-1")
    TimetableVersion.objects.create(department=dept, period=period)
    version, _ = TimetableVersion.objects.get_or_create(department=dept, period=period, major=0)
    random.seed(10)
    possible_slots = {
        duration: [
            TimetableSlot.objects.create(
                start_time=dt.datetime.combine(dt.date(2024, 1, d), h),
                duration=duration,
                department=dept,
            )
            for d in range(1, len(allowed_weekdays) + 1)
            for h in possible_start_times
        ]
        for duration, possible_start_times in start_time_constraints.items()
    }
    used_slots = {}
    for bg in StructuralGroup.objects.filter(basic=True):
        used_slots[bg] = set()
    for tg in TransversalGroup.objects.all():
        used_slots[tg] = set()
    for t in Tutor.objects.all():
        used_slots[t] = set()
    for r in Room.objects.all():
        used_slots[r] = set()
    for course in Course.objects.all():
        ok = False
        while not ok:
            group = course.groups.first()
            slot = random.choice(list(possible_slots[course.duration]))
            if group.is_transversal:
                basic_groups = group.transversalgroup.conflicting_basic_groups()
                if (
                    any(sl.is_simultaneous_to(slot) for sl in used_slots[group.transversalgroup])
                    or any(
                        sl.is_simultaneous_to(slot) for bg in basic_groups for sl in used_slots[bg]
                    )
                    or (
                        course.tutor is not None
                        and any(sl.is_simultaneous_to(slot) for sl in used_slots[course.tutor])
                    )
                ):
                    continue
            else:
                basic_groups = group.structuralgroup.basic_groups()
                if any(
                    sl.is_simultaneous_to(slot) for bg in basic_groups for sl in used_slots[bg]
                ) or (
                    course.tutor is not None
                    and any(sl.is_simultaneous_to(slot) for sl in used_slots[course.tutor])
                ):
                    continue
            tutor = course.tutor
            if tutor is None:
                tutor = random.choice(
                    list(course.module.modulepossibletutors.possible_tutors.all())
                )
            if not random.randint(0, 10):
                supp_tutor = random.choice(
                    list(course.module.modulepossibletutors.possible_tutors.exclude(id=tutor.id))
                )
                course.supp_tutors.add(supp_tutor)
            sc = ScheduledCourse.objects.create(
                course=course,
                start_time=slot.start_time,
                tutor=tutor,
                version=version,
            )
            while not ok:
                room = random.choice(list(course.room_type.members.all()))
                if any(sl.is_simultaneous_to(slot) for sl in used_slots[room]):
                    continue
                ok = True
                if group.is_transversal:
                    used_slots[group.transversalgroup].add(slot)
                for bg in basic_groups:
                    used_slots[bg].add(slot)
                if tutor is not None:
                    used_slots[tutor].add(slot)
                used_slots[room].add(slot)
                sc.room = room
                sc.save()
