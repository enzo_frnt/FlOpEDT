import random
import pytest

from base.models import ScheduledCourse, TransversalGroup, StructuralGroup
from TTapp.models import ScheduleAllCourses, NoSimultaneousGroupCourses
from TTapp.tests.testcases import ConstraintTestCase


class TestNoSimultaneousGroupCourses(ConstraintTestCase):
    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        NoSimultaneousGroupCourses.objects.create(department=cls.department())

    @classmethod
    def relevant_constraint(cls):
        return NoSimultaneousGroupCourses.objects.get()

    def chose_random_structural_group_and_scheduled_courses(self):
        """
        choose randomly one structural group that appears in a scheduled course
        """
        sg = random.choice(list(StructuralGroup.objects.filter(courses__isnull=False)))
        return sg, ScheduledCourse.objects.filter(course__groups=sg)

    def test_no_group_courses_are_simultaneous(self):
        self.ok()

    def test_one_group_has_simultaneous_courses(self):
        _, considered_scheduled_courses = (
            self.chose_random_structural_group_and_scheduled_courses()
        )
        sc = random.choice(list(considered_scheduled_courses))
        same_group_sc = considered_scheduled_courses.exclude(id=sc.id)
        other_sc = random.choice(list(same_group_sc))
        other_sc.start_time = sc.start_time + (sc.end_time - sc.start_time) / 2
        other_sc.save()
        self.assertIn(
            NoSimultaneousGroupCourses.VIOLATIONS.GROUP_WITH_SIMULTANEOUS_COURSES, self.validate()
        )

    def test_one_group_has_simultaneous_courses_with_connected_group(self):
        considered_group, considered_scheduled_courses = (
            self.chose_random_structural_group_and_scheduled_courses()
        )
        sc = random.choice(list(considered_scheduled_courses))
        connected_groups_sc = ScheduledCourse.objects.filter(
            course__groups__in=considered_group.connected_groups() - {considered_group}
        ).exclude(id=sc.id)
        other_sc = random.choice(list(connected_groups_sc))
        other_sc.start_time = sc.start_time + (sc.end_time - sc.start_time) / 2
        other_sc.save()
        self.assertIn(
            NoSimultaneousGroupCourses.VIOLATIONS.GROUP_WITH_SIMULTANEOUS_COURSES, self.validate()
        )

    def test_one_group_has_simultaneous_courses_with_transversal_conflicting_group(self):
        transversal_groups = TransversalGroup.objects.filter(courses__isnull=False)
        t_g_nb = transversal_groups.count()
        if t_g_nb == 0:
            pytest.skip("No transversal groups")
        elif transversal_groups.count() == 1:
            tg = transversal_groups.get()
        else:
            tg = random.choice(list(transversal_groups))
        considered_scheduled_courses = ScheduledCourse.objects.filter(course__groups=tg)
        sc = random.choice(list(considered_scheduled_courses))
        conflicting_groups_sc = ScheduledCourse.objects.filter(
            course__groups__in=tg.conflicting_groups.all()
        )
        other_sc = random.choice(list(conflicting_groups_sc))
        other_sc.start_time = sc.start_time + (sc.end_time - sc.start_time) / 2
        other_sc.save()
        self.assertIn(
            NoSimultaneousGroupCourses.VIOLATIONS.GROUP_WITH_SIMULTANEOUS_COURSES_WITH_TRANSVERSAL_GROUP,
            self.validate(),
        )

    def test_no_simultaneous_in_ttmodel(self):
        self.timetable_model_ok()
