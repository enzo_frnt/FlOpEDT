import random
import datetime as dt

from base.models import ScheduledCourse, UserAvailability
from people.models import Tutor
from TTapp.models import ScheduleAllCourses, AssignAllCourses, ConsiderTutorsUnavailability
from TTapp.tests.testcases import ConstraintTestCase


class TestConsiderTutorsUnavailability(ConstraintTestCase):
    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        AssignAllCourses.objects.create(department=cls.department(), pre_assigned_only=False)
        ConsiderTutorsUnavailability.objects.create(department=cls.department())

    @classmethod
    def relevant_constraint(cls):
        return ConsiderTutorsUnavailability.objects.get()

    def test_ok_if_all_availability_declared(self):
        self.ok()

    def test_ok_if_no_availability_declared(self):
        UserAvailability.objects.all().delete()
        self.ok()

    def test_one_tutor_is_unavailable_for_one_course(self):
        sc = random.choice(list(ScheduledCourse.objects.all()))
        t = sc.tutor
        u = UserAvailability.objects.get(
            user=t,
            start_time=dt.datetime(
                sc.start_time.year, sc.start_time.month, sc.start_time.day, sc.start_time.hour
            ),
            duration=dt.timedelta(hours=1),
        )
        u.value = 0
        u.save()
        self.assertIn(ConsiderTutorsUnavailability.VIOLATIONS.TUTOR_UNAVAILABLE, self.validate())

    def test_one_supp_tutor_is_unavailable_for_one_course(self):
        sc = random.choice(list(ScheduledCourse.objects.all()))
        t = random.choice(list(Tutor.objects.exclude(id=sc.tutor.id)))
        sc.course.supp_tutors.add(t)
        u = UserAvailability.objects.get(
            user=t,
            start_time=dt.datetime(
                sc.start_time.year, sc.start_time.month, sc.start_time.day, sc.start_time.hour
            ),
            duration=dt.timedelta(hours=1),
        )
        u.value = 0
        u.save()
        self.assertIn(
            ConsiderTutorsUnavailability.VIOLATIONS.SUPP_TUTOR_UNAVAILABLE, self.validate()
        )

    def test_one_tutor_is_unavailable_for_one_minute_inside_one_course(self):
        sc = random.choice(list(ScheduledCourse.objects.all()))
        t = sc.tutor
        UserAvailability.objects.create(
            user=t,
            start_time=sc.start_time + (sc.end_time - sc.start_time) / 2,
            duration=dt.timedelta(minutes=1),
            value=0,
        )
        self.assertIn(ConsiderTutorsUnavailability.VIOLATIONS.TUTOR_UNAVAILABLE, self.validate())

    def test_one_supp_tutor_is_unavailable_for_one_minute_inside_one_course(self):
        sc = random.choice(list(ScheduledCourse.objects.all()))
        t = random.choice(list(Tutor.objects.exclude(id=sc.tutor.id)))
        sc.course.supp_tutors.add(t)
        UserAvailability.objects.create(
            user=t,
            start_time=sc.start_time + (sc.end_time - sc.start_time) / 2,
            duration=dt.timedelta(minutes=1),
            value=0,
        )
        self.assertIn(
            ConsiderTutorsUnavailability.VIOLATIONS.SUPP_TUTOR_UNAVAILABLE, self.validate()
        )

    def test_consider_tutors_unavailability_in_ttmodel(self):
        for t in Tutor.objects.all():
            for day_rank in range(1, 8):
                for hour in range(6, 23):
                    u = UserAvailability.objects.get(
                        user=t,
                        start_time=dt.datetime(2024, 1, day_rank, hour=hour),
                        duration=dt.timedelta(hours=1),
                    )
                    u.value = random.randint(0, 8)
                    u.save()

        self.timetable_model_ok()
