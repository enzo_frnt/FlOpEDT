import datetime as dt
import random

from base.models import ScheduledCourse
from TTapp.models import ScheduleAllCourses
from TTapp.tests.testcases import ConstraintTestCase


class TestScheduleAllCourses(ConstraintTestCase):

    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())

    @classmethod
    def relevant_constraint(cls):
        return ScheduleAllCourses.objects.get(department=cls.department())

    def test_all_courses_are_scheduled(self):
        self.ok()

    def test_one_course_is_not_scheduled(self):
        random.choice(list(ScheduledCourse.objects.filter())).delete()
        self.assertIn(ScheduleAllCourses.VIOLATIONS.UNSCHEDULED, self.validate())

    def test_one_course_is_scheduled_twice(self):
        sc = random.choice(list(ScheduledCourse.objects.filter()))
        ScheduledCourse.objects.create(
            course=sc.course,
            start_time=sc.start_time,
            version=self.version(0),
        )
        self.assertIn(ScheduleAllCourses.VIOLATIONS.SCHEDULED_MORE_THAN_ONCE, self.validate())

    def test_one_course_out_of_period(self):
        sc = random.choice(list(ScheduledCourse.objects.filter()))
        sc.start_time += dt.timedelta(days=7)
        sc.save()
        self.assertIn(ScheduleAllCourses.VIOLATIONS.OUT_OF_PERIOD, self.validate())

    def test_one_course_on_not_allowed_start_time(self):
        sc = random.choice(list(ScheduledCourse.objects.filter()))
        sc.start_time += dt.timedelta(minutes=1)
        sc.save()
        self.assertIn(ScheduleAllCourses.VIOLATIONS.NOT_ALLOWED_START_TIMES, self.validate())

    def test_all_courses_are_scheduled_in_ttmodel(self):
        self.timetable_model_ok()
