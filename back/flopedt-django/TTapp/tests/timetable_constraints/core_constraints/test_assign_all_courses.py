import random
import pytest

from base.models import ScheduledCourse, CoursePossibleTutors, ModulePossibleTutors, Department
from people.models import Tutor
from TTapp.models import ScheduleAllCourses, AssignAllCourses
from TTapp.tests.testcases import ConstraintTestCase


class TestAssignAllCoursesNoPreassignOnly(ConstraintTestCase):
    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        AssignAllCourses.objects.create(department=cls.department(), pre_assigned_only=False)

    @classmethod
    def relevant_constraint(cls):
        return AssignAllCourses.objects.get()

    def test_all_courses_are_assigned(self):
        self.ok()

    def test_one_course_is_not_assigned(self):
        no_tutor_courses_sc = ScheduledCourse.objects.filter(course__tutor__isnull=True)
        if no_tutor_courses_sc.exists():
            if no_tutor_courses_sc.count() == 1:
                sc = no_tutor_courses_sc.first()
            else:
                sc = random.choice(
                    list(ScheduledCourse.objects.filter(course__tutor__isnull=False))
                )
            sc.tutor = None
            sc.save()
            self.assertIn(AssignAllCourses.VIOLATIONS.UNASSIGNED, self.validate())
        else:
            pytest.skip("No unassigned courses")

    def test_one_preassigned_course_is_assigned_to_a_wrong_tutor(self):
        sc = random.choice(list(ScheduledCourse.objects.filter(course__tutor__isnull=False)))
        new_tutor = random.choice(list(Tutor.objects.exclude(id=sc.course.tutor.id)))
        sc.tutor = new_tutor
        sc.save()
        self.assertIn(AssignAllCourses.VIOLATIONS.ASSIGNED_TO_WRONG_TUTOR, self.validate())

    def test_one_unpreassigned_course_is_assigned_to_a_wrong_tutor(self):
        considered_sched_courses = ScheduledCourse.objects.filter(course__tutor__isnull=True)
        if considered_sched_courses.exists():
            sc = random.choice(list(considered_sched_courses))
            if CoursePossibleTutors.objects.filter(course=sc.course).exists():
                possible_tutors = CoursePossibleTutors.objects.get(
                    course=sc.course
                ).possible_tutors.all()
                new_tutor = random.choice(
                    list(Tutor.objects.exclude(id__in=[tut.id for tut in possible_tutors]))
                )
                sc.tutor = new_tutor
                sc.save()
            elif ModulePossibleTutors.objects.filter(module=sc.course.module).exists():
                possible_tutors = ModulePossibleTutors.objects.get(
                    module=sc.course.module
                ).possible_tutors.all()
                new_tutor = random.choice(
                    list(Tutor.objects.exclude(id__in=[tut.id for tut in possible_tutors]))
                )
                sc.tutor = new_tutor
                sc.save()
            else:
                department = Department.objects.first()
                unwanted_tutors = Tutor.objects.exclude(departments=department)
                if unwanted_tutors.count() == 0:
                    pytest.skip("No tutors to exclude")
                new_tutor = random.choice(list(unwanted_tutors))
                sc.tutor = new_tutor
            self.assertIn(AssignAllCourses.VIOLATIONS.ASSIGNED_TO_WRONG_TUTOR, self.validate())
        else:
            pytest.skip("No unpreassigned courses")

    def test_assign_all_courses_in_ttmodel(self):
        self.timetable_model_ok()


class TestAssignAllCoursesPreassignOnly(ConstraintTestCase):

    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        AssignAllCourses.objects.create(department=cls.department(), pre_assigned_only=True)

    @classmethod
    def relevant_constraint(cls):
        return AssignAllCourses.objects.get()

    def test_all_preassigned_courses_are_assigned(self):
        ScheduledCourse.objects.filter(course__tutor__isnull=True).update(tutor=None)
        self.ok()

    def test_one_course_is_not_assigned(self):
        sc = random.choice(list(ScheduledCourse.objects.filter(course__tutor__isnull=False)))
        sc.tutor = None
        sc.save()
        self.assertIn(AssignAllCourses.VIOLATIONS.UNASSIGNED, self.validate())

    def test_one_preassigned_course_is_assigned_to_a_wrong_tutor(self):
        sc = random.choice(list(ScheduledCourse.objects.filter(course__tutor__isnull=False)))
        new_tutor = random.choice(list(Tutor.objects.exclude(id=sc.course.tutor.id)))
        sc.tutor = new_tutor
        sc.save()
        self.assertIn(AssignAllCourses.VIOLATIONS.ASSIGNED_TO_WRONG_TUTOR, self.validate())

    def test_one_unpreassigned_course_is_assigned(self):
        considered_sched_courses = ScheduledCourse.objects.filter(course__tutor__isnull=True)
        if considered_sched_courses.exists():
            sc = random.choice(list(considered_sched_courses))
            sc.tutor = random.choice(list(Tutor.objects.all()))
            sc.save()
            self.assertIn(
                AssignAllCourses.VIOLATIONS.ASSIGNED_IF_PRE_ASSIGNED_ONLY, self.validate()
            )
        else:
            pytest.skip("No unpreassigned courses")

    def test_assign_all_courses_in_ttmodel(self):
        self.timetable_model_ok()
