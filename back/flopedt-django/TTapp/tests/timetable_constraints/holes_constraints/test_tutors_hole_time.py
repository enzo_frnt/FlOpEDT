import datetime as dt

from base.models import Course, ScheduledCourse
from people.models import Tutor
from TTapp.models import AssignAllCourses, LimitTutorsHoleTime, ScheduleAllCourses
from TTapp.tests.testcases import ConstraintTestCase


class TestLimitTutorsHoleTime(ConstraintTestCase):

    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        AssignAllCourses.objects.create(department=cls.department())
        LimitTutorsHoleTime.objects.create(department=cls.department())

    @classmethod
    def relevant_constraint(cls):
        return LimitTutorsHoleTime.objects.get(department=cls.department())

    def test_no_holes_declared(self):
        self.ok()

    def test_half_day_hole_time_exceeded_monday_morning(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_half_day = dt.timedelta(minutes=30)
        constraint.save()
        courses = Course.objects.filter(period=self.period())
        tutor = Tutor.objects.first()
        monday_9h = dt.datetime(2024, 1, 1, 9)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            tutor=tutor,
            start_time=monday_9h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            tutor=tutor,
            start_time=monday_9h + dt.timedelta(hours=1),
            version=version,
        )
        self.assertIn(LimitTutorsHoleTime.VIOLATIONS.AM, self.validate(10))

    def test_half_day_hole_time_exceeded_monday_afternoon(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_half_day = dt.timedelta(hours=1)
        constraint.save()
        courses = Course.objects.filter(period=self.period())
        tutor = Tutor.objects.first()
        monday_15h = dt.datetime(2024, 1, 1, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            tutor=tutor,
            start_time=monday_15h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            tutor=tutor,
            start_time=monday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitTutorsHoleTime.VIOLATIONS.PM, self.validate(10))

    def test_day_hole_time_exceeded_tuesday(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_day = dt.timedelta(hours=3)
        constraint.save()
        courses = Course.objects.filter(period=self.period())
        tutor = Tutor.objects.first()
        tuesday_10h = dt.datetime(2024, 1, 2, 8)
        tuesday_15h = dt.datetime(2024, 1, 2, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            tutor=tutor,
            start_time=tuesday_10h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            tutor=tutor,
            start_time=tuesday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[2],
            tutor=tutor,
            start_time=tuesday_15h - courses[2].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[3],
            tutor=tutor,
            start_time=tuesday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitTutorsHoleTime.VIOLATIONS.DAY, self.validate(10))

    def test_period_hole_time_exceeded(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_period = dt.timedelta(hours=5)
        constraint.save()
        courses = Course.objects.filter(period=self.period())
        tutor = Tutor.objects.first()
        monday_10h = dt.datetime(2024, 1, 1, 10)
        tuesday_10h = dt.datetime(2024, 1, 2, 10)
        wednesday_15h = dt.datetime(2024, 1, 3, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            tutor=tutor,
            start_time=monday_10h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            tutor=tutor,
            start_time=monday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[2],
            tutor=tutor,
            start_time=tuesday_10h - courses[2].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[3],
            tutor=tutor,
            start_time=tuesday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[4],
            tutor=tutor,
            start_time=wednesday_15h - courses[4].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[5],
            tutor=tutor,
            start_time=wednesday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitTutorsHoleTime.VIOLATIONS.PERIOD, self.validate(10))

    # def test_hole_times_in_ttmodel(self):
    #     constraint = self.relevant_constraint()
    #     constraint.max_hole_time_per_half_day = dt.timedelta(hours=2)
    #     constraint.max_hole_time_per_day = dt.timedelta(hours=10)
    #     constraint.max_hole_time_per_period = dt.timedelta(hours=50)
    #     constraint.save()
    #     self.timetable_model_ok()
