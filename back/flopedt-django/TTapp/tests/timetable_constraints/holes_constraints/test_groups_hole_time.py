import datetime as dt
import random
from base.models import Course, ScheduledCourse, StructuralGroup
from people.models import Tutor
from TTapp.models import AssignAllCourses, LimitGroupsHoleTime, ScheduleAllCourses
from TTapp.tests.testcases import ConstraintTestCase


class TestLimitGroupsHoleTime(ConstraintTestCase):

    @classmethod
    def make_constraints(cls):
        ScheduleAllCourses.objects.create(department=cls.department())
        LimitGroupsHoleTime.objects.create(department=cls.department())

    @classmethod
    def relevant_constraint(cls):
        return LimitGroupsHoleTime.objects.get(department=cls.department())

    def test_no_holes_declared(self):
        self.ok()

    def test_half_day_hole_time_exceeded_monday_morning(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_half_day = dt.timedelta(minutes=30)
        constraint.save()
        basic_group = random.choice(list(StructuralGroup.objects.filter(basic=True)))
        courses = Course.objects.filter(
            period=self.period(), groups__in=basic_group.connected_groups()
        )
        monday_9h = dt.datetime(2024, 1, 1, 9)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            start_time=monday_9h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            start_time=monday_9h + dt.timedelta(hours=1),
            version=version,
        )
        self.assertIn(LimitGroupsHoleTime.VIOLATIONS.AM, self.validate(10))

    def test_half_day_hole_time_exceeded_monday_afternoon(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_half_day = dt.timedelta(hours=1)
        constraint.save()
        basic_group = random.choice(list(StructuralGroup.objects.filter(basic=True)))
        courses = Course.objects.filter(
            period=self.period(), groups__in=basic_group.connected_groups()
        )
        monday_15h = dt.datetime(2024, 1, 1, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            start_time=monday_15h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            start_time=monday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitGroupsHoleTime.VIOLATIONS.PM, self.validate(10))

    def test_day_hole_time_exceeded_tuesday(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_day = dt.timedelta(hours=3)
        constraint.save()
        basic_group = random.choice(list(StructuralGroup.objects.filter(basic=True)))
        courses = Course.objects.filter(
            period=self.period(), groups__in=basic_group.connected_groups()
        )
        tuesday_10h = dt.datetime(2024, 1, 2, 8)
        tuesday_15h = dt.datetime(2024, 1, 2, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            start_time=tuesday_10h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            start_time=tuesday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[2],
            start_time=tuesday_15h - courses[2].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[3],
            start_time=tuesday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitGroupsHoleTime.VIOLATIONS.DAY, self.validate(10))

    def test_period_hole_time_exceeded(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_period = dt.timedelta(hours=5)
        constraint.save()
        basic_group = random.choice(list(StructuralGroup.objects.filter(basic=True)))
        courses = Course.objects.filter(
            period=self.period(), groups__in=basic_group.connected_groups()
        )
        monday_10h = dt.datetime(2024, 1, 1, 10)
        tuesday_10h = dt.datetime(2024, 1, 2, 10)
        wednesday_15h = dt.datetime(2024, 1, 3, 15)
        version = self.version(10)
        ScheduledCourse.objects.create(
            course=courses[0],
            start_time=monday_10h - courses[0].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[1],
            start_time=monday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[2],
            start_time=tuesday_10h - courses[2].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[3],
            start_time=tuesday_10h + dt.timedelta(hours=2),
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[4],
            start_time=wednesday_15h - courses[4].duration,
            version=version,
        )
        ScheduledCourse.objects.create(
            course=courses[5],
            start_time=wednesday_15h + dt.timedelta(hours=2),
            version=version,
        )
        self.assertIn(LimitGroupsHoleTime.VIOLATIONS.PERIOD, self.validate(10))

    def test_hole_times_in_ttmodel(self):
        constraint = self.relevant_constraint()
        constraint.max_hole_time_per_half_day = dt.timedelta(hours=2)
        constraint.max_hole_time_per_day = dt.timedelta(hours=10)
        constraint.max_hole_time_per_period = dt.timedelta(hours=50)
        constraint.save()
        self.timetable_model_ok()
