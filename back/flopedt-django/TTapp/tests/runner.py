import importlib
import os
import pprint
import sys
import warnings
from argparse import ArgumentParser
from typing import Any, Dict, Iterable, List, Sequence
from unittest import TestCase, TestResult, TestSuite

from django.conf import settings
from django.core.management import call_command
from django.db import DEFAULT_DB_ALIAS, connection, connections, transaction
from django.test.runner import DiscoverRunner
from django.test.utils import get_unique_databases_and_mirrors
from django.utils.deprecation import RemovedInDjango50Warning

from .populate import make_courses, make_random_schedule


class ConstraintTestRunner(DiscoverRunner):
    # This runner flushes then prepopulates the database according to
    # prepopulate setting
    # If we need more control, we might look at setUpModule of unittest.TestCase
    # (ok if classes are in the same file, but not in separate files)

    def __init__(
        self,
        prepopulate: str | None,
        pattern: str | None = ...,
        top_level: None = ...,
        verbosity: int = ...,
        interactive: bool = ...,
        failfast: bool = ...,
        keepdb: bool = ...,
        reverse: bool = ...,
        debug_mode: bool = ...,
        debug_sql: bool = ...,
        parallel: int = ...,
        tags: list[str] | None = ...,
        exclude_tags: list[str] | None = ...,
        **kwargs: Any,
    ) -> None:
        super().__init__(
            pattern,
            top_level,
            verbosity,
            interactive,
            failfast,
            keepdb,
            reverse,
            debug_mode,
            debug_sql,
            parallel,
            tags,
            exclude_tags,
            **kwargs,
        )
        self.prepopulate = prepopulate

    @classmethod
    def add_arguments(cls, parser: ArgumentParser) -> None:
        super().add_arguments(parser)
        parser.add_argument(
            "--prepopulate",
            dest="prepopulate",
            choices=["courses"],
            help="Prepopulate the database with the requested data (and its needed dependencies)",
        )

    def setup_databases(self, **kwargs):
        ret = super().setup_databases(**kwargs)
        call_command("flush", interactive=False)
        if self.prepopulate == "courses":
            make_courses()
            make_random_schedule()
        return ret
