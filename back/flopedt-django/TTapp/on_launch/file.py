import os
import shutil
from pathlib import Path

from django.conf import settings as ds

from TTapp.colors import Tcolors
from TTapp.on_launch.settings import settings

TEMP_DIR = os.path.join(ds.TMP_DIRECTORY, "constraints")
CLEAR_TEMP_FILES = settings()["clearTempFile"]

LANG_LIST = settings()["langs"]


def init_temp():
    if not Path.exists(Path(TEMP_DIR)):
        print(Tcolors.WARNING, "Temp dir does not exist, creating it", Tcolors.ENDC)
        try:
            os.mkdir(TEMP_DIR)
        except Exception:  # pylint:disable=broad-exception-caught
            print(
                Tcolors.WARNING,
                "Temp dir has not been created, aborting creation",
                Tcolors.ENDC,
            )
            return
    purge_temp_folder()


def purge_temp_folder():
    if CLEAR_TEMP_FILES:
        for l in LANG_LIST:
            lang_dir_path = os.path.join(TEMP_DIR, l)

            try:
                shutil.rmtree(lang_dir_path)
            except Exception:  # pylint:disable=broad-exception-caught
                print(
                    Tcolors.OKBLUE,
                    "Directory",
                    lang_dir_path,
                    "does not exist",
                    Tcolors.ENDC,
                )

            try:
                os.mkdir(lang_dir_path)
            except Exception:  # pylint:disable=broad-exception-caught
                print(
                    Tcolors.WARNING,
                    "Directory",
                    lang_dir_path,
                    "has not been created",
                    Tcolors.ENDC,
                )
    else:
        print(
            Tcolors.WARNING,
            "Temp directory will not be cleared, you can modify it in : \n",
            "FlopEDT/TTapp/apps.py",
            Tcolors.ENDC,
        )
