import json
import os

module_dir = os.path.abspath(__file__)
SETTINGS_DIR = os.path.dirname(module_dir)


def settings():
    with open(os.path.join(SETTINGS_DIR, "settings.json"), encoding="utf-8") as f:
        data = json.load(f)
        return data
