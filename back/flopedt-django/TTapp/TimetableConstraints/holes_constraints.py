# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.


import datetime as dt
from enum import Enum
from typing import TYPE_CHECKING, Any, Dict, List

from django.db import models
from django.utils.translation import gettext_lazy as _

from base.models import StructuralGroup, SchedulingPeriod, TimetableVersion, TimeGeneralSettings
from base.timing import Time
from people.models import Tutor
from TTapp.ilp_constraints.constraint import Constraint
from TTapp.ilp_constraints.constraint_type import ConstraintType
from TTapp.slots import days_filter, slots_filter
from TTapp.TimetableConstraints.orsay_constraints import (
    GroupsLunchBreak,
    TutorsLunchBreak,
)
from TTapp.TimetableConstraints.timetable_constraint import TimetableConstraint

if TYPE_CHECKING:
    from base.models.modifications import TimetableVersion
    from base.models.timing import SchedulingPeriod
    from TTapp.timetable_model import TimetableModel

SLOT_PAUSE = 5


############################
# Tools for hole analysis
############################


def holes_number(ttmodel: "TimetableModel", tutor_or_group):
    if isinstance(tutor_or_group, Tutor):
        is_busy = ttmodel.tutor_busy_slot
    elif isinstance(tutor_or_group, StructuralGroup):
        is_busy = ttmodel.group_busy_slot
    avail_slots = ttmodel.data.availability_slots
    last_slots_number = ttmodel.add_expr()
    for i, slot in enumerate(avail_slots):
        successor = avail_slots[i + 1] if i + 1 < len(avail_slots) else None
        if successor:
            if slot.end_time != successor.start_time:
                continue
            # add if slot is busy and succesor is not busy
            last_slots_number += ttmodel.add_floor(
                is_busy[tutor_or_group, slot] - is_busy[tutor_or_group, successor], 1, 2
            )
        else:
            # add if slot is busy
            last_slots_number += is_busy[tutor_or_group, slot]
    return last_slots_number - ttmodel.one_var


class LimitHoleTime(TimetableConstraint):
    """
    Limit the total hole time in each day, half-day and or every period
    """

    max_hole_time_per_half_day = models.DurationField(null=True, blank=True)
    max_hole_time_per_day = models.DurationField(null=True, blank=True)
    max_hole_time_per_period = models.DurationField(null=True, blank=True)
    except_lunch_break = models.BooleanField(default=True)

    class Meta:
        abstract = True
        verbose_name = _("Limit hole time")
        verbose_name_plural = verbose_name

    class VIOLATIONS(Enum):
        DAY = _("day")
        AM = _("morning")
        PM = _("afternoon")
        PERIOD = _("period")

    def get_viewmodel(self):
        view_model = super().get_viewmodel()
        details = view_model["details"]

        if self.tutors.exists():
            details.update({"tutors": ", ".join([tutor.username for tutor in self.tutors.all()])})

        return view_model

    def one_line_description(self):
        text = "Limite le nombre de trous "
        if self.max_hole_time_per_day is not None:
            text += f"à {self.max_hole_time_per_day} par jour "
            if self.max_hole_time_per_period is not None:
                text += "et "
        if self.max_hole_time_per_period is not None:
            text += f"à {self.max_hole_time_per_period} par semaine "
        if self.tutors.exists():
            text += " pour : " + ", ".join([tutor.username for tutor in self.tutors.all()])
            text += " pour : " + ", ".join([tutor.username for tutor in self.tutors.all()])
        else:
            text += " pour tous les profs"
        if self.train_progs.exists():
            text += " en " + ", ".join(
                [train_prog.abbrev for train_prog in self.train_progs.all()]
            )
        return text

    def hole_between_slots(
        self, ttmodel, tutor_or_group, sorted_avail_slots_list, slot, other_slot
    ):
        """
        returns a variable that is 1 if there is a hole between slot and other_slot
        """
        if isinstance(tutor_or_group, Tutor):
            is_busy = ttmodel.tutor_busy_slot
        elif isinstance(tutor_or_group, StructuralGroup):
            is_busy = ttmodel.group_busy_slot
        hole_between_slots = ttmodel.add_var()
        i = sorted_avail_slots_list.index(slot)
        j = sorted_avail_slots_list.index(other_slot)

        # if hole_between_slots is 1, then both slots are busy
        ttmodel.add_constraint(
            2 * hole_between_slots
            - is_busy[tutor_or_group, slot]
            - is_busy[tutor_or_group, other_slot],
            "<=",
            0,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )

        # if hole_between_slots is 1, then all slots between are free
        gap = j - i - 1
        ttmodel.add_constraint(
            gap * hole_between_slots
            + ttmodel.sum(
                is_busy[tutor_or_group, sorted_avail_slots_list[k]] for k in range(i + 1, j)
            ),
            "<=",
            gap,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )

        # if hole_between_slots is 0,
        # then some slots between are busy OR one of the border slots is free
        ttmodel.add_constraint(
            is_busy[tutor_or_group, slot]
            + is_busy[tutor_or_group, other_slot]
            - hole_between_slots
            - ttmodel.sum(
                is_busy[tutor_or_group, sorted_avail_slots_list[k]] for k in range(i + 1, j)
            ),
            "<=",
            1,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )
        return hole_between_slots

    def lunch_break_duration(self, tutor_or_group, period, date):
        if isinstance(tutor_or_group, Tutor):
            lunch_constraints = TutorsLunchBreak.objects.filter(
                models.Q(periods=period) | models.Q(periods=None),
                models.Q(tutors=tutor_or_group) | models.Q(tutors=None),
                models.Q(weekdays__contains=[date.weekday]) | models.Q(weekdays=[]),
                weight=None,
                department=self.department,
            )
        else:
            lunch_constraints = GroupsLunchBreak.objects.filter(
                models.Q(periods=period) | models.Q(periods=None),
                models.Q(groups__in=tutor_or_group.connected_groups()) | models.Q(groups=None),
                models.Q(train_progs=tutor_or_group.train_prog) | models.Q(train_progs=None),
                models.Q(weekdays__contains=[date.weekday]) | models.Q(weekdays=[]),
                weight=None,
                department=self.department,
            )

        if lunch_constraints.exists():
            return max(l_c.lunch_length for l_c in lunch_constraints)

        return dt.timedelta()

    def hole_time_expression(self, ttmodel: "TimetableModel", tutor_or_group, period):
        avail_slots = slots_filter(ttmodel.data.availability_slots, period=period)
        hole_time = {period: ttmodel.lin_expr()}
        for d in days_filter(ttmodel.data.days, period=period):
            hole_time[d] = {Time.AM: ttmodel.lin_expr(), Time.PM: ttmodel.lin_expr()}
            morning_avail_slots_list = list(slots_filter(avail_slots, day=d, apm=Time.AM))
            afternoon_avail_slots_list = list(slots_filter(avail_slots, day=d, apm=Time.PM))
            morning_avail_slots_list.sort()
            afternoon_avail_slots_list.sort()
            day_avail_slots_list = morning_avail_slots_list + afternoon_avail_slots_list

            for apm in [Time.AM, Time.PM]:
                if apm == Time.AM:
                    avail_slots_list = morning_avail_slots_list
                else:
                    avail_slots_list = afternoon_avail_slots_list
                for i, slot in enumerate(avail_slots_list):
                    for j in range(i + 2, len(avail_slots_list)):
                        other_slot = avail_slots_list[j]
                        hole_between_slots = self.hole_between_slots(
                            ttmodel,
                            tutor_or_group,
                            avail_slots_list,
                            slot,
                            other_slot,
                        )
                        hole_time[d][apm] += (
                            hole_between_slots
                            * (other_slot.start_time - slot.end_time).total_seconds()
                            / 60
                        )
            hole_time[d]["full_day"] = hole_time[d][Time.AM] + hole_time[d][Time.PM]
            lunch_length = dt.timedelta()
            if self.except_lunch_break:
                lunch_length = self.lunch_break_duration(tutor_or_group, period, d)

            for i, slot in enumerate(morning_avail_slots_list):
                for other_slot in afternoon_avail_slots_list:
                    if other_slot == day_avail_slots_list[i + 1]:
                        continue
                    hole_between_slots = self.hole_between_slots(
                        ttmodel,
                        tutor_or_group,
                        day_avail_slots_list,
                        slot,
                        other_slot,
                    )
                    if other_slot.start_time - slot.end_time > lunch_length:
                        hole_time[d]["full_day"] += (
                            hole_between_slots
                            * (
                                other_slot.start_time - slot.end_time - lunch_length
                            ).total_seconds()
                            / 60
                        )

            hole_time[period] += hole_time[d]["full_day"]
        return hole_time

    def enrich_model_for_one_object(self, ttmodel, tutor_or_group, period, ponderation):
        if isinstance(tutor_or_group, Tutor):
            kwargs = {"instructors": tutor_or_group}
            add_cost_method = ttmodel.add_to_inst_cost
        elif isinstance(tutor_or_group, StructuralGroup):
            kwargs = {"groups": tutor_or_group}
            add_cost_method = ttmodel.add_to_group_cost

        hole_time = self.hole_time_expression(ttmodel, tutor_or_group, period)
        if self.max_hole_time_per_half_day:
            for d in days_filter(ttmodel.data.days, period=period):
                for apm in [Time.AM, Time.PM]:
                    if self.weight is None:
                        ttmodel.add_constraint(
                            hole_time[d][apm],
                            "<=",
                            self.max_hole_time_per_half_day.total_seconds() / 60,
                            Constraint(
                                constraint_type=ConstraintType.HOLE_TIME_PER_HALF_DAY,
                                days=d,
                                apm=apm,
                                **kwargs,
                            ),
                        )
                    else:
                        cost = ponderation * self.local_weight()
                        for i in range(1, 5):
                            unwanted = ttmodel.add_floor(
                                hole_time[d][apm],
                                i * self.max_hole_time_per_half_day.total_seconds() / 60 + 1,
                                i * self.max_hole_time_per_half_day.total_seconds() / 60 + 1,
                                10000,
                            )
                            add_cost_method(
                                tutor_or_group,
                                unwanted * cost,
                                period,
                            )
                            cost *= 2

        if self.max_hole_time_per_day:
            if self.weight is None:
                for d in days_filter(ttmodel.data.days, period=period):
                    ttmodel.add_constraint(
                        hole_time[d]["full_day"],
                        "<=",
                        self.max_hole_time_per_day.total_seconds() / 60,
                        Constraint(
                            constraint_type=ConstraintType.HOLE_TIME_PER_DAY,
                            days=d,
                            **kwargs,
                        ),
                    )
            else:
                for d in days_filter(ttmodel.data.days, period=period):
                    cost = ponderation * self.local_weight()
                    for i in range(1, 5):
                        unwanted = ttmodel.add_floor(
                            hole_time[d]["full_day"],
                            i * self.max_hole_time_per_day.total_seconds() / 60 + 1,
                            10000,
                        )
                        add_cost_method(tutor_or_group, unwanted * cost, period)
                        cost *= 2
        if self.max_hole_time_per_period:
            if self.weight is None:
                ttmodel.add_constraint(
                    hole_time[period],
                    "<=",
                    self.max_hole_time_per_period.total_seconds() / 60,
                    Constraint(
                        constraint_type=ConstraintType.HOLE_TIME_PER_PERIOD,
                        periods=period,
                        **kwargs,
                    ),
                )
            else:
                cost = ponderation * self.local_weight()
                for i in range(1, 5):
                    unwanted = ttmodel.add_floor(
                        hole_time[period],
                        i * self.max_hole_time_per_period.total_seconds() / 60 + 1,
                        10000,
                    )
                    add_cost_method(tutor_or_group, unwanted * cost, period)
                    cost *= 2

    def enrich_ttmodel(self, ttmodel: "TimetableModel", period: "SchedulingPeriod", ponderation=1):
        raise NotImplementedError

    def steps_list(self, period: "SchedulingPeriod", version):
        # TODO: Use the partition structure ?
        result = {}
        relevant_scheduled_courses = self.period_version_scheduled_courses_queryset(
            period, version
        )
        for d in period.dates():
            day_scheduled_courses = relevant_scheduled_courses.filter(start_time__date=d)
            day_scheduled_courses = relevant_scheduled_courses.filter(start_time__date=d)
            steps = set()
            for sc in day_scheduled_courses.distinct("start_time", "course__duration"):
                steps |= {sc.start_time, sc.end_time}
            result[d] = sorted(list(steps))
        return result

    @property
    def objects_queryset(self):
        raise NotImplementedError

    def scheduled_courses_filter_query(self, obj):
        raise NotImplementedError

    def is_satisfied_for_old(
        self, period: SchedulingPeriod, version: TimetableVersion
    ) -> Dict[Enum, List[Any]]:
        invalid = {violation: [] for violation in self.VIOLATIONS}
        if hasattr(self.department, "timegeneralsettings"):
            time_general_settings = self.department.timegeneralsettings
        else:
            raise ValueError("No time general settings for department")
        relevant_scheduled_courses = self.period_version_scheduled_courses_queryset(
            period, version
        )
        steps_list = self.steps_list(period, version)
        hole_time = {obj: {} for obj in self.objects_queryset}
        for obj in self.objects_queryset:
            hole_time[obj] = {
                d: {
                    Time.AM: dt.timedelta(),
                    Time.PM: dt.timedelta(),
                    "day": dt.timedelta(),
                }
                for d in period.dates()
            }
            obj_scheduled_courses = relevant_scheduled_courses.filter(
                self.scheduled_courses_filter_query(obj)
            )
            if not obj_scheduled_courses.exists():
                continue
            for d in period.dates():
                i = 1
                j = 2
                while i < len(steps_list[d]) and j < len(steps_list[d]):
                    busy_end = steps_list[d][i]
                    hole_end = steps_list[d][j]
                    if slots_filter(
                        obj_scheduled_courses, starts_before=hole_end, ends_after=hole_end
                    ):
                        j += 1
                        continue
                    if j - i > 1:
                        if steps_list[d][j].time() <= time_general_settings.morning_end_time:
                            hole_time[obj][d][Time.AM] += hole_end - busy_end
                        elif steps_list[d][i].time() >= time_general_settings.afternoon_start_time:
                            hole_time[obj][d][Time.PM] += hole_end - busy_end
                        hole_time[obj][d]["day"] += hole_end - busy_end
                    i = j
                    j += 1
        for obj in self.objects_queryset:
            if self.max_hole_time_per_half_day:
                for d in period.dates():
                    if hole_time[obj][d][Time.AM] > self.max_hole_time_per_half_day:
                        invalid[self.VIOLATIONS.AM].append((d, obj))
                    if hole_time[obj][d][Time.PM] > self.max_hole_time_per_half_day:
                        invalid[self.VIOLATIONS.PM].append((d, obj))
            if self.max_hole_time_per_day:
                for d in period.dates():
                    if hole_time[obj][d]["day"] > self.max_hole_time_per_day:
                        invalid[self.VIOLATIONS.DAY].append((d, obj))
            if self.max_hole_time_per_period:
                if (
                    sum(hole_time[obj][d]["day"] for d in period.dates())
                    > self.max_hole_time_per_period
                ):
                    invalid[self.VIOLATIONS.PERIOD].append(obj)
        for violation in self.VIOLATIONS:
            if not invalid[violation]:
                invalid.pop(violation)
        return invalid

    def is_satisfied_for(
        self, period: SchedulingPeriod, version: TimetableVersion
    ) -> Dict[Enum, List[Any]]:
        invalid = {violation: [] for violation in self.VIOLATIONS}
        if hasattr(self.department, "timegeneralsettings"):
            time_general_settings = self.department.timegeneralsettings
        else:
            raise ValueError("No time general settings for department")
        for tutor_or_group in self.objects_queryset:
            hole_time = {}
            tutor_or_group_sched_courses = self.period_version_scheduled_courses_queryset(
                period, version
            ).filter(self.scheduled_courses_filter_query(tutor_or_group))
            if not tutor_or_group_sched_courses.exists():
                continue
            for course_date in tutor_or_group_sched_courses.distinct("date"):
                date = course_date.date
                date_courses = tutor_or_group_sched_courses.filter(date=date)
                if not date_courses.exists():
                    continue
                hole_time[date] = {}
                last_morning_endtime = dt.datetime.combine(
                    date, time_general_settings.morning_end_time
                )
                first_afternoon_start_time = dt.datetime.combine(
                    date, time_general_settings.afternoon_start_time
                )
                for apm in (Time.AM, Time.PM):
                    half_date_courses = slots_filter(date_courses, apm=apm)
                    sched_courses_list = list(half_date_courses)
                    if not sched_courses_list:
                        hole_time[date][apm] = dt.timedelta(minutes=0)
                        continue
                    sched_courses_list.sort()
                    hole_time[date][apm] = dt.timedelta(minutes=0)
                    current_sched_course = sched_courses_list[0]
                    i = 1
                    while i < len(sched_courses_list):
                        compared_sched_course = sched_courses_list[i]
                        if compared_sched_course.start_time < current_sched_course.end_time:
                            if compared_sched_course.end_time > current_sched_course.end_time:
                                current_sched_course = compared_sched_course
                        else:
                            hole_time[date][apm] += (
                                compared_sched_course.start_time - current_sched_course.end_time
                            )
                            current_sched_course = compared_sched_course
                        i += 1
                    if apm == Time.AM:
                        violation = self.VIOLATIONS.AM
                        last_morning_endtime = max(c.end_time for c in half_date_courses)
                    else:
                        first_afternoon_start_time = list(half_date_courses)[0].start_time
                        violation = self.VIOLATIONS.PM
                    if (
                        self.max_hole_time_per_half_day is not None
                        and self.max_hole_time_per_half_day < hole_time[date][apm]
                    ):
                        invalid[violation].append((tutor_or_group, date, sched_courses_list))
                hole_time[date]["full_day"] = hole_time[date][Time.AM] + hole_time[date][Time.PM]
                if last_morning_endtime < first_afternoon_start_time:
                    difference = first_afternoon_start_time - last_morning_endtime
                    if self.except_lunch_break:
                        lunch_break_duration = self.lunch_break_duration(
                            tutor_or_group=tutor_or_group, period=period, date=date
                        )
                        if difference > lunch_break_duration:
                            hole_time[date]["full_day"] += difference - lunch_break_duration
                    else:
                        hole_time[date]["full_day"] += difference
                if (
                    self.max_hole_time_per_day is not None
                    and self.max_hole_time_per_day < hole_time[date]["full_day"]
                ):
                    invalid[self.VIOLATIONS.DAY].append(
                        (tutor_or_group, date, sorted(list(date_courses)))
                    )

            if self.max_hole_time_per_period is not None and self.max_hole_time_per_period < sum(
                (hole_time[date]["full_day"] for date in hole_time), dt.timedelta()
            ):
                invalid[self.VIOLATIONS.PERIOD].append(
                    (tutor_or_group, sorted(list(tutor_or_group_sched_courses)))
                )
        for violation in self.VIOLATIONS:
            if not invalid[violation]:
                invalid.pop(violation)
        return invalid


class LimitTutorsHoleTime(LimitHoleTime):
    tutors = models.ManyToManyField("people.Tutor", blank=True)

    class Meta:
        verbose_name = _("Limit tutors hole time")
        verbose_name_plural = verbose_name

    def get_viewmodel(self):
        view_model = super().get_viewmodel()
        details = view_model["details"]

        if self.tutors.exists():
            details.update({"tutors": ", ".join([tutor.username for tutor in self.tutors.all()])})

        return view_model

    def one_line_description(self):
        text = "Limite le nombre de trous "
        if self.max_hole_time_per_day is not None:
            text += f"à {self.max_hole_time_per_day} par jour "
            if self.max_hole_time_per_period is not None:
                text += "et "
        if self.max_hole_time_per_period is not None:
            text += f"à {self.max_hole_time_per_period} par période "
        if self.tutors.exists():
            text += " pour : " + ", ".join([tutor.username for tutor in self.tutors.all()])
            text += " pour : " + ", ".join([tutor.username for tutor in self.tutors.all()])
        else:
            text += " pour tous les profs"

    def enrich_ttmodel(self, ttmodel, period, ponderation=1):
        for tutor in self.considered_tutors(ttmodel):
            self.enrich_model_for_one_object(ttmodel, tutor, period, ponderation)

    @property
    def objects_queryset(self):
        return self.considered_tutors()

    def scheduled_courses_filter_query(self, obj: Tutor):
        return models.Q(tutor=obj) | models.Q(course__supp_tutors=obj)


class LimitGroupsHoleTime(LimitHoleTime):
    """
    Limit the total hole time in each day, half-day and or every week for every considered basic groups
    """

    train_progs = models.ManyToManyField("base.TrainingProgramme", blank=True)
    groups = models.ManyToManyField("base.StructuralGroup", blank=True)

    class Meta:
        verbose_name = _("Limit groups hole time")
        verbose_name_plural = verbose_name

    def get_viewmodel(self):
        view_model = super().get_viewmodel()
        details = view_model["details"]

        if self.groups.exists():
            details.update({"groups": ", ".join([group.name for group in self.groups.all()])})
            details.update({"groups": ", ".join([group.name for group in self.groups.all()])})
        if self.train_progs.exists():
            details.update(
                {
                    "train_progs": ", ".join(
                        [train_prog.abbrev for train_prog in self.train_progs.all()]
                    )
                }
            )

        return view_model

    def one_line_description(self):
        text = "Limite le nombre de trous "
        if self.max_hole_time_per_day is not None:
            text += f"à {self.max_hole_time_per_day} par jour "
            if self.max_hole_time_per_period is not None:
                text += "et "
        if self.max_hole_time_per_period is not None:
            text += f"à {self.max_hole_time_per_period} par période "
        if self.groups.exists():
            text += " pour : " + ", ".join([group.name for group in self.groups.all()])
        else:
            text += " pour tous les groupes"
        if self.train_progs.exists():
            text += " en " + ", ".join(
                [train_prog.abbrev for train_prog in self.train_progs.all()]
            )
        return text

    def enrich_ttmodel(self, ttmodel, period, ponderation=1):
        for bg in self.considered_basic_groups(ttmodel):
            self.enrich_model_for_one_object(ttmodel, bg, period, ponderation)

    @property
    def objects_queryset(self):
        return self.considered_basic_groups()

    def scheduled_courses_filter_query(self, obj: StructuralGroup):
        return models.Q(course__groups__in=obj.connected_groups())
