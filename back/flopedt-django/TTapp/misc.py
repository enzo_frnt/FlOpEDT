# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

from django.db.models import Q


from base.models import SchedulingPeriod, TimetableVersion, ModuleTutorRepartition
from core.decorators import timer
from TTapp.flop_constraint import FlopConstraint, all_subclasses
from TTapp.models import (
    NoSimultaneousGroupCourses,
    ScheduleAllCourses,
    RespectTutorsMaxTimePerDay,
    RespectTutorsMinTimePerDay,
    MinimizeTutorsBusyDays,
    ConsiderPivots,
    MinGroupsHalfDays,
    ConsiderDependencies,
    AssignAllCourses,
    ConsiderTutorsUnavailability,
    ConsiderModuleTutorRepartitions,
    LimitSimultaneousRoomCourses,
    LocateAllCourses,
    MinNonPreferedTutorsSlot,
    MinNonPreferedTrainProgsSlot,
)

MAX_WEIGHT = 8


@timer
def are_all_flop_constraints_satisfied_for(
    period: SchedulingPeriod,
    version: TimetableVersion,
    strong_constraints_only: bool = False,
    active_constraints_only: bool = True,
):
    errors = []
    for cl in all_subclasses(FlopConstraint):
        considered_objects = cl.objects.filter(
            Q(periods__isnull=True) | Q(periods=period), department=version.department
        )
        if strong_constraints_only:
            considered_objects = considered_objects.filter(weight=None)
        if active_constraints_only:
            considered_objects = considered_objects.filter(is_active=True)
        for a in considered_objects:
            try:
                a.is_satisfied_for(period, version)
            except NotImplementedError:
                continue
            except AssertionError as e:
                errors.append(e)
    return errors


def add_basic_timetable_constraints(department):
    """
    Add the core constraints to the PuLP model :
        - a course is scheduled once and only once
        - no group has two courses in parallel
        - + a teacher does not have 2 courses in parallel
            + the teachers are available on the chosen slots
        - no course on vacation days
    """
    # constraint : only one course per basic group on simultaneous slots
    # (and None if transversal ones)
    # Do not consider it if mode.cosmo == 2!
    if department.mode.cosmo != 2:
        if not NoSimultaneousGroupCourses.objects.filter(department=department).exists():
            NoSimultaneousGroupCourses.objects.create(department=department)

    # constraint : courses are scheduled only once
    if not ScheduleAllCourses.objects.filter(department=department).exists():
        ScheduleAllCourses.objects.create(department=department)

    # Check if RespectBound constraint is in database, and add it if not
    if not RespectTutorsMaxTimePerDay.objects.filter(department=department).exists():
        RespectTutorsMaxTimePerDay.objects.create(department=department)

    # Check if RespectMinHours constraint is in database, and add it if not
    if not RespectTutorsMinTimePerDay.objects.filter(department=department).exists():
        RespectTutorsMinTimePerDay.objects.create(department=department)

    # Check if MinimizeBusyDays constraint is in database, and add it if not
    if not MinimizeTutorsBusyDays.objects.filter(department=department).exists():
        MinimizeTutorsBusyDays.objects.create(department=department, weight=MAX_WEIGHT)

    if not department.mode.cosmo:
        # Check if ConsiderPivots constraint is in database, and add it if not
        if not ConsiderPivots.objects.filter(department=department).exists():
            ConsiderPivots.objects.create(department=department)

        # Check if MinGroupsHalfDays constraint is in database, and add it if not
        if not MinGroupsHalfDays.objects.filter(department=department).exists():
            MinGroupsHalfDays.objects.create(department=department, weight=MAX_WEIGHT)

        # Check if ConsiderDependencies constraint is in database, and add it if not
        if not ConsiderDependencies.objects.filter(department=department).exists():
            ConsiderDependencies.objects.create(department=department)

    # Each course is assigned to a unique tutor
    if not AssignAllCourses.objects.filter(department=department).exists():
        AssignAllCourses.objects.create(department=department)

    if not ConsiderTutorsUnavailability.objects.filter(department=department).exists():
        ConsiderTutorsUnavailability.objects.create(department=department)

    if (
        not ConsiderModuleTutorRepartitions.objects.filter(department=department).exists()
        and ModuleTutorRepartition.objects.filter(course_type__department=department).exists()
    ):
        ConsiderModuleTutorRepartitions.objects.create(department=department)

    # constraint : each Room is only used once on simultaneous slots
    if not LimitSimultaneousRoomCourses.objects.filter(department=department).exists():
        LimitSimultaneousRoomCourses.objects.create(department=department)

    # each course is located into a room
    if not LocateAllCourses.objects.filter(department=department).exists():
        LocateAllCourses.objects.create(department=department)

    # first objective  => minimise use of unpreferred slots for teachers
    if not MinNonPreferedTutorsSlot.objects.filter(department=department).exists():
        MinNonPreferedTutorsSlot.objects.create(weight=MAX_WEIGHT, department=department)

    # second objective  => minimise use of unpreferred slots for courses
    if not MinNonPreferedTrainProgsSlot.objects.filter(department=department).exists():
        MinNonPreferedTrainProgsSlot.objects.create(weight=MAX_WEIGHT, department=department)
