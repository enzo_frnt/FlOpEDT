# coding: utf-8
# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.

import logging
import os

from django.conf import settings as ds
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render

from base.models import Department, TrainingPeriod, SchedulingPeriod
from base.weeks import current_year
from flopeditor.configuration.deploy_database import extract_database_file
from flopeditor.configuration.extract_planif_file import extract_planif
from flopeditor.configuration.file_manipulation import check_ext_file, upload_file
from flopeditor.configuration.forms import ImportConfig, ImportPlanif
from flopeditor.configuration.make_filled_database_file import make_filled_database_file
from flopeditor.configuration.make_planif_file import make_planif_file

from core.decorators import tutor_or_superuser_required


logger = logging.getLogger(__name__)


@tutor_or_superuser_required
def configuration(request, department_abbrev):
    department = get_object_or_404(Department, abbrev=department_abbrev)
    arg_req = {}
    arg_req["form_config"] = ImportConfig()
    arg_req["form_planif"] = ImportPlanif()
    arg_req["department"] = {"name": department.name, "abbrev": department.abbrev}
    arg_req["list_departments"] = [
        {"name": department.name, "abbrev": department.abbrev}
        for department in Department.objects.exclude(abbrev=department_abbrev)
    ]
    arg_req["training_periods"] = [
        {"name": training_period.name, "department": department_abbrev}
        for training_period in TrainingPeriod.objects.filter(department__abbrev=department_abbrev)
    ]
    arg_req["current_year"] = current_year
    arg_req["scheduling_periods"] = [
        {"name": period.name} for period in department.scheduling_periods()
    ]
    arg_req["has_dept_perm"] = request.user.has_department_perm(department=department, admin=True)

    return render(request, "flopeditor/configuration.html", arg_req)


@tutor_or_superuser_required
def import_config_file(req, department_abbrev, **kwargs):
    """
    View for the first step of the configuration. It imports the file
    to build the database, clear the database, extract the file to
    build the database and make the planif file for the second step
    of the configuration.
    Ajax request.

    :param req:
    :return:
    """
    if req.method == "POST":
        form = ImportConfig(req.POST, req.FILES)
        logger.debug(req)
        logger.debug(req.FILES)
        if form.is_valid():
            logger.debug(req.FILES["fichier"])
            logger.debug(req.FILES["fichier"].name)
            if check_ext_file(req.FILES["fichier"], [".xlsx", ".xls"]):
                path = upload_file(req.FILES["fichier"], "uploaded_database_file.xlsx")
                # If one of method fail the transaction will be not commit.
                department = Department.objects.get(abbrev=department_abbrev)
                try:
                    with transaction.atomic():
                        extract_database_file(
                            department_name=department.name,
                            department_abbrev=department_abbrev,
                            bookname=path,
                        )
                        logger.debug("extract OK")
                        os.rename(
                            path,
                            os.path.join(
                                ds.CONF_XLS_DIR, f"database_file_{department_abbrev}.xlsx"
                            ),
                        )
                        logger.warning("rename OK")
                        response = {
                            "status": "ok",
                            "data": "OK",
                            "dept_abbrev": department_abbrev,
                            "dept_fullname": department.name,
                        }
                except Exception as e:  # pylint: disable=broad-exception-caught
                    os.remove(path)
                    logger.debug(e)
                    response = {"status": "error", "data": str(e)}
                    return JsonResponse(response)

                source = os.path.join(os.path.dirname(__file__), "xls/empty_planif_file.xlsx")
                target_repo = ds.CONF_XLS_DIR
                logger.info("start planif")
                make_planif_file(department, empty_bookname=source, target_repo=target_repo)
                logger.info("make planif OK")
            else:
                response = {"status": "error", "data": "Invalid format"}
        else:
            response = {"status": "error", "data": "Form not valid"}
    return JsonResponse(response)


@tutor_or_superuser_required
def get_config_file(_, **kwargs):
    """
    Resend the empty configuration's file.

    :param req:
    :return:
    """

    with open(
        f"{os.path.join(os.path.dirname(__file__))}/xls/empty_database_file.xlsx", "rb"
    ) as f:
        response = HttpResponse(f, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = 'attachment; filename="database_file.xls"'
        f.close()
        return response


@tutor_or_superuser_required
def get_planif_file(_, department_abbrev, with_courses=True, **kwargs):
    """
    Send an empty planification's file.
    Rely on the configuration step if it was taken.
    :param req:
    :return:
    """
    filename = f"planif_file_{department_abbrev}.xlsx"
    final_filename = os.path.join(ds.CONF_XLS_DIR, filename)
    if with_courses:
        with_courses_filename = f"planif_file_{department_abbrev}_with_courses.xlsx"
        with_courses_final_filename = os.path.join(ds.CONF_XLS_DIR, with_courses_filename)
        if os.path.exists(with_courses_final_filename):
            filename = with_courses_filename
            final_filename = with_courses_final_filename
    if not os.path.exists(final_filename):
        filename = "empty_planif_file.xlsx"
        final_filename = os.path.join(os.path.join(os.path.dirname(__file__)), f"xls/{filename}")

    with open(final_filename, "rb") as f:
        response = HttpResponse(f, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = f'attachment; filename="{filename}"'
        f.close()
        return response


@tutor_or_superuser_required
def get_filled_database_file(_, department_abbrev, **kwargs):
    """
    Send an filled database file.
    Rely on the configuration step if it was taken.
    :param req:
    :return:
    """
    basic_filename = f"database_file_{department_abbrev}.xlsx"
    filename = os.path.join(ds.CONF_XLS_DIR, basic_filename)

    if not os.path.exists(filename):
        basic_filename = "empty_database_file.xlsx"
        filename = os.path.join(os.path.dirname(__file__), f"xls/{basic_filename}")
        print(filename)
    with open(filename, "rb") as f:
        response = HttpResponse(f, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = f"attachment; filename={basic_filename}"
        f.close()
        return response


@tutor_or_superuser_required
def mk_and_dl_planif(req, department_abbrev, with_courses, **kwargs):
    department = Department.objects.get(abbrev=department_abbrev)
    source = os.path.join(os.path.dirname(__file__), "xls/empty_planif_file.xlsx")
    target_repo = os.path.join(ds.CONF_XLS_DIR)
    logger.info("start planif")
    make_planif_file(
        department, empty_bookname=source, target_repo=target_repo, with_courses=with_courses
    )
    return get_planif_file(req, department_abbrev, with_courses, **kwargs)


@tutor_or_superuser_required
def mk_and_dl_database_file(req, department_abbrev, **kwargs):
    department = Department.objects.get(abbrev=department_abbrev)
    logger.info("start filled database file")
    make_filled_database_file(department)
    return get_filled_database_file(req, department_abbrev, **kwargs)


@tutor_or_superuser_required
def import_planif_file(req, department_abbrev, **kwargs):
    """
    Import a planification's file filled. Before data processing, it must to
    check if the first step of te configuration is done. Extract the data of the xlsx file.

    :param req:
    :return:
    """
    form = ImportPlanif(req.POST, req.FILES)
    department = Department.objects.get(abbrev=department_abbrev)
    if form.is_valid():
        if check_ext_file(req.FILES["fichier"], [".xlsx", ".xls"]):
            logger.info(req.FILES["fichier"])
            # If one of methods fail, the transaction will be not commit.
            with transaction.atomic():
                try:
                    path = upload_file(
                        req.FILES["fichier"], f"planif_file_{department_abbrev}.xlsx"
                    )
                    department = Department.objects.get(abbrev=department_abbrev)
                except Exception as e:  # pylint: disable=broad-exception-caught
                    response = {"status": "error", "data": str(e)}
                    return JsonResponse(response)
                stabilize_courses = "stabilize" in req.POST
                assign_colors = "assign_colors" in req.POST
                choose_scheduling_periods = "choose_scheduling_periods" in req.POST
                choose_training_periods = "choose_training_periods" in req.POST
                if choose_scheduling_periods:
                    scheduling_periods = SchedulingPeriod.objects.filter(
                        name__in=req.POST.getlist("scheduling_periods"),
                    )
                else:
                    scheduling_periods = None
                if choose_training_periods:
                    training_periods = TrainingPeriod.objects.filter(
                        department=department,
                        name__in=req.POST.getlist("training_periods"),
                    )
                else:
                    training_periods = None

                result = extract_planif(
                    department,
                    bookname=path,
                    scheduling_periods=scheduling_periods,
                    training_periods=training_periods,
                    stabilize_courses=stabilize_courses,
                    assign_colors=assign_colors,
                )

                if not result:
                    logger.info("Extract file OK")
                    rep = "OK !"
                    os.rename(path, f"{ds.CONF_XLS_DIR}/planif_file_{department_abbrev}.xlsx")
                    logger.info("Rename OK")
                    response = {"status": "ok", "data": rep}
                else:
                    os.remove(path)
                    rep = ""
                    for period_name, period_exceptions in result.items():
                        rep += f"{period_name} : \n"
                        for period_name, row_exceptions in period_exceptions.items():
                            rep += f"- {period_name}: \n"
                            for row, exceptions in row_exceptions.items():
                                rep += f"---> row {row} : {exceptions}\n"
                    response = {"status": "error", "data": rep}
        else:
            response = {"status": "error", "data": "Invalid format"}
    else:
        response = {"status": "error", "data": "Form can't be valid"}
    return JsonResponse(response)
