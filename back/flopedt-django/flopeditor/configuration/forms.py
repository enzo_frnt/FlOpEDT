from django import forms
from django.core.validators import RegexValidator


DAYS = [
    ("m", "Monday"),
    ("tu", "Tuesday"),
    ("w", "Wenesday"),
    ("th", "Thursday"),
    ("f", "Friday"),
]

validator_time = RegexValidator(r"^\d{2}:\d{2}$", "Must be as 00:00")


class ImportFile(forms.Form):
    file = forms.FileField()


class ImportPlanif(forms.Form):
    fichier = forms.FileField()


class ImportConfig(forms.Form):
    fichier = forms.FileField()
