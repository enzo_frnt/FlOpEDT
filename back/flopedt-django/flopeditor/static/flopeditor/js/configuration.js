
function send_form(form) {
    $("#"+form).submit(function(event){
        event.preventDefault();
        let post_url = $(this).attr("action");
        let request_method = $(this).attr("method");
        let data = new FormData($("#"+form).get(0));
        let form_enctype = $(this).attr("enctype");
        show_loader(true);
        $.ajax({
            url : post_url,
            type: request_method,
            data : data,
            enctype : form_enctype,
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                if(data.status === "ok") {
                    tmp = "";
                    if (data.data instanceof Array) {
                        for (er of data.data) {
                        tmp += er + "<br>";
                    }
                    } else {
                        tmp = data.data;
                    }
                    $("#error_"+form+" p").html(tmp);
                } else {
                    var obj = $("#error_"+form+" p").text(data.data);
                    // FIXME : la ligne suivante doit pouvoir être amélioré avec `white-space: pre-line;` dans les css
                    //  Cf https://stackoverflow.com/questions/4535888/jquery-text-and-newlines
                    obj.html(obj.html().replace(/\n/g,'<br/>'));
                }
                var option = {value:data.dept_abbrev, text:data.dept_fullname} ;
                if (form == 'config') {
                    $('#dropdown_dpt_1').append($('<option>', option));
                    $('#dropdown_dpt_5').append($('<option>', option));
                }
                show_loader(false);
            },
            error: function (data) {
                console.log(data);
                $("#error_"+form+" p").text("Error");
                show_loader(false);
            },
        })
    });
}

send_form("config");
send_form("planif");


function handlePlanifDeptChanges() {
    let selectTrainingPeriods = document.getElementById("dropdown_training_periods");
    selectTrainingPeriods.innerHTML = '';
    for (let training_period of training_periods) {
            let opt = document.createElement('option');
            opt.value = training_period.name;
            opt.innerHTML = training_period.name;
            selectTrainingPeriods.appendChild(opt);
    }
    let selectSchedulingPeriods = document.getElementById("dropdown_scheduling_periods");
    selectSchedulingPeriods.innerHTML = '';
    for (let scheduling_period of scheduling_periods) {
            let opt = document.createElement('option');
            opt.value = scheduling_period.name;
            opt.innerHTML = scheduling_period.name;
            selectSchedulingPeriods.appendChild(opt);
    }
}


// Training Period div gesture
let trainingPeriodsDiv = document.getElementById("choose_training_periods");
let trainingPeriodsCheckbox = trainingPeriodsDiv.querySelector("input[type=checkbox]");
let trainingPeriodsInput = trainingPeriodsDiv.querySelectorAll("select");
trainingPeriodsInput.forEach( (c) => {
        c.addEventListener('change', (event) => {
            trainingPeriodsCheckbox.checked = true;
            confirm_text.training_periods = Array.from(c.selectedOptions).map(el => el.value);
        })
})
trainingPeriodsCheckbox.addEventListener('change', (event) => {
    if (trainingPeriodsCheckbox.checked === true){
        confirm_text.training_periods = Array.from(trainingPeriodsInput).map(el => Array.from(el.selectedOptions).map(el => el.value));
    }
    else {
        confirm_text.training_periods = translated_all;
    }
})


// Scheduling periods div gesture
let schedulingPeriodsDiv = document.getElementById("choose_scheduling_periods");
let schedulingPeriodsCheckbox = schedulingPeriodsDiv.querySelector("input[type=checkbox]");
let schedulingPeriodsInput = schedulingPeriodsDiv.querySelectorAll("select");
schedulingPeriodsInput.forEach( (c) => {
        c.addEventListener('change', (event) => {
            schedulingPeriodsCheckbox.checked = true;
            confirm_text.scheduling_periods = Array.from(c.selectedOptions).map(el => el.value);
        })
})
schedulingPeriodsCheckbox.addEventListener('change', (event) => {
            if (schedulingPeriodsCheckbox.checked === true){
                confirm_text.scheduling_periods = Array.from(schedulingPeriodsInput).map(el => Array.from(el.selectedOptions).map(el => el.value));
            }
            else {
                confirm_text.scheduling_periods = translated_all;
            }
})


handlePlanifDeptChanges();
