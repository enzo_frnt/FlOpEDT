DOCKER_OR_PODMAN ?= docker

RED=\033[0;31m
ORANGE=\033[0;33m
GREEN=\033[0;32m
BLUE=\033[0;34m
NC=\033[0m 

check-health:
	@echo "${BLUE}Checking health status of the container...${NC}"
	@STATUS=$$(${DOCKER_OR_PODMAN} inspect --format='{{.State.Health.Status}}' $$(${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml ps -q backend)); \
	if [ "$$STATUS" = "healthy" ]; then \
		echo "${GREEN}Container is healthy${NC}"; \
	else \
		echo "${RED}Container is not healthy. Current status: $$STATUS${NC}"; \
		exit 1; \
	fi

is-backend-container-running:
	@echo "${BLUE}Checking if the container is running...${NC}"
	@CONTAINER_ID=$$(${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml ps -q backend); \
	if [ -z "$$CONTAINER_ID" ]; then \
		echo "${RED}Error: The backend container is not running or cannot be found. Please check the status.${NC}"; \
		echo "${RED}It may have crashed. Check the logs for more details.${NC}"; \
		exit 1; \
	fi; \
	echo "${GREEN}Container is running.${NC}"

wait-for-backend-health:
	@echo "${BLUE}Waiting for the backend container to be healthy...${NC}"
	@echo "${ORANGE}This may take a long time...${NC}"
	@$(MAKE) is-backend-container-running
	@i=1; while : ; do \
		CONTAINER_ID=$$(${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml ps -q backend); \
		if [ -z "$$CONTAINER_ID" ]; then \
			echo "${RED}Error: The backend container is not running or cannot be found. Please check the status.${NC}"; \
			${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml logs backend; \
			exit 1; \
		fi; \
		STATUS=$$(${DOCKER_OR_PODMAN} inspect --format='{{.State.Health.Status}}' $$CONTAINER_ID); \
		if [ "$$STATUS" = "healthy" ]; then \
			printf "\n${GREEN}Backend is healthy now.${NC}\n"; \
			break; \
		elif [ "$$STATUS" = "unhealthy" ]; then \
			echo "${RED}Error: The backend container is unhealthy. Check the logs for more details.${NC}"; \
			${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml logs backend; \
			echo "${ORANGE}Above are the logs of the backend container.${NC}"; \
			echo "${ORANGE}So if in the logs there is no error and the backend looks healthy, you can ignore this message and restart the command to load the dump.${NC}"; \
			exit 1; \
		elif [ "$$STATUS" = "exited" ]; then \
			echo "${RED}Error: The backend container has exited. Check the logs for more details.${NC}"; \
			${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml logs backend; \
			exit 1; \
		else \
			dots=$$(printf "%0.s." $$(seq 1 $$i)); \
			printf "\r\033[2K${ORANGE}Still waiting%s${NC}" "$$dots"; \
			sleep 5; \
			i=$$(($$i + 1)); \
		fi; \
	done;

load-dump:
	@echo "${BLUE}Waiting for the backend to be healthy...${NC}"
	@$(MAKE) wait-for-backend-health
	@echo "${GREEN}Backend is healthy and ready to use.${NC}"
	@echo "${BLUE}Load dump only works in deployment-dev environment${NC}"
	@echo "${BLUE}Loading dump...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml exec backend python manage.py loaddata /data-dump/dump.json.bz2
	@echo "${GREEN}Dump loaded.${NC}"

bootstrap-dev:
	@echo "${BLUE}This only works in the dev environment${NC}"
	@echo "${BLUE}Bootstrapping dev env...${NC}"
	@$(MAKE) start-dev
	@$(MAKE) load-dump
	@echo "${GREEN}Ready to Flop 🚀"

start-dev:
	@echo "${BLUE}This only works in dev environment${NC}"
	@echo "${BLUE}Starting development server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml up -d
	@echo "${GREEN}Development compose started.${NC}"

build-dev:
	@echo "${BLUE}Building development server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml build
	@echo "${GREEN}Development compose built.${NC}"

rebuilt-dev:
	@echo "${BLUE}Rebuilding development server...${NC}"
	@$(MAKE) down-dev
	@$(MAKE) build-dev
	@$(MAKE) start-dev
	@echo "${GREEN}Development compose rebuilt.${NC}"

stop-dev:
	@echo "${BLUE}This only works in dev environment${NC}"
	@echo "${BLUE}Stopping development server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml stop
	@echo "${GREEN}Development compose stopped.${NC}"

down-dev:
	@echo "${BLUE}This only works in dev environment${NC}"
	@echo "${BLUE}Downing development server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-dev/docker-compose.yml down
	@echo "${GREEN}Development compose downed.${NC}"

start-prod:
	@echo "${BLUE}This only works in prod environment${NC}"
	@echo "${BLUE}Starting production server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-prod/docker-compose.yml up -d
	@echo "${GREEN}Production compose started.${NC}"

build-prod:
	@echo "${BLUE}Building production server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-prod/docker-compose.yml build
	@echo "${GREEN}Production compose built.${NC}"

rebuilt-prod:
	@echo "${BLUE}Rebuilding production server...${NC}"
	@$(MAKE) down-prod
	@$(MAKE) build-prod
	@$(MAKE) start-prod
	@echo "${GREEN}Production compose rebuilt.${NC}"

bootstrap-prod:
	@echo "${BLUE}This only works in the prod environment${NC}"
	@echo "${BLUE}Bootstrapping prod env...${NC}"
	@$(MAKE) start-prod
	@echo "${GREEN}Ready to Flop 🚀"

stop-prod:
	@echo "${BLUE}This only works in prod environment${NC}"
	@echo "${BLUE}Stopping production server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-prod/docker-compose.yml stop
	@echo "${GREEN}Production compose stopped.${NC}"

down-prod:
	@echo "${BLUE}This only works in prod environment${NC}"
	@echo "${BLUE}Downing production server...${NC}"
	@${DOCKER_OR_PODMAN} compose -f ./deployment-prod/docker-compose.yml down
	@echo "${GREEN}Production compose downed.${NC}"
