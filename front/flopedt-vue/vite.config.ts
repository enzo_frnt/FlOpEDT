import { defineConfig } from 'vitest/config'
import { fileURLToPath } from 'url'
import svg from 'vite-plugin-magical-svg'
import vue from '@vitejs/plugin-vue'
import vueI18n from '@intlify/unplugin-vue-i18n/vite'
import { visualizer } from 'rollup-plugin-visualizer'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

const FLOP_BACKEND_URL = process.env.FLOP_BACKEND_URL || 'http://127.0.0.1:8000'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svg({ target: 'vue', restoreMissingViewBox: true, setFillStrokeColor: true }),
    vue({ template: { transformAssetUrls } }),
    vueI18n({ include: fileURLToPath(new URL('./src/locales/**', import.meta.url)), dropMessageCompiler: true }),
    quasar(), // Required for tree-shaking... *sigh*
    visualizer({
      filename: '.reports/bundle_stats.html',
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '~mingcute': fileURLToPath(new URL('./node_modules/mingcute_icon/svg', import.meta.url)),
    },
  },
  server: {
    proxy: {
      '/fr': FLOP_BACKEND_URL,
      '/en': FLOP_BACKEND_URL,
      '/es': FLOP_BACKEND_URL,
      '/api': FLOP_BACKEND_URL,
      '/static': FLOP_BACKEND_URL,
    },
  },
  define: {
    // https://vitest.dev/guide/in-source.html#production-build
    'import.meta.vitest': 'undefined',
    'import.meta.env.TEST': 'undefined',
  },
  test: {
    include: ['src/**/*.{test,spec}.?(c|m)[jt]s?(x)', 'tests/**/*.{test,spec}.?(c|m)[jt]s?(x)'],
    // In-source testing - https://vitest.dev/guide/in-source.html
    includeSource: ['src/**/*.{js,ts}'],
    environment: 'happy-dom',
    reporters: ['default', 'html', 'junit'],
    outputFile: {
      html: './.reports/vitest/report.html',
      junit: './.reports/vitest/junit.xml',
    },
    coverage: {
      provider: 'v8',
      reporter: ['html', 'cobertura', 'text'],
      reportsDirectory: './.reports/vitest/coverage',
    },
    typecheck: {
      checker: 'vue-tsc',
    },
  },
})
