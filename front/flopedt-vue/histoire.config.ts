import { defineConfig } from 'histoire'
import { HstVue } from '@histoire/plugin-vue'

const PRIORITIZED_STORIES = ['Colors', 'Icons', 'Terminology']

function orderStories(a: string, b: string) {
  // Prioritize some stories
  const priorityA = PRIORITIZED_STORIES.indexOf(a)
  const priorityB = PRIORITIZED_STORIES.indexOf(b)

  if (priorityA >= 0 && priorityB >= 0) return priorityB - priorityA
  if (priorityA >= 0) return -1
  if (priorityB >= 0) return 1

  // Folders first
  const isFolderA = a[0] >= 'a' && a[0] <= 'z'
  const isFolderB = b[0] >= 'a' && b[0] <= 'z'
  if (isFolderA !== isFolderB) return +isFolderB - +isFolderA

  // Default sort
  return a.localeCompare(b)
}

export default defineConfig({
  setupFile: '/src/histoire.setup.ts',
  plugins: [HstVue()],

  theme: { darkClass: 'theme-dark' },
  tree: {
    order: orderStories,
    groups: [
      {
        title: 'UI Kit',
        include: (file) => file.path.includes('components/uikit'),
      },
    ],
  },

  defaultStoryProps: {
    autoPropsDisabled: true,
  },

  // Make it a pure SPA without router
  // Allows it to be viewed from CI artifacts and other static places
  routerMode: 'hash',
  vite: {
    base: './',
    server: {
      host: '0.0.0.0',
      port: 6006,
    },
  },
})
