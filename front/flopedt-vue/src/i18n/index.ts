import { nextTick } from 'vue'
import { createI18n } from 'vue-i18n'
import en from '@/locales/en.json'

// Structure inspired by article from Lokalise - https://lokalise.com/blog/vue-i18n/
// It itself takes most of its info from the vue-i18n docs

const LANGUAGE_STORAGE_KEY = 'i18n-locale'

// https://vitejs.dev/guide/features#glob-import
// Note regarding aliases: https://github.com/vitejs/vite/pull/12221
const LOCALES = import.meta.glob<{ default: typeof en }>('/src/locales/*.json')

// Note: we can't use availableLocales from i18n anymore as locales are not immediately loaded.
export const SUPPORTED_LOCALES = Object.keys(LOCALES).map((k) => k.slice('/src/locales/'.length, -'.json'.length))

// "Last resort" fallback if there is no available locale that matches the user's browser settings.
export const FALLBACK_LOCALE = 'en'

export function getPreferredLocale() {
  // 1. Configured locale in-app
  const persistedLocale = localStorage.getItem(LANGUAGE_STORAGE_KEY)
  if (persistedLocale && SUPPORTED_LOCALES.includes(persistedLocale)) {
    return persistedLocale
  }

  // 2. Configured locale in the browser
  // https://developer.mozilla.org/en-US/docs/Web/API/Navigator/languages
  for (const locale of navigator.languages) {
    if (SUPPORTED_LOCALES.includes(locale)) return locale

    const dash = locale.indexOf('-')
    if (dash > 0) {
      const localeWithoutRegion = locale.slice(0, dash)
      if (SUPPORTED_LOCALES.includes(localeWithoutRegion)) return localeWithoutRegion
    }
  }

  // 3. Fallback locale
  return FALLBACK_LOCALE
}

export async function changeLocale(locale: string) {
  // Load messages if necessary
  if (!i18n.global.availableLocales.includes(locale)) {
    await loadMessages(locale)
  }

  // Set the locale
  i18n.global.locale.value = locale
  document.documentElement.setAttribute('lang', locale)
  document.documentElement.setAttribute('dir', i18n.global.t('_meta.dir'))

  // Persist to localstorage for later use
  localStorage.setItem(LANGUAGE_STORAGE_KEY, locale)
}

export async function loadMessages(locale: string) {
  if (!(`/src/locales/${locale}.json` in LOCALES)) {
    if (locale.includes('-')) {
      console.warn(`Locale ${locale} not found`)
      return loadMessages(locale.split('-')[0])
    }
    console.error(`No such lang ${locale}!`)
  }

  const messages = await LOCALES[`/src/locales/${locale}.json`]()
  i18n.global.setLocaleMessage(locale, messages.default)
  return nextTick()
}

// Export i18n
const locale = getPreferredLocale()
export const i18n = createI18n<[typeof en], string, false>({
  legacy: false,
  locale: locale,
  fallbackLocale: 'en',
  // Only the en locale is loaded, as it is the fallback locale.
  // Other locales are lazy-loaded for performance and bundle size concerns.
  messages: { en },
})

// Load locale data
void loadMessages(locale)
