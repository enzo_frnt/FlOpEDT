import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

let idPool = 0

/** Type of action. Meant as a purely informative property for the views. */
export enum ActionType {
  /** Action that created something. */
  CREATE = 'create',
  /** Action that updated something. */
  UPDATE = 'update',
  /** Action that deleted something. */
  DELETE = 'delete',

  /** Action that represents the initialization of the data before any action. Reserved for internal use. */
  INIT = 'init',
  /** Action that reverted committed actions. Reserved for internal use. */
  REVERT = 'revert',
}

/** State of an action. */
export enum ActionStatus {
  // I doubt it'll be very useful here, but an interesting properties of TS enums is that the first member, by default,
  // has a falsy value. Sometimes it's valuable to set the first enum member to equal 1, to avoid this behavior which
  // can be considered a pitfall (e.g. naïve null check fails due to this).
  //
  // Here, it has the effect of making the status falsy if an action is not applied currently. Might be useful!
  /** The action has been undone */
  UNDONE,
  /** The action is not yet committed */
  PENDING,
  /** The action has been committed */
  COMMITTED,
  /** The action has been reverted */
  REVERTED,
}

export type UndoRedoAction = {
  /** Internal ID of the action. Can be used to uniquely identify the action. */
  $id: number

  /** Type of action. Meant as a purely informative property for the views. */
  type: ActionType

  /** Source of the action. Intended to be usable as a part of i18n keys. */
  source: string

  /** Status of the action. */
  status: ActionStatus

  /** Whether the action is the latest one applied locally (and as such represents the current data) or not. */
  isLocalLatest: boolean

  /** Whether the action is the latest one applied remotely (and as such represents the current data) or not. */
  isRemoteLatest: boolean
}

export type UndoRedoActionParams = {
  /** Type of action. Meant as a purely informative property for the views. */
  type: ActionType

  /** Source of the action. Intended to be usable as a part of i18n keys. */
  source: string

  /** Function to execute to perform the operation. Must gracefully handle already committed operations. */
  commit: (wasCommitted: boolean) => unknown

  /** Function to execute to revert the operation. Must gracefully h andle already committed operations. */
  rollback: (wasCommitted: boolean) => unknown
}

type UndoRedoActionParamsInternal = UndoRedoActionParams & {
  id: number
  reverted?: boolean
}

const INIT_ACTION: UndoRedoActionParamsInternal = {
  id: -1,
  type: ActionType.INIT,
  source: '_system',

  /* v8 ignore start */
  // Excluded from test coverage as they're just no-ops and are not meant to ever be called
  // For what it's worth, if they're called it's actually a problem...!
  commit: () => {},
  rollback: () => {},
  /* v8 ignore end */
}

export const useUndoRedoStore = defineStore('undoredo', () => {
  // Internal state. Not exposed.
  const trackedActions = ref<UndoRedoActionParamsInternal[]>([INIT_ACTION])
  const localLatestPtr = ref(0)
  const remoteLatestPtr = ref(0)

  const canUndo = computed(() => localLatestPtr.value > remoteLatestPtr.value)
  const canRedo = computed(() => localLatestPtr.value !== trackedActions.value.length - 1)

  const actions = computed<UndoRedoAction[]>(() => {
    return trackedActions.value.map((action, idx) => {
      let status = ActionStatus.UNDONE
      if (idx <= localLatestPtr.value) status = ActionStatus.PENDING
      if (idx <= remoteLatestPtr.value) status = ActionStatus.COMMITTED
      if (action.reverted) status = ActionStatus.REVERTED

      return {
        $id: action.id,
        type: action.type,
        source: action.source,
        status: status,
        isLocalLatest: localLatestPtr.value === idx,
        isRemoteLatest: remoteLatestPtr.value === idx,
      }
    })
  })

  /**
   * Tracks (and executes) an action as undo/redo-able.
   *
   * @param action The action to track and execute
   */
  function track(action: UndoRedoActionParams) {
    if (canRedo.value) {
      // If we can redo, this means we're overwriting actions that have been undone. Get rid of them.
      trackedActions.value = trackedActions.value.slice(0, localLatestPtr.value + 1)
    }

    trackedActions.value.push({ ...action, id: idPool++ })
    localLatestPtr.value++
    action.commit(false)
  }

  /**
   * Undoes the latest action.
   * No-op if there are no available action, or if the action has already been committed.
   *
   * @see {@link goto} for undoing changes that are past the commit checkpoint.
   */
  function undo() {
    if (!canUndo.value) return

    const action = trackedActions.value[localLatestPtr.value--]
    action.rollback(false)
  }

  /**
   * Redoes the latest undone action.
   * No-op if there are no available action.
   */
  function redo() {
    if (!canRedo.value) return

    const action = trackedActions.value[++localLatestPtr.value]
    action.commit(false)
  }

  /**
   * Goes to the specified target action.
   * Actions between current and the target will be undone (or redone).
   *
   * If the target action has already been committed, all pending operations will be dropped and a new
   * {@link ActionType#REVERT|revert} action will be added to the list of pending changes.
   *
   * @param target Action to go to.
   * @throws Error if the target is not an action tracked.
   */
  function goto(target: UndoRedoAction) {
    const index = trackedActions.value.findIndex((action) => action.id === target.$id)
    if (index === -1) throw new Error('Undo/Redo: Attempted to go to an untracked action')

    if (index < remoteLatestPtr.value) {
      // This has already been committed. Create a new action that tracks the rollback to this action
      localLatestPtr.value = remoteLatestPtr.value

      const targetedActions = trackedActions.value.slice(index + 1, remoteLatestPtr.value + 1)
      return track({
        type: ActionType.REVERT,
        source: '_system',
        commit: () => {
          for (const action of targetedActions) {
            action.reverted = true
            action.rollback(true)
          }
        },
        rollback: () => {
          for (const action of targetedActions) {
            action.reverted = false
            action.commit(true)
          }
        },
      })
    }

    if (index < localLatestPtr.value) {
      // Bulk undo (target < current)
      // Get the list of actions between the requested point in time and now, and rollback everything.
      const targetedActions = trackedActions.value.slice(index + 1, localLatestPtr.value)

      targetedActions.reverse() // We want to revert them from newest to oldest, so we reverse the array.
      for (const action of targetedActions) {
        action.rollback(false)
      }
    } else {
      // Bulk redo (current < target)
      // Get the list of actions between the requested point in time and now, and commit everything
      const targetedActions = trackedActions.value.slice(localLatestPtr.value, index + 1)
      for (const action of targetedActions) {
        action.commit(false)
      }
    }

    // Update the current version
    localLatestPtr.value = index
  }

  /**
   * Marks pending operations as committed. Actions will no longer be undoable, unless via {@link goto}.
   * Does not affect actions that have been undone but are still accessible.
   */
  function markCommitted() {
    remoteLatestPtr.value = localLatestPtr.value
  }

  return {
    actions,
    canUndo,
    canRedo,

    track,
    undo,
    redo,
    goto,
    markCommitted,
  }
})
