// noinspection DuplicatedCode

import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'

import { defineCrudStore, OperationType } from '@/stores/defineCrudStore.ts'
import { createDeferred } from '@/utils/deferred.ts'
import { ActionType, useUndoRedoStore } from '@/stores/undoredo.ts'
import { Entity } from '@/ts/entities/base/entity.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'
import { EntitySet } from '@/ts/entities/base/entityset.ts'
import { withVueSetup } from '@/utils/vitest.ts'
import { nextTick, watch } from 'vue'

type Id = Identifier | number

// Fake entity used in the tests
class TestEntity extends Entity<null> {
  // noinspection JSUnusedGlobalSymbols
  constructor(
    id: Identifier,
    public readonly uwu: boolean | undefined
  ) {
    super(id)
  }

  toJSON() {
    return null
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any -- We want a "whatever type" here
function whatever(): any {}

beforeEach(() => void setActivePinia(createPinia()))

it('defines a proper store', () => {
  const useStore = defineCrudStore({
    name: 'test!',
    fetchAll: vi.fn(),
    fetchOne: vi.fn(),
    create: vi.fn(),
    update: vi.fn(),
    remove: vi.fn(),
  })

  expect(useStore).toBeTypeOf('function')

  const store = useStore()
  expect(store).toHaveProperty('$id', 'test!')
  expect(store).toHaveProperty('entities')
  expect(store.entities).toBeInstanceOf(EntitySet)
  expect(store).toHaveProperty('isLoading', false)
  expect(store).toHaveProperty('error', undefined)
  expect(store).toHaveProperty('fetchAll')
})

it('combines base store properties with extra props from setup', () => {
  const useStore = defineCrudStore({
    name: 'test!',
    fetchAll: vi.fn(),
    fetchOne: vi.fn(),
    create: vi.fn(),
    update: vi.fn(),
    remove: vi.fn(),
    setup: () => ({ meow: true }),
  })

  const store = useStore()
  expect(store).toHaveProperty('meow', true)
})

it('passes the "real" base state to the setup', async () => {
  const fetchAll = vi.fn()
  const useStore = defineCrudStore({
    name: 'test!',
    fetchAll: fetchAll,
    fetchOne: vi.fn(),
    create: vi.fn(),
    update: vi.fn(),
    remove: vi.fn(),
    setup: (state) => ({ $passed_entities: state.entities, $passed_fetchAll: state.fetchAll }),
  })

  const store = useStore()
  expect(store.$passed_entities).toBe(store.entities)

  // Functions are wrapped by the store. To test them we check if calling both does wake up the mock.
  await store.$passed_fetchAll()
  await store.fetchAll()

  expect(fetchAll).toHaveBeenCalledTimes(2)
})

it('lets setup function override base state properties', () => {
  const useStore = defineCrudStore({
    name: 'test!',
    fetchAll: vi.fn(),
    fetchOne: vi.fn(),
    create: vi.fn(),
    update: vi.fn(),
    remove: vi.fn(),
    setup: () => ({ entities: true }),
  })

  const store = useStore()
  expect(store.entities).toBe(true)
})

describe('fetchAll', () => {
  it('puts all fetched entities in store', async () => {
    const testResponse = [
      new TestEntity(new Identifier(1234), undefined),
      new TestEntity(new Identifier(1337), undefined),
    ]

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve(testResponse)),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchAll()).resolves.toBeUndefined()
    expect(store.error).toBeUndefined()
    expect(Array.from(store.entities)).toEqual(testResponse)
  })

  it('does not put duplicate entries in store, but updates them', async () => {
    let fetched = false
    const testResponse1 = [new TestEntity(new Identifier(1234), undefined), new TestEntity(new Identifier(1337), false)]
    const testResponse2 = [new TestEntity(new Identifier(1337), true), new TestEntity(new Identifier(9999), undefined)]

    const fetchAll = vi.fn(() => {
      if (!fetched) {
        fetched = true
        return Promise.resolve(testResponse1)
      }

      return Promise.resolve(testResponse2)
    })

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: fetchAll,
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchAll()).resolves.toBeUndefined()
    expect(store.error).toBeUndefined()
    expect(Array.from(store.entities)).toEqual(testResponse1)

    await expect(store.fetchAll()).resolves.toBeUndefined()
    expect(store.error).toBeUndefined()
    expect(Array.from(store.entities)).toContainEqual(new TestEntity(new Identifier(1337), true))
    expect(Array.from(store.entities)).toContainEqual(new TestEntity(new Identifier(9999), undefined))
    expect(Array.from(store.entities)).not.toContainEqual(new TestEntity(new Identifier(1337), false))
    expect(Array.from(store.entities)).toHaveLength(3)
  })

  it('does handle fetchAll parameters appropriately', async () => {
    const fetchAll = vi.fn((x) => Promise.resolve(x))
    const e1337 = new TestEntity(new Identifier(1337), undefined)
    const e1234 = new TestEntity(new Identifier(1234), undefined)

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: fetchAll,
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()

    await expect(store.fetchAll([e1337])).resolves.toBeUndefined()
    expect(Array.from(store.entities)).toEqual([e1337])

    await expect(store.fetchAll([e1234])).resolves.toBeUndefined()
    expect(Array.from(store.entities)).toEqual([e1337, e1234])
  })

  it('does swallow errors and expose state separately', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any -- We don't care about the type here
    const deferred = createDeferred<any[]>()

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => deferred.promise),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    const resPromise = store.fetchAll()

    expect(store.isLoading).toBe(true)
    deferred.reject(new Error())

    await expect(resPromise).resolves.toBeUndefined()

    expect(store.isLoading).toBe(false)
    expect(store.error).not.toBeUndefined()
  })
})

describe('fetchOne', () => {
  it('puts an entity in store', async () => {
    const testResponse = new TestEntity(new Identifier(1337), undefined)
    const fetchOne = vi.fn(() => Promise.resolve(testResponse))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: fetchOne,
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchOne(1337)).resolves.toBeUndefined()

    expect(fetchOne).toHaveBeenCalledTimes(1)
    expect(fetchOne).toHaveBeenCalledWith(1337)
    expect(Array.from(store.entities)).toContainEqual(testResponse)
  })

  it('does not put duplicate entries in store, but updates them', async () => {
    let test = true
    const fetchOne = vi.fn(() => Promise.resolve(new TestEntity(new Identifier(1337), (test = !test))))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: fetchOne,
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchOne(1337)).resolves.toBeUndefined()
    expect(Array.from(store.entities)).toHaveLength(1)
    expect(Array.from(store.entities)).toContainEqual(new TestEntity(new Identifier(1337), false))

    await expect(store.fetchOne(1337)).resolves.toBeUndefined()
    expect(Array.from(store.entities)).toContainEqual(new TestEntity(new Identifier(1337), true))
    expect(Array.from(store.entities)).toHaveLength(1)
  })

  it('does not swallow errors', async () => {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(() => {
        throw new Error('oopsie woopsie')
        // noinspection UnreachableCodeJS
        return Promise.resolve(whatever())
      }),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchOne(1337)).rejects.toThrowError()
  })

  it('handles extra parameters', async () => {
    const testResponse = new TestEntity(new Identifier(1337), undefined)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars -- Needed for type inference
    const fetchOne = vi.fn((_x: string, _y: number, _id: Id) => Promise.resolve(testResponse))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: fetchOne,
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.fetchOne('uwu', 10, 1337)).resolves.toBeUndefined()

    expect(fetchOne).toHaveBeenCalledTimes(1)
    expect(fetchOne).toHaveBeenCalledWith('uwu', 10, 1337)
    expect(Array.from(store.entities)).toContainEqual(testResponse)
  })
})

describe('create', () => {
  it('calls the API and puts the result in store', async () => {
    const testData = { a: true }
    const testResponse = new TestEntity(new Identifier(1337), true)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars -- Needed for type inference
    const create = vi.fn((_: typeof testData) => Promise.resolve(testResponse))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: create,
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.create(testData)).resolves.toBeUndefined()

    expect(create).toHaveBeenCalledTimes(1)
    expect(create).toHaveBeenCalledWith(testData)
    expect(Array.from(store.entities)).toContainEqual(testResponse)
  })

  it('does not swallow errors', async () => {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn(() => {
        throw new Error('oopsie woopsie')
        // noinspection UnreachableCodeJS
        return Promise.resolve(whatever())
      }),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.create({})).rejects.toThrowError()
  })

  it('handles extra parameters', async () => {
    const testData = { a: true }
    const testResponse = new TestEntity(new Identifier(1337), true)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const create = vi.fn((_x: string, _y: number, _: typeof testData) => Promise.resolve(testResponse))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: create,
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.create('uwu', 10, testData)).resolves.toBeUndefined()

    expect(create).toHaveBeenCalledTimes(1)
    expect(create).toHaveBeenCalledWith('uwu', 10, testData)
    expect(Array.from(store.entities)).toContainEqual(testResponse)
  })
})

describe('update', () => {
  it('calls the API and updates the entity in store', async () => {
    const entity = new TestEntity(new Identifier(1337), false)
    const entityUpdated = new TestEntity(new Identifier(1337), true)

    const update = vi.fn(() => Promise.resolve(entityUpdated))
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([entity])),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: update,
      remove: vi.fn(),
    })

    const store = useStore()
    await store.fetchAll()

    expect(Array.from(store.entities)[0]).toEqual(entity)

    const data = { uwu: true }
    await expect(store.update(1337, data)).resolves.toBeUndefined()

    expect(update).toHaveBeenCalledWith(1337, data)
    expect(Array.from(store.entities)[0]).toEqual(entityUpdated)
  })

  it('does not call the API if entity is not in store', async () => {
    const update = vi.fn()
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: update,
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.update(1337, {})).rejects.toThrowError()
    expect(update).not.toHaveBeenCalled()
  })

  it('does not swallow errors', async () => {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(() => {
        throw new Error('oopsie woopsie')
        // noinspection UnreachableCodeJS
        return Promise.resolve(whatever())
      }),
      remove: vi.fn(),
    })

    const store = useStore()
    await expect(store.update(1337, {})).rejects.toThrowError()
  })

  it('handles extra parameters', async () => {
    const testData = { uwu: true }
    const entity = new TestEntity(new Identifier(1337), false)
    const entityUpdated = new TestEntity(new Identifier(1337), true)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars -- Needed for type inference
    const update = vi.fn((_x: string, _y: number, _id: Id, _: typeof testData) => Promise.resolve(entityUpdated))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([entity])),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: update,
      remove: vi.fn(),
    })

    const store = useStore()
    await store.fetchAll()
    expect(Array.from(store.entities)[0]).toEqual(entity)

    await expect(store.update('uwu', 10, 1337, testData)).resolves.toBeUndefined()

    expect(update).toHaveBeenCalledTimes(1)
    expect(update).toHaveBeenCalledWith('uwu', 10, 1337, testData)
    expect(Array.from(store.entities)[0]).toEqual(entityUpdated)
  })
})

describe('delete', () => {
  it('deletes from the store', async () => {
    const remove = vi.fn()
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([new TestEntity(new Identifier(1337), true)])),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: remove,
    })

    const store = useStore()
    await store.fetchAll()
    await store.remove(1337)

    expect(remove).toHaveBeenCalledTimes(1)
    expect(remove).toHaveBeenCalledWith(1337)
    expect(Array.from(store.entities)).toHaveLength(0)
  })

  it('does not call the API if entity is not in store', async () => {
    const remove = vi.fn()
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: remove,
    })

    const store = useStore()
    await expect(store.remove(1337)).rejects.toThrowError()
    expect(remove).not.toHaveBeenCalled()
  })

  it('does not swallow errors', async () => {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: vi.fn(() => {
        throw new Error('oopsie woopsie')
        // noinspection UnreachableCodeJS
        return Promise.resolve()
      }),
    })

    const store = useStore()
    await expect(store.remove(1337)).rejects.toThrowError()
  })

  it('handles extra parameters', async () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars -- Needed for type inference
    const remove = vi.fn((_x: string, _y: number, _id: Id) => Promise.resolve(null))

    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([new TestEntity(new Identifier(1337), true)])),
      fetchOne: vi.fn(),
      create: vi.fn(),
      update: vi.fn(),
      remove: remove,
    })

    const store = useStore()
    await store.fetchAll()
    await store.remove('uwu', 10, 1337)

    expect(remove).toHaveBeenCalledTimes(1)
    expect(remove).toHaveBeenCalledWith('uwu', 10, 1337)
    expect(Array.from(store.entities)).toHaveLength(0)
  })
})

describe('Deferred mode', () => {
  // We're closer to integration tests than unit tests here
  // But semantics is a concept invented so people can schedule pointless meetings instead of doing actual work ;)

  it('pushes to undo/redo operations that are performed', async () => {
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: vi.fn(() => Promise.resolve()),
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()

    expect(Array.from(store.entities)).toHaveLength(0)

    const id = Identifier.localIdentifier()
    await store.create(new TestEntity(id, undefined))
    expect(Array.from(store.entities)).toHaveLength(1)
    expect(Array.from(store.entities)[0].uwu).toBeFalsy()

    await store.update(id, { uwu: true })
    expect(Array.from(store.entities)[0].uwu).toBeTruthy()

    await store.remove(id)
    expect(Array.from(store.entities)).toHaveLength(0)

    expect(undoredo.actions).toHaveLength(4)
    expect(undoredo.actions[1].type).toBe(ActionType.CREATE)
    expect(undoredo.actions[2].type).toBe(ActionType.UPDATE)
    expect(undoredo.actions[3].type).toBe(ActionType.DELETE)
  })

  it('properly handles rollback requests from undo/redo', async () => {
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: vi.fn(() => Promise.resolve()),
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()

    const id = Identifier.localIdentifier()
    await store.create(new TestEntity(id, undefined))
    await store.update(id, { uwu: true })
    await store.remove(id)

    expect(Array.from(store.entities)).toHaveLength(0)

    undoredo.undo()
    expect(Array.from(store.entities)).toHaveLength(1)
    expect(Array.from(store.entities)[0].uwu).toBeTruthy()

    undoredo.undo()
    expect(Array.from(store.entities)[0].uwu).toBeFalsy()

    undoredo.undo()
    expect(Array.from(store.entities)).toHaveLength(0)
  })

  it('passes pending operations to the committer', async () => {
    const committer = vi.fn(() => Promise.resolve())
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: committer,
    })

    const store = useStore()
    await store.fetchOne(1)
    await store.fetchOne(2)
    await store.fetchOne(3)

    await store.create(new TestEntity(new Identifier(4), false))
    await store.update(2, { uwu: true })
    await store.remove(3)

    expect(committer).not.toHaveBeenCalled()
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(1)
    expect(committer).toHaveBeenCalledWith([
      {
        type: OperationType.CREATE,
        entity: new TestEntity(new Identifier(4), false),
      },
      {
        type: OperationType.UPDATE,
        prev: new TestEntity(new Identifier(2), undefined),
        next: new TestEntity(new Identifier(2), true),
      },
      {
        type: OperationType.DELETE,
        entity: new TestEntity(new Identifier(3), undefined),
      },
    ])
  })

  it('does not pass undone operations to the committer', async () => {
    const committer = vi.fn(() => Promise.resolve())
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: committer,
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()
    await store.fetchOne(1)
    await store.fetchOne(2)
    await store.fetchOne(3)

    await store.create(new TestEntity(new Identifier(4), false))
    await store.update(2, { uwu: true })
    await store.remove(3)

    undoredo.undo()

    expect(committer).not.toHaveBeenCalled()
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(1)
    expect(committer).toHaveBeenCalledWith([
      {
        type: OperationType.CREATE,
        entity: new TestEntity(new Identifier(4), false),
      },
      {
        type: OperationType.UPDATE,
        prev: new TestEntity(new Identifier(2), undefined),
        next: new TestEntity(new Identifier(2), true),
      },
    ])
  })

  it('does not pass a already committed operations to the committer', async () => {
    const committer = vi.fn(() => Promise.resolve())
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: committer,
    })

    const store = useStore()
    await store.create(new TestEntity(new Identifier(1), false))
    await store.commit()

    await store.create(new TestEntity(new Identifier(2), false))
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(2)
    expect(committer).toHaveBeenNthCalledWith(1, [
      {
        type: OperationType.CREATE,
        entity: new TestEntity(new Identifier(1), false),
      },
    ])
    expect(committer).toHaveBeenNthCalledWith(2, [
      {
        type: OperationType.CREATE,
        entity: new TestEntity(new Identifier(2), false),
      },
    ])
  })

  it('handles rollbacks of already committed data', async () => {
    const committer = vi.fn(() => Promise.resolve())
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: committer,
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()
    await store.create(new TestEntity(new Identifier(1), false))
    await store.commit()

    undoredo.goto(undoredo.actions[0])
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(2)
    expect(committer).toHaveBeenNthCalledWith(1, [
      {
        type: OperationType.CREATE,
        entity: new TestEntity(new Identifier(1), false),
      },
    ])
    expect(committer).toHaveBeenNthCalledWith(2, [
      {
        type: OperationType.DELETE,
        entity: new TestEntity(new Identifier(1), false),
      },
    ])
  })

  it('handles undoing reverts', async () => {
    const committer = vi.fn(() => Promise.resolve())
    const useStore = defineCrudStore({
      deferred: true,
      name: 'test!',
      fetchAll: vi.fn(() => Promise.resolve([])),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      commit: committer,
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()
    await store.create(new TestEntity(new Identifier(1), false))
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(1)

    undoredo.goto(undoredo.actions[0])
    undoredo.undo()
    await store.commit()

    expect(committer).toHaveBeenCalledTimes(1)
  })

  it('does not push actions to undo/redo when operating in immediate mode', async () => {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(),
      fetchOne: vi.fn(),
      create: vi.fn((a: boolean) => Promise.resolve(new TestEntity(Identifier.localIdentifier(), a))),
      update: vi.fn(),
      remove: vi.fn(),
    })

    const store = useStore()
    const undoredo = useUndoRedoStore()

    expect(undoredo.actions).toHaveLength(1)
    await store.create(true)
    expect(undoredo.actions).toHaveLength(1)
  })
})

describe('Reactivity', () => {
  function createReactivityTest() {
    const useStore = defineCrudStore({
      name: 'test!',
      fetchAll: vi.fn(() =>
        Promise.resolve([new TestEntity(new Identifier(1), true), new TestEntity(new Identifier(2), false)])
      ),
      fetchOne: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), true))),
      create: vi.fn((id: number) => Promise.resolve(new TestEntity(new Identifier(id), undefined))),
      update: vi.fn((id: number, a: boolean) => Promise.resolve(new TestEntity(new Identifier(id), a))),
      remove: vi.fn(),
    })

    const watcherA = vi.fn()
    const watcherB = vi.fn()
    const watcherC = vi.fn()

    const [[store, entityA, entityB, entityC]] = withVueSetup(() => {
      const store = useStore()
      const entityA = store.getEntityById(1)
      const entityB = store.getEntityById(2)
      const entityC = store.getEntityById(999)

      watch(entityA, watcherA)
      watch(entityB, watcherB)
      watch(entityC, watcherC)

      return [store, entityA, entityB, entityC] as const
    })

    expect(entityA.value).toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(0)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)

    return [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] as const
  }

  it('should cause a reactive update whenever entities are fetched', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    await store.fetchAll()

    expect(entityA.value).not.toBeUndefined()
    expect(entityB.value).not.toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(1)
    expect(watcherC).toHaveBeenCalledTimes(0)
  })

  it('should cause a reactive update whenever an entity is fetched', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    await store.fetchOne(1)

    expect(entityA.value).not.toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)
  })

  it('should cause a reactive update whenever an entity is created', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    await store.create(2)

    expect(entityA.value).toBeUndefined()
    expect(entityB.value).not.toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(0)
    expect(watcherB).toHaveBeenCalledTimes(1)
    expect(watcherC).toHaveBeenCalledTimes(0)
  })

  it('should cause a reactive update whenever an entity is updated', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    // Fetch one
    await store.fetchOne(1)

    expect(entityA.value).not.toBeUndefined()
    expect(entityA.value!.uwu).toBe(true)
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)

    // Update
    await store.update(1, false)

    expect(entityA.value).not.toBeUndefined()
    expect(entityA.value!.uwu).toBe(false)
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(2)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)
  })

  it('should cause a reactive update whenever an entity is deleted', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    // Fetch one
    await store.fetchOne(1)

    expect(entityA.value).not.toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)

    // Delete
    await store.remove(1)

    expect(entityA.value).toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(2)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)
  })

  it('should cause a reactive update whenever an entity has its ID updated', async () => {
    const [store, [entityA, entityB, entityC], [watcherA, watcherB, watcherC]] = createReactivityTest()

    // Fetch one
    await store.fetchOne(1)

    expect(entityA.value).not.toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(0)

    // Change the ID
    entityA.value!.id.ref.value = 999
    await nextTick()

    expect(entityA.value).not.toBeUndefined()
    expect(entityB.value).toBeUndefined()
    expect(entityC.value).not.toBeUndefined()

    expect(watcherA).toHaveBeenCalledTimes(1)
    expect(watcherB).toHaveBeenCalledTimes(0)
    expect(watcherC).toHaveBeenCalledTimes(1)
  })
})
