import { api } from '@/utils/api'
import { StartTime } from '@/ts/type'

import { computed, ref, watch } from 'vue'
import { type NavigationGuardNext, type RouteLocationNormalized, useRoute } from 'vue-router'
import { RouteNames } from '@/router/routes.ts'

import {
  createDepartment,
  deleteDepartment,
  getAllDepartments,
  getDepartment,
  updateDepartment,
} from '@/ts/api/departments.ts'
import { defineCrudStore } from './defineCrudStore.ts'

export const useDepartmentsStore = defineCrudStore({
  name: 'departments',
  fetchAll: getAllDepartments,
  fetchOne: getDepartment,
  create: createDepartment,
  update: updateDepartment,
  remove: deleteDepartment,
  setup({ entities }) {
    const route = useRoute()
    const currentDepartmentAbbrev = computed(() => route.params.dept as string)
    const isCurrentDepartmentSelected = computed(() => !!currentDepartmentAbbrev.value)

    const current = computed(
      // TYPE SAFETY: This expression is asserted, which weakens type safety.
      // This is done for convenience, and most likely safe since in practice it should never be undefined wherever used.
      // The navigation will wait for the store to have loaded all the available departments and will reject the
      // navigation to a nonexistent department. The only other case is if this is used in a non-department route, which
      // is not something that makes sense (so undefined behavior is expected).
      () => entities.find((dept) => dept.abbrev === currentDepartmentAbbrev.value)!
    )

    // TODO: move elsewhere
    //region legacy start times fetch
    const startTimes = ref<StartTime[]>([])
    watch(current, async (dept) => {
      if (dept) startTimes.value = await api.getStartTimes(dept.id.valueOf())
    })
    //endregion

    return {
      current,
      currentDepartmentAbbrev,
      isCurrentDepartmentSelected,
      startTimes,
    }
  },
})

// https://router.vuejs.org/guide/advanced/navigation-guards.html#Per-Route-Guard
export function handleRouteDepartment(
  to: RouteLocationNormalized,
  _from: RouteLocationNormalized,
  next: NavigationGuardNext
) {
  const departments = useDepartmentsStore()
  if (!departments.entities.find((dept) => dept.abbrev === to.params.dept)) {
    // Department does not exist, go back home
    return next({ name: RouteNames.HOME })
  }

  return next()
}
