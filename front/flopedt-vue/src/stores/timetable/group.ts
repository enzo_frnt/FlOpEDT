import { acceptHMRUpdate, defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { remove } from 'lodash-es'

import { type Group, StructuralGroup, TransversalGroup } from '@/ts/entities/group.ts'
import { useStructuralGroupsStore } from '@/stores/departments/groups/structuralGroups.ts'
import { useTransversalGroupsStore } from '@/stores/departments/groups/transversalGroups.ts'
import { useDepartmentsStore } from '@/stores/departments.ts'

/**
 * TODO: replace everywhere with the "proper" group store
 *
 * This store is a work in progress,
 * related to the ScheduleView for beginning.
 *
 * This store is not related to the scheduledCourse
 */
export const useGroupStore = defineStore('group', () => {
  const departmentsStore = useDepartmentsStore()
  const newStGroupStore = useStructuralGroupsStore()
  const newTrGroupStore = useTransversalGroupsStore()
  const { descendantsMap } = newStGroupStore.getDepartmentGroups(() => departmentsStore.current.id.valueOf())

  const fetchedStructuralGroups = computed(() => [...newStGroupStore.entities])
  const fetchedTransversalGroups = computed(() => [...newTrGroupStore.entities])

  const selectedTransversalGroups = ref<TransversalGroup[]>([])
  const groupsSelected = ref<StructuralGroup[]>([])

  const groups = computed(() => {
    return [
      ...(groupsSelected.value.length ? groupsSelected.value : fetchedStructuralGroups.value),
      ...(selectedTransversalGroups.value.length ? selectedTransversalGroups.value : fetchedTransversalGroups.value),
    ]
  })

  function clearSelected(): void {
    selectedTransversalGroups.value = []
  }

  // Recursive function to collect all descendant leaf node IDs
  function collectDescendantLeafNodeIds(groupId: number): number[] {
    // Very suboptimal but it's just so old code doesn't break before it gets properly replaced
    const set = descendantsMap.value.get(groupId)
    const all = set ? [groupId, ...set] : [groupId]
    return all.filter((g) => !descendantsMap.value.get(g)?.size)
  }

  function addTransversalGroupToSelection(group: Group): void {
    if (!(group instanceof TransversalGroup)) return
    selectedTransversalGroups.value.push(group)
  }

  function removeTransversalGroupToSelection(group: Group): void {
    if (!(group instanceof TransversalGroup)) return
    remove(selectedTransversalGroups.value, (g) => group.id.equals(g.id))
  }

  return {
    groups,
    fetchedStructuralGroups,
    fetchedTransversalGroups,
    addTransversalGroupToSelection,
    removeTransversalGroupToSelection,
    clearSelected,
    groupsSelected,
    collectDescendantLeafNodeIds,
  }
})

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useGroupStore, import.meta.hot))
}
