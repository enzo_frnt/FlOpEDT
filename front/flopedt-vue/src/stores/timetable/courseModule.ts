import { ModuleAPI } from '@/ts/type'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { ref } from 'vue'

import { api } from '@/utils/api'

export const useCourseModuleStore = defineStore('courseModule', () => {
  const isLoading = ref(false)
  const error = ref<Error>()
  const courseModules = ref<ModuleAPI[]>([])

  async function fetchAllCourseModules(dept: string) {
    isLoading.value = true
    courseModules.value = []
    error.value = void 0

    try {
      courseModules.value = await api.fetch.courseModules({ department: dept })
    } catch (e) {
      if (!(e instanceof Error)) {
        error.value = new Error('Unrecognized error: ' + e?.toString())
      } else {
        error.value = e
      }
    } finally {
      isLoading.value = false
    }
  }

  return {
    isLoading,
    error,
    courseModules,
    fetchAllCourseModules,
  }
})

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useCourseModuleStore, import.meta.hot))
}
