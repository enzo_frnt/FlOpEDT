import { ref } from 'vue'
import { api } from '@/utils/api'
import { defineStore } from 'pinia'
import { Department, TrainingPeriod } from '@/ts/type'

export const useTrainingPeriodStore = defineStore('trainingPeriod', () => {
  const trainingPeriods = ref<TrainingPeriod[]>([])
  const error = ref<Error>()
  const isAllTrainingPeriodsFetched = ref(false)

  const isLoading = ref<boolean>(false)

  async function fetchTrainingPeriods(department?: Department) {
    isLoading.value = true
    trainingPeriods.value = []
    error.value = void 0

    try {
      trainingPeriods.value = await api.getTrainingPeriods(department?.id)
    } catch (e) {
      if (!(e instanceof Error)) {
        error.value = new Error('Unrecognized error: ' + e?.toString())
      } else {
        error.value = e
      }
    } finally {
      isLoading.value = false
    }
  }

  function clearTrainingPeriods(): void {
    trainingPeriods.value = []
    isAllTrainingPeriodsFetched.value = false
  }

  return {
    trainingPeriods,
    isAllTrainingPeriodsFetched,
    fetchTrainingPeriods,
    clearTrainingPeriods,
  }
})
