import { acceptHMRUpdate, defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { Course } from '@/ts/type'

export const useSelectedCourseStore = defineStore('selectedCourse', () => {
  const selectedCourses = ref<Course[]>([])
  const isModifying = ref(false)

  function isUniqueById(prop: 'module' | 'type' | 'tutor' | 'period') {
    return computed(
      () =>
        selectedCourses.value.length <= 1 ||
        selectedCourses.value.every((c) => selectedCourses.value[0][prop]?.id === c[prop]?.id)
    )
  }

  function isUniqueByArrayId(prop: 'supp_tutors' | 'groups') {
    return computed(
      () =>
        selectedCourses.value.length <= 1 ||
        selectedCourses.value.every(
          (c) =>
            selectedCourses.value[0][prop].length === c[prop].length &&
            selectedCourses.value[0][prop].every(({ id }) => c[prop].find((el) => el.id === id))
        )
    )
  }

  function isUniqueByValue(prop: 'duration') {
    return computed(
      () =>
        selectedCourses.value.length <= 1 ||
        selectedCourses.value.every((c) => selectedCourses.value[0][prop] === c[prop])
    )
  }

  return {
    /** The list of currently selected courses */
    selectedCourses,
    /** Whether we are currently modifying the selected courses */
    isModifying,
    /** Whether a property is the same across all selected entities or not */
    unique: {
      module: isUniqueById('module'),
      type: isUniqueById('type'),
      tutor: isUniqueById('tutor'),
      suppTutors: isUniqueByArrayId('supp_tutors'),
      groups: isUniqueByArrayId('groups'),
      period: isUniqueById('period'),
      duration: isUniqueByValue('duration'),
    },
  }
})

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useSelectedCourseStore, import.meta.hot))
}
