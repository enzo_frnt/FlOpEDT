import { defineStore } from 'pinia'
import { ref } from 'vue'
import { Dependency, DependencyCreateData, DependencyUpdateData } from '@/ts/type'
import { api } from '@/utils/api'

export const useDependencyStore = defineStore('dependency', () => {
  const dependencies = ref<Array<Dependency>>([])
  const isLoading = ref<boolean>(false)
  const isDependencyFetched = ref<boolean>(false)
  const loadingError = ref<Error | null>(null)

  async function fetchDependencies(): Promise<void> {
    isLoading.value = true
    try {
      await api.getDependencies().then((result) => {
        result.forEach((d: Dependency) => {
          dependencies.value.push(d)
        })
        isLoading.value = false
        isDependencyFetched.value = true
      })
    } catch (e) {
      loadingError.value = e as Error
    }
  }

  async function updateDependency(dependencyUpdateData: DependencyUpdateData): Promise<Dependency> {
    const actualDependencyIndex = dependencies.value.findIndex((element) => element.id === dependencyUpdateData.id)
    if (actualDependencyIndex === -1) throw new Error('Dependency not found')

    const updatedDependency = await api.patch.patchDependency(dependencyUpdateData)
    Object.assign(dependencies.value[actualDependencyIndex], updatedDependency)
    return updatedDependency
  }

  async function postDependency(dependency: DependencyCreateData): Promise<Dependency> {
    const createdDependency = await api.post.postDependency(dependency)
    dependencies.value.push(createdDependency)
    return createdDependency
  }

  async function deleteDependency(dependencyToDelete: Dependency): Promise<void> {
    await api.delete.deleteDependency(dependencyToDelete.id)
    const index = dependencies.value.indexOf(dependencyToDelete, 0)
    if (index > -1) {
      dependencies.value.splice(index, 1)
    }
  }

  return { fetchDependencies, updateDependency, postDependency, deleteDependency, dependencies, isDependencyFetched }
})
