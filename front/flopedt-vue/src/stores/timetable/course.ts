import { Course, CourseCreateData, CourseUpdateData } from '@/ts/type'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { ref } from 'vue'

import { api } from '@/utils/api'

export const useCourseStore = defineStore('courses', () => {
  const isLoading = ref(false)
  const error = ref<Error>()
  const courses = ref<Course[]>([])

  async function fetchAllCourses(dept: string) {
    isLoading.value = true
    courses.value = []
    error.value = void 0

    try {
      courses.value = await api.fetch.courses({ department: dept })
    } catch (e) {
      if (!(e instanceof Error)) {
        error.value = new Error('Unrecognized error: ' + e?.toString())
      } else {
        error.value = e
      }
    } finally {
      isLoading.value = false
    }
  }

  async function updateCourse(courseUpdateData: CourseUpdateData): Promise<Course> {
    const actualCourseIndex = courses.value.findIndex((element) => element.id === courseUpdateData.id)
    if (actualCourseIndex === -1) throw new Error('Course not found')

    const updatedCourse = await api.patch.patchCourse(courseUpdateData)
    Object.assign(courses.value[actualCourseIndex], updatedCourse)
    return updatedCourse
  }

  async function postCourse(courseData: CourseCreateData): Promise<Course> {
    const createdCourse = await api.post.postCourse(courseData)
    courses.value.push(createdCourse)
    return createdCourse
  }

  async function deleteCourse(courseToDelete: Course): Promise<void> {
    await api.delete.deleteCourse(courseToDelete.id)
    const index = courses.value.indexOf(courseToDelete, 0)
    if (index > -1) {
      courses.value.splice(index, 1)
    }
  }

  return {
    isLoading,
    error,
    courses,
    fetchAllCourses,
    updateCourse,
    postCourse,
    deleteCourse,
  }
})

if (import.meta.hot) {
  // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
  import.meta.hot.accept(acceptHMRUpdate(useCourseStore, import.meta.hot))
}
