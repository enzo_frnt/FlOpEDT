import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useAppDataStore } from '@/stores/appdata'
import { Preferences } from '@/ts/entities/preferences'

// TODO: add more things to this lonely store..!
export const usePrefsStore = defineStore('prefs', () => {
  const appdata = useAppDataStore()

  const loaded = computed(() => appdata.loaded)
  const preferences = ref<Preferences | null>(null)

  return {
    loaded,
    preferences,
  }
})
