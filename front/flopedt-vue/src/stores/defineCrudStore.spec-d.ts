// Oh my, you know you're having a good time when you bring out tests for your typings

import { describe, expectTypeOf, it } from 'vitest'

import { ComputedRef, Ref, ref } from 'vue'
import { ReadonlyEntitySet } from '@/ts/entities/base/entityset.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

import { defineCrudStore } from '@/stores/defineCrudStore.ts'

type Id = Identifier | number

/* eslint-disable */
function makeFetchAllTest<T>(): () => Promise<T[]> { return null as any }
function makeFetchAllTestWithProps<T, U>(): (props: U) => Promise<T[]> { return null as any }
function makeFetchOneTest<T>(): (id: number) => Promise<T> { return null as any }
function makeCreateTest<T, U>(): (data: U) => Promise<T> { return null as any }
function makeUpdateTest<T, U>(): (id: number, data: U) => Promise<T> { return null as any }
function makeRemoveTest(): (id: number) => Promise<void> { return null as any }

function makeFetchOneWithArgsTest<T>(): (arg1: string, arg2: number, id: number) => Promise<T> { return null as any }
function makeCreateWithArgsTest<T, U>(): (arg1: string, arg2: number, data: U) => Promise<T> { return null as any }
function makeUpdateWithArgsTest<T, U>(): (arg1: string, arg2: number, id: number, data: U) => Promise<T> { return null as any }
function makeRemoveWithArgsTest(): (arg1: string, arg2: number, id: number) => Promise<void> { return null as any }
/* eslint-enable */

it('allows defining a store', () => {
  abstract class TestEntity extends Entity {}
  type TestEntityCreateData = { _create: true }
  type TestEntityUpdateData = { _update: true }

  const useTestStore = defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })

  const test = useTestStore()
  expectTypeOf(test.fetchOne).parameters.toEqualTypeOf<[Id]>()
  expectTypeOf(test.create).parameters.toEqualTypeOf<[TestEntityCreateData]>()
  expectTypeOf(test.update).parameters.toEqualTypeOf<[Id, TestEntityUpdateData]>()
  expectTypeOf(test.remove).parameters.toEqualTypeOf<[Id]>()
})

it('has correct entity type inferred from the functions it received', () => {
  abstract class TestEntity extends Entity {}
  type TestEntityCreateData = { _create: true }
  type TestEntityUpdateData = { _update: true }

  const useTestStore = defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })

  const test = useTestStore()
  expectTypeOf(test.$id).toMatchTypeOf<'testStore'>()
  expectTypeOf(test.entities).toMatchTypeOf<ReadonlyEntitySet<TestEntity>>()
  expectTypeOf(test.entities.get(0)!.id.ref).toMatchTypeOf<Ref<number>>()

  expectTypeOf(test.getEntityById).returns.toMatchTypeOf<ComputedRef<TestEntity | undefined>>()
  expectTypeOf(test.getEntitiesByIds).returns.toMatchTypeOf<ComputedRef<TestEntity[]>>()
})

it('has correct fetchAll arguments inferred from the functions it received', () => {
  abstract class TestEntity extends Entity {}
  type TestEntityFetchParams = { meow: true }
  type TestEntityCreateData = { _create: true }
  type TestEntityUpdateData = { _update: true }

  const useTestStore = defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })

  const useTestStoreWithProps = defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTestWithProps<TestEntity, TestEntityFetchParams>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })

  const test = useTestStore()
  const testWithProps = useTestStoreWithProps()

  expectTypeOf(test.fetchAll).parameters.toEqualTypeOf<[]>()
  expectTypeOf(testWithProps.fetchAll).parameters.toEqualTypeOf<[TestEntityFetchParams]>()
})

it('causes a type error if it detects a mismatch in API functions', () => {
  abstract class TestEntity extends Entity {}
  type TestEntityOther = { id: number; uwu: false }
  type TestEntityCreateData = { _create: true }
  type TestEntityUpdateData = { _update: true }

  defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })

  defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneTest<TestEntity>(),
    // @ts-expect-error -- Expected for the purposes of this test
    create: makeCreateTest<TestEntityOther, TestEntityCreateData>(),
    update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveTest(),
  })
})

it('allows functions with extra params', () => {
  abstract class TestEntity extends Entity {}
  type TestEntityCreateData = { _create: true }
  type TestEntityUpdateData = { _update: true }

  const useTestStore = defineCrudStore({
    name: 'testStore',
    fetchAll: makeFetchAllTest<TestEntity>(),
    fetchOne: makeFetchOneWithArgsTest<TestEntity>(),
    create: makeCreateWithArgsTest<TestEntity, TestEntityCreateData>(),
    update: makeUpdateWithArgsTest<TestEntity, TestEntityUpdateData>(),
    remove: makeRemoveWithArgsTest(),
  })

  const test = useTestStore()
  expectTypeOf(test.fetchOne).parameters.toEqualTypeOf<[string, number, Id]>()
  expectTypeOf(test.create).parameters.toEqualTypeOf<[string, number, TestEntityCreateData]>()
  expectTypeOf(test.update).parameters.toEqualTypeOf<[string, number, Id, TestEntityUpdateData]>()
  expectTypeOf(test.remove).parameters.toEqualTypeOf<[string, number, Id]>()
})

describe('setup parameter', () => {
  it('receives inferred types to the setup function', () => {
    abstract class TestEntity extends Entity {}
    type TestEntityCreateData = { _create: true }
    type TestEntityUpdateData = { _update: true }

    defineCrudStore({
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      create: makeCreateTest<TestEntity, TestEntityCreateData>(),
      update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
      remove: makeRemoveTest(),
      setup: (ctx) => {
        expectTypeOf(ctx.entities).toMatchTypeOf<ReadonlyEntitySet<TestEntity>>()
        expectTypeOf(ctx.entities.get(0)!.id.ref).toMatchTypeOf<Ref<number>>()
        return {}
      },
    })
  })

  it('adds extra store state to the resulting store', () => {
    abstract class TestEntity extends Entity {}
    type TestEntityCreateData = { _create: true }
    type TestEntityUpdateData = { _update: true }

    const useTestStore = defineCrudStore({
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      create: makeCreateTest<TestEntity, TestEntityCreateData>(),
      update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
      remove: makeRemoveTest(),
      setup: () => {
        const test = ref<1337>()
        return { test }
      },
    })

    const test = useTestStore()
    expectTypeOf(test.test).toMatchTypeOf<1337 | undefined>()
  })
})

describe('deferred mode', () => {
  it('allows defining a deferred store', () => {
    abstract class TestEntity extends Entity {}

    defineCrudStore({
      deferred: true,
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      commit: () => Promise.resolve(),
    })
  })

  it('does not accept immediate mode arguments when using deferred mode', () => {
    abstract class TestEntity extends Entity {}
    type TestEntityCreateData = { _create: true }
    type TestEntityUpdateData = { _update: true }

    defineCrudStore({
      deferred: true,
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      commit: () => Promise.resolve(),
      // @ts-expect-error -- expected for the purposes of this test
      create: makeCreateTest<TestEntity, TestEntityCreateData>(),
    })

    defineCrudStore({
      deferred: true,
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      commit: () => Promise.resolve(),
      // @ts-expect-error -- expected for the purposes of this test
      update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
    })

    defineCrudStore({
      deferred: true,
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      commit: () => Promise.resolve(),
      // @ts-expect-error -- expected for the purposes of this test
      remove: makeRemoveTest(),
    })
  })

  it('does not accept deferred mode arguments in immediate mode', () => {
    abstract class TestEntity extends Entity {}
    type TestEntityCreateData = { _create: true }
    type TestEntityUpdateData = { _update: true }

    defineCrudStore({
      name: 'testStore',
      deferred: false,
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneTest<TestEntity>(),
      create: makeCreateTest<TestEntity, TestEntityCreateData>(),
      update: makeUpdateTest<TestEntity, TestEntityUpdateData>(),
      remove: makeRemoveTest(),
      // @ts-expect-error -- Expected for the purposes of this test
      commit: () => Promise.resolve(),
    })
  })

  it('allows fetcher with extra params if a compatible argument provider is configured', () => {
    abstract class TestEntity extends Entity {}

    defineCrudStore({
      deferred: true,
      name: 'testStore',
      fetchAll: makeFetchAllTest<TestEntity>(),
      fetchOne: makeFetchOneWithArgsTest<TestEntity>(),
      commit: () => Promise.resolve(),
    })
  })
})
