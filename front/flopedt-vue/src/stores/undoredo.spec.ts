import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'

import { ActionStatus, ActionType, useUndoRedoStore } from '@/stores/undoredo'

beforeEach(() => void setActivePinia(createPinia()))

it('executes actions passed to UndoRedo', () => {
  const undoredo = useUndoRedoStore()
  const commit = vi.fn()
  const rollback = vi.fn()

  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests',
    commit: commit,
    rollback: rollback,
  })

  expect(commit).toHaveBeenCalledTimes(1)
  expect(rollback).not.toHaveBeenCalled()
  expect(undoredo.actions).toHaveLength(2)
  expect(undoredo.canUndo).toBe(true)
  expect(undoredo.canRedo).toBe(false)
})

it('undoes and redoes actions (basic case)', () => {
  const undoredo = useUndoRedoStore()
  const commit = vi.fn()
  const rollback = vi.fn()

  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests',
    commit: commit,
    rollback: rollback,
  })

  expect(undoredo.actions).toHaveLength(2)
  expect(undoredo.canUndo).toBe(true)
  expect(undoredo.canRedo).toBe(false)
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[1].isLocalLatest).toBe(true)

  undoredo.undo()
  expect(undoredo.actions).toHaveLength(2)
  expect(commit).toHaveBeenCalledTimes(1)
  expect(rollback).toHaveBeenCalledTimes(1)
  expect(commit).toHaveBeenCalledWith(false)
  expect(rollback).toHaveBeenCalledWith(false)
  expect(undoredo.canUndo).toBe(false)
  expect(undoredo.canRedo).toBe(true)
  expect(undoredo.actions[1].status).toBe(ActionStatus.UNDONE)
  expect(undoredo.actions[1].isLocalLatest).toBe(false)

  undoredo.redo()
  expect(undoredo.actions).toHaveLength(2)
  expect(commit).toHaveBeenCalledTimes(2)
  expect(rollback).toHaveBeenCalledTimes(1)
  expect(undoredo.canUndo).toBe(true)
  expect(undoredo.canRedo).toBe(false)
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[1].isLocalLatest).toBe(true)
})

it('undoes and redoes actions (complex case)', () => {
  const undoredo = useUndoRedoStore()

  // 0 -> 10 -> 5 -> 13 -> 9
  let value = 0
  for (const val of [10, 5, 13, 9]) {
    const prev = value
    undoredo.track({
      type: ActionType.CREATE,
      source: 'tests',
      commit: () => (value = val),
      rollback: () => (value = prev),
    })
  }

  expect(undoredo.actions).toHaveLength(5)

  undoredo.undo()
  undoredo.undo()
  expect(value).toBe(5)

  undoredo.undo()
  expect(value).toBe(10)

  undoredo.redo()
  undoredo.redo()
  expect(value).toBe(13)

  undoredo.undo()
  expect(value).toBe(5)

  undoredo.redo()
  undoredo.redo()
  expect(value).toBe(9)

  // Check it behaves as no-op
  expect(undoredo.canRedo).toBe(false)
  undoredo.redo()
  expect(value).toBe(9)

  undoredo.undo()
  undoredo.undo()
  undoredo.undo()
  undoredo.undo()
  expect(value).toBe(0)

  // Check it behaves as no-op
  expect(undoredo.canUndo).toBe(false)
  undoredo.undo()
  expect(value).toBe(0)
})

it('removes undone operations when tracking a new action', () => {
  const undoredo = useUndoRedoStore()
  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests1',
    commit: () => {},
    rollback: () => {},
  })
  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests2',
    commit: () => {},
    rollback: () => {},
  })
  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests3',
    commit: () => {},
    rollback: () => {},
  })

  expect(undoredo.actions).toHaveLength(4)

  undoredo.undo()
  undoredo.undo()
  expect(undoredo.actions).toHaveLength(4)
  expect(undoredo.actions[1].source).toBe('tests1')
  expect(undoredo.actions[2].source).toBe('tests2')
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[2].status).toBe(ActionStatus.UNDONE)

  undoredo.track({
    type: ActionType.CREATE,
    source: 'tests4',
    commit: () => {},
    rollback: () => {},
  })
  expect(undoredo.actions).toHaveLength(3)
  expect(undoredo.actions[1].source).toBe('tests1')
  expect(undoredo.actions[2].source).toBe('tests4')
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[2].status).toBe(ActionStatus.PENDING)
})

it('jumps to previous versions using goto', () => {
  const undoredo = useUndoRedoStore()

  // 0 -> 10 -> 8 -> 13 -> 9
  let value = 0
  for (const val of [10, 8, 13, 9]) {
    const prev = value
    undoredo.track({
      type: ActionType.CREATE,
      source: 'tests',
      commit: () => (value = val),
      rollback: () => (value = prev),
    })
  }

  expect(undoredo.actions).toHaveLength(5)

  const ten = undoredo.actions[1]
  const thirteen = undoredo.actions[3]

  expect(value).toBe(9)

  undoredo.goto(ten)
  expect(value).toBe(10)
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[2].status).toBe(ActionStatus.UNDONE)
  expect(undoredo.actions[3].status).toBe(ActionStatus.UNDONE)
  expect(undoredo.actions[4].status).toBe(ActionStatus.UNDONE)
  expect(undoredo.actions[1].isLocalLatest).toBe(true)
  expect(undoredo.actions[3].isLocalLatest).toBe(false)
  expect(undoredo.actions[4].isLocalLatest).toBe(false)

  undoredo.goto(thirteen)
  expect(value).toBe(13)
  expect(undoredo.actions[1].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[2].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[3].status).toBe(ActionStatus.PENDING)
  expect(undoredo.actions[4].status).toBe(ActionStatus.UNDONE)
  expect(undoredo.actions[1].isLocalLatest).toBe(false)
  expect(undoredo.actions[3].isLocalLatest).toBe(true)
  expect(undoredo.actions[4].isLocalLatest).toBe(false)
})

it('does not allow going to an action that is no longer tracked', () => {
  const undoredo = useUndoRedoStore()
  function track() {
    undoredo.track({
      type: ActionType.CREATE,
      source: 'tests',
      commit: () => {},
      rollback: () => {},
    })
  }

  track()
  track()
  const target = undoredo.actions[2]
  undoredo.undo()
  track()
  track()

  expect(() => undoredo.goto(target)).toThrow()
})

describe('committed actions', () => {
  it('marks actions as committed properly', () => {
    const undoredo = useUndoRedoStore()
    const rollback = vi.fn()

    undoredo.track({
      type: ActionType.CREATE,
      source: 'tests',
      commit: () => {},
      rollback: rollback,
    })

    undoredo.markCommitted()

    expect(undoredo.canUndo).toBe(false)
    expect(undoredo.actions[1].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[1].isLocalLatest).toBe(true)
    expect(undoredo.actions[1].isRemoteLatest).toBe(true)
    expect(undoredo.actions[0].isLocalLatest).toBe(false)
    expect(undoredo.actions[0].isRemoteLatest).toBe(false)

    undoredo.undo()
    expect(rollback).not.toHaveBeenCalled()
    expect(undoredo.actions[1].status).toBe(ActionStatus.COMMITTED)

    undoredo.track({
      type: ActionType.CREATE,
      source: 'tests',
      commit: () => {},
      rollback: () => {},
    })

    expect(undoredo.canUndo).toBe(true)
    expect(undoredo.actions[1].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[2].status).toBe(ActionStatus.PENDING)
    expect(undoredo.actions[1].isLocalLatest).toBe(false)
    expect(undoredo.actions[1].isRemoteLatest).toBe(true)
    expect(undoredo.actions[2].isLocalLatest).toBe(true)
    expect(undoredo.actions[2].isRemoteLatest).toBe(false)
  })

  it('does not affect undone operations when committing', () => {
    const undoredo = useUndoRedoStore()

    for (let i = 0; i < 5; i++) {
      undoredo.track({
        type: ActionType.CREATE,
        source: 'tests',
        commit: () => {},
        rollback: () => {},
      })
    }

    undoredo.undo()
    undoredo.undo()
    undoredo.markCommitted()
    expect(undoredo.actions[0].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[1].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[2].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[3].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[4].status).toBe(ActionStatus.UNDONE)
    expect(undoredo.actions[5].status).toBe(ActionStatus.UNDONE)
    expect(undoredo.canUndo).toBe(false)
    expect(undoredo.canRedo).toBe(true)
  })

  it('creates a new revert action if trying to go to a previous state that has been committed', () => {
    const undoredo = useUndoRedoStore()
    const commit = vi.fn()
    const rollback = vi.fn()

    for (let i = 0; i < 5; i++) {
      undoredo.track({
        type: ActionType.CREATE,
        source: 'tests',
        commit: commit,
        rollback: rollback,
      })
    }

    undoredo.markCommitted()
    expect(undoredo.actions).toHaveLength(6)
    expect(rollback).not.toHaveBeenCalled()

    undoredo.goto(undoredo.actions[3])
    expect(undoredo.actions).toHaveLength(7)

    expect(undoredo.actions[6].type).toBe(ActionType.REVERT)
    expect(undoredo.actions[6].status).toBe(ActionStatus.PENDING)

    expect(undoredo.actions[3].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[4].status).toBe(ActionStatus.REVERTED)
    expect(undoredo.actions[5].status).toBe(ActionStatus.REVERTED)
    expect(rollback).toHaveBeenCalledTimes(2)
    expect(rollback).toHaveBeenCalledWith(true)
    expect(rollback).not.toHaveBeenCalledWith(false)
    commit.mockReset()

    undoredo.undo()
    expect(commit).toHaveBeenCalledTimes(2)
    expect(commit).toHaveBeenLastCalledWith(true)
    expect(commit).not.toHaveBeenLastCalledWith(false)
    expect(rollback).toHaveBeenCalledTimes(2)
    expect(undoredo.actions[4].status).toBe(ActionStatus.COMMITTED)
    expect(undoredo.actions[5].status).toBe(ActionStatus.COMMITTED)
  })
})
