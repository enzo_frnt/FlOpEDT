import { CalendarColumn } from '@/components/calendar/declaration'
import { defineStore, storeToRefs } from 'pinia'
import { computed } from 'vue'
import { useGroupStore } from '@/stores/timetable/group'
import { useGroupColumns } from '@/composables/useGroupColumns.ts'
import { useStructuralGroupsStore } from '@/stores/departments/groups/structuralGroups.ts'
import { useDepartmentsStore } from '@/stores/departments.ts'

/**
 * TODO: move to the composable instead.
 * TODO: move selected to a URL backed store.
 *
 * This store is a work in progress,
 * related to the ScheduleView for beginning.
 *
 * This store is not related to the scheduledCourse
 */
export const useColumnStore = defineStore('column', () => {
  const groupStore = useGroupStore()
  const { groups, fetchedTransversalGroups } = storeToRefs(groupStore)

  const department = useDepartmentsStore()
  const structuralGroupsStore = useStructuralGroupsStore()
  const { tree, descendantsMap } = structuralGroupsStore.getDepartmentGroups(department.current.id)
  const cols = useGroupColumns(tree, descendantsMap, fetchedTransversalGroups, groups)

  const columns = computed(() => {
    const columns = cols.value.columns.map((c) => ({ ...c, weight: 1 })) as CalendarColumn[]
    columns.push({ id: 999999999, name: 'Avail', weight: 1 })
    return columns
  })

  const groupsColumnMap = computed(() => {
    const map = new Map<number, number[]>()
    for (const [group, spans] of cols.value.groupsColumnMap) {
      const ids = []
      for (const span of spans) {
        for (let i = span.start; i < span.end; i++) {
          ids.push(columns.value[i].id)
        }
      }
      map.set(group, ids)
    }
    return map
  })

  return {
    columns,
    groupsColumnMap,
  }
})
