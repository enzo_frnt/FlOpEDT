// This file is quite complicated, as it has been written in a library-oriented manner with many advanced TS concepts.
// There are comments throughout the file to try making it more digestible, but it's still quite the journey.
// Make sure your TypeScript skills are sharpened beforehand! ;)
//
// There are a lot of generics involved, you can learn about them here:
// https://www.typescriptlang.org/docs/handbook/2/generics.html
//
// The reason so many generics are involved is to use type inference to our advantage.
// When `defineCrudStore` is called, TypeScript will be able to observe the type of the arguments. From this, it will
// be able to automagically determine specific types based on the input to the function, and from there these generics
// will be propagated everywhere since everything depends on them, and the result will be neatly typed.
//
// An example of how powerful this can be, is that just thanks to the functions it receives as handlers for fetching
// data etc., it can infer the type of entities the store will be handling, and you will have the correct types
// everywhere you use the store, and that without specifying the type anywhere in the CrudStore definition.
//
// Additionally, if you pass a function that creates objects of type A and a function that updates objects of type B,
// because the generic is used to define both of the accepted types for these, a type error will be emitted which
// helps prevent mistakes like that.
//
// Another concept used here is a more common concept of type discrimination. Discrimination allows narrowing a union
// type to just one of its members based on the value of a field. Value can be tested via an if check, or via a switch
// statement if there are many different values to look for.
//
// https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes-func.html#discriminated-unions
// https://www.typescriptlang.org/docs/handbook/2/narrowing.html

import type { Merge } from '@/utils/types'

import { isEqual } from 'lodash-es' // Cynthia: Not a fan of lodash, but if it's here we might as well use it >:)
import {
  type Store,
  type StateTree,
  type StoreActions,
  type StoreDefinition,
  type StoreGetters,
  type StoreState,
  acceptHMRUpdate,
  defineStore,
} from 'pinia'
import {
  type ComputedRef,
  type DeepReadonly,
  type MaybeRefOrGetter,
  type Ref,
  computed,
  readonly,
  ref,
  toValue,
} from 'vue'

import type { Entity, EntityData } from '@/ts/entities/base/entity.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'
import { ActionType, useUndoRedoStore } from '@/stores/undoredo.ts'
import { EntitySet, ReadonlyEntitySet } from '@/ts/entities/base/entityset.ts'

export enum OperationType {
  CREATE,
  UPDATE,
  DELETE,
}

type FnWithPreExtraArgs<T extends (...args: never[]) => unknown, TExtraArgs extends unknown[] = []> = T extends (
  ...args: infer TArgs
) => infer TReturn
  ? (...args: [...TExtraArgs, ...TArgs]) => TReturn
  : never

// Helper to split things apart while pleasing the type system.
/*@__INLINE__*/ function arrayPop<T extends unknown[], U>(array: [...T, U]): [T, U] {
  const popped = array.pop()

  // Force the cast - this isn't ideal, which is why it's isolated here where it's trivial to see it's correct.
  return [array, popped] as unknown as [T, U]
}

/**
 * A CRUD operation (minus the read part). Used by deferred stores to keep track of pending changes.
 */
export type CrudOperation<TEntity extends Entity> =
  | { type: OperationType.CREATE; entity: TEntity }
  | { type: OperationType.UPDATE; prev: TEntity; next: TEntity }
  | { type: OperationType.DELETE; entity: TEntity }

/**
 * Inverts a CRUD operation. Used when rolling back committed changes.
 * For example, a committed creation is converted to a deletion.
 *
 * @param operation The operation to invert.
 * @return the inverse operation.
 */
function invertOperation<TEntity extends Entity>(operation: CrudOperation<TEntity>): CrudOperation<TEntity> {
  switch (operation.type) {
    case OperationType.CREATE:
      return { type: OperationType.DELETE, entity: operation.entity }
    case OperationType.UPDATE:
      return { type: OperationType.UPDATE, prev: operation.next, next: operation.prev }
    case OperationType.DELETE:
      return { type: OperationType.CREATE, entity: operation.entity }
  }
}

/**
 * Base properties of a CrudStore, shared by both Immediate CrudStores and Deferred CrudStores.
 */
type CrudStorePropertiesBase<
  TName extends string,
  TEntity extends Entity,
  TFetchParams extends unknown[],
  TEntityCreateData,
  TEntityUpdateData,
  TSetupState extends StateTree,
  TExtraArgs extends unknown[],
> = {
  /** Name of the store. Used for identification purposes and should be unique. May be used in i18n keys. */
  name: TName

  /**
   * Called whenever the store needs to fetch multiple entities.
   * This function can accept arguments, and the store will require them for calling its own fetchAll.
   * Should be a function from @/ts/http/...
   */
  fetchAll: (...fetchParams: TFetchParams) => Promise<TEntity[]>

  /** Called whenever the store needs to fetch a singular entity. Should be a function from @/ts/http/... */
  fetchOne: FnWithPreExtraArgs<(id: number) => Promise<TEntity>, TExtraArgs>

  /**
   * Optional setup function to add extra properties to the state tree, or to override some.
   * Functions the same as setup functions in plain Pinia stores. Receives the base state tree of the CrudStore.
   */
  setup?: (
    state: CrudStoreStateTree<TEntity, TFetchParams, TEntityCreateData, TEntityUpdateData, TExtraArgs>
  ) => TSetupState
}

/**
 * Properties specific to an Immediate CrudStore.
 */
type CrudStorePropertiesImmediate<
  TEntity extends Entity,
  TEntityCreateData,
  TEntityUpdateData,
  TExtraArgs extends unknown[],
> = {
  /** Whether the store is deferred or not. */
  deferred?: false // Do not allow the boolean to be true, to allow discriminating the union.

  /** Called whenever an entity needs to be created. Should be a function from @/ts/http/... */
  create: FnWithPreExtraArgs<(entityData: TEntityCreateData) => Promise<TEntity>, TExtraArgs>

  /** Called whenever an entity needs to be updated. Should be a function from @/ts/http/... */
  update: FnWithPreExtraArgs<(id: number, entityData: TEntityUpdateData) => Promise<TEntity>, TExtraArgs>

  /** Called whenever an entity needs to be removed. Should be a function from @/ts/http/... */
  remove: FnWithPreExtraArgs<(id: number) => Promise<unknown>, TExtraArgs> // "unknown" because we don't really care what it returns
}

/**
 * Properties specific to a Deferred CrudStore.
 */
type CrudStorePropertiesDeferred<TEntity extends Entity> = {
  /** Whether the store is deferred or not. */
  deferred: true // Enforce the definition of deferred to true, to allow discriminating the union.

  /** Used to commit a list of changes to the API. */
  commit: (ops: CrudOperation<TEntity>[]) => Promise<unknown>
}

/**
 * Properties of a CrudStore.
 * @see {@link CrudStorePropertiesBase} for shared properties.
 * @see {@link CrudStorePropertiesImmediate} for properties applicable in Immediate mode.
 * @see {@link CrudStorePropertiesDeferred} for properties applicable in Deferred mode.
 */
export type CrudStoreProperties<
  TName extends string,
  TEntity extends Entity,
  TFetchParams extends unknown[],
  TEntityCreateData,
  TEntityUpdateData,
  TSetupState extends StateTree,
  TExtraArgs extends unknown[],
> = CrudStorePropertiesBase<
  TName,
  TEntity,
  TFetchParams,
  TEntityCreateData,
  TEntityUpdateData,
  TSetupState,
  TExtraArgs
> &
  (
    | CrudStorePropertiesImmediate<TEntity, TEntityCreateData, TEntityUpdateData, TExtraArgs>
    | CrudStorePropertiesDeferred<TEntity>
  )

/**
 * StateTree of a CrudStore. This is everything the CrudStore exposes (minus user-specified store setup).
 */
export type CrudStoreStateTree<
  TEntity extends Entity,
  TFetchParams extends unknown[],
  TEntityCreateData,
  TEntityUpdateData,
  TExtraArgs extends unknown[],
> = {
  /** All entities held by the store. It is a readonly view, use the appropriate actions to update the store. */
  entities: ReadonlyEntitySet<TEntity>
  /** Whether the store is currently loading data, and it is not yet ready to be consumed. Readonly. */
  isLoading: DeepReadonly<Ref<boolean>>
  /** The last error that occurred while fetching all entities, if any. Only relevant for fetchAll. Readonly. */
  error: DeepReadonly<Ref<Error | undefined>>

  /**
   * Initiates a fetches of all (or at least a bunch of) entities. Will update `data` upon completion.
   * Callers are not expected to consume the resulting promise. Instead, they'd use `isLoading` and `error`.
   *
   * If the store was already populated with data matching the specified params, `isLoading` will not be explicitly
   * updated to show a pending state, as consumers are invited to immediately consume data from the store.
   * Newer data will be fetched "in background" and the data in store will be refreshed if necessary.
   *
   * In case the caller absolutely needs fresh data, the returned promise can be observed. Please any potential error
   * will be absorbed (the promise will resolve anyway), and the `error` state should still be used.
   *
   * @param fetchParams Eventual parameters to pass down to the API function specified during store creation.
   */
  fetchAll: (...fetchParams: TFetchParams) => Promise<void>

  /**
   * Initiates the retrieval of a singular entity. Will update `data` upon completion.
   * The entity is not returned as a result of this function call. Callers can use {@link getEntityById}.
   *
   * The caller is fully responsible for handling loading states and error handling. Errors are not swallowed.
   *
   * @param id ID of the entity to fetch.
   */
  fetchOne: FnWithPreExtraArgs<(id: Identifier | number) => Promise<void>, TExtraArgs>

  /**
   * Creates a new entity. By default, the entity will also be pushed to the API.
   * If the store is operating in deferred mode, the operation will be added to the pending operations.
   *
   * The caller is fully responsible for handling loading states and error handling. Errors are not swallowed.
   *
   * @param entityData Data to pass down to the create handler specified during store creation.
   */
  create: FnWithPreExtraArgs<(entityData: TEntityCreateData) => Promise<void>, TExtraArgs>

  /**
   * Updates an entity. By default, the entity will also be pushed to the API.
   * If the store is operating in deferred mode, the operation will be added to the pending operations.
   *
   * The caller is fully responsible for handling loading states and error handling. Errors are not swallowed.
   *
   * @param id ID of the entity to update.
   * @param entityData Data to pass down to the update handler specified during store creation.
   * @throws Error if the entity does not exist in the store.
   */
  update: FnWithPreExtraArgs<(id: Identifier | number, entityData: TEntityUpdateData) => Promise<void>, TExtraArgs>

  /**
   * Deletes an entity. By default, the entity will also be removed from the API.
   * If the store is operating in deferred mode, the operation will be added to the pending operations.
   *
   * The caller is fully responsible for handling loading states and error handling. Errors are not swallowed.
   *
   * @param id ID of the entity to remove.
   * @throws Error if the entity does not exist in the store.
   */
  remove: FnWithPreExtraArgs<(id: Identifier | number) => Promise<void>, TExtraArgs>

  /**
   * Commits pending changes to the API. If the store is not operating in deferred mode, this is a no-op.
   * Pending operations will be optimized before passing them to the handler specified during store operations.
   *
   * The caller is fully responsible for handling loading states and error handling. Errors are not swallowed.
   */
  commit: () => Promise<void>

  /**
   * Creates a computed ref that yields the entity matching the ID passed.
   * If this function receives reactive parameters, the computed ref will react to changes to the value.
   * Otherwise, it will only update when the data saved in the store changes.
   *
   * @param id The id of the entity to get. Can be static or reactive.
   */
  getEntityById: (id: MaybeRefOrGetter<Identifier | number>) => ComputedRef<TEntity | undefined>

  /**
   * Creates a computed ref that yields entities matching the list of IDs passed.
   * If this function receives reactive parameters, the computed ref will react to changes to the value.
   * Otherwise, it will only update when the data saved in the store changes.
   *
   * @param ids The list of ids of the entities to get. Can be static or reactive.
   */
  getEntitiesByIds: (id: MaybeRefOrGetter<Array<Identifier | number>>) => ComputedRef<TEntity[]>
}

// Internal helper to avoid having to repeat what's necessary to get to TStateTree.
//
// Think of TypeScript's type system as functional programming; `type` define functions and Generics are variables.
// To "store" the computed StateTree, we use a new "function" that receives it as a "variable".
//
// Avoiding repetitions starts to matter with large and complex declarations like here, not just for the sake of
// clarity, but also for the performance of the type checker, which doesn't have to compute it 3 times.
type _CrudStoreDefinitionInner<TName extends string, TStateTree> = StoreDefinition<
  TName,
  StoreState<TStateTree>,
  StoreGetters<TStateTree>,
  StoreActions<TStateTree>
>

/**
 * StoreDefinition of a CrudStore
 */
type CrudStoreDefinition<
  TName extends string,
  TEntity extends Entity,
  TFetchParams extends unknown[],
  TEntityCreateData,
  TEntityUpdateData,
  TSetupState extends StateTree,
  TExtraArgs extends unknown[],
> = _CrudStoreDefinitionInner<
  TName,
  Merge<CrudStoreStateTree<TEntity, TFetchParams, TEntityCreateData, TEntityUpdateData, TExtraArgs>, TSetupState>
>

type _CrudStoreInner<TName extends string, TStateTree> = Store<
  TName,
  StoreState<TStateTree>,
  StoreGetters<TStateTree>,
  StoreActions<TStateTree>
>

type DefsReq = {
  Name: string
  Entity: Entity
  FetchParams: unknown[]
  CreateData: unknown
  UpdateData: unknown
  SetupState: StateTree
  ExtraArgs: unknown[]
}

type Defs = Partial<DefsReq>
type _GetOptional<T extends Defs, TKey extends keyof Defs, TDefault = undefined> = T[TKey] extends DefsReq[TKey]
  ? T[TKey]
  : TDefault extends undefined
    ? DefsReq[TKey]
    : TDefault

export type CrudStore<TDefs extends Defs = Record<string, undefined>> = _CrudStoreInner<
  _GetOptional<TDefs, 'Name'>,
  Merge<
    CrudStoreStateTree<
      _GetOptional<TDefs, 'Entity'>,
      _GetOptional<TDefs, 'FetchParams'>,
      _GetOptional<TDefs, 'CreateData'>,
      _GetOptional<TDefs, 'UpdateData'>,
      _GetOptional<TDefs, 'ExtraArgs'>
    >,
    _GetOptional<TDefs, 'SetupState', Record<string, never>>
  >
>

/**
 * Defines a CrudStore. A CrudStore is a special type of store that comes with out-of-the-box handling for common
 * CRUD operations (Create, Read, Update, Delete) as well as helpers for consuming data stored in it.
 *
 * Whenever reading entities, depending on the context there may be options to restrict the set of entities to fetch
 * from the API. These will match the ones specified by the fetchAll handler passed during store creation. Keep in mind
 * that when fetching 2 distinct sets of data, the 2nd set will NOT override the first set of data, and both sets will
 * be stored. Make sure to filter the entities you want in these cases using a computed property, or one of the helpers
 * provided by the store.
 *
 * By default, the CrudStore immediately synchronizes changes with the API. The definition requires passing the
 * functions to perform these HTTP requests, and will use them while updating the data stored in the store. This is
 * commonly referred to as "immediate mode".
 *
 * If this behavior is undesirable, it is possible to configure the store to operate in deferred mode. In this mode,
 * changes to the data is done in cooperation with the UndoRedo store, and are not immediately pushed. It is possible
 * to locally cancel certain operations through the UndoRedo store and the data will be automatically reverted. To push
 * the changes to the API, the commit function must be called and will invoke the committer function defined during
 * store creation.
 *
 * In deferred mode, before committing the list of operations to perform is optimized to avoid redundant operations.
 * Beware, this means the order of operations may not match with the order of operations performed by the user. If this
 * is undesirable, it is possible to disable this using the optimize parameter.
 *
 * Note that if multiple Deferred CrudStores are updated, the UndoRedo store does not currently support partial commits.
 * To ensure consistent behavior, make sure that when committing, all the modification across stores are committed as
 * well. The UndoRedo store will assume everything has been pushed even if it's not the case, which will lead to an
 * incorrect representation of the data living remotely in the database.
 *
 * For performance reasons, the store is assuming entities are immutable and as a result they are not reactive on
 * their own. If an object from this store is unwrapped and stored elsewhere, it will lose reactivity and may not be
 * kept up to date properly!
 *
 * Additionally, direct modification of the store's list of entities is not allowed and will be rejected by Vue.
 * You must always pass through the exposed actions to mutate the store, so modifications can be tracked appropriately.
 *
 * If you want to extend the store, or override/supercharge parts of the CrudStore, you can specify a setup function
 * that behaves like the setup function of a plain Pinia store. It takes as an entry the {@link CrudStoreStateTree}
 * generated, and must return an object with the properties to add (or override).
 *
 * **Beware of errors reported by TypeScript!!** Because the type is very complex, malformed input during creation of
 * the store may cause the type checker to fail to properly instantiate the types, causing bogus type errors.
 * The error is indicative of an issue, but may not accurately report what is the issue.
 *
 * @param props Properties of the store.
 */
export function defineCrudStore<
  TName extends string,
  TEntity extends Entity,
  TFetchParams extends unknown[] = [],
  TEntityCreateData = TEntity,
  TEntityUpdateData = Partial<EntityData<TEntity>>,
  TSetupState extends StateTree = Record<string, never>,
  TExtraArgs extends unknown[] = [],
>(
  props: CrudStoreProperties<
    TName,
    TEntity,
    TFetchParams,
    TEntityCreateData,
    TEntityUpdateData,
    TSetupState,
    TExtraArgs
  >
): CrudStoreDefinition<TName, TEntity, TFetchParams, TEntityCreateData, TEntityUpdateData, TSetupState, TExtraArgs> {
  const store = defineStore(props.name, () => {
    const undoredo = useUndoRedoStore()

    // Internal variables
    // These are used to track internal state of the store, and are not meant to be exposed
    const params: TFetchParams[] = []
    let pendingOperations: CrudOperation<TEntity>[] = []

    const entities = new EntitySet<TEntity>() // EntitySet is reactive by itself
    const isLoading = ref(false)
    const error = ref<Error>()

    function trackUndoRedo(op: CrudOperation<TEntity>, type: ActionType, commit: () => void, rollback: () => void) {
      const revert = invertOperation(op)

      undoredo.track({
        type: type,
        source: props.name,
        commit: (isRevert) => {
          if (isRevert) {
            const idx = pendingOperations.indexOf(revert)
            pendingOperations.splice(idx, 1)
          } else {
            pendingOperations.push(op)
          }

          commit()
        },
        rollback: (isRevert) => {
          if (isRevert) {
            pendingOperations.push(revert)
          } else {
            const idx = pendingOperations.indexOf(op)
            pendingOperations.splice(idx, 1)
          }

          rollback()
        },
      })
    }

    /** @see CrudStoreStateTree#fetchAll */
    async function fetchAll(...fetchParams: TFetchParams) {
      // If we already fetched for a given set of parameters, don't "explicitly" refresh.
      // We want to perform the fetch to update data in store, but while it's in-flight let the UI show current data.
      // Cases which require always fresh data may implement additional wrapping based on the returned promise.
      const didFetchForParams = params.find((p) => isEqual(p, fetchParams))
      if (!didFetchForParams) {
        isLoading.value = true
        params.push(fetchParams)
      }

      try {
        const fetched = await props.fetchAll(...fetchParams)
        entities.put(...fetched)
      } catch (e) {
        error.value = e instanceof Error ? e /* v8 ignore next */ : new Error(e?.toString())
      } finally {
        isLoading.value = false
      }
    }

    /** @see CrudStoreStateTree#fetchOne */
    async function fetchOne(...args: [...TExtraArgs, Identifier | number]) {
      const [extraArgs, id] = arrayPop(args)

      const fetched = await props.fetchOne(...extraArgs, id.valueOf())
      entities.put(fetched)
    }

    /** @see CrudStoreStateTree#create */
    async function create(...args: [...TExtraArgs, TEntityCreateData]) {
      const [extraArgs, entityData] = arrayPop(args)

      if (props.deferred) {
        // Cast because TS fears generics may have been instantiated with unrelated types...
        // It's not wrong, but never supposed to occur. So let's just cast it and call it a day...
        // It would be possible to avoid that by using overloads and making types even more complicated than right now.
        // While it would be the "right thing" to do, as it's an internal lib we can trust it'll not be misused :shrug:
        const entity = entityData as unknown as TEntity

        const operation: CrudOperation<TEntity> = { type: OperationType.CREATE, entity: entity }
        return trackUndoRedo(
          operation,
          ActionType.CREATE,
          () => entities.put(entity),
          () => entities.delete(entity)
        )
      }

      const result = await props.create(...extraArgs, entityData)
      entities.put(result)
    }

    /** @see CrudStoreStateTree#update */
    async function update(...args: [...TExtraArgs, Identifier | number, TEntityUpdateData]) {
      const [_args, entityData] = arrayPop(args)
      const [extraArgs, id] = arrayPop(_args)

      const entity = entities.get(id)
      if (!entity) throw new Error('Entity not found')

      if (props.deferred) {
        // Cast because TS fears generics may have been instantiated with unrelated types...
        // It's not wrong, but never supposed to occur. So let's just cast it and call it a day...
        // It would be possible to avoid that by using overloads and making types even more complicated than right now.
        // While it would be the "right thing" to do, as it's an internal lib we can trust it'll not be misused :shrug:
        const result = entity.cloneWith(entityData as Partial<EntityData<TEntity>>)

        const operation: CrudOperation<TEntity> = { type: OperationType.UPDATE, prev: entity, next: result }
        return trackUndoRedo(
          operation,
          ActionType.UPDATE,
          () => entities.put(result),
          () => entities.put(entity)
        )
      }

      const updatedEntity = await props.update(...extraArgs, id.valueOf(), entityData)
      entities.put(updatedEntity)
    }

    /** @see CrudStoreStateTree#remove */
    async function remove(...args: [...TExtraArgs, Identifier | number]) {
      const [extraArgs, id] = arrayPop(args)

      const entity = entities.get(id)
      if (!entity) throw new Error('Entity not found')

      if (props.deferred) {
        const operation: CrudOperation<TEntity> = { type: OperationType.DELETE, entity }
        return trackUndoRedo(
          operation,
          ActionType.DELETE,
          () => entities.delete(entity),
          () => entities.put(entity)
        )
      }

      await props.remove(...extraArgs, id.valueOf())
      entities.delete(entity)
    }

    /** @see CrudStoreStateTree#commit */
    async function commit() {
      /* v8 ignore start */
      if (!props.deferred) {
        if (import.meta.env.DEV) {
          // This isn't harmful, but may be indicative of an issue. Warn in dev mode just in case.
          // Excluded from code coverage reports as it's a dev-only helper, not meant for production.
          console.warn('[CrudStore#%s] Call to commit is a no-op: store is not in deferred mode.', props.name)
        }

        return
      }
      /* v8 ignore end */

      if (!pendingOperations.length) {
        // Nothing to do.
        return
      }

      // TODO: optimize ops?
      await props.commit(pendingOperations)
      undoredo.markCommitted()

      // Clear operations
      pendingOperations = []
    }

    /** @see CrudStoreStateTree#getEntityById */
    function getEntityById(id: MaybeRefOrGetter<Identifier | number>) {
      return computed(() => {
        const idValue = toValue<Identifier | number>(id)
        return entities.get(idValue)
      })
    }

    /** @see CrudStoreStateTree#getEntitiesByIds */
    function getEntitiesByIds(ids: MaybeRefOrGetter<Array<Identifier | number>>) {
      return computed(() => {
        const idsValue = toValue<Array<Identifier | number>>(ids)
        return entities.getAll(idsValue)
      })
    }

    const state: CrudStoreStateTree<TEntity, TFetchParams, TEntityCreateData, TEntityUpdateData, TExtraArgs> = {
      // Mark these as readonly to avoid misuse of these properties.
      // It's not necessary, but done here as the store is meant to be a safe building block with guard rails.
      isLoading: readonly(isLoading),
      error: readonly(error),
      entities,

      fetchAll,
      fetchOne,
      create,
      update,
      remove,
      commit,

      getEntityById,
      getEntitiesByIds,
    }

    // This is a workaround to reduce the amount of casting needed to please TSC.
    // There is no easy way to make TSC understand that TSetupState depends on the presence of setup.
    // The only case this would not hold true is if we're forcing types manually which is not supposed to ever happen.
    const setupState = props.setup ? props.setup(state) : ({} as TSetupState)

    // Since TSetupState concerns have been manually suppressed, we can just return the merged objects.
    return { ...state, ...setupState }
  })

  if (import.meta.hot) {
    // HMR support - https://pinia.vuejs.org/cookbook/hot-module-replacement.html
    import.meta.hot.accept(acceptHMRUpdate(store, import.meta.hot))
  }

  // Explicit cast as the type system is a bit overwhelmed with what it's currently facing.
  // Don't worry little TypeScript, you're trying your best! 🥹 Have a cookie, little boi 🍪🍪🍪
  return store as CrudStoreDefinition<
    TName,
    TEntity,
    TFetchParams,
    TEntityCreateData,
    TEntityUpdateData,
    TSetupState,
    TExtraArgs
  >
}
