import type { Identifier } from '@/ts/entities/base/identifier.ts'
import type { StructuralGroup } from '@/ts/entities/group.ts'

import { type MaybeRefOrGetter, computed, toValue } from 'vue'
import { EntitySet } from '@/ts/entities/base/entityset.ts'

import {
  createStructuralGroup,
  deleteStructuralGroup,
  getAllStructuralGroups,
  getStructuralGroup,
  updateStructuralGroup,
} from '@/ts/api/structuralGroups.ts'
import { defineCrudStore } from '../../defineCrudStore.ts'

export type StructuralGroupTreeBranch = StructuralGroup & { parentId: number; children?: StructuralGroupTreeNode[] }
export type StructuralGroupTreeLeaf = StructuralGroup & { children?: never }
export type StructuralGroupTreeNode = StructuralGroupTreeBranch | StructuralGroupTreeLeaf

// TODO: deferred store?
export const useStructuralGroupsStore = defineCrudStore({
  name: 'structural-groups',
  fetchAll: getAllStructuralGroups,
  fetchOne: getStructuralGroup,
  create: createStructuralGroup,
  update: updateStructuralGroup,
  remove: deleteStructuralGroup,
  setup: (ctx) => {
    function getDepartmentGroups(department: MaybeRefOrGetter<Identifier | number>) {
      const groups = computed(() => {
        const departmentValue = toValue(department)
        return new EntitySet(ctx.entities.filter((e) => e.departmentId === departmentValue.valueOf()))
      })

      const tree = computed(() => {
        const groupsSet = new EntitySet(
          Array.from(groups.value.values()).map((e) => e.clone() as StructuralGroupTreeNode)
        )

        const roots: StructuralGroupTreeNode[] = []
        for (const group of groupsSet) {
          if (group.parentId !== null) {
            const parent = groupsSet.get(group.parentId)
            /* v8 ignore start */
            // This is never supposed to happen with sane data.
            if (!parent) {
              throw new Error(
                `Invalid reference: group #${group.id.toString()} points to non-existing group #${group.parentId}`
              )
            }
            /* v8 ignore end */

            if (!parent.children) parent.children = []
            parent.children.push(group)
          } else {
            roots.push(group)
          }
        }

        // Sort groups alphabetically in the tree
        function sort(groups: StructuralGroupTreeNode[]) {
          groups.sort((a, b) => a.fullName.localeCompare(b.fullName))
          groups.forEach((g) => g.children && sort(g.children))
          return groups
        }

        return sort(roots)
      })

      const descendantsMap = computed(() => {
        const descendantsMap = new Map<number, Set<number>>()

        function processDescendants(group: StructuralGroupTreeNode) {
          const descendants = new Set<number>()
          descendantsMap.set(group.id.valueOf(), descendants)

          if (group.children) {
            for (const child of group.children) {
              descendants.add(child.id.valueOf())

              const childrenDescendants = processDescendants(child)
              childrenDescendants.forEach((d) => descendants.add(d))
            }
          }

          return descendants
        }

        tree.value.forEach((root) => processDescendants(root))
        return descendantsMap
      })

      return {
        groups,
        tree,
        descendantsMap,
      }
    }

    return {
      getDepartmentGroups,
    }
  },
})
