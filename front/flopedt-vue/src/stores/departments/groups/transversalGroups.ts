import type { Identifier } from '@/ts/entities/base/identifier.ts'

import { type MaybeRefOrGetter, computed, toValue } from 'vue'
import { EntitySet } from '@/ts/entities/base/entityset.ts'

import {
  createTransversalGroup,
  deleteTransversalGroup,
  getAllTransversalGroups,
  getTransversalGroup,
  updateTransversalGroup,
} from '@/ts/api/transversalGroups.ts'
import { defineCrudStore } from '../../defineCrudStore.ts'

// TODO: deferred store?
export const useTransversalGroupsStore = defineCrudStore({
  name: 'transversal-groups',
  fetchAll: getAllTransversalGroups,
  fetchOne: getTransversalGroup,
  create: createTransversalGroup,
  update: updateTransversalGroup,
  remove: deleteTransversalGroup,
  setup: (ctx) => {
    function getDepartmentGroups(department: MaybeRefOrGetter<Identifier | number>) {
      const groups = computed(() => {
        const departmentValue = toValue(department)
        return new EntitySet(ctx.entities.filter((e) => e.departmentId === departmentValue.valueOf()))
      })

      return {
        groups,
      }
    }

    return {
      getDepartmentGroups,
    }
  },
})
