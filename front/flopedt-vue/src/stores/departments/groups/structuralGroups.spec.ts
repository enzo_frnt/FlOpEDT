import { it, expect, beforeEach } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'
import { shuffle } from 'lodash-es'

import { StructuralGroupTreeNode, useStructuralGroupsStore } from '@/stores/departments/groups/structuralGroups.ts'
import { StructuralGroup } from '@/ts/entities/group.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'

import { toWritableEntitySet } from '@/utils/vitest.ts'

beforeEach(() => void setActivePinia(createPinia()))

function createGroup(id: number, name: string, parent: number | null) {
  return new StructuralGroup(new Identifier(id), name, `FLOP-${name}`, 0, 0, 0, 1, parent, false)
}

let irrelevantCounter = 0
function createIrrelevantGroup() {
  return new StructuralGroup(
    Identifier.localIdentifier(),
    `X${++irrelevantCounter}`,
    `FLAP-${irrelevantCounter}`,
    0,
    0,
    0,
    2,
    null,
    false
  )
}

// Simple tree:
//         1
//     2       3
//   4   5   6   7
function createSimpleTree() {
  const groups = [
    createGroup(1, 'CM', null),
    createGroup(2, 'TD1', 1),
    createGroup(3, 'TD2', 1),
    createGroup(4, 'TP11', 2),
    createGroup(5, 'TP12', 2),
    createGroup(6, 'TP21', 3),
    createGroup(7, 'TP22', 3),
  ]

  const groupsNode = groups.map((g) => g.clone() as StructuralGroupTreeNode)
  groupsNode[0].children = [groupsNode[1], groupsNode[2]]
  groupsNode[1].children = [groupsNode[3], groupsNode[4]]
  groupsNode[2].children = [groupsNode[5], groupsNode[6]]
  const expected = [groupsNode[0]]

  const descendants = new Map([
    [1, new Set([2, 3, 4, 5, 6, 7])],
    [2, new Set([4, 5])],
    [3, new Set([6, 7])],
    [4, new Set()],
    [5, new Set()],
    [6, new Set()],
    [7, new Set()],
  ])

  return {
    groups: shuffle([
      ...groups,
      ...Array(15)
        .fill(null)
        .map(() => createIrrelevantGroup()),
    ]),
    filtered: groups,
    expected,
    descendants,
  }
}

// "Multi-root" tree:
//           1                    2                    3
//      4         5          6         7          8         9
//   10   11   12   13    14   15   16   17    18   19   20   21
function createMultiRootTree() {
  const groups = [
    createGroup(1, 'CMA', null),
    createGroup(2, 'CMB', null),
    createGroup(3, 'CMC', null),

    createGroup(4, 'TDA1', 1),
    createGroup(5, 'TDA2', 1),
    createGroup(6, 'TDB1', 2),
    createGroup(7, 'TDB2', 2),
    createGroup(8, 'TDC1', 3),
    createGroup(9, 'TDC2', 3),

    createGroup(10, 'TPA11', 4),
    createGroup(11, 'TPA12', 4),
    createGroup(12, 'TPA21', 5),
    createGroup(13, 'TPA22', 5),
    createGroup(14, 'TPB11', 6),
    createGroup(15, 'TPB12', 6),
    createGroup(16, 'TPB21', 7),
    createGroup(17, 'TPB22', 7),
    createGroup(18, 'TPC11', 8),
    createGroup(19, 'TPC12', 8),
    createGroup(20, 'TPC21', 9),
    createGroup(21, 'TPC22', 9),
  ]

  const groupsNode = groups.map((g) => g.clone() as StructuralGroupTreeNode)
  groupsNode[0].children = [groupsNode[3], groupsNode[4]]
  groupsNode[1].children = [groupsNode[5], groupsNode[6]]
  groupsNode[2].children = [groupsNode[7], groupsNode[8]]
  groupsNode[3].children = [groupsNode[9], groupsNode[10]]
  groupsNode[4].children = [groupsNode[11], groupsNode[12]]
  groupsNode[5].children = [groupsNode[13], groupsNode[14]]
  groupsNode[6].children = [groupsNode[15], groupsNode[16]]
  groupsNode[7].children = [groupsNode[17], groupsNode[18]]
  groupsNode[8].children = [groupsNode[19], groupsNode[20]]
  const expected = [groupsNode[0], groupsNode[1], groupsNode[2]]

  const descendants = new Map([
    [1, new Set([4, 5, 10, 11, 12, 13])],
    [2, new Set([6, 7, 14, 15, 16, 17])],
    [3, new Set([8, 9, 18, 19, 20, 21])],
    [4, new Set([10, 11])],
    [5, new Set([12, 13])],
    [6, new Set([14, 15])],
    [7, new Set([16, 17])],
    [8, new Set([18, 19])],
    [9, new Set([20, 21])],
    [10, new Set()],
    [11, new Set()],
    [12, new Set()],
    [13, new Set()],
    [14, new Set()],
    [15, new Set()],
    [16, new Set()],
    [17, new Set()],
    [18, new Set()],
    [19, new Set()],
    [20, new Set()],
    [21, new Set()],
  ])

  return {
    groups: shuffle([
      ...groups,
      ...Array(30)
        .fill(null)
        .map(() => createIrrelevantGroup()),
    ]),
    filtered: groups,
    expected,
    descendants,
  }
}

it('should properly construct the structural group tree (single-root)', () => {
  const store = useStructuralGroupsStore()

  const entities = toWritableEntitySet(store.entities)
  const data = createSimpleTree()
  entities.put(...data.groups)

  const { groups, tree, descendantsMap } = store.getDepartmentGroups(1)
  const groupTree = tree.value

  // Workaround to ensure that order doesn't matter; [a, b, c] == [c, b, a]
  const relevantGroups = Array.from(groups.value).sort()
  expect(relevantGroups).toEqual(data.filtered.sort())

  expect(groupTree).toEqual(data.expected)
  expect(descendantsMap.value).toEqual(data.descendants)
})

it('should properly construct the structural group tree (multi-root)', () => {
  const store = useStructuralGroupsStore()

  const entities = toWritableEntitySet(store.entities)
  const data = createMultiRootTree()
  entities.put(...data.groups)

  const { groups, tree, descendantsMap } = store.getDepartmentGroups(1)
  const groupTree = tree.value

  // Workaround to ensure that order doesn't matter; [a, b, c] == [c, b, a]
  const relevantGroups = Array.from(groups.value).sort()
  expect(relevantGroups).toEqual(data.filtered.sort())

  expect(groupTree).toEqual(data.expected)
  expect(descendantsMap.value).toEqual(data.descendants)
})
