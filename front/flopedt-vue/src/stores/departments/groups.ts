import type { ReadonlyEntitySet } from '@/ts/entities/base/entityset.ts'
import type { Identifier } from '@/ts/entities/base/identifier.ts'
import type { Group } from '@/ts/entities/group.ts'

import { type MaybeRefOrGetter, computed, toValue } from 'vue'
import { defineStore } from 'pinia'

import { useStructuralGroupsStore } from './groups/structuralGroups.ts'
import { useTransversalGroupsStore } from './groups/transversalGroups.ts'

export const useGroupsStore = defineStore('groups', () => {
  const structuralGroups = useStructuralGroupsStore()
  const transversalGroups = useTransversalGroupsStore()

  const groups = computed<ReadonlyEntitySet<Group>>(() => structuralGroups.entities.union(transversalGroups.entities))

  const isLoading = computed(() => structuralGroups.isLoading || transversalGroups.isLoading)
  const error = computed(() => structuralGroups.error || transversalGroups.error)

  function fetchAll(dept: number) {
    return Promise.all([structuralGroups.fetchAll(dept), transversalGroups.fetchAll(dept)])
  }

  function getGroupById(id: MaybeRefOrGetter<Identifier | number>) {
    return computed(() => groups.value.get(toValue(id)))
  }

  function getGroupsByIds(ids: MaybeRefOrGetter<Array<Identifier | number>>) {
    return computed(() => {
      const idsValue = toValue(ids)
      return groups.value.getAll(idsValue)
    })
  }

  function getGroupsByDepartment(dept: MaybeRefOrGetter<Identifier | number>) {
    const structural = structuralGroups.getDepartmentGroups(dept)
    const transversal = transversalGroups.getDepartmentGroups(dept)
    const all = computed<Group[]>(() => [...structural.groups.value, ...transversal.groups.value])

    return { structural, transversal, all }
  }

  return {
    groups,
    isLoading,
    error,

    fetchAll,

    getGroupById,
    getGroupsByIds,
    getGroupsByDepartment,
  }
})
