import {
  createTrainingProgram,
  deleteTrainingProgram,
  getAllTrainingPrograms,
  getTrainingProgram,
  updateTrainingProgram,
} from '@/ts/api/trainingPrograms.ts'
import { defineCrudStore } from '../defineCrudStore.ts'

// TODO: deferred store?
export const useTrainingProgramsStore = defineCrudStore({
  name: 'training-programs',
  fetchAll: getAllTrainingPrograms,
  fetchOne: getTrainingProgram,
  create: createTrainingProgram,
  update: updateTrainingProgram,
  remove: deleteTrainingProgram,
})
