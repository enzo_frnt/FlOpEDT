import { ref } from 'vue'
import { defineStore } from 'pinia'
import { getHello } from '@/ts/api/hello'
import { useAuthStore } from '@/stores/auth'
import { usePrefsStore } from '@/stores/prefs'

export const useAppDataStore = defineStore('appdata', () => {
  const auth = useAuthStore()
  const prefs = usePrefsStore()

  const loaded = ref(false)
  const config = ref<unknown>() // TODO: proper type

  async function fetchAppData() {
    try {
      const data = await getHello()
      loaded.value = true

      if (data.whoami) auth.user = data.whoami
      prefs.preferences = data.preferences
      config.value = data.configuration
    } catch (e) {
      console.error('Failed to say hello to the server', e)
      throw e
    }
  }

  return {
    loaded,
    config,
    fetchAppData,
  }
})
