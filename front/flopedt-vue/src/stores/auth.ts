import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useRouter } from 'vue-router'
import { usePrefsStore } from '@/stores/prefs'
import { useAppDataStore } from '@/stores/appdata'

import { Identifier } from '@/ts/entities/base/identifier'
import { MinimalUser } from '@/ts/entities/user'

// TODO: make it a full user
const LOGGED_OUT_USER = new MinimalUser(new Identifier(-1), 'anon', 'Anonymous', undefined, undefined)

export const useAuthStore = defineStore('auth', () => {
  const appdata = useAppDataStore()
  const prefs = usePrefsStore()

  const router = useRouter()

  const user = ref(LOGGED_OUT_USER)
  const loaded = computed(() => appdata.loaded)
  const isAuthenticated = computed(() => user.value.id.valueOf() > 0)

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function login(username: string, password: string) {
    // TODO: proper implementation
    prefs.preferences = null
    throw new Error('Not implemented!')
  }

  function logout() {
    // TODO: proper implementation
    fetch('/fr/accounts/logout-vue', { method: 'GET', credentials: 'include' })
      .then((response) => {
        if (response.ok) {
          user.value = LOGGED_OUT_USER
          prefs.preferences = null
        }
      })
      .then(() => router.push('/'))
      .catch((error) => {
        console.log(error)
      })
  }

  return {
    user,
    loaded,
    isAuthenticated,

    login,
    logout,
  }
})
