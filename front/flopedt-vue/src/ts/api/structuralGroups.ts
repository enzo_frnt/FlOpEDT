import type { BodyOf, ResponseOf } from './core/schema.utils.d.ts'
import { get, post, patch, del } from './core/http.ts'

import { StructuralGroup } from '../entities/group.ts'

export type APIAllStructuralGroupsData = ResponseOf<'/en/api/v1/departments/{dept_id}/groups/structural/', 'get', 200>

export type APIStructuralGroupData = ResponseOf<'/en/api/v1/departments/{dept_id}/groups/structural/{id}/', 'get', 200>

export type APICreateStructuralGroupParams = BodyOf<'/en/api/v1/departments/{dept_id}/groups/structural/', 'post'>
export type APICreateStructuralGroupData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/groups/structural/',
  'post',
  201
>

export type APIUpdateStructuralGroupParams = BodyOf<'/en/api/v1/departments/{dept_id}/groups/structural/{id}/', 'patch'>
export type APIUpdateStructuralGroupData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/groups/structural/{id}/',
  'patch',
  200
>

export async function getAllStructuralGroups(dept: number) {
  const groups = await get<APIAllStructuralGroupsData>(`/en/api/v1/departments/${dept}/groups/structural`)
  return groups.map((g) => StructuralGroup.fromJSON(g))
}

export async function getStructuralGroup(dept: number, id: number) {
  const group = await get<APIStructuralGroupData>(`/en/api/v1/departments/${dept}/groups/structural/${id}`)
  return StructuralGroup.fromJSON(group)
}

export async function createStructuralGroup(dept: number, data: APICreateStructuralGroupParams) {
  const group = await post<APICreateStructuralGroupData>(`/en/api/v1/departments/${dept}/groups/structural`, data)
  return StructuralGroup.fromJSON(group)
}

export async function updateStructuralGroup(dept: number, id: number, data: APIUpdateStructuralGroupParams) {
  const group = await patch<APIUpdateStructuralGroupData>(
    `/en/api/v1/departments/${dept}/groups/structural/${id}`,
    data
  )
  return StructuralGroup.fromJSON(group)
}

export async function deleteStructuralGroup(dept: number, id: number) {
  await del(`/en/api/v1/departments/${dept}/groups/structural/${id}`)
}
