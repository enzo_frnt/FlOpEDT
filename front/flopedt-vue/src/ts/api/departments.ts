import type { BodyOf, ResponseOf } from './core/schema.utils.d.ts'
import { get, post, patch, del } from './core/http.ts'

import { Department } from '../entities/department.ts'

export type APIAllDepartmentsData = ResponseOf<'/en/api/v1/departments/', 'get', 200>

export type APIDepartmentData = ResponseOf<'/en/api/v1/departments/{dept_id}/', 'get', 200>

export type APICreateDepartmentParams = BodyOf<'/en/api/v1/departments/', 'post'>
export type APICreateDepartmentData = ResponseOf<'/en/api/v1/departments/', 'post', 201>

export type APIUpdateDepartmentParams = BodyOf<'/en/api/v1/departments/{dept_id}/', 'patch'>
export type APIUpdateDepartmentData = ResponseOf<'/en/api/v1/departments/{dept_id}/', 'patch', 200>

export async function getAllDepartments() {
  const departments = await get<APIAllDepartmentsData>('/en/api/v1/departments')
  return departments.map((d) => Department.fromJSON(d))
}

export async function getDepartment(id: number) {
  const department = await get<APIDepartmentData>(`/en/api/v1/departments/${id}`)
  return Department.fromJSON(department)
}

export async function createDepartment(data: APICreateDepartmentParams) {
  const department = await post<APICreateDepartmentData>(`/en/api/v1/departments`, data)
  return Department.fromJSON(department)
}

export async function updateDepartment(id: number, data: APIUpdateDepartmentParams) {
  const department = await patch<APIUpdateDepartmentData>(`/en/api/v1/departments/${id}`, data)
  return Department.fromJSON(department)
}

export async function deleteDepartment(id: number) {
  await del(`/en/api/v1/departments/${id}`)
}
