import type { BodyOf, ResponseOf } from './core/schema.utils.d.ts'
import { get, post, patch, del } from './core/http.ts'

import { TransversalGroup } from '../entities/group.ts'

export type APIAllTransversalGroupsData = ResponseOf<'/en/api/v1/departments/{dept_id}/groups/transversal/', 'get', 200>

export type APITransversalGroupData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/groups/transversal/{id}/',
  'get',
  200
>

export type APICreateTransversalGroupParams = BodyOf<'/en/api/v1/departments/{dept_id}/groups/transversal/', 'post'>
export type APICreateTransversalGroupData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/groups/transversal/',
  'post',
  201
>

export type APIUpdateTransversalGroupParams = BodyOf<
  '/en/api/v1/departments/{dept_id}/groups/transversal/{id}/',
  'patch'
>
export type APIUpdateTransversalGroupData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/groups/transversal/{id}/',
  'patch',
  200
>

export async function getAllTransversalGroups(dept: number) {
  const groups = await get<APIAllTransversalGroupsData>(`/en/api/v1/departments/${dept}/groups/transversal`)
  return groups.map((g) => TransversalGroup.fromJSON(g))
}

export async function getTransversalGroup(dept: number, id: number) {
  const group = await get<APITransversalGroupData>(`/en/api/v1/departments/${dept}/groups/transversal/${id}`)
  return TransversalGroup.fromJSON(group)
}

export async function createTransversalGroup(dept: number, data: APICreateTransversalGroupParams) {
  const group = await post<APICreateTransversalGroupData>(`/en/api/v1/departments/${dept}/groups/transversal`, data)
  return TransversalGroup.fromJSON(group)
}

export async function updateTransversalGroup(dept: number, id: number, data: APIUpdateTransversalGroupParams) {
  const group = await patch<APIUpdateTransversalGroupData>(
    `/en/api/v1/departments/${dept}/groups/transversal/${id}`,
    data
  )
  return TransversalGroup.fromJSON(group)
}

export async function deleteTransversalGroup(dept: number, id: number) {
  await del(`/en/api/v1/departments/${dept}/groups/transversal/${id}`)
}
