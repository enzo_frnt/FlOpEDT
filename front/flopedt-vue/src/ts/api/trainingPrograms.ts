import type { BodyOf, ResponseOf } from './core/schema.utils.d.ts'
import { get, post, patch, del } from './core/http.ts'

import { TrainingProgram } from '../entities/trainingProgram.ts'

export type APIAllTrainingProgramsData = ResponseOf<'/en/api/v1/departments/{dept_id}/training_programs/', 'get', 200>

export type APITrainingProgramData = ResponseOf<'/en/api/v1/departments/{dept_id}/training_programs/{id}/', 'get', 200>

export type APICreateTrainingProgramParams = BodyOf<'/en/api/v1/departments/{dept_id}/training_programs/', 'post'>
export type APICreateTrainingProgramData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/training_programs/',
  'post',
  201
>

export type APIUpdateTrainingProgramParams = BodyOf<'/en/api/v1/departments/{dept_id}/training_programs/{id}/', 'patch'>
export type APIUpdateTrainingProgramData = ResponseOf<
  '/en/api/v1/departments/{dept_id}/training_programs/{id}/',
  'patch',
  200
>

export async function getAllTrainingPrograms(dept: number) {
  const trainingPrograms = await get<APIAllTrainingProgramsData>(`/en/api/v1/departments/${dept}/training_programs`)
  return trainingPrograms.map((tp) => TrainingProgram.fromJSON(tp))
}

export async function getTrainingProgram(dept: number, id: number) {
  const trainingProgram = await get<APITrainingProgramData>(`/en/api/v1/departments/${dept}/training_programs/${id}`)
  return TrainingProgram.fromJSON(trainingProgram)
}

export async function createTrainingProgram(dept: number, data: APICreateTrainingProgramParams) {
  const trainingProgram = await post<APICreateTrainingProgramData>(
    `/en/api/v1/departments/${dept}/training_programs`,
    data
  )
  return TrainingProgram.fromJSON(trainingProgram)
}

export async function updateTrainingProgram(dept: number, id: number, data: APIUpdateTrainingProgramParams) {
  const trainingProgram = await patch<APIUpdateTrainingProgramData>(
    `/en/api/v1/departments/${dept}/training_programs/${id}`,
    data
  )
  return TrainingProgram.fromJSON(trainingProgram)
}

export async function deleteTrainingProgram(dept: number, id: number) {
  await del(`/en/api/v1/departments/${dept}/training_programs/${id}`)
}
