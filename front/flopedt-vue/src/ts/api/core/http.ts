let _csrf: string
function getCsrfToken(): string {
  if (!_csrf) {
    _csrf =
      document.cookie
        .split(';')
        .find((c) => c.trim().startsWith('csrftoken='))
        ?.split('=')[1]
        ?.trim() ?? ''
  }

  return _csrf
}

export type HttpResponse<TResponse> = {
  url: string
  status: number
  statusText: string
  headers: Headers
  data: TResponse
}

export class HttpError extends Error {
  constructor(
    public res: Response,
    public body: unknown
  ) {
    super(`HTTP Error while performing request: ${res.url} ${res.status} ${res.statusText}`)
  }
}

export class ValidationError extends HttpError {
  constructor(
    res: Response,
    public problems: Record<string, string[]>
  ) {
    super(res, problems)
  }
}

export async function request<TResponse = unknown>(input: string | URL, init?: RequestInit) {
  const hasBody = init?.body !== undefined && init.body !== null

  const headers = new Headers(init?.headers)
  // TODO: make sure Django reacts to this header for validation errors etc
  headers.set('Accept-Language', document.documentElement.lang)

  if (hasBody) {
    const csrf = getCsrfToken()
    if (csrf) headers.set('X-CSRFToken', csrf)

    headers.set('Content-Type', 'application/json')
  }

  const res = await fetch(input, {
    ...(init || {}),
    headers: headers,
    redirect: 'follow',
  })

  const body: unknown = res.headers.get('content-type') === 'application/json' ? await res.json() : await res.text()

  if (!res.ok) {
    if (res.status === 400 && typeof body === 'object') {
      // Just cast the body for convenience and hope for the best :D
      // It's fine so long we know for sure there will not be another format
      throw new ValidationError(res, body as Record<string, string[]>)
    }

    throw new HttpError(res, body)
  }

  return body as TResponse
}

export function get<TResponse = unknown>(input: string | URL, init?: RequestInit) {
  return request<TResponse>(input, {
    ...(init || {}),
    method: 'GET',
  })
}

export function post<TResponse = unknown>(input: string | URL, body?: unknown, init?: RequestInit) {
  return request<TResponse>(input, {
    ...(init || {}),
    method: 'POST',
    body: body !== null ? JSON.stringify(body) : null,
  })
}

export function patch<TResponse = unknown>(input: string | URL, body?: unknown, init?: RequestInit) {
  return request<TResponse>(input, {
    ...(init || {}),
    method: 'PATCH',
    body: body !== null ? JSON.stringify(body) : null,
  })
}

export function del<TResponse = unknown>(input: string | URL, body?: unknown, init?: RequestInit) {
  return request<TResponse>(input, {
    ...(init || {}),
    method: 'DELETE',
    body: body !== null && body !== undefined ? JSON.stringify(body) : null,
  })
}
