import { paths } from './schema.generated.d.ts'
import { Pretty } from '@/utils/types.d.ts'

type UnwrapContent<T> = T extends { content: { 'application/json': infer R } } ? R : never

type UnwrapResponseData<T> = Pretty<{ [C in keyof T]: UnwrapContent<T[C]> }>

export type Path = keyof paths

export type MethodsOf<P extends Path> = keyof paths[P]

export type StatusesOf<P extends Path, M extends MethodsOf<P>> = keyof paths[P][M]['responses']

export type ResponseOf<P extends Path, M extends MethodsOf<P>, S extends StatusesOf<P, M>> = UnwrapResponseData<
  paths[P][M]['responses']
>[S]

export type BodyOf<P extends Path, M extends MethodsOf<P>> = UnwrapContent<paths[P][M]['requestBody']>
