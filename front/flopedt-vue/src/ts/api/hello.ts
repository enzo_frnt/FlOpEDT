import type { ResponseOf } from './core/schema.utils.d.ts'
import { get } from './core/http.ts'

import { MinimalUser } from '@/ts/entities/user.ts'
import { Preferences } from '@/ts/entities/preferences.ts'

export type APIHelloData = ResponseOf<'/en/api/v1/hello/', 'get', 200>

export async function getHello() {
  const data = await get<APIHelloData>('/api/v1/hello')

  return {
    whoami: data.whoami && MinimalUser.fromJSON(data.whoami), // TODO: get a complete user object with perms and what not
    preferences: Preferences.fromJSON(null), // TODO
    configuration: data.configuration,
  }
}
