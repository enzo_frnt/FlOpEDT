import { type Ref, shallowReactive, watch } from 'vue'

import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

export interface ReadonlyEntitySet<T extends Entity> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Abysmal typechecking performance otherwise
  forEach(callbackFn: (value: T, key: Identifier, set: ReadonlyEntitySet<T>) => void, thisArg?: any): void

  get(key: Identifier | number): T | undefined
  getAll(key: Array<Identifier | number>): T[]
  has(element: T | Identifier | number): boolean

  /* eslint-disable @typescript-eslint/no-explicit-any -- abysmal typechecking perf otherwise */
  find<U extends T>(predicate: (val: T, set: ReadonlyEntitySet<T>) => val is U, thisArg?: any): U | undefined
  find(predicate: (val: T, set: ReadonlyEntitySet<T>) => any, thisArg?: any): T | undefined

  filter<U extends T>(predicate: (val: T, set: ReadonlyEntitySet<T>) => val is U, thisArg?: any): U[]
  filter(predicate: (val: T, set: ReadonlyEntitySet<T>) => any, thisArg?: any): T[]
  /* eslint-enable @typescript-eslint/no-explicit-any */

  union<U extends Entity>(other: ReadonlyEntitySet<U>): ReadonlyEntitySet<U | T>

  entries(): IterableIterator<[Identifier, T]>
  keys(): IterableIterator<Identifier>
  values(): IterableIterator<T>

  [Symbol.iterator](): IterableIterator<T>

  readonly size: number
}

export class EntitySet<T extends Entity> implements ReadonlyEntitySet<T> {
  #map: Map<number, T> = shallowReactive(new Map())
  #seenIds: Set<Ref<number>> = new Set()

  constructor(entries?: readonly T[] | null) {
    if (entries?.length) this.put(...entries)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Abysmal typechecking performance otherwise
  forEach(callbackFn: (value: T, key: Identifier, set: EntitySet<T>) => void, thisArg?: any) {
    this.#map.forEach((v) => callbackFn(v, v.id, this), thisArg)
  }

  get(key: Identifier | number): T | undefined {
    const id = this.#getRealId(key)
    return this.#map.get(id)
  }

  getAll(keys: Array<Identifier | number>): T[] {
    const res = []
    for (const key of keys) {
      const id = this.#getRealId(key)
      const val = this.#map.get(id)
      if (val) res.push(val)
    }

    return res
  }

  has(key: T | Identifier | number): boolean {
    const id = this.#getRealId(key instanceof Entity ? key.id : key)
    return this.#map.has(id)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Abysmal typechecking performance otherwise
  find(predicate: (val: T, set: ReadonlyEntitySet<T>) => any, thisArg?: any) {
    for (const val of this) {
      if (predicate.call(thisArg, val, this)) {
        return val
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Abysmal typechecking performance otherwise
  filter(predicate: (val: T, set: ReadonlyEntitySet<T>) => any, thisArg?: any) {
    const res = []
    for (const val of this) {
      if (predicate.call(thisArg, val, this)) {
        res.push(val)
      }
    }

    return res
  }

  // Inspiration from https://tc39.es/proposal-set-methods/#sec-set.prototype.union
  union<U extends Entity>(other: EntitySet<U>): EntitySet<U | T> {
    const result = new EntitySet<U | T>(Array.from(this))
    other.forEach((v) => result.put(v))
    return result
  }

  // -- Write ops

  put(...values: T[]): this {
    values.forEach((value) => {
      this.#trackId(value.id)
      this.#map.set(value.id.valueOf(), value)
    })
    return this
  }

  delete(key: T | Identifier | number): boolean {
    const id = this.#getRealId(key instanceof Entity ? key.id : key)
    return this.#map.delete(id)
  }

  clear() {
    this.#map.clear()
  }

  // -- Iterators & misc

  entries(): IterableIterator<[Identifier, T]> {
    const it = this.#map.values()
    return {
      next() {
        const next = it.next()
        return next.done ? next : { done: false, value: [next.value.id, next.value] }
      },
      [Symbol.iterator]() {
        return this
      },
    }
  }

  keys(): IterableIterator<Identifier> {
    const it = this.#map.values()
    return {
      next() {
        const next = it.next()
        return next.done ? next : { done: false, value: next.value.id }
      },
      [Symbol.iterator]() {
        return this
      },
    }
  }

  values(): IterableIterator<T> {
    return this.#map.values()
  }

  [Symbol.iterator](): IterableIterator<T> {
    return this.values()
  }

  get size() {
    return this.#map.size
  }

  get [Symbol.toStringTag]() {
    return 'EntityMap'
  }

  #getRealId(id: Identifier | number) {
    if (!(id instanceof Identifier)) id = new Identifier(id)
    return id.valueOf()
  }

  #trackId(id: Identifier) {
    if (!this.#seenIds.has(id.ref)) {
      this.#seenIds.add(id.ref)
      watch(
        id.ref,
        (val, old) => {
          if (this.#map.has(old)) {
            this.#map.set(val, this.#map.get(old)!)
            this.#map.delete(old)
          }
        },
        { flush: 'sync' }
      )
    }
  }
}
