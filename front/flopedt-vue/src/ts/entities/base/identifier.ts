import { type Ref, customRef, toRaw } from 'vue'

// This will be used to assign fake IDs to the entities, so they can be tracked using an internal ID.
// To make them distinguishable, they're negative and at least -1000 (so -1 can be used for other purposed for example)
//
// Useful note: JavaScript is strictly single-threaded (unless Worker is used).
// There is no risk of reading the same value twice.
let localIdPool = -1000

// To make all instances of Identifier react to changes, it uses a single instance of Ref.
// They're indexed here, so they can be fetched in Identifier's constructor.
const identifiers = new Map<number, Ref<number>>()

/**
 * Represents an entity identifier. Capable of handling local identifier and synchronization with remote id later on.
 * The identifier wraps all operations that need to be performed on IDs, such as equality through {@link equals} and
 * serialization through {@link toJSON}.
 *
 * Under the hood, it is a fancy wrapper around Vue {@link Ref}. The property {@link ref} can be freely mutated as
 * you'd mutate a ref, and every instance of Identifier will be updated accordingly. As it is a {@link Ref}, you can
 * also {@link import('vue').watch|watch} it and react to updates.
 *
 * @see {@link localIdentifier} to generate temporary local identifiers.
 */
export class Identifier {
  public readonly ref: Ref<number>

  constructor(id: number) {
    if (!identifiers.has(id)) {
      // https://vuejs.org/api/reactivity-advanced.html#customref
      // Since we want this to be a ref, using customRef makes it easy to have custom logic in the setter.
      const ref = customRef<number>((track, trigger) => {
        let _value = id
        return {
          get() {
            track()
            return _value
          },
          set(newId) {
            // Add the "new" mapping
            identifiers.set(newId, ref)

            _value = newId
            trigger()
          },
        }
      })

      // Track ref
      identifiers.set(id, ref)
    }

    // Use the same Ref instance of all instances of Identifier.
    this.ref = toRaw(identifiers.get(id)!)
  }

  /**
   * Checks for the equality of identifiers.
   *
   * @param id The identifier to compare against.
   */
  equals(id: Identifier | number): boolean {
    return id.valueOf() === this.valueOf()
  }

  /**
   * Returns the underlying value of this object.
   * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/valueOf
   */
  valueOf(): number {
    return this.ref.value
  }

  /**
   * Returns the string representation of this object.
   * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/toString
   */
  toString(): string {
    return this.valueOf().toString()
  }

  /**
   * Returns the JSON data this object should be serialized as. Automatically called by {@link JSON.stringify}.
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
   */
  toJSON(): number {
    return this.valueOf()
  }

  /**
   * Creates a unique local identifier.
   */
  static localIdentifier() {
    return new Identifier(localIdPool--)
  }
}

if (import.meta.env.TEST === 'true') {
  const vitest = await import('vitest')
  vitest.beforeEach(() => identifiers.clear())
}
