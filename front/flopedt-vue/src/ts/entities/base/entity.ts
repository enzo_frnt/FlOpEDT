import type { ClassProperties, SerializableToResult, Pretty } from '@/utils/types.d.ts'
import { type Raw, markRaw } from 'vue'
import { Identifier } from './identifier.ts'

/**
 * Base class all entities must inherit to be recognized as entities.
 */
export abstract class Entity<TEntityJson = unknown> {
  /**
   * ID of the entity.
   */
  public readonly id: Identifier & Raw<Identifier>

  protected constructor(id: Identifier) {
    // Fight Vue and Pinia trying to unwrap identifier refs.
    this.id = markRaw(id)
  }

  /**
   * Creates a clone of the current entity. `obj !== obj.clone()`.
   */
  clone(): this {
    // Object.* functions are generally annoyingly typed because TS doesn't want to be "too strict" in stdlib...
    return Object.assign(Object.create(Object.getPrototypeOf(this) as object) as this, this)
  }

  /**
   * Creates a clone of the current entity and updates its properties. `obj !== obj.cloneWith(...)`.
   *
   * @param update The data to merge into the clone.
   */
  cloneWith(update: Partial<EntityData<this, TEntityJson>>): this {
    // Object.* functions are generally annoyingly typed because TS doesn't want to be "too strict" in stdlib...
    return Object.assign(Object.create(Object.getPrototypeOf(this) as object) as this, this, update) as this
  }

  /**
   * Returns the string representation of this object.
   * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/toString
   */
  toString() {
    const kv = Object.entries(this)
      .map(([k, v]) => `${k}=${v}`)
      .join(', ')

    return `${this.constructor.name}(${kv})`
  }

  /**
   * Returns the value this entity should be displayed as when shown in forms.
   * Should be overridden if the entity will be used in forms.
   */
  toFormDisplayValue(): string {
    if (import.meta.env.DEV) {
      console.warn(
        '[%s Entity #%s] Default implementation of toFormDisplayValue was used. This warning will not be shown in production.',
        this.constructor.name,
        this.id
      )
    }

    return this.toString()
  }

  /**
   * Returns the list of string values this entity should match against.
   * Useful to provide multiple ways to search for an entity within searchable fields such as Combobox.
   */
  getFormAliases(): string[] {
    return []
  }

  /**
   * Returns the JSON data this object should be serialized as. Automatically called by {@link JSON.stringify}.
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
   */
  abstract toJSON(): SerializableToResult<TEntityJson>
}

export type EntityClass<TEntity extends Entity<TEntityJson>, TEntityJson> = {
  // class Test extends Entity {} --- "typeof Test" is EntityClass
  new (...args: unknown[]): TEntity
  fromJSON(json: TEntityJson): TEntity
}

export type EntityData<TEntity extends Entity<TEntityJson>, TEntityJson = unknown> = Pretty<
  Omit<ClassProperties<TEntity>, 'id'>
>
