import { it, expect } from 'vitest'
import { Entity } from './entity'
import { Identifier } from './identifier'

class TestEntity extends Entity<number> {
  constructor(
    id: Identifier,
    public test: number
  ) {
    super(id)
  }

  toJSON() {
    return this.test
  }
}

it('clones itself and returns a new reference with the same values', () => {
  const entity = new TestEntity(new Identifier(1337), 1234)
  const newEntity = entity.clone()

  expect(newEntity).not.toBe(entity)
  expect(newEntity).toBeInstanceOf(TestEntity)
  expect(newEntity).toEqual(entity)
})

it('clones itself and returns a new reference with the updated values', () => {
  const entity = new TestEntity(new Identifier(1337), 1234)
  const newEntity = entity.cloneWith({ test: 9999 })

  expect(newEntity).not.toBe(entity)
  expect(newEntity).toBeInstanceOf(TestEntity)
  expect(newEntity).not.toEqual(entity)
  expect(newEntity.test).toBe(9999)

  expect(JSON.stringify(entity)).toBe(JSON.stringify(1234))
  expect(JSON.stringify(newEntity)).toBe(JSON.stringify(9999))
})

it('stringifies to the expected debuggable representation', () => {
  const entity = new TestEntity(new Identifier(1337), 1234)

  expect(entity.toString()).toBe('TestEntity(id=1337, test=1234)')
})
