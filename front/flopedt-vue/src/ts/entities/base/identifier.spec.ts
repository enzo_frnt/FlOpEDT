import { expect, it, vi } from 'vitest'
import { Identifier } from './identifier'
import { nextTick, watch } from 'vue'

it('correctly implements equality checks', () => {
  const id1 = new Identifier(1)
  const id2 = new Identifier(1)
  const id3 = new Identifier(2)

  expect(id1.equals(id1)).toBe(true)
  expect(id1.equals(id2)).toBe(true)
  expect(id1.equals(id3)).toBe(false)

  expect(id1.equals(1)).toBe(true)
})

it('serializes as a plain JSON number', () => {
  const id = new Identifier(1)
  expect(JSON.stringify({ id })).toBe(JSON.stringify({ id: 1 }))
})

it('gives correct string representation', () => {
  const id = new Identifier(1)
  expect(id.toString()).toBe('1')

  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
  expect(`ID is ${id}`).toBe('ID is 1')
})

it('handles comparison as expected', () => {
  const id1 = new Identifier(1)
  const id2 = new Identifier(2)

  expect(id1 < id2).toBe(true)
})

it('updates all references when id is updated', () => {
  const id1 = Identifier.localIdentifier()
  const id2 = new Identifier(id1.ref.value)

  expect(id1.equals(id2)).toBe(true)

  id1.ref.value = 1337
  expect(id1.equals(id2)).toBe(true)

  expect(JSON.stringify({ id: id2 })).toBe(JSON.stringify({ id: 1337 }))
  expect(id2.toString()).toBe('1337')
})

it('works with Vue watchers', async () => {
  const id1 = new Identifier(1234)
  const id2 = new Identifier(1234)

  const fn1 = vi.fn()
  const fn2 = vi.fn()

  watch(id1.ref, fn1)
  watch(id2.ref, fn2)

  id1.ref.value = 1337

  expect(fn1).not.toHaveBeenCalled()
  expect(fn2).not.toHaveBeenCalled()

  await nextTick()
  expect(fn1).toHaveBeenCalledWith(1337, 1234, expect.any(Function))
  expect(fn2).toHaveBeenCalledWith(1337, 1234, expect.any(Function))
})
