let bit = 0n

/** @enum */
export const UserPermission = {
  /**
   * Administrator permission. Grants unlimited access to all the resources.
   */
  CORE_ADMIN: 1n << bit++,

  // Departments

  /**
   * Allows creating a department.
   *
   * Implies `CORE_DEPARTMENTS_UPDATE`, `CORE_DEPARTMENTS_IMPORT`.
   */
  CORE_DEPARTMENTS_CREATE: 1n << bit++,

  /**
   * Allows updating departments the user is a member of.
   */
  CORE_DEPARTMENTS_UPDATE: 1n << bit++,

  /**
   * Allows importing (and export) raw planification data in xlsx format.
   *
   * Implies `CORE_COURSES_CREATE`, `CORE_COURSES_UPDATE`, `CORE_COURSES_DELETE`.
   */
  CORE_DEPARTMENTS_IMPORT: 1n << bit++,

  /**
   * Extends department permissions to all departments regardless of membership status.
   *
   * Does not grant additional rights regarding the permitted operations. If a user is not allowed to import
   * planification data, they will still not be able to do so even if this permission is set.
   */
  CORE_DEPARTMENTS_MANAGE_ALL: 1n << bit++,

  // Courses

  /**
   * Allows creating a course, scheduled or not.
   * The course can be scheduled only if the user has sufficient permissions to place a course at the desired time.
   *
   * If the user is not allowed to update upcoming days, they will not be able to place it at this time. If the user
   * does not have any schedule modification rights, the course can only be created as unscheduled.
   */
  CORE_COURSES_CREATE: 1n << bit++,

  /**
   * Allows updating a course's properties.
   *
   * Beware, updating a course MAY have side effects that will trigger additional permission checks.
   * For example, assigning the course to another tutor may end up overloading the target tutor.
   */
  CORE_COURSES_UPDATE: 1n << bit++,

  /**
   * Allows deleting a course.
   *
   * Implies `CORE_SCHEDULE_CANCEL_COURSE`.
   */
  CORE_COURSES_DELETE: 1n << bit++,

  // Constraints

  /**
   * Allows creating weak constraints (preferences) that explicitly apply to the user.
   * Also allows the user to add themselves as a target of an existing weak constraint.
   *
   * Does not allow making the constraint apply to other people.
   */
  CORE_CONSTRAINTS_ADD_OWN_WEAK_CONSTRAINTS: 1n << bit++,

  /**
   * Allows creating string constraints (requirements) that explicitly apply to the user.
   * Also allows the user to add themselves as a target of an existing strong constraint.
   *
   * Does not allow making the constraint apply to other people.
   *
   * Implies `CORE_CONSTRAINTS_ADD_OWN_STRONG_CONSTRAINTS`.
   */
  CORE_CONSTRAINTS_ADD_OWN_STRONG_CONSTRAINTS: 1n << bit++,

  /**
   * Allows updating constraints that explicitly apply to the user.
   *
   * The user must be explicitly targeted by the constraint (i.e. not because of an "everyone" fallback).
   * If a constraint targets multiple users, the user is only allowed to retire themselves from the constraint.
   *
   * To update the constraint a user can retire themselves from said constraint, clone it as a constraint that only
   * affects them, and then modify it. The user must be allowed to create a constraint to do this.
   */
  CORE_CONSTRAINTS_MANAGE_OWN_CONSTRAINTS: 1n << bit++,

  /**
   * Allows managing contraints within departments the user is a member of.
   *
   * Implies `CORE_CONSTRAINTS_ADD_OWN_WEAK_CONSTRAINTS`, `CORE_CONSTRAINTS_ADD_OWN_STRONG_CONSTRAINTS`,
   *         `CORE_CONSTRAINTS_MANAGE_OWN_CONSTRAINTS`.
   */
  CORE_CONSTRAINTS_MANAGE_CONSTRAINTS: 1n << bit++,

  /**
   * Extends constraint management permissions to all departments regardless of membership status.
   */
  CORE_CONSTRAINTS_MANAGE_OTHER_DEPT: 1n << bit++,

  // Schedule
  /**
   * Allows updating the schedule of the current user.
   * Updates can only be in the non-near future, unless additional permissions are granted.
   *
   * The exact amount of time that corresponds to "near future" is defined by the server's configuration.
   */
  CORE_SCHEDULE_UPDATE_SELF: 1n << bit++,

  /**
   * Allows updating everyone's schedule within the user's department(s).
   * Updates can only be in the non-near future, unless additional permissions are granted.
   *
   * The exact amount of time that corresponds to "near future" is defined by the server's configuration.
   *
   * Implies `CORE_SCHEDULE_UPDATE_SELF`.
   */
  CORE_SCHEDULE_UPDATE_OTHERS: 1n << bit++,

  /**
   * Extends schedule update permissions to updating within the past.
   */
  CORE_SCHEDULE_UPDATE_PAST: 1n << bit++,

  /**
   * Extends schedule update permissions to updating the near future.
   *
   * The exact amount of time that corresponds to "near future" is defined by the server's configuration.
   */
  CORE_SCHEDULE_UPDATE_NEAR_FUTURE: 1n << bit++,

  /**
   * Allows overloading the user's schedule.
   */
  CORE_SCHEDULE_UPDATE_OVERLOAD_SELF: 1n << bit++,

  /**
   * Allows overloading other's schedule. Does NOT imply the user's ability to overload themselves.
   */
  CORE_SCHEDULE_UPDATE_OVERLOAD_OTHERS: 1n << bit++,

  /**
   * Allows overloading a room.
   */
  CORE_SCHEDULE_UPDATE_OVERLOAD_ROOMS: 1n << bit++,

  /**
   * Allows the user to ignore restrictions put in place, such as break times.
   */
  CORE_SCHEDULE_UPDATE_BYPASS_RESTRICTIONS: 1n << bit++,

  /**
   * Allows the user to cancel a course. The act of cancelling a course removes it from the schedule, and notifies the
   * department's managers the course needs to be rescheduled.
   *
   * Ignores `CORE_SCHEDULE_UPDATE_NEAR_FUTURE`.
   */
  CORE_SCHEDULE_CANCEL_COURSE: 1n << bit++,

  /**
   * Extends schedule management permissions to all departments regardless of membership status.
   *
   * Does not grant additional rights regarding the permitted operations. If a user is not allowed to overload others,
   * they will still not be able to do so even if this permission is set.
   */
  CORE_SCHEDULE_MANAGE_OTHER_DEPT: 1n << bit++,

  // Accounts & prefs.
  /**
   * Allows creating, updating, and deleting users at will.
   *
   * Implies `CORE_USERS_MANAGE_PROPERTIES`, `CORE_USERS_MANAGE_AVAILABILITIES`
   */
  CORE_USERS_MANAGE: 1n << bit++,

  /**
   * Allows setting availability preferences.
   */
  CORE_USERS_SET_AVAILABILITY_PREFERENCES: 1n << bit++,

  /**
   * Allows bypassing availability requirements, such as the minimum number of available slots.
   */
  CORE_USERS_BYPASS_AVAILABILITY_REQUIREMENTS: 1n << bit++,

  /**
   * Allows managing properties of user accounts, such as ranks, permissions, etc.
   * A user is always allowed to do so on their own account.
   */
  CORE_USERS_MANAGE_PROPERTIES: 1n << bit++,

  /**
   * Allows updating the availability preferences of other users.
   *
   * Implies `CORE_USERS_SET_AVAILABILITY_PREFERENCES`.
   */
  CORE_USERS_MANAGE_AVAILABILITIES: 1n << bit++,

  /// ROOM RESERVATIONS PERMISSIONS
  /**
   * Allows seeing which rooms are reserved, by whom and when.
   */
  ROOM_RSV_SEE_RESERVED_ROOMS: 1n << bit++,

  /**
   * Allows reserving a room within the departments the user is a member of.
   * The user is allowed to update or delete the reservation at any time (unless it's in the past).
   */
  ROOM_RSV_RESERVE_ROOM: 1n << bit++,

  /**
   * Allows reserving any room (so long it is available).
   *
   * Implies `ROOM_RSV_RESERVE_ROOM`
   */
  ROOM_RSV_RESERVE_ANY_ROOM: 1n << bit++,

  /**
   * Allows managing all reservations.
   *
   * Implies `ROOM_RSV_RESERVE_ANY_ROOM`, `ROOM_RSV_SEE_RESERVED_ROOMS`
   */
  ROOM_RSV_MANAGE_RESERVATIONS: 1n << bit++,

  /**
   * Allows to bypass restrictions on reservations, such as maximum time or availability hours.
   */
  ROOM_RSV_BYPASS_RESTRICTIONS: 1n << bit++,
}
