import type { components } from '@/ts/api/core/schema.generated.ts'

import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

export type APIDepartment = components['schemas']['DepartmentFull']

export class Department extends Entity<APIDepartment> {
  constructor(
    id: Identifier,
    public name: string,
    public abbrev: string
  ) {
    super(id)
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      abbrev: this.abbrev,
    }
  }

  static fromJSON(department: APIDepartment): Department {
    return new Department(new Identifier(department.id), department.name, department.abbrev)
  }
}
