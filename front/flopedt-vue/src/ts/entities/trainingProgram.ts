import type { components } from '@/ts/api/core/schema.generated.ts'

import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

export type APITrainingProgram = components['schemas']['TrainingProgram']

export class TrainingProgram extends Entity<APITrainingProgram> {
  constructor(
    id: Identifier,
    public name: string,
    public abbrev: string,
    public departmentId: number
  ) {
    super(id)
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      abbrev: this.abbrev,
      department_id: this.departmentId,
    }
  }

  static fromJSON(data: APITrainingProgram): TrainingProgram {
    return new TrainingProgram(new Identifier(data.id), data.name, data.abbrev, data.department_id)
  }
}
