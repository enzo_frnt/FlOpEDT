import type { components } from '@/ts/api/core/schema.generated.d.ts'

import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

export type APIMinimalUser = components['schemas']['ShortUsers']

export type APIUser = components['schemas']['User']

export class MinimalUser extends Entity<APIMinimalUser> {
  constructor(
    id: Identifier,
    public username: string,
    public firstName: string | undefined,
    public lastName: string | undefined,
    public email: string | undefined
  ) {
    super(id)
  }

  getFullName(): string {
    if (!this.firstName && !this.lastName) return this.username
    if (!this.firstName) return this.lastName!
    if (!this.lastName) return this.firstName
    return `${this.firstName} ${this.lastName}`
  }

  toJSON() {
    return {
      id: this.id,
      username: this.username,
      first_name: this.firstName,
      last_name: this.lastName,
      email: this.email,
    }
  }

  static fromJSON(user: APIMinimalUser) {
    return new MinimalUser(new Identifier(user.id), user.username, user.first_name, user.last_name, user.email)
  }
}

export class User extends MinimalUser implements Entity<APIUser> {
  constructor(
    id: Identifier,
    username: string,
    firstName: string | undefined,
    lastName: string | undefined,
    email: string | undefined,
    public rights: bigint,
    public departments: Array<{ departmentId: number; isAdmin: boolean }>
  ) {
    super(id, username, firstName, lastName, email)
  }

  hasPermissions(permissions: bigint) {
    return this.rights && (this.rights & permissions) === permissions
  }

  toJSON() {
    return {
      id: this.id,
      username: this.username,
      first_name: this.firstName,
      last_name: this.lastName,
      email: this.email,
      rights: Number(this.rights), // TODO: consider not using a number to avoid the 53bit limit
      departments: this.departments.map((d) => ({
        department_id: d.departmentId,
        is_admin: d.isAdmin,
      })),
    }
  }

  static fromJSON(user: APIUser) {
    return new User(
      new Identifier(user.id),
      user.username,
      user.first_name,
      user.last_name,
      user.email,
      BigInt(user.rights ?? 0),
      user.departments.map((d) => ({
        departmentId: d.department_id,
        isAdmin: d.is_admin,
      }))
    )
  }
}
