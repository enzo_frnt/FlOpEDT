import type { components } from '@/ts/api/core/schema.generated.d.ts'

import { Identifier } from '@/ts/entities/base/identifier.ts'
import { Entity } from '@/ts/entities/base/entity.ts'

type APIShortGroup = components['schemas']['StructuralShort']

type APIStructuralGroup = components['schemas']['StructuralGroup']
type APITransversalGroup = components['schemas']['TransversalGroup']
type APIGroup = APIStructuralGroup | APITransversalGroup

export class MinimalGroup extends Entity<APIShortGroup> {
  constructor(
    id: Identifier,
    public name: string
  ) {
    super(id)
  }

  toJSON() {
    return { id: this.id, name: this.name }
  }

  static fromJSON(data: APIShortGroup) {
    return new MinimalGroup(new Identifier(data.id), data.name)
  }
}

abstract class BaseGroup<T extends APIGroup> extends Entity<T> {
  protected constructor(
    id: Identifier,
    public name: string,
    public fullName: string,
    public size: number,
    public typeId: number | null,
    public trainingProgramId: number,
    public departmentId: number
  ) {
    super(id)
  }

  toFormDisplayValue(): string {
    return this.fullName
  }

  isStructural(): this is StructuralGroup {
    return this instanceof StructuralGroup
  }

  isTransversal(): this is TransversalGroup {
    return this instanceof TransversalGroup
  }
}

export class StructuralGroup extends BaseGroup<APIStructuralGroup> {
  constructor(
    id: Identifier,
    name: string,
    fullName: string,
    size: number,
    typeId: number | null,
    trainingProgramId: number,
    departmentId: number,
    public parentId: number | null,
    public isBasic: boolean
  ) {
    super(id, name, fullName, size, typeId, trainingProgramId, departmentId)
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      full_name: this.fullName,
      size: this.size,
      type: this.typeId,
      train_prog: this.trainingProgramId,
      department: this.departmentId,
      parent_group: this.parentId,
      basic: this.isBasic,
    }
  }

  static fromJSON(data: APIStructuralGroup) {
    return new StructuralGroup(
      new Identifier(data.id),
      data.name,
      data.full_name,
      data.size,
      data.type ?? null,
      data.train_prog,
      data.department,
      data.parent_group,
      data.basic ?? false
    )
  }
}

export class TransversalGroup extends BaseGroup<APITransversalGroup> {
  constructor(
    id: Identifier,
    name: string,
    fullName: string,
    size: number,
    typeId: number | null,
    trainingProgramId: number,
    departmentId: number,
    public conflictingGroups: number[],
    public parallelGroups: number[]
  ) {
    super(id, name, fullName, size, typeId, trainingProgramId, departmentId)
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      full_name: this.fullName,
      size: this.size,
      type: this.typeId,
      train_prog: this.trainingProgramId,
      department: this.departmentId,
      conflicting_groups: this.conflictingGroups,
      parallel_groups: this.parallelGroups,
    }
  }

  static fromJSON(data: APITransversalGroup) {
    return new TransversalGroup(
      new Identifier(data.id),
      data.name,
      data.full_name,
      data.size,
      data.type ?? null,
      data.train_prog,
      data.department,
      data.conflicting_groups ?? [],
      data.parallel_groups ?? []
    )
  }
}

export type Group = StructuralGroup | TransversalGroup
