<script setup lang="ts">
import type { Course, Dependency, TrainingPeriod } from '@/ts/type'

import { computed, onMounted, provide, Ref, ref } from 'vue'
import { storeToRefs } from 'pinia'
import { useAuthStore } from '@/stores/auth'
import { useDepartmentsStore } from '@/stores/departments.ts'
import { useGroupsStore } from '@/stores/departments/groups.ts'

import { useCourseStore } from '@/stores/timetable/course'
import { useRoomStore } from '@/stores/timetable/room'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useCourseModuleStore } from '@/stores/timetable/courseModule'
import { useCourseTypeStore } from '@/stores/timetable/courseType'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'
import { usePeriodStore } from '@/stores/timetable/period'
import { useTrainingPeriodStore } from '@/stores/timetable/trainingPeriod'
import { useDependencyStore } from '@/stores/timetable/dependency'
import {
  PseudoGlobalDependency,
  baseGlobalDependencies,
} from '@/components/management/dependencies/tempGlobalDependencies'

import { FilteredCourses, SelectedFilter, useFilteredCourses } from '@/composables/filterCourses'

import FilterSelector from '@/components/management/filters/FilterSelector.vue'
import CoursesWrapper from '@/components/management/courses/CoursesWrapper.vue'
import CourseInformation from '@/components/management/detailed/CourseInformation.vue'
import CourseModification from '@/components/forms/CourseModification.vue'
import SingleCourseCreationDialog from '@/components/forms/courseCreation/SingleCourseCreationDialog.vue'
import MultipleCourseCreationDialog from '@/components/forms/courseCreation/MultipleCourseCreationDialog.vue'
import AlertDialog, {
  AlertDialogColor,
  AlertDialogInstance,
  AlertDialogType,
} from '@/components/uikit/dialog/AlertDialog.vue'
import Dialog from '@/components/uikit/dialog/Dialog.vue'
import DialogButton from '@/components/uikit/dialog/DialogButton.vue'
import SelectInput from '@/components/uikit/form/input/SelectInput.vue'
import DependenciesInterface from '@/components/management/dependencies/DependenciesInterface.vue'
import { ColorMode } from '@/components/management/courses/CourseItems.vue'
import SelectItem from '@/components/uikit/form/select/SelectItem.vue'
import Legend, { LegendItem } from '@/components/management/infos/Legend.vue'
import SplitterGroup from '@/components/uikit/splitter/SplitterGroup.vue'
import SplitterPanel from '@/components/uikit/splitter/SplitterPanel.vue'
import SplitterResizeHandle from '@/components/uikit/splitter/SplitterResizeHandle.vue'

import PublishableToast, { PublishableToastInstance } from '@/components/uikit/PublishableToast.vue'
import { ToastAppearance } from '@/components/uikit/Toast.vue'
import ToggleButton from '@/components/uikit/button/ToggleButton.vue'

const auth = useAuthStore()
const deptStore = useDepartmentsStore()

const courseStore = useCourseStore()
const { isLoading, courses } = storeToRefs(courseStore)
const selectedCourseStore = useSelectedCourseStore()
const trainingPeriodStore = useTrainingPeriodStore()

// TODO: Delete when filter by dept id works
const deptTrainingPeriods: Ref<TrainingPeriod[]> = computed(() =>
  trainingPeriodStore.trainingPeriods.filter(
    (trainingPeriod) => trainingPeriod.department === deptStore.current.id.valueOf()
  )
)

const currentTrainingPeriodName = ref<string>('Aucune')
const allowedPeriods: Ref<number[] | undefined> = computed(
  () =>
    deptTrainingPeriods.value.find((trainingPeriod) => trainingPeriod.name === currentTrainingPeriodName.value)?.periods
)

const dependencyStore = useDependencyStore()

const filters = ref<SelectedFilter[]>([])
const { filtered, entities } = useFilteredCourses(courses, auth.user.id.valueOf(), filters, allowedPeriods)

const { selectedCourses, isModifying } = storeToRefs(selectedCourseStore)

const openedDependencies = ref<boolean>(false)

const globalDependencies = ref<PseudoGlobalDependency[]>([])

provide('globalDependencies', globalDependencies)

const enum DeletionState {
  IDLE,
  CONFIRM,
  IN_PROGRESS,
  ERRORED,
  SUCCESS,
}

const dialogDeleteConfirm = ref<AlertDialogInstance>()
const toastDeleteSuccess = ref<PublishableToastInstance>()

const courseDeletionState = ref(DeletionState.IDLE)
const courseDeletionErrors = ref<Error[]>([])

const courseColorMode = ref<ColorMode>(ColorMode.COURSE_TYPE)
const courseColorModes = computed(() => Object.values(ColorMode))

function traverse(nodes: FilteredCourses): Course[] {
  return nodes.map((e) => (e.leaf ? e.children : traverse(e.children))).flat()
}

const filteredCourses = computed(() => traverse(filtered.value))
const legendItems = computed(() => {
  let colors: Array<LegendItem> = []

  if (courseColorMode.value === ColorMode.COURSE_TYPE) {
    colors = filteredCourses.value.map((course) => ({
      id: course.type.id,
      name: course.type.name,
      color: `var(--color-${course.type.name})`,
    }))
  } else if (courseColorMode.value === ColorMode.MODULE) {
    colors = filteredCourses.value.map((course) => ({
      id: course.module.id,
      name: course.module.abbrev,
      color: course.module.display.color_bg,
    }))
  }

  const seen = new Set<number>()
  return colors.filter(({ id }) => {
    if (seen.has(id)) return false
    seen.add(id)
    return true
  })
})

async function deleteCourses() {
  courseDeletionState.value = DeletionState.IN_PROGRESS

  const errors: Error[] = []
  const stillSelected: Course[] = []

  for (const course of selectedCourses.value) {
    // Collect all dependencies related to the course
    const relatedDependencies: Dependency[] = dependencyStore.dependencies.filter(
      (dependency) => course.id === dependency.course1 || course.id === dependency.course2
    )

    try {
      for (const relatedDependency of relatedDependencies) {
        await dependencyStore.deleteDependency(relatedDependency)
      }

      await courseStore.deleteCourse(course)
    } catch (e) {
      stillSelected.push(course)
      if (e instanceof Error) errors.push(e)
    }
  }

  if (!errors.length) {
    dialogDeleteConfirm.value?.closeDialog()
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call -- typescript eslint is weird
    toastDeleteSuccess.value?.publish()
    courseDeletionState.value = DeletionState.IDLE
    selectedCourses.value = []
    return
  }

  dialogDeleteConfirm.value?.closeDialog()
  courseDeletionState.value = DeletionState.ERRORED
  courseDeletionErrors.value = errors
  selectedCourses.value = stillSelected
}

onMounted(() => {
  const groupsStore = useGroupsStore()

  const courseTypeStore = useCourseTypeStore()
  const roomStore = useRoomStore()
  const moduleStore = useCourseModuleStore()
  const tutorStore = useTutorStore()
  const periodStore = usePeriodStore()
  const dependencyStore = useDependencyStore()

  // Fetch courses
  void courseStore.fetchAllCourses(deptStore.current.abbrev)

  // TODO: Replace by fetchTrainingPeriods(deptStore.current) when the filter works
  void trainingPeriodStore.fetchTrainingPeriods()

  // Fetch related data
  void groupsStore.fetchAll(deptStore.current.id.valueOf())

  // Old
  void courseTypeStore.fetchAllCourseTypes(deptStore.current.abbrev)
  void roomStore.fetchRooms({ ...deptStore.current, id: deptStore.current.id.valueOf() })
  void moduleStore.fetchAllCourseModules(deptStore.current.abbrev)
  void tutorStore.fetchTutors()
  void periodStore.fetchPeriods()
  void dependencyStore.fetchDependencies()

  globalDependencies.value = baseGlobalDependencies
})
</script>

<template>
  <div v-if="isLoading" class="loading">
    {{ $t('common.loading') }}
  </div>
  <div v-else class="container">
    <div class="filters-column">
      <FilterSelector
        v-model="filters"
        :available-modules="entities.modules"
        :available-groups="entities.groups"
        :available-teachers="entities.teachers"
        :available-periods="entities.periods"
        :available-room-types="entities.roomTypes"
        :available-course-types="entities.courseTypes"
      />
      <div class="legend">
        <div class="legend-title">{{ $t('management.legend') }}</div>
        <div class="legend-container">
          <Legend :items="legendItems" />
        </div>
      </div>
    </div>
    <div class="courses-container">
      <div class="options-bar">
        <div class="left-options">
          <div class="option">
            <div class="option-title">
              {{ $t('management.option.color') }}
            </div>
            <div class="color-mode-select">
              <SelectInput v-model="courseColorMode">
                <SelectItem v-for="mode in courseColorModes" :key="mode" :value="mode" />
              </SelectInput>
            </div>
          </div>
          <div class="option">
            <div class="option-title">Training Period:</div>
            <div class="training-period-select">
              <SelectInput v-model="currentTrainingPeriodName">
                <SelectItem :key="-1" value="Aucune" />
                <SelectItem
                  v-for="trainingPeriod in deptTrainingPeriods"
                  :key="trainingPeriod.id"
                  :value="trainingPeriod.name"
                />
              </SelectInput>
            </div>
          </div>
        </div>
        <div class="right-options">
          <SingleCourseCreationDialog :allowed-periods="allowedPeriods" />
          <MultipleCourseCreationDialog :allowed-periods="allowedPeriods" />
          <ToggleButton v-model="openedDependencies">
            <template v-if="openedDependencies">Fermer les dépendances</template>
            <template v-else>Ouvrir les dépendances</template>
          </ToggleButton>
        </div>
      </div>

      <!-- FIXME: Repair broken lazy scroller when DependenciesInterface is displayed -->
      <SplitterGroup v-if="openedDependencies" id="splitter-group-1" direction="vertical">
        <SplitterPanel id="splitter-panel-1" class="dependencies-container" :min-size="20">
          <DependenciesInterface :color-mode="courseColorMode" />
        </SplitterPanel>
        <SplitterResizeHandle id="splitter-handle-1" />
        <SplitterPanel id="splitter-panel-2" :min-size="20" style="overflow-y: auto">
          <CoursesWrapper :courses="filtered" :filters="filters" :color-mode="courseColorMode" />
        </SplitterPanel>
      </SplitterGroup>

      <CoursesWrapper v-else :courses="filtered" :filters="filters" :color-mode="courseColorMode" />
    </div>
    <div v-if="selectedCourses.length > 0" class="visualization-column">
      <CourseModification v-if="isModifying" :allowed-periods="allowedPeriods" />
      <CourseInformation v-else @delete-courses="courseDeletionState = DeletionState.CONFIRM" />
    </div>
  </div>

  <AlertDialog
    ref="dialogDeleteConfirm"
    :type="AlertDialogType.QUESTION"
    :color="AlertDialogColor.DANGER"
    :open="courseDeletionState === DeletionState.CONFIRM || courseDeletionState === DeletionState.IN_PROGRESS"
    :processing="courseDeletionState === DeletionState.IN_PROGRESS"
    @action.prevent="deleteCourses"
  >
    <template #title>{{ $t('common.areYouSure') }}</template>

    <template #content>
      <div>{{ $t('management.courses.suppression.confirmation.header', courses.length) }}</div>
      <ul>
        <li v-for="course in selectedCourses" :key="course.id">
          {{
            $t('management.courses.suppression.confirmation.summary', {
              type: course.type.name,
              duration: course.duration,
              module: course.module.abbrev,
              tutor: course.tutor?.username ?? $t('management.courses.undefined'),
              period: course.period?.name ?? $t('management.courses.undefined'),
              groups: course.groups.map((g) => g.name).join(', '),
            })
          }}
        </li>
      </ul>
      <div>{{ $t('management.courses.suppression.confirmation.footer') }}</div>
    </template>

    <template #cancel>{{ $t('form.cancel') }}</template>
    <template #action>{{ $t('common.delete') }}</template>
  </AlertDialog>

  <Dialog
    :open="courseDeletionState === DeletionState.ERRORED"
    @update:open="!$event && (courseDeletionState = DeletionState.IDLE)"
  >
    <template #title>Une erreur est survenue</template>
    <template #description>
      <ul>
        <li v-for="error in courseDeletionErrors" :key="error.message">
          {{ error.message }}
        </li>
      </ul>
    </template>
    <template #footer>
      <DialogButton close>Fermer</DialogButton>
    </template>
  </Dialog>

  <PublishableToast ref="toastDeleteSuccess" :appearance="ToastAppearance.SUCCESS">
    <template #title>{{ $t('management.courses.suppression.success.title') }}</template>
    <template #description>{{ $t('management.courses.suppression.success.description') }}</template>
  </PublishableToast>
</template>

<style scoped>
.loading,
.container {
  /* 4.5rem is the height of the top navbar. Make sure to update if if required! */
  height: calc(100vh - 4.5rem);
  border-top: 1px currentColor solid;
}

.loading {
  display: flex;
  align-items: center;
  justify-content: center;
}

.container {
  display: flex;
}

.filters-column,
.visualization-column {
  flex-shrink: 0;
  width: 300px;
  padding: 1rem;
  overflow-y: auto;
}

.filters-column {
  display: flex;
  flex-direction: column;
  border-right: 1px currentColor solid;
  justify-content: space-between;
}

.visualization-column {
  border-left: 1px currentColor solid;
  margin-left: -1px;
}

.courses-container {
  display: flex;
  flex-direction: column;
  flex: 1;

  /* flexbox wizardry - prevent some unfortunate properties of flexbox to show up uninvited */
  min-width: 0;
}

.options-bar {
  display: flex;
  padding: 0.5rem;
  border-bottom: 1px currentColor solid;
  justify-content: space-between;
}

.left-options,
.right-options,
.option {
  display: flex;
  align-items: center;
}

.left-options,
.right-options {
  gap: 1rem;
}

.option {
  gap: 0.5rem;
}

.left-options {
  padding-left: 8px;
}

.right-options {
  padding-right: 8px;
}

.option-title {
  font-weight: bold;
}

.legend {
  border-top: 1px currentColor solid;
  margin: 16px -16px;
}

.legend-container {
  height: 300px;
  overflow-y: auto;
  padding: 8px;
}

.legend-title {
  font-size: 20px;
  padding: 8px;
  font-weight: bold;
  position: sticky;
  top: 0;
}

.dependencies-container {
  position: relative;
  border-bottom: 1px currentColor solid;
}
</style>
