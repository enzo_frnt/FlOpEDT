// https://router.vuejs.org/guide/advanced/meta.html#TypeScript
import 'vue-router'
declare module 'vue-router' {
  interface RouteMeta {
    // Note: no effect if flop is configured with mandatory authentication
    requiresAuth: boolean
    undoredoEnabled?: boolean
  }
}
