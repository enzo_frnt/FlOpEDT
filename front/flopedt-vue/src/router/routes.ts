import { type RouteRecordRaw, RouterView } from 'vue-router'
import { handleRouteDepartment } from '@/stores/departments.ts'

// Enforce the presence of route metadata everywhere
type ObjectWithMetadata = {
  meta: object
  children?: ObjectWithMetadata[]
  [key: string]: unknown
}

export const RouteNames = {
  HOME: 'home',
  CONTACT: 'contact',
  DEPARTMENT_SELECTION: 'department.select',

  // Department-related
  DEPARTMENT_SCHEDULE: 'department.schedule',
  DEPARTMENT_ICAL: 'department.ical',
  DEPARTMENT_ROOM_RESERVATION: 'department.room-reservation',

  // Department management
  DEPARTMENT_MANAGEMENT: 'department.management.base',
  DEPARTMENT_MANAGEMENT_GENERATION: 'department.management.generation', // cst + solver
  DEPARTMENT_MANAGEMENT_COURSES: 'department.management.courses',
  DEPARTMENT_MANAGEMENT_MODULES: 'department.management.modules',
  DEPARTMENT_MANAGEMENT_GROUPS: 'department.management.groups',
  DEPARTMENT_MANAGEMENT_TUTORS: 'department.management.tutors',
  DEPARTMENT_MANAGEMENT_STUDENTS: 'department.management.students',
  DEPARTMENT_MANAGEMENT_ROOMS: 'department.management.rooms',
  DEPARTMENT_MANAGEMENT_ROOM_TYPES: 'department.management.room-types',
  DEPARTMENT_MANAGEMENT_BREAKING_NEWS: 'department.management.breaking-news',

  // Global management
  MANAGEMENT: 'management.base',
  MANAGEMENT_DEPARTMENTS: 'management.departments',
  MANAGEMENT_ROOMS: 'management.rooms', // Global version of dept rooms
  MANAGEMENT_ROOM_ATTRIBUTES: 'management.room-attributes',
  MANAGEMENT_BREAKING_NEWS: 'management.breaking-news',
  MANAGEMENT_ACCOUNTS: 'management.accounts', // Global version of dept tutors + students

  // Account-related
  LOGIN: 'login',
  MY_ACCOUNT: 'self.account',
  MY_ACCOUNT_SETTINGS: 'self.settings',
}

export const routes = [
  {
    path: '/',
    name: RouteNames.HOME,
    component: () => import('@/views/HomeView.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/select-department',
    name: RouteNames.DEPARTMENT_SELECTION,
    // eslint-disable-next-line -- TODO
    component: async () => { throw new Error('TODO!') },
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/:dept',
    beforeEnter: handleRouteDepartment,
    component: RouterView,
    children: [
      {
        path: '/:dept',
        name: RouteNames.DEPARTMENT_SCHEDULE,
        component: () => import('@/views/ScheduleView.vue'),
        meta: {
          requiresAuth: false,
        },
      },
      {
        path: '/:dept/ical',
        name: RouteNames.DEPARTMENT_ICAL,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: false,
        },
      },
      {
        path: '/:dept/room-reservation',
        name: RouteNames.DEPARTMENT_ROOM_RESERVATION,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/:dept/management',
        component: RouterView,
        children: [
          {
            path: '/:dept/management',
            name: RouteNames.DEPARTMENT_MANAGEMENT,
            component: () => import('@/views/department/DepartmentManagementView.vue'),
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/generation',
            name: RouteNames.DEPARTMENT_MANAGEMENT_GENERATION,
            component: () => import('@/views/department/GenerationSettingsView.vue'),
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/courses',
            name: RouteNames.DEPARTMENT_MANAGEMENT_COURSES,
            component: () => import('@/views/department/CourseManagementView.vue'),
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/modules',
            name: RouteNames.DEPARTMENT_MANAGEMENT_MODULES,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/groups',
            name: RouteNames.DEPARTMENT_MANAGEMENT_GROUPS,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/tutors',
            name: RouteNames.DEPARTMENT_MANAGEMENT_TUTORS,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/students',
            name: RouteNames.DEPARTMENT_MANAGEMENT_STUDENTS,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/room-types',
            name: RouteNames.DEPARTMENT_MANAGEMENT_ROOM_TYPES,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/rooms',
            name: RouteNames.DEPARTMENT_MANAGEMENT_ROOMS,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
          {
            path: '/:dept/management/breaking-news',
            name: RouteNames.DEPARTMENT_MANAGEMENT_BREAKING_NEWS,
            // eslint-disable-next-line -- TODO
            component: async () => { throw new Error('TODO!') },
            meta: {
              requiresAuth: true,
            },
          },
        ],
        meta: {
          requiresAuth: true,
        },
      },
    ],
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/management',
    component: RouterView,
    children: [
      {
        path: '/management',
        name: RouteNames.MANAGEMENT,
        component: () => import('@/views/management/ManagementView.vue'),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/management/departments',
        name: RouteNames.MANAGEMENT_DEPARTMENTS,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/management/rooms',
        name: RouteNames.MANAGEMENT_ROOMS,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/management/room-attributes',
        name: RouteNames.MANAGEMENT_ROOM_ATTRIBUTES,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/management/breaking-news',
        name: RouteNames.MANAGEMENT_BREAKING_NEWS,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/management/accounts',
        name: RouteNames.MANAGEMENT_ACCOUNTS,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/account',
    component: RouterView,
    children: [
      {
        path: '/account',
        name: RouteNames.MY_ACCOUNT,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: '/account/settings',
        name: RouteNames.MY_ACCOUNT_SETTINGS,
        // eslint-disable-next-line -- TODO
        component: async () => { throw new Error('TODO!') },
        meta: {
          requiresAuth: true,
        },
      },
    ],
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/login',
    name: RouteNames.LOGIN,
    component: () => import('@/views/LoginView.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/contact',
    name: RouteNames.CONTACT,
    component: () => import('@/views/ContactView.vue'),
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/:pathMatch(.*)',
    component: () => import('@/views/NotFoundView.vue'),
    meta: {
      requiresAuth: false,
    },
  },
] as const satisfies RouteRecordRaw[] satisfies ObjectWithMetadata[]
