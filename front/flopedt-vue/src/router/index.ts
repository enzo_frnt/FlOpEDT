import { createRouter, createWebHistory } from 'vue-router'
import { storeToRefs } from 'pinia'
import { useAuthStore } from '@/stores/auth'
import { usePermanentStore } from '@/stores/timetable/permanent'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useRoomStore } from '@/stores/timetable/room'

import { RouteNames, routes } from './routes'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  scrollBehavior: () => ({ top: 0 }),
  routes: routes,
})

router.beforeEach(async (to, from, next) => {
  const roomStore = useRoomStore()
  const authStore = useAuthStore()
  const permanentStore = usePermanentStore()
  const tutorStore = useTutorStore()
  const { isModulesFetched } = storeToRefs(permanentStore)
  if (!isModulesFetched.value) await permanentStore.fetchModules()
  if (!tutorStore.isAllTutorsFetched) await tutorStore.fetchTutors()
  if (!roomStore.isRoomFetched) await roomStore.fetchRooms()
  if (!permanentStore.areTimeSettingsFetched) await permanentStore.fetchTimeSettings()

  if (to.meta.requiresAuth && !authStore.isAuthenticated) {
    return next({ name: RouteNames.LOGIN, query: { next: to.fullPath } })
  } else {
    next()
  }
})

export default router
