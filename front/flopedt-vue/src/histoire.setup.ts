import { i18n } from '@/i18n'
import { createPinia } from 'pinia'
import { defineSetupVue3 } from '@histoire/plugin-vue'
import { Quasar } from 'quasar'

import AppWrapper from '@/AppWrapper.vue'
import './styles/histoire.css'

// noinspection JSUnusedGlobalSymbols
export const setupVue3 = defineSetupVue3(({ app, addWrapper }) => {
  addWrapper(AppWrapper)

  const pinia = createPinia()
  app.use(pinia)
  app.use(i18n)
  app.use(Quasar, {
    plugins: {},
  })
})
