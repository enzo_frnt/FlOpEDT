<script lang="ts">
import type { FilteredCourseLeaf, FilteredCourseNode } from '@/composables/filterCourses'

export type DisplayInformation = {
  type: boolean
  module: boolean
  teacher: boolean
  group: boolean
  period: boolean
}

export type FilteredCourseLeafLayout = {
  leaf: true
  children: FilteredCourseLeaf['children']
  totalTime: { hours: number; minutes: number }

  height: number
  itemWidth: number
  visible: true

  skipped: number
  rendered: number
  topFiller: number
  bottomFiller: number
  key: string
}

export type FilteredCourseNodeLayout = {
  leaf: false
  filter: FilteredCourseNode['filter']
  entity: FilteredCourseNode['entity']
  children: FilteredCourseColumns
  totalTime: { hours: number; minutes: number }

  visible: boolean
  height: number
  key: string
}

export type FilteredCourseNodeLayoutColumns = {
  leaf: false
  left: FilteredCourseNodeLayout[]
  right: FilteredCourseNodeLayout[]
  height: number
  visible: boolean
}

export type FilteredCourseColumns = FilteredCourseNodeLayoutColumns | FilteredCourseLeafLayout
</script>

<script setup lang="ts">
import type { FilteredCourses, SelectedFilter } from '@/composables/filterCourses'

import { computed, onBeforeUnmount, ref, watch } from 'vue'
import { FilterType } from '@/composables/filterCourses'
import CoursesDisplay from '@/components/management/courses/CoursesDisplay.vue'
import { ColorMode } from '@/components/management/courses/CourseItems.vue'

function computeTextProperties(fontSize: number, lineHeight: number, padding: number) {
  const div = document.createElement('div')
  const span = document.createElement('span')
  div.style.position = 'absolute'
  div.style.padding = `${padding}px`
  span.style.width = '100%'
  span.style.position = 'absolute'
  span.style.fontSize = `${fontSize}px`
  span.style.lineHeight = lineHeight.toString()
  span.innerText = 'meow'

  div.appendChild(span)
  document.body.appendChild(div)
  const { x, height } = span.getBoundingClientRect()
  div.remove()

  return { height, subpixelOffset: Math.abs(padding - x) }
}

const REM = parseFloat(getComputedStyle(document.documentElement).fontSize)
const CONTAINER_PADDING_V = REM
const CONTAINER_PADDING_H = REM
const BLOCK_GAP = 0.5 * REM
const BLOCK_PADDING_V = 0.25 * REM
const BLOCK_PADDING_H = 0.5 * REM
const BLOCK_TITLE_FONT_SIZE = 1.1 * REM
const BLOCK_TITLE_LINE_HEIGHT = 1.15
const BLOCK_TITLE_MARGIN_BOTTOM = 0.5 * REM
const ITEM_MIN_WIDTH = 3.5 * REM
const ITEM_GAP = 0.25 * REM
const ITEM_PADDING_V = 0.2 * REM + 1 // +1 to account for the border as padding
// const ITEM_PADDING_H = 0.25 * REM + 1 // +1 to account for the border as padding
const ITEM_FONT_SIZE = 0.8 * REM
const ITEM_LINE_HEIGHT = 1.15

// For inline elements, knowing the line height and the font size aren't enough to determine the final height.
// In addition, some engines like Blink may apply very small subpixel offsets depending on the container's properties.
// To reliably compute these properties, we use a hidden test element that will be rendered by the engine, so we can
// retrieve its height, subpixel offset values, etc. once rendered by the current engine.
const ITEM_TEXT_PROPERTIES = computeTextProperties(ITEM_FONT_SIZE, ITEM_LINE_HEIGHT, ITEM_PADDING_V)
const BLOCK_TEXT_PROPERTIES = computeTextProperties(BLOCK_TITLE_FONT_SIZE, BLOCK_TITLE_LINE_HEIGHT, BLOCK_PADDING_V)

const MINIMUM_BLOCK_WIDTH = ITEM_MIN_WIDTH + BLOCK_PADDING_H * 2

// Artificially expand the visible scroller area to reduce a bit of flickers
const SCROLLER_AREA_SIZE_MARGIN = 300

type Props = {
  filters: SelectedFilter[]
  courses: FilteredCourses
  colorMode: ColorMode
}

const props = defineProps<Props>()

const containerRef = ref<HTMLDivElement>()
const containerWidth = ref(0)
const containerHeight = ref(0)
const containerScrollY = ref(0)

const displayed = computed(() => {
  const displayed: DisplayInformation = {
    type: props.colorMode !== ColorMode.COURSE_TYPE,
    module: props.colorMode !== ColorMode.MODULE,
    teacher: true,
    group: true,
    period: true,
  }

  for (const { filter } of props.filters) {
    switch (filter) {
      case FilterType.COURSE_TYPE:
        displayed.type = false
        break
      case FilterType.MODULE:
        displayed.module = false
        break
      case FilterType.TEACHER:
        displayed.teacher = false
        break
      case FilterType.GROUP:
        displayed.group = false
        break
      case FilterType.PERIOD:
        displayed.period = false
        break
    }
  }

  return displayed
})

function computeLayoutOfLeaf(courses: FilteredCourseLeaf, width: number, y: number): FilteredCourseLeafLayout {
  // Step 0. compute a key for the leaf
  // This will be used by Vue to avoid re-rendering stuff when nothing changed
  const key = courses.children.map((c) => c.id).join('-')

  // Step 1. compute the height (and width) of an item
  // We need to do it dynamically as it varies based on filters.
  const lines =
    +displayed.value.type +
    +displayed.value.module +
    +displayed.value.teacher +
    +displayed.value.group +
    +displayed.value.period

  // Height calculation is a bit tricky due to how rendering engines work.
  // For inline elements, knowing the line height and the font size aren't enough to determine the final height.
  // Engines like Blink may in addition place the box with a very small subpixel offset.
  const itemHeight = lines
    ? ITEM_PADDING_V * 2 - // top and bottom padding
      ITEM_TEXT_PROPERTIES.subpixelOffset * 2 + // subpixel text offsets
      lines * ITEM_TEXT_PROPERTIES.height // lines of text
    : REM // If there is no text, sise is enforced to be 1rem

  // We don't need to consider the real width. If an item is expanded to fill in empty space, that empty space
  // is caused because there is not enough room to put another item anyway - so the min width is a good estimate.
  // Just like height, when there is no text the size is enforced to be 1rem.
  const itemWidth = lines ? ITEM_MIN_WIDTH : REM

  // Step 2. compute the amount of items that fits on a single line
  // To make the calculation simpler, we assume there will always be enough room for a single element
  // If it isn't the case, the UI would be pretty broken anyway
  const availableWidth = Math.max(0, width - itemWidth)
  const extraItems = (availableWidth / (itemWidth + ITEM_GAP)) | 0
  const itemsPerLine = extraItems + 1

  // Step 3. compute the number of lines needed to display all the items
  const linesRequired = Math.ceil(courses.children.length / itemsPerLine)
  const height = linesRequired * itemHeight + (linesRequired - 1) * ITEM_GAP

  // Step 4. compute the items that actually will be displayed
  // The calculations are cheap enough we don't really need to failfast.
  const scrollTopY = containerScrollY.value - SCROLLER_AREA_SIZE_MARGIN
  const scrollBottomY = containerScrollY.value + containerHeight.value + SCROLLER_AREA_SIZE_MARGIN

  const itemHeightPlusGap = itemHeight + ITEM_GAP
  const rawSkippedLines = ((scrollTopY - y) / itemHeightPlusGap) | 0
  const linesOffset = Math.min(0, rawSkippedLines)
  const skippedLines = Math.max(0, rawSkippedLines)
  const visibleLines = Math.ceil(Math.max(0, (scrollBottomY - scrollTopY) / itemHeightPlusGap)) + linesOffset
  const nextLines = Math.max(0, linesRequired - skippedLines - visibleLines)

  // Step 5. compute the width of the item blocks
  // This is done to bypass the grid calculation of the browser and save a bit on layout cost
  const leftoverWidth = width - (itemsPerLine * itemWidth + (itemsPerLine - 1) * ITEM_GAP)
  const itemRenderWidth = lines ? ITEM_MIN_WIDTH + leftoverWidth / itemsPerLine : REM

  return {
    leaf: true,
    children: courses.children,
    totalTime: courses.totalTime,
    height: height,
    itemWidth: itemRenderWidth - 0.05, // Subtract 0.05 to ensure consistent behavior. Some engines struggle otherwise
    skipped: skippedLines * itemsPerLine,
    rendered: visibleLines * itemsPerLine,
    topFiller: skippedLines ? skippedLines * itemHeight + skippedLines * ITEM_GAP : 0,
    bottomFiller: nextLines ? nextLines * itemHeight + nextLines * ITEM_GAP : 0,
    visible: true,
    key: key,
  }
}

function computeLayoutOfNode(
  node: FilteredCourseNode,
  width: number,
  y: number,
  depth: number
): FilteredCourseNodeLayout {
  // The title element won't have any subpixel offset
  const topBlockHeight = BLOCK_PADDING_V + BLOCK_TEXT_PROPERTIES.height + BLOCK_TITLE_MARGIN_BOTTOM
  const blockInnerWidth = width - BLOCK_PADDING_H * 2

  if (isLeaf(node.children)) {
    const leaf = computeLayoutOfLeaf(node.children[0], blockInnerWidth, y + topBlockHeight)

    return {
      leaf: false,
      children: leaf,
      entity: node.entity,
      filter: node.filter,
      totalTime: node.totalTime,

      height: leaf.height + topBlockHeight + BLOCK_PADDING_V,
      // If nothing in the leaf is rendered, we can safely assume the block is not in view
      // This avoids having to do extra calculations for this case
      visible: leaf.rendered !== 0,
      key: `${node.filter}:${node.entity?.id}:${leaf.key}`,
    }
  }

  const children = computeLayoutOfNodes(node.children, blockInnerWidth, y + topBlockHeight, depth + 1)

  return {
    leaf: false,
    children,
    entity: node.entity,
    filter: node.filter,
    totalTime: node.totalTime,
    height: children.height + topBlockHeight + BLOCK_PADDING_V,
    visible: children.visible,
    key: children.left.map((c) => c.key).join('-') + children.right.map((c) => c.key).join('-'),
  }
}

function computeLayoutOfNodes(
  nodes: FilteredCourseNode[],
  width: number,
  y: number,
  depth: number
): FilteredCourseNodeLayoutColumns {
  const leftColumn: FilteredCourseNodeLayout[] = []
  const rightColumn: FilteredCourseNodeLayout[] = []
  let leftColumnHeight = 0
  let rightColumnHeight = 0
  let isVisible = false

  const isSingleColumn = nodes.length === 1 || MINIMUM_BLOCK_WIDTH * 2.5 >= width
  const availableWidthPerColumn = isSingleColumn ? width : (width - BLOCK_GAP) / 2

  for (let i = 0; i < nodes.length; i++) {
    const isRightColumn = i % 2 && !isSingleColumn
    const currentHeight = isRightColumn ? rightColumnHeight : leftColumnHeight
    const gap = currentHeight ? BLOCK_GAP : 0

    const node = nodes[i]
    const layout = computeLayoutOfNode(node, availableWidthPerColumn, y + gap + currentHeight, depth)
    isVisible = isVisible || layout.visible

    if (isRightColumn) {
      rightColumn.push(layout)
      rightColumnHeight += gap + layout.height
    } else {
      leftColumn.push(layout)
      leftColumnHeight += gap + layout.height
    }
  }

  return {
    leaf: false,
    left: leftColumn,
    right: rightColumn,
    height: Math.max(leftColumnHeight, rightColumnHeight),
    visible: isVisible,
  }
}

/*@__INLINE__*/
const isLeaf = (nodes: FilteredCourses): nodes is [FilteredCourseLeaf] => nodes[0]?.leaf

const coursesWithLayout = computed((): FilteredCourseColumns | null => {
  if (!containerWidth.value) return null

  const availableWidth = containerWidth.value - CONTAINER_PADDING_H * 2
  if (isLeaf(props.courses)) {
    return computeLayoutOfLeaf(props.courses[0], availableWidth, CONTAINER_PADDING_V)
  }

  return computeLayoutOfNodes(props.courses, availableWidth, CONTAINER_PADDING_V, 0)
})

function updateDimensions() {
  const div = containerRef.value
  if (!div) return

  const rect = div.getBoundingClientRect()

  containerWidth.value = div.scrollWidth
  containerHeight.value = rect.height
}

function scrollHandler(e: Event) {
  if (e.target instanceof HTMLElement) {
    containerScrollY.value = ((e.target.scrollTop / 50) | 0) * 50
  }
}

const observer = new ResizeObserver(() => updateDimensions())
onBeforeUnmount(() => observer.disconnect())
watch(containerRef, (next, prev) => {
  if (prev) {
    prev.removeEventListener('scroll', scrollHandler)
    observer.unobserve(prev)
  }

  if (next) {
    next.addEventListener('scroll', scrollHandler)
    observer.observe(next)
    updateDimensions()
  }
})
</script>

<template>
  <div ref="containerRef" class="courses-lazy-container">
    <div v-if="coursesWithLayout" class="courses-wrapper" :style="{ height: `${coursesWithLayout.height}px` }">
      <CoursesDisplay :displayed="displayed" :courses="coursesWithLayout" :color-mode="colorMode" />
    </div>
  </div>
</template>

<style scoped>
/***************************************************************************************
                                   !!! WARNING !!!
     Changing anything here may affect the calculations of the lazy scroller!!!!!!!!
  MAKE SURE TO DO THE APPROPRIATE CHANGES IF THE LAYOUT CHANGES, EVEN BY A FEW PIXELS!!
 ***************************************************************************************/

.courses-lazy-container {
  flex: 1;
  padding: 1rem;
  overflow-y: auto;
}
</style>
