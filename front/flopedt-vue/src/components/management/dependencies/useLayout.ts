import dagre from '@dagrejs/dagre'
import { Position } from '@vue-flow/core'
import { ref } from 'vue'
import { DependencyGraph, CustomNode } from '@/composables/useDependencies'
import { GlobalDependencyEdgeI, GlobalNodeI } from '@/components/management/dependencies/DependenciesInterface.vue'

const simpleDagreGraph = ref(new dagre.graphlib.Graph())
const globalDagreGraph = ref(new dagre.graphlib.Graph())

export function layoutGroups(groupNodes: CustomNode[]): CustomNode[] {
  let currentXPosition = -700
  const positionedGroupNodes: CustomNode[] = []

  for (const groupNode of groupNodes) {
    positionedGroupNodes.push({ ...groupNode, position: { x: currentXPosition, y: 0 } })
    currentXPosition -= 700
  }

  return positionedGroupNodes
}

export function layoutAllGraphs(graphs: DependencyGraph[], baseY: number = 0): DependencyGraph[] {
  const maxNodesPerColumn = 5
  const nodeSpacing = 50
  const graphSpacing = 350
  const nodeHeight = 150

  const positionedGraphs: DependencyGraph[] = []
  const singleNodeGraphs: DependencyGraph[] = []
  const multiNodeGraphs: DependencyGraph[] = []

  // Collect all single nodes and multi-node graphs
  for (const graph of graphs) {
    if (graph.nodes.length === 1 && graph.nodes[0].type === 'course') {
      singleNodeGraphs.push(graph)
    } else {
      multiNodeGraphs.push(graph)
    }
  }

  // Calculate total height of single nodes grid
  const numRows = singleNodeGraphs.length >= 5 ? 5 : singleNodeGraphs.length
  const singleNodesGridHeight = numRows * (nodeHeight + nodeSpacing) - nodeSpacing

  // Calculate heights and vertical centers of multi-node graphs
  const graphHeights = multiNodeGraphs.map((graph) => {
    const selfPositionedGraph = layoutGraph(graph)
    const allY = selfPositionedGraph.nodes.map((node) => node.position.y)
    const minY = Math.min(...allY)
    const maxY = Math.max(...allY)
    const graphHeight = maxY - minY
    return { graph: selfPositionedGraph, height: graphHeight }
  })

  // Position single nodes grid centered at y = 0
  let currentYOffset = -singleNodesGridHeight / 2 + nodeHeight / 2
  let currentDepth = 0
  let currentXOffset = 0

  singleNodeGraphs.forEach((graph) => {
    graph.nodes[0].position = { x: currentXOffset, y: currentYOffset }

    currentYOffset += nodeHeight + nodeSpacing
    currentDepth++

    if (currentDepth >= maxNodesPerColumn) {
      currentXOffset += graphSpacing
      currentYOffset = -singleNodesGridHeight / 2 + nodeHeight / 2
      currentDepth = 0
    }
  })

  // Add the single nodes as a graph to the positionedGraphs
  if (singleNodeGraphs.length > 0) {
    positionedGraphs.push(...singleNodeGraphs)
  }

  // Reset offsets for multi-node graphs
  currentXOffset += graphSpacing

  let graphNumber: number = 1

  // Position multi-node graphs centered at y = 0
  for (const { graph, height } of graphHeights) {
    const minY = Math.min(...graph.nodes.map((node) => node.position.y))
    const verticalCenter = minY + height / 2

    const globalPositionedGraphNodes = graph.nodes.map((node) => ({
      ...node,
      position: {
        x: node.position.x + currentXOffset,
        y: node.position.y - verticalCenter + baseY,
      },
    }))

    positionedGraphs.push({ ...graph, nodes: globalPositionedGraphNodes, graphNumber: graphNumber++ })

    /* Calculate the total width of the current graph */
    const maxNodeX = Math.max(...globalPositionedGraphNodes.map((node) => node.position.x))
    const graphWidth = maxNodeX - currentXOffset

    /* Ensure there's a consistent spacing between graphs */
    currentXOffset += graphWidth + graphSpacing
  }

  return positionedGraphs
}

/* This function uses the same logic as the layout() function in the vue flow documentation */
function layoutGraph(dependencyGraph: DependencyGraph): DependencyGraph {
  const dagreGraph = new dagre.graphlib.Graph()

  simpleDagreGraph.value = dagreGraph

  dagreGraph.setDefaultEdgeLabel(() => ({}))
  dagreGraph.setGraph({ rankdir: 'LR' })

  for (const node of dependencyGraph.nodes) {
    if (node.type == 'course-group') dagreGraph.setNode(node.id, { width: 600, height: 150 })
    else dagreGraph.setNode(node.id, { width: 350, height: 150 })
  }

  for (const edge of dependencyGraph.edges) {
    dagreGraph.setEdge(edge.source, edge.target)
  }

  dagre.layout(dagreGraph)

  const newNodes: CustomNode[] = dependencyGraph.nodes.map((node) => {
    const nodeWithPosition = dagreGraph.node(node.id)
    return {
      ...node,
      targetPosition: Position.Left,
      sourcePosition: Position.Right,
      position: { x: nodeWithPosition.x, y: nodeWithPosition.y - 75 },
    }
  })

  return { ...dependencyGraph, nodes: newNodes } as DependencyGraph
}

/* This function uses the same logic as the layout() function in the vue flow documentation */
export function layoutGlobalNodes(globalNodes: GlobalNodeI[], globalEdges: GlobalDependencyEdgeI[]): GlobalNodeI[] {
  const dagreGraph = new dagre.graphlib.Graph()

  globalDagreGraph.value = dagreGraph

  dagreGraph.setDefaultEdgeLabel(() => ({}))
  dagreGraph.setGraph({ rankdir: 'LR' })

  for (const node of globalNodes) {
    dagreGraph.setNode(node.id, { width: 500, height: 250 })
  }

  for (const edge of globalEdges) {
    dagreGraph.setEdge(edge.source, edge.target)
  }

  dagre.layout(dagreGraph)

  return globalNodes.map((node) => {
    const nodeWithPosition = dagreGraph.node(node.id)
    return {
      ...node,
      targetPosition: Position.Left,
      sourcePosition: Position.Right,
      position: { x: nodeWithPosition.x, y: nodeWithPosition.y - 75 },
    }
  })
}
