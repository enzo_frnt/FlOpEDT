export interface PseudoGlobalDependency {
  id: number
  title: string
  name: 'GlobalModuleDependency'
  is_active: boolean
  parameters: [
    {
      name: 'department'
      type: 'base.Department'
      required: true
      multiple: false
      id_list: number[]
    },
    {
      name: 'day_gap'
      type: 'PositiveSmallIntegerField'
      required: true
      multiple: false
      id_list: number[]
    },
    {
      name: 'course1_type'
      type: 'base.CourseType'
      required: true
      multiple: false
      id_list: number[]
    },
    {
      name: 'course1_tutor'
      type: 'people.Tutor'
      required: false
      multiple: false
      id_list: number[]
    },
    {
      name: 'course2_type'
      type: 'base.CourseType'
      required: true
      multiple: false
      id_list: number[]
    },
    {
      name: 'course2_tutor'
      type: 'people.Tutor'
      required: false
      multiple: false
      id_list: number[]
    },
    {
      name: 'periods'
      type: 'base.SchedulingPeriod'
      required: false
      multiple: true
      id_list: number[]
    },
    {
      name: 'excepted_courses'
      type: 'base.Course'
      required: false
      multiple: true
      id_list: number[]
    },
    {
      name: 'modules'
      type: 'base.Module'
      required: false
      multiple: true
      id_list: number[]
    },
    {
      name: 'train_progs'
      type: 'base.TrainingProgramme'
      required: false
      multiple: true
      id_list: number[]
    },
    {
      name: 'groups'
      type: 'base.StructuralGroup'
      required: false
      multiple: true
      id_list: number[]
    },
  ]
}

export const baseGlobalDependencies: PseudoGlobalDependency[] = [
  {
    id: 1,
    title: 'PS-CM-TD',
    name: 'GlobalModuleDependency',
    is_active: true,
    parameters: [
      {
        name: 'department',
        type: 'base.Department',
        required: true,
        multiple: false,
        id_list: [103],
      },
      {
        name: 'day_gap',
        type: 'PositiveSmallIntegerField',
        required: true,
        multiple: false,
        id_list: [1],
      },
      {
        name: 'course1_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [664],
      },
      {
        name: 'course1_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [],
      },
      {
        name: 'course2_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [665],
      },
      {
        name: 'course2_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [],
      },
      {
        name: 'periods',
        type: 'base.SchedulingPeriod',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'excepted_courses',
        type: 'base.Course',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'modules',
        type: 'base.Module',
        required: false,
        multiple: true,
        id_list: [6231],
      },
      {
        name: 'train_progs',
        type: 'base.TrainingProgramme',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'groups',
        type: 'base.StructuralGroup',
        required: false,
        multiple: true,
        id_list: [],
      },
    ],
  },
  {
    id: 2,
    title: 'PS-TD-TP',
    name: 'GlobalModuleDependency',
    is_active: true,
    parameters: [
      {
        name: 'department',
        type: 'base.Department',
        required: true,
        multiple: false,
        id_list: [103],
      },
      {
        name: 'day_gap',
        type: 'PositiveSmallIntegerField',
        required: true,
        multiple: false,
        id_list: [0],
      },
      {
        name: 'course1_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [665],
      },
      {
        name: 'course1_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [],
      },
      {
        name: 'course2_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [666],
      },
      {
        name: 'course2_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [],
      },
      {
        name: 'periods',
        type: 'base.SchedulingPeriod',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'excepted_courses',
        type: 'base.Course',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'modules',
        type: 'base.Module',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'train_progs',
        type: 'base.TrainingProgramme',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'groups',
        type: 'base.StructuralGroup',
        required: false,
        multiple: true,
        id_list: [],
      },
    ],
  },
  {
    id: 3,
    title: 'PS-TP-DS-QTP',
    name: 'GlobalModuleDependency',
    is_active: true,
    parameters: [
      {
        name: 'department',
        type: 'base.Department',
        required: true,
        multiple: false,
        id_list: [103],
      },
      {
        name: 'day_gap',
        type: 'PositiveSmallIntegerField',
        required: true,
        multiple: false,
        id_list: [0],
      },
      {
        name: 'course1_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [666],
      },
      {
        name: 'course1_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [],
      },
      {
        name: 'course2_type',
        type: 'base.CourseType',
        required: true,
        multiple: false,
        id_list: [668],
      },
      {
        name: 'course2_tutor',
        type: 'people.Tutor',
        required: false,
        multiple: false,
        id_list: [129],
      },
      {
        name: 'periods',
        type: 'base.SchedulingPeriod',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'excepted_courses',
        type: 'base.Course',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'modules',
        type: 'base.Module',
        required: false,
        multiple: true,
        id_list: [6231],
      },
      {
        name: 'train_progs',
        type: 'base.TrainingProgramme',
        required: false,
        multiple: true,
        id_list: [],
      },
      {
        name: 'groups',
        type: 'base.StructuralGroup',
        required: false,
        multiple: true,
        id_list: [691, 694, 697],
      },
    ],
  },
]
