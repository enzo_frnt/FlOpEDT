/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument */
/* These are getting annoying because we're dealing with Vue internals, and their internal types are quite lax (understandably so, it's internal stuff) */

import { beforeEach, expect, it } from 'vitest'
import { mount, VueWrapper } from '@vue/test-utils'
import { FilterType, SelectedFilter } from '@/composables/filterCourses'
import FilterSelector from '@/components/management/filters/FilterSelector.vue'
import FilterElement from '@/components/management/filters/FilterElement.vue'
import DropdownMenu from '@/components/uikit/menu/DropdownMenu.vue'
import DropdownMenuItem from '@/components/uikit/menu/dropdown/DropdownMenuItem.vue'
import { ComboboxContent } from 'radix-vue'

const TEST_MODULES = [
  {
    id: 1,
    name: 'UE1',
  },
  {
    id: 2,
    name: 'UE2',
  },
  {
    id: 3,
    name: 'UE3',
  },
  {
    id: 4,
    name: 'UE4',
  },
  {
    id: 5,
    name: 'UE5',
  },
]

const TEST_GROUPS = [
  {
    id: 1,
    name: 'GR1',
  },
  {
    id: 2,
    name: 'GR2',
  },
  {
    id: 3,
    name: 'GR3',
  },
  {
    id: 4,
    name: 'GR4',
  },
  {
    id: 5,
    name: 'GR5',
  },
]

const TEST_TEACHERS = [
  {
    id: 1,
    name: 'PRC',
  },
  {
    id: 2,
    name: 'PR1',
  },
  {
    id: 3,
    name: 'PR2',
  },
  {
    id: 4,
    name: 'PR3',
  },
  {
    id: 5,
    name: 'PR4',
  },
]

const TEST_PERIODS = [
  {
    id: 1,
    name: 'S1',
  },
  {
    id: 2,
    name: 'S2',
  },
  {
    id: 3,
    name: 'S3',
  },
  {
    id: 4,
    name: 'S4',
  },
  {
    id: 5,
    name: 'S5',
  },
]

const TEST_ROOM_TYPES = [
  {
    id: 1,
    name: 'Amphithéatre',
  },
  {
    id: 2,
    name: 'Multimedia',
  },
  {
    id: 3,
    name: 'Cours',
  },
  {
    id: 4,
    name: 'Laboratoire',
  },
]

const TEST_COURSE_TYPES = [
  {
    id: 1,
    name: 'CM',
  },
  {
    id: 2,
    name: 'TD',
  },
  {
    id: 3,
    name: 'TP',
  },
  {
    id: 4,
    name: 'Reunion',
  },
  {
    id: 5,
    name: 'Examen',
  },
]

const FILTER_TYPES = [
  FilterType.MODULE,
  FilterType.GROUP,
  FilterType.TEACHER,
  FilterType.PERIOD,
  FilterType.ROOM_TYPE,
  FilterType.COURSE_TYPE,
]

let wrapper: ReturnType<typeof mount<typeof FilterSelector>>

//region Utilities for drag & drop

const VIRTUAL_FILTER_ELEMENT_HEIGHT = 20
const VIRTUAL_FILTER_ELEMENT_GAP = 5

/**
 * Updates the layout of the filter selector elements. Used to emulate drag & drop in a browser-less env.
 * Must be called before attempting to a drag & drop simulation.
 */
function updateLayout() {
  const elements = wrapper.findAll('.filter-list-element')
  for (let i = 0; i < elements.length; i++) {
    const y = (VIRTUAL_FILTER_ELEMENT_GAP + VIRTUAL_FILTER_ELEMENT_HEIGHT) * i
    elements[i].element.getBoundingClientRect = () => new DOMRect(0, y, 250, VIRTUAL_FILTER_ELEMENT_HEIGHT)
    elements[i].element.children[0].getBoundingClientRect = () => new DOMRect(0, y, 250, VIRTUAL_FILTER_ELEMENT_HEIGHT)
  }
}

/**
 * Performs a one-shot drag & drop of a filter element up or down a given increment amount.
 * May not be suitable for more advanced test cases but reduces the amount of duplicated code.
 *
 * @param element The element to drag (Must be a {@link FilterElement})
 * @param increments The amount of increments (or items) to drag it over. Positive means drag down, negative drag up.
 */
async function performDrag(element: VueWrapper, increments: number) {
  updateLayout()

  const dragArea = element.find('.drag-area')
  const rect = element.element.getBoundingClientRect()

  const start = (rect.y + rect.height / 2) | 0
  const end = start + (VIRTUAL_FILTER_ELEMENT_HEIGHT + VIRTUAL_FILTER_ELEMENT_GAP) * increments

  await dragArea.trigger('mousedown', {
    clientX: 0,
    clientY: start,
  })

  await wrapper.trigger('mousemove', {
    clientX: 0,
    clientY: end - (end - start) / 2,
  })

  await wrapper.trigger('mouseup', {
    clientX: 0,
    clientY: end,
  })
}

//endregion

// mount filterSelector with the correct props before each test
beforeEach(() => {
  wrapper = mount(FilterSelector, {
    props: {
      availableGroups: TEST_GROUPS,
      availableCourseTypes: TEST_COURSE_TYPES,
      availableModules: TEST_MODULES,
      availableRoomTypes: TEST_ROOM_TYPES,
      availableTeachers: TEST_TEACHERS,
      availablePeriods: TEST_PERIODS,
      modelValue: [],
      'onUpdate:modelValue': (selected: SelectedFilter[]) => wrapper.setProps({ modelValue: selected }),
    },
    attachTo: document.body,
    global: {
      mocks: {
        $t: (k: string) => k,
      },
    },
  })

  function mouseEventHandler(e: Event) {
    // Hack to force the layout to be recalculated everytime clientY is accessed
    // This makes sure the FilterSelector's loop does always work with fresh data when it needs it
    const clientY = (e as MouseEvent).clientY
    Object.defineProperty(e, 'clientY', {
      configurable: true,
      enumerable: true,
      get() {
        updateLayout()

        void wrapper.findComponent(FilterSelector).vm.$nextTick(() => updateLayout())
        return clientY
      },
    })
  }

  wrapper.element.addEventListener('mousemove', mouseEventHandler)
  wrapper.element.addEventListener('mouseup', mouseEventHandler)
})

it('Component initialized contains "add filter" button and nothing else', () => {
  expect(wrapper.getComponent(DropdownMenu))
  expect(() => wrapper.getComponent(FilterElement)).toThrowError()
})

it('There are as many filters to select as filter types', async () => {
  // Verify that no dropdownItems can be seen
  expect(() => wrapper.getComponent(DropdownMenuItem)).toThrowError()

  // Find the button to add filter and click to expand
  await wrapper.find('button').trigger('click')

  // Check that the number of DropdownItems is the same as the number of filter types
  expect(wrapper.findAllComponents(DropdownMenuItem).length).toBe(FILTER_TYPES.length)
})

it("Add filter can't be clicked when all filters are selected", async () => {
  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' one
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)

  // Close dropdown before adding all
  await addFilterButton.trigger('click')

  // Add all filters one by one
  for (const item of dropdownItems) {
    await addFilterButton.trigger('click')
    await item.trigger('click')
  }

  // The dropdown button can't be found
  expect(() => wrapper.getComponent(DropdownMenu)).toThrowError()
})

it('New filter element created when dropdownItem clicked', async () => {
  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' one
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Click on the teacher dropdownItem
  await dropdownTeacher?.trigger('click')

  // Check that the corresponding FilterElement has been created
  expect(wrapper.findComponent(FilterElement).attributes('data-test')).toBe('filter-element-teacher')
})

it("Filter element is selected when added, and can't be selected again", async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]
  expect(selected.length).toBe(0)

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher'
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Click on the teacher dropdownItem
  await dropdownTeacher?.trigger('click')

  // Check that one and only one SelectedFilter of type 'teacher' has been added
  expect(selected.length).toBe(1)
  expect(selected[0].filter).toBe('teacher')

  // Check that the dropdownItem is disabled
  await addFilterButton.trigger('click')
  expect(dropdownTeacher?.attributes('aria-disabled'))
})

it('FilterElements and SelectedFilters are added in the correct order', async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' and 'module' ones
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')
  const dropdownModule = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-module')

  // Click on the teacher dropdownItem & on the module dropdownItem
  await dropdownTeacher?.trigger('click')
  await addFilterButton.trigger('click')
  await dropdownModule?.trigger('click')

  // Check right order in selected
  expect(selected[0].filter).toBe('teacher')
  expect(selected[1].filter).toBe('module')

  // Get all filterElements
  const filterElements = wrapper.findAllComponents(FilterElement)

  // Check right order in filterElements
  expect(filterElements[0].attributes('data-test')).toBe('filter-element-teacher')
  expect(filterElements[1].attributes('data-test')).toBe('filter-element-module')
})

it('SelectedFilter deleted correctly', async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the desired items
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')
  const dropdownModule = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-module')
  const dropdownPeriod = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-period')

  // Add the 3 filters
  await dropdownPeriod?.trigger('click')
  await addFilterButton.trigger('click')
  await dropdownTeacher?.trigger('click')
  await addFilterButton.trigger('click')
  await dropdownModule?.trigger('click')

  // Get the newly created filterElement
  let filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')

  // Click on its delete button
  await teacherElement?.find('.delete').trigger('click')

  // Check that it's been deleted correctly
  expect(selected[0].filter).toBe('period')
  expect(selected[1].filter).toBe('module')

  // Get the updated filterElements
  filterElements = wrapper.findAllComponents(FilterElement)

  // Check that the teacher filterElement can't be found anymore
  expect(filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')).toBe(undefined)
})

it('FilterElement entities can be opened', async () => {
  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' item
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Add the teacher filter
  await dropdownTeacher?.trigger('click')

  // Get the newly created filterElement
  const filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')

  // Verify that details are hidden when not expanded
  expect(() => teacherElement?.get('.filter-details-container')).toThrowError()

  // Click to expand the element
  await teacherElement?.find('.expand').trigger('click')

  // Verify that details are visible, and that you can select entities and mode
  expect(
    teacherElement?.get('.filter-details-wrapper') &&
      teacherElement?.get('.filter-mode-selector') &&
      teacherElement?.get('.filter-entities-selector')
  )
})

it('SelectedFilter deleted correctly when opened', async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' item
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Add the teacher filter
  await dropdownTeacher?.trigger('click')

  // Get the newly created filterElement
  const filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')

  // Click to expand the element and click on delete
  await teacherElement?.find('.expand').trigger('click')
  await teacherElement?.find('[data-test="expanded-delete"]').trigger('click')

  // Verify empty selected
  expect(selected.length).toBe(0)
  expect(() => wrapper.getComponent(FilterElement)).toThrowError()
})

it('Icon and SelectedFilter[] changed on inclusion change', async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' item
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Add the teacher filter
  await dropdownTeacher?.trigger('click')

  // Get the newly created filterElement
  const filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')

  //Verify include indicator and selected after element has just been added
  expect(teacherElement?.find('[data-test="include-indicator"]'))
  expect(selected[0].exclude).toBe(false)

  // Click to expand the element
  await teacherElement?.find('.expand').trigger('click')

  // Verify exclude icon on click
  await teacherElement?.find('[data-test="exclude-btn"]').trigger('click')
  expect(teacherElement?.find('[data-test="exclude-indicator"]'))
  expect(selected[0].exclude).toBe(true)

  // Verify include icon on click
  await teacherElement?.find('[data-test="include-btn"]').trigger('click')
  expect(teacherElement?.find('[data-test="include-indicator"]'))
  expect(selected[0].exclude).toBe(false)
})

it('FilterElement text and SelectedFilter[] changed when entities added', async () => {
  // Get selected v-model and verify that its empty
  const selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the 'teacher' item
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')

  // Add the teacher filter
  await dropdownTeacher?.trigger('click')

  // Get the newly created filterElement
  const filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') == 'filter-element-teacher')

  // Click to expand the element
  await teacherElement?.find('.expand').trigger('click')
  await teacherElement?.find('.flopui-combobox-trigger').trigger('click')
  const teacherSelect = teacherElement?.getComponent(ComboboxContent)

  // Click on third entity and verify that it's been added
  await teacherSelect?.find('.flopui-select-item:nth-child(3)').trigger('click')
  expect(teacherElement?.find('.filter-entity-count').text().includes(TEST_TEACHERS[2].name))
  expect(selected[0].entities.includes(TEST_TEACHERS[2].id))

  // Click on first entity and verify that it's been added
  await teacherSelect?.find('.flopui-select-item:nth-child(1)').trigger('click')
  expect(teacherElement?.find('.filter-entity-count').text().includes('+ 1'))
  expect(selected[0].entities.includes(TEST_TEACHERS[0].id))

  // Click on fifth entity and verify that it's been added
  await teacherSelect?.find('.flopui-select-item:nth-child(5)').trigger('click')
  expect(teacherElement?.find('.filter-entity-count').text().includes('+ 2'))
  expect(selected[0].entities.includes(TEST_TEACHERS[4].id))

  // Click on third entity and verify that it's been removed
  await teacherSelect?.find('.flopui-select-item:nth-child(3)').trigger('click')
  expect(teacherElement?.find('.filter-entity-count').text().includes(TEST_TEACHERS[0].name))
  expect(teacherElement?.find('.filter-entity-count').text().includes('+ 1'))
  expect(selected[0].entities.includes(TEST_TEACHERS[1].id)).toBe(false)
})

it('Drag changes order in selected and items list with same values', async () => {
  // Get selected v-model and verify that its empty
  let selected = wrapper.props('modelValue') as SelectedFilter[]

  // Find the dropdown button and click to open the dropdown
  const addFilterButton = wrapper.find('button')
  await addFilterButton.trigger('click')

  // Get all dropdownItems and find the items
  const dropdownItems = wrapper.findAllComponents(DropdownMenuItem)
  const dropdownTeacher = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-teacher')
  const dropdownModule = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-module')
  const dropdownRoomType = dropdownItems.find((item) => item.attributes('data-test') == 'dropdown-item-room_type')

  // Add the filters
  await dropdownTeacher?.trigger('click')
  await addFilterButton.trigger('click')
  await dropdownModule?.trigger('click')
  await addFilterButton.trigger('click')
  await dropdownRoomType?.trigger('click')

  // Get the newly created filterElements
  const filterElements = wrapper.findAllComponents(FilterElement)
  const teacherElement = filterElements.find((element) => element.attributes('data-test') === 'filter-element-teacher')
  const moduleElement = filterElements.find((element) => element.attributes('data-test') === 'filter-element-module')
  let roomTypeElement = filterElements.find((element) => element.attributes('data-test') === 'filter-element-room_type')

  // Modify the teacherElement
  await teacherElement?.find('.expand').trigger('click')
  await teacherElement?.find('.flopui-combobox-trigger').trigger('click')

  const teacherSelect = teacherElement?.getComponent(ComboboxContent)
  await teacherSelect?.find('.flopui-select-item:nth-child(1)').trigger('click')
  await teacherSelect?.find('.flopui-select-item:nth-child(2)').trigger('click')
  await teacherSelect?.find('.flopui-select-item:nth-child(4)').trigger('click')
  await teacherElement?.find('.flopui-combobox-trigger').trigger('click')
  await teacherElement?.find('[data-test="exclude-btn"]').trigger('click')
  await teacherElement?.find('.expand').trigger('click')

  // Modify the moduleElement
  await moduleElement?.find('.expand').trigger('click')
  await moduleElement?.find('.flopui-combobox-trigger').trigger('click')

  const moduleSelect = moduleElement?.getComponent(ComboboxContent)
  await moduleSelect?.find('.flopui-select-item:nth-child(5)').trigger('click')
  await moduleSelect?.find('.flopui-select-item:nth-child(4)').trigger('click')
  await moduleElement?.find('.flopui-combobox-trigger').trigger('click')
  await moduleElement?.find('[data-test="exclude-btn"]').trigger('click')
  await moduleElement?.find('.expand').trigger('click')

  // Modify the roomTypeElement
  await roomTypeElement?.find('.expand').trigger('click')
  await roomTypeElement?.find('.flopui-combobox-trigger').trigger('click')

  const roomTypeSelect = roomTypeElement?.getComponent(ComboboxContent)
  await roomTypeSelect?.find('.flopui-select-item:nth-child(1)').trigger('click')
  await roomTypeSelect?.find('.flopui-select-item:nth-child(2)').trigger('click')
  await roomTypeSelect?.find('.flopui-select-item:nth-child(4)').trigger('click')
  await roomTypeSelect?.find('.flopui-select-item:nth-child(3)').trigger('click')
  await roomTypeElement?.find('.flopui-combobox-trigger').trigger('click')
  await roomTypeElement?.find('.expand').trigger('click')

  // Verify selected currentState for further testing
  expect(selected).toStrictEqual([
    { filter: 'teacher', entities: [1, 2, 4], exclude: true },
    { filter: 'module', entities: [5, 4], exclude: true },
    { filter: 'room_type', entities: [1, 2, 4, 3], exclude: false },
  ])

  roomTypeElement = wrapper
    .findAllComponents(FilterElement)
    .find((element) => element.attributes('data-test') == 'filter-element-room_type')

  // Move roomTypeElement over the teacherElement and update selected

  await performDrag(roomTypeElement!, -2)
  selected = wrapper.props('modelValue') as SelectedFilter[]

  // Verify new state of selected
  expect(selected).toStrictEqual([
    { filter: 'room_type', entities: [1, 2, 4, 3], exclude: false },
    { filter: 'teacher', entities: [1, 2, 4], exclude: true },
    { filter: 'module', entities: [5, 4], exclude: true },
  ])

  // Verify new state of filterElements
  expect(wrapper.findAllComponents(FilterElement)[0].attributes('data-test')).toBe('filter-element-room_type')
  expect(wrapper.findAllComponents(FilterElement)[1].attributes('data-test')).toBe('filter-element-teacher')
  expect(wrapper.findAllComponents(FilterElement)[2].attributes('data-test')).toBe('filter-element-module')
})
