<script setup lang="ts">
import { computed, nextTick, ref } from 'vue'
import SimpleButton, { ButtonColor, ButtonLook } from '@/components/uikit/button/SimpleButton.vue'
import ComboboxTagsInput from '@/components/uikit/form/input/ComboboxTagsInput.vue'
import ComboboxItem from '@/components/uikit/form/combobox/ComboboxItem.vue'
import FilterIcon from '@/components/management/filters/FilterIcon.vue'

import { FilterEntity, FilterType } from '@/composables/filterCourses'

import Remove from '~mingcute/system/minimize_line.svg'
import DragIndicator from '~mingcute/system/dots_line.svg'
import Expand from '~mingcute/arrow/down_line.svg'
import IntersectIcon from '~mingcute/design/intersect_fill.svg'
import ExcludeIcon from '~mingcute/design/exclude_fill.svg'
import ToggleGroupButton from '@/components/uikit/button/ToggleGroupButton.vue'
import ToggleGroup from '@/components/uikit/button/ToggleGroup.vue'

interface Props {
  filter: FilterType
  entities: FilterEntity[]
}

type Emit = {
  delete: []
  dragstart: [MouseEvent]
}

const props = defineProps<Props>()

const selected = defineModel<number[]>('selected', { required: true })
const exclude = defineModel<boolean>('exclude', { required: true })
const filterMode = computed({
  get: () => (exclude.value ? 'exclude' : 'include'),
  set: (m) => (exclude.value = m === 'exclude'),
})

const emit = defineEmits<Emit>()

const containerRef = ref<HTMLElement>()
const expandedWrapperRef = ref<HTMLElement>()

const expanded = ref(false)

const firstSelected = computed(() => {
  return props.entities.find((e) => e.id === selected.value[0])?.name
})

function handleToggleExpanded() {
  if (!expandedWrapperRef.value) {
    // Should never be happening, but handled gracefully anyway
    expanded.value = !expanded.value
    return
  }

  if (expanded.value) {
    // This allows to bypass the Vue render pipeline and immediately start animations
    containerRef.value?.classList?.remove('expanded')

    if (expandedWrapperRef.value) {
      // Necessary for the animation to work as expected
      const { height } = expandedWrapperRef.value.getBoundingClientRect()
      expandedWrapperRef.value.style.height = `${height}px` // Necessary for the animation to work as expected
    }

    // Start the close animation next tick (so height gets applied)
    window.requestAnimationFrame(() => {
      expandedWrapperRef.value?.removeAttribute('style')

      // Actually close the thing 250ms later, to let the animations run
      setTimeout(() => (expanded.value = false), 250)
    })

    return
  }

  expanded.value = true
  void nextTick(() => {
    if (!expandedWrapperRef.value) return

    const targetHeight = expandedWrapperRef.value.firstElementChild?.getBoundingClientRect().height
    if (targetHeight) {
      expandedWrapperRef.value.style.height = `${targetHeight}px`
      setTimeout(() => {
        if (!expandedWrapperRef.value) return

        // These are not necessary once the animation is complete and break the behavior of the element
        // So let's get rid of them while it's not being animated
        expandedWrapperRef.value.style.overflow = 'visible'
        expandedWrapperRef.value.style.height = 'auto'
      }, 250)
    }
  })
}

function handleDragStart(event: MouseEvent) {
  if (expanded.value && expandedWrapperRef.value) {
    expandedWrapperRef.value.style.transition = 'none'
    expandedWrapperRef.value.style.height = '0px'
    emit('dragstart', event)
    void nextTick(() => {
      expandedWrapperRef.value?.removeAttribute('style')
    })
  } else {
    emit('dragstart', event)
  }
}
</script>

<template>
  <div ref="containerRef" class="filter-element" :class="{ expanded }">
    <div class="filter-info">
      <div class="drag-area" @mousedown="handleDragStart($event)">
        <DragIndicator />
      </div>
      <div class="icon">
        <FilterIcon :filter="filter" />
      </div>
      <span class="filter-name">
        {{ $t(`management.courses.filters.types.${filter}`) }}
      </span>
      <div v-if="selected.length" class="filter-entities">
        <span class="filter-mode">
          <ExcludeIcon v-if="exclude" data-test="include-indicator" />
          <IntersectIcon v-else data-test="exclude-indicator" />
        </span>
        <span class="filter-entity-count">
          {{ firstSelected }}
          <template v-if="selected.length > 1"> +{{ selected.length - 1 }}</template>
        </span>
      </div>
      <button class="expand" @click="handleToggleExpanded">
        <Expand />
      </button>
      <button class="delete" @click="emit('delete')">
        <Remove />
      </button>
    </div>
    <div ref="expandedWrapperRef" class="filter-details-wrapper">
      <div v-if="expanded" class="filter-details-container">
        <div class="filter-details">
          <div class="filter-mode-selector">
            <label>
              {{ $t('management.courses.filters.mode.label') }}
            </label>
            <ToggleGroup v-model="filterMode" no-empty-value>
              <ToggleGroupButton value="include" data-test="include-btn">
                {{ $t('management.courses.filters.mode.include') }}
              </ToggleGroupButton>
              <ToggleGroupButton value="exclude" data-test="exclude-btn">
                {{ $t('management.courses.filters.mode.exclude') }}
              </ToggleGroupButton>
            </ToggleGroup>
          </div>
          <div class="filter-entities-selector">
            <ComboboxTagsInput
              v-model="selected"
              :placeholder="$t('form.searchable.search')"
              :display-value="(id) => entities.find((e) => e.id === id)?.name ?? '???'"
              :filter-function="
                (_, s) => entities.filter((e) => e.name.toLowerCase().includes(s.toLowerCase())).map((e) => e.id)
              "
            >
              <ComboboxItem v-for="e in entities" :key="e.id" :value="e.id">
                {{ e.name }}
              </ComboboxItem>
            </ComboboxTagsInput>
          </div>
          <div class="filter-actions">
            <SimpleButton
              data-test="expanded-delete"
              :color="ButtonColor.DANGER"
              :look="ButtonLook.OUTLINED"
              @click="emit('delete')"
            >
              {{ $t('management.courses.filters.delete') }}
            </SimpleButton>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped>
.filter-element {
  display: flex;
  flex-direction: column;
  background-color: #eee;
  padding: 0.5rem 0.5rem 0;
  border-radius: 4px;
  position: relative;
}

.filter-info {
  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.expand,
.delete {
  all: unset;
  box-sizing: border-box;

  cursor: pointer;
}

.expand {
  transition: transform 0.2s;
}

.drag-area,
.icon,
.expand,
.delete,
.filter-mode {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 1.5rem;
  height: 1.5rem;
  flex-shrink: 0;
}

.delete {
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(35%, -35%);
  background-color: var(--color-background-danger);
  width: 1.25rem;
  height: 1.25rem;
  border-radius: 50%;
  transition: opacity 0.2s;
  color: white;

  pointer-events: none;
  opacity: 0;
}

.filter-element:hover:not(.expanded) .delete {
  pointer-events: auto;
  opacity: 1;
}

.drag-area {
  cursor: move;
  color: var(--color-text-muted);
}

.filter-name {
  flex: 1;
}

.filter-details-wrapper {
  margin-top: 0.5rem;
  overflow: hidden;
  transition: height 0.3s;
  height: 0;
}

.filter-entities {
  display: flex;
  align-items: center;
  gap: 0.125rem;
  font-size: 0.8rem;
  line-height: 1rem;
}

.filter-mode {
  width: 1.2rem;
  height: 1.2rem;
}

.filter-details {
  padding: 0.5rem;
  /* TODO: adjust colors with flop's theme + dark theme */
  border-top: 1px #ddd solid;
  display: flex;
  flex-direction: column;
  gap: 1rem;
}

.filter-mode-selector {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.filter-actions {
  display: flex;
  justify-content: flex-end;
  align-items: center;
  gap: 0.5rem;
}

.expand svg,
.delete svg,
.drag-area svg {
  width: 100%;
  height: 100%;
  fill: currentColor;
}

/* Expanded */
.expanded .expand {
  transform: rotate(180deg);
}
</style>
