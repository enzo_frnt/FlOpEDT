<script setup lang="ts">
import type { Identifier } from '@/ts/entities/base/identifier.ts'
import type { Course, CourseCreateData, CourseUpdateData } from '@/ts/type'

import { computed, ref, watch } from 'vue'
import { storeToRefs } from 'pinia'
import { useI18n } from 'vue-i18n'

import { useCourseModuleStore } from '@/stores/timetable/courseModule'
import { useCourseTypeStore } from '@/stores/timetable/courseType'
import { useTutorStore } from '@/stores/timetable/tutor'
import { useGroupsStore } from '@/stores/departments/groups'
import { usePeriodStore } from '@/stores/timetable/period'
import { useCourseStore } from '@/stores/timetable/course'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'

import SimpleButton, { ButtonColor, ButtonLook } from '@/components/uikit/button/SimpleButton.vue'
import SelectInput from '@/components/uikit/form/input/SelectInput.vue'
import SelectItem from '@/components/uikit/form/select/SelectItem.vue'
import ComboboxInput from '@/components/uikit/form/input/ComboboxInput.vue'
import ComboboxItem from '@/components/uikit/form/combobox/ComboboxItem.vue'
import ComboboxTagsInput from '@/components/uikit/form/input/ComboboxTagsInput.vue'
import MultipleValueGatedField from '@/components/forms/MultipleValueGatedField.vue'
import Loader from '@/components/uikit/Loader.vue'
import MoreVert from '~mingcute/system/more_2_line.svg'
import DropdownMenuItem from '@/components/uikit/menu/dropdown/DropdownMenuItem.vue'
import DropdownMenu from '@/components/uikit/menu/DropdownMenu.vue'
import { useDependencyStore } from '@/stores/timetable/dependency'

const NULL_ID = -1
const DURATION_RE = /^(((\d+:)?[0-5]\d)|\d):[0-5]\d$/

const ERRORS_CLEAR = {
  message: null as string | null,
  groups: null as string | null,
  duration: null as string | null,
  period: null as string | null,
  tutor: null as string | null,
}

const enum FormState {
  IDLE,
  SUBMITTING,
  ERRORED,
}

interface Props {
  allowedPeriods: number[] | undefined
}

const props = defineProps<Props>()

const { t } = useI18n()
const selectedCourseStore = useSelectedCourseStore()
const { selectedCourses, isModifying } = storeToRefs(selectedCourseStore)

const courseStore = useCourseStore()
const courseModulesStore = useCourseModuleStore()
const tutorStore = useTutorStore()
const courseTypeStore = useCourseTypeStore()
const groupsStore = useGroupsStore()
const periodStore = usePeriodStore()
const dependencyStore = useDependencyStore()

const formState = ref(FormState.IDLE)
const errors = ref(ERRORS_CLEAR)

const tutorOptions = computed(() =>
  tutorStore.tutors.map((t) => ({
    value: t.id,
    name: `${t.firstname} ${t.lastname}`,
    alias: [t.username],
  }))
)

const tutorOptionsMain = computed(() =>
  tutorOptions.value.map((t) => ({ ...t, disabled: suppTutors.value.includes(t.value) }))
)

const courseTypeEditOverride = ref(selectedCourseStore.unique.type)
const moduleEditOverride = ref(selectedCourseStore.unique.module)
const tutorEditOverride = ref(selectedCourseStore.unique.tutor)
const suppTutorsEditOverride = ref(selectedCourseStore.unique.suppTutors)
const groupsEditOverride = ref(selectedCourseStore.unique.groups)
const periodEditOverride = ref(selectedCourseStore.unique.period)
const durationEditOverride = ref(selectedCourseStore.unique.duration)

const baseCourse: Course = selectedCourses.value[0]
const courseType = ref(baseCourse.type.id.toString())
const module = ref(baseCourse.module.id)
const tutor = ref(baseCourse.tutor?.id ?? NULL_ID)
const suppTutors = ref(baseCourse.supp_tutors.map((st) => st.id))
const groups = ref(baseCourse.groups.map((g) => g.id))
const period = ref(baseCourse.period?.id ?? NULL_ID)
const duration = ref(baseCourse.duration)

watch(groups, () => (errors.value.groups = null))
watch(duration, () => (errors.value.duration = null))
watch(period, () => (errors.value.period = null))
watch(tutor, () => (errors.value.tutor = null))

// This will be more friendly to deal with once UIKit is more complete and Field elements are there
// <editor-fold name="Combobox filters and display functions">
const createFilterByName = (entities: Array<{ id: Identifier | number; name: string }>) =>
  function (values: number[], term: string) {
    term = term.toLowerCase()
    return entities
      .filter((e) => values.includes(e.id.valueOf()) && e.name.toLowerCase().includes(term))
      .map((e) => e.id.valueOf())
  }

const periods = computed(() => {
  const allowedPeriods = props.allowedPeriods

  if (!allowedPeriods) return periodStore.periods

  return periodStore.periods.filter((period) => allowedPeriods.includes(period.id))
})

const filterModule = computed(() => createFilterByName(courseModulesStore.courseModules))
const filterGroup = computed(() => createFilterByName([...groupsStore.groups]))
const filterPeriod = computed(() => createFilterByName(periods.value))

function filterTutor(values: number[], term: string) {
  term = term.toLowerCase()
  return tutorStore.tutors
    .filter(
      (t) =>
        values.includes(t.id) &&
        (t.lastname.toLowerCase().includes(term) ||
          t.firstname.toLowerCase().includes(term) ||
          t.username.toLowerCase().includes(term))
    )
    .map((e) => e.id)
}

const displayModule = (id: number) =>
  id === NULL_ID ? t('form.noSelection') : (courseModulesStore.courseModules.find((m) => m.id === id)?.name ?? '???')
const displayGroup = (id: number) =>
  id === NULL_ID ? t('form.noSelection') : (groupsStore.groups.get(id)?.toFormDisplayValue() ?? '???')
const displayPeriod = (id: number) =>
  // TODO: check merge choice. Picked periods.value; could have been periodStore.periods
  id === NULL_ID ? t('form.noSelection') : (periods.value.find((p) => p.id === id)?.name ?? '???')
function displayTutor(id: number) {
  if (id === NULL_ID) return t('form.noSelection')
  const tutor = tutorStore.tutors.find((t) => t.id === id)
  return tutor ? `${tutor.firstname} ${tutor.lastname}` : '???'
}
// </editor-fold>

async function submitForm(saveAsNew: boolean) {
  formState.value = FormState.SUBMITTING
  errors.value = ERRORS_CLEAR

  if (groups.value.length === 0) {
    formState.value = FormState.ERRORED
    errors.value.groups = 'required'
  }

  if (!DURATION_RE.test(duration.value)) {
    formState.value = FormState.ERRORED
    errors.value.duration = duration.value ? 'format' : 'required'
  }

  if (!saveAsNew && baseCourse.period && period.value === NULL_ID) {
    formState.value = FormState.ERRORED
    errors.value.period = 'required'
  }

  if (!saveAsNew && baseCourse.tutor && tutor.value === NULL_ID) {
    formState.value = FormState.ERRORED
    errors.value.tutor = 'required'
  }

  // Halt if we have errors
  if (formState.value === FormState.ERRORED) return

  formState.value = FormState.SUBMITTING
  const update: Omit<CourseUpdateData, 'id'> = {}

  if (courseTypeEditOverride.value) update.type_id = Number(courseType.value)
  if (moduleEditOverride.value) update.module_id = module.value
  if (tutorEditOverride.value) update.tutor_id = tutor.value === NULL_ID ? void 0 : tutor.value
  if (suppTutorsEditOverride.value) update.supp_tutor_ids = suppTutors.value
  if (groupsEditOverride.value) update.group_ids = groups.value
  if (periodEditOverride.value) update.period_id = period.value === NULL_ID ? void 0 : period.value
  if (durationEditOverride.value) update.duration = duration.value

  const pushErrors: [number, Error][] = []

  const createdCourses: Course[] = []

  for (const course of selectedCourses.value) {
    try {
      if (saveAsNew) {
        const newCourse: CourseCreateData = {
          type_id: update.type_id ? update.type_id : course.type.id,
          module_id: update.module_id ? update.module_id : course.module.id,
          supp_tutor_ids: update.supp_tutor_ids ? update.supp_tutor_ids : course.supp_tutors.map((t) => t.id),
          group_ids: update.group_ids ? update.group_ids : course.groups.map((g) => g.id),
          duration: update.duration ? update.duration : course.duration,
          is_graded: false,
        }

        if (update.period_id) newCourse.period_id = update.period_id

        if (update.tutor_id) newCourse.tutor_id = update.tutor_id

        createdCourses.push(await courseStore.postCourse({ ...newCourse }))
      } else {
        // If the period is modified, delete all dependencies related to the course
        if (course.period && course.period.id !== update.period_id) {
          const relatedDependencies = dependencyStore.dependencies.filter(
            (dependency) => course.id === dependency.course1 || course.id === dependency.course2
          )

          for (const relatedDependency of relatedDependencies) {
            await dependencyStore.deleteDependency(relatedDependency)
          }
        }

        // TODO: Modify http request to allow value -> null on update
        await courseStore.updateCourse({ id: course.id, ...update })
      }
    } catch (e) {
      pushErrors.push([course.id, e as Error])
    }
  }

  if (pushErrors.length) {
    formState.value = FormState.ERRORED

    // TODO: i18n, more robust reporting, etc
    errors.value.message = pushErrors.length === 1 ? 'An error occurred' : `${pushErrors.length} errors occurred`
    errors.value.message += ' while updating the courses.\n'
    for (const [courseId, err] of pushErrors) {
      errors.value.message += `\n- Course #${courseId}: ${err.message}`
    }

    return
  }

  if (createdCourses.length > 0) selectedCourseStore.selectedCourses = createdCourses

  isModifying.value = false
}
</script>

<template>
  <h3>{{ $t('management.courses.update', selectedCourses.length) }}</h3>
  <div v-if="errors.message" class="error-message">
    {{ errors.message }}
  </div>
  <form class="form" @submit.prevent="submitForm(false)">
    <div class="field">
      <b>{{ $t('management.courses.courseType.simple') }}</b>
      <MultipleValueGatedField v-model="courseTypeEditOverride" :unique="selectedCourseStore.unique.type">
        <SelectInput v-model="courseType" :placeholder="$t('management.courses.courseType.placeholder')">
          <SelectItem v-for="type in courseTypeStore.courseTypes" :key="type.id" :value="type.id.toString()">
            {{ type.name }}
          </SelectItem>
        </SelectInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.module.simple') }}</b>
      <MultipleValueGatedField v-model="moduleEditOverride" :unique="selectedCourseStore.unique.module">
        <ComboboxInput
          v-model="module"
          :placeholder="$t('management.courses.module.placeholder')"
          :filter-function="filterModule"
          :display-value="displayModule"
        >
          <ComboboxItem v-for="mdl in courseModulesStore.courseModules" :key="mdl.id" :value="mdl.id">
            {{ mdl.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.tutor.simple') }}</b>
      <MultipleValueGatedField v-model="tutorEditOverride" :unique="selectedCourseStore.unique.tutor">
        <ComboboxInput
          v-model="tutor"
          :placeholder="$t('management.courses.tutor.placeholder')"
          :filter-function="filterTutor"
          :display-value="displayTutor"
        >
          <ComboboxItem :value="NULL_ID">
            <i>{{ $t('form.noSelection') }}</i>
          </ComboboxItem>
          <ComboboxItem v-for="tut in tutorOptionsMain" :key="tut.value" :value="tut.value" :disabled="tut.disabled">
            {{ tut.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
      <div v-if="errors.tutor" class="form-error">{{ $t(`form.errors.${errors.tutor}`) }}</div>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.suppTutors.simple', Number.MAX_SAFE_INTEGER) }}</b>
      <MultipleValueGatedField v-model="suppTutorsEditOverride" :unique="selectedCourseStore.unique.suppTutors">
        <ComboboxTagsInput v-model="suppTutors" :placeholder="$t('management.courses.suppTutors.placeholder')">
          <ComboboxItem
            v-for="tut in tutorStore.tutors"
            :key="tut.id"
            :value="tut.id"
            :disabled="tut.id === tutor"
            :display-value="displayTutor"
            :filter-function="filterTutor"
          >
            {{ tut.firstname }} {{ tut.lastname }}
          </ComboboxItem>
        </ComboboxTagsInput>
      </MultipleValueGatedField>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.group.simple', Number.MAX_SAFE_INTEGER) }}</b>
      <MultipleValueGatedField v-model="groupsEditOverride" :unique="selectedCourseStore.unique.groups">
        <ComboboxTagsInput
          v-model="groups"
          :placeholder="$t('management.courses.group.placeholder')"
          :display-value="displayGroup"
          :filter-function="filterGroup"
        >
          <ComboboxItem v-for="g in groupsStore.groups" :key="g.id.valueOf()" :value="g.id.valueOf()">
            {{ g.fullName }}
          </ComboboxItem>
        </ComboboxTagsInput>
      </MultipleValueGatedField>
      <div v-if="errors.groups" class="form-error">{{ $t(`form.errors.${errors.groups}`) }}</div>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.period.simple') }}</b>
      <MultipleValueGatedField v-model="periodEditOverride" :unique="selectedCourseStore.unique.period">
        <ComboboxInput
          v-model="period"
          :placeholder="$t('management.courses.period.placeholder')"
          :filter-function="filterPeriod"
          :display-value="displayPeriod"
        >
          <ComboboxItem :value="NULL_ID">
            <i>{{ $t('form.noSelection') }}</i>
          </ComboboxItem>
          <ComboboxItem v-for="per in periods" :key="per.id" :value="per.id">
            {{ per.name }}
          </ComboboxItem>
        </ComboboxInput>
      </MultipleValueGatedField>
      <div v-if="errors.period" class="form-error">{{ $t(`form.errors.${errors.period}`) }}</div>
    </div>

    <div class="field">
      <b>{{ $t('management.courses.duration.simple') }}</b>
      <MultipleValueGatedField v-model="durationEditOverride" :unique="selectedCourseStore.unique.duration">
        <!-- TODO: make UIKit element -->
        <input v-model="duration" type="text" class="flopui-input" placeholder="00:00" />
        <div v-if="errors.duration" class="form-error">{{ $t(`form.errors.${errors.duration}`) }}</div>
      </MultipleValueGatedField>
    </div>

    <div class="buttons-container">
      <SimpleButton :color="ButtonColor.SUCCESS" :disabled="formState === FormState.SUBMITTING">
        <template v-if="formState === FormState.SUBMITTING">
          <Loader />
          <span>{{ $t('form.saving') }}</span>
        </template>
        <template v-else>{{ $t('form.save') }}</template>
      </SimpleButton>
      <DropdownMenu no-icon icon-button :aria-label="$t('common.seeMoreActionsLabelButton')">
        <template #trigger>
          <MoreVert />
        </template>
        <template #items>
          <DropdownMenuItem :disabled="formState === FormState.SUBMITTING" @click="submitForm(true)">
            {{ $t('form.saveNew') }}
          </DropdownMenuItem>
        </template>
      </DropdownMenu>
      <SimpleButton
        :look="ButtonLook.OUTLINED"
        :color="ButtonColor.DANGER"
        :disabled="formState === FormState.SUBMITTING"
        @click="isModifying = false"
      >
        {{ $t('form.cancel') }}
      </SimpleButton>
    </div>
  </form>
</template>

<style scoped>
h3 {
  font-size: 1.5rem;
  margin: 0 0 1.5rem;
}

.error-message {
  /* TODO: UIKit component for all "boxed" messages */
  background-color: var(--color-background-danger-light);
  padding: 0.5rem;
  border-radius: 4px;
  margin-bottom: 1rem;
  white-space: pre-wrap;
}

.form-error {
  /* This will no longer be needed once we have proper input fields */
  font-size: 0.875rem;
  color: var(--color-text-danger);
  margin-top: 0.25rem;
}

.form {
  display: flex;
  flex-direction: column;
  list-style-type: none;
  padding-left: 0;
  gap: 1.25rem;
}

.field > b {
  display: block;
  margin-bottom: 0.25rem;
}

.buttons-container {
  display: flex;
  justify-content: flex-end;
  width: 100%;
  gap: 0.5rem;
}
</style>
