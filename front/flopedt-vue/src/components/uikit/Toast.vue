<script lang="ts">
import type { ToastRootProps, ToastRootEmits } from 'radix-vue'

export enum ToastAppearance {
  INFO = 'info',
  WARNING = 'warning',
  DANGER = 'danger',
  SUCCESS = 'success',
}

export interface ToastProps extends Omit<ToastRootProps, 'open'> {
  class?: string
  appearance?: ToastAppearance
  altText?: string
}

export type ToastEmits = Omit<ToastRootEmits, 'update:open'> & {
  action: []
}

type ToastSlots = {
  icon?: () => unknown
  title: () => unknown
  description: () => unknown
  action?: () => unknown
}
</script>

<script setup lang="ts">
import { computed, getCurrentInstance, onMounted, ref } from 'vue'
import { ToastAction, ToastClose, ToastDescription, ToastRoot, ToastTitle } from 'radix-vue'
import { useForwardPropsEmitsSkip } from '@/composables/uikit/useForwardPropsEmitsSkip'
import SimpleButton, { ButtonColor, ButtonLook } from '@/components/uikit/button/SimpleButton.vue'

import ClearIcon from '~mingcute/system/close_line.svg'
import InformationIcon from '~mingcute/system/information_fill.svg'
import WarningIcon from '~mingcute/system/alert_fill.svg'
import AlertIcon from '~mingcute/system/warning_fill.svg'
// ^ these are not typos, the alert triangle is used as warning
import CheckIcon from '~mingcute/system/check_circle_fill.svg'

const props = defineProps<ToastProps>()
const emits = defineEmits<ToastEmits>()
const open = defineModel<boolean>('open')

const slots = defineSlots<ToastSlots>()
const forwarded = useForwardPropsEmitsSkip(props, emits, ['altText', 'defaultOpen', '@action'], ['open'])

const toastRef = ref<InstanceType<typeof ToastRoot>>()

const buttonColor = computed(() => {
  switch (props.appearance) {
    case ToastAppearance.WARNING:
      return ButtonColor.WARNING
    case ToastAppearance.DANGER:
      return ButtonColor.DANGER
    case ToastAppearance.SUCCESS:
      return ButtonColor.SUCCESS
    default:
      return ButtonColor.ACCENT
  }
})

onMounted(() => {
  if (props.defaultOpen) {
    open.value = true
  }
})

if (import.meta.env.DEV) {
  if (slots.action) {
    if (!props.altText) {
      console.error('[UI Kit] Missing a11y property `alt-text` on a Toast with an action! No action will be displayed')
    }

    const instance = getCurrentInstance()
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
    if (!instance?.type.emits.includes('action')) {
      console.error('[UI Kit] Toast action has no @action handler!')
    }
  }
}
</script>

<template>
  <ToastRoot ref="toastRef" v-bind="forwarded" v-model:open="open" class="flopui-toast" :class="appearance">
    <template #default="{ remaining }">
      <ToastTitle class="flopui-toast-title">
        <div v-if="$slots.icon || appearance" class="flopui-toast-icon" aria-hidden="true">
          <slot v-if="$slots.icon" name="icon" />
          <InformationIcon v-else-if="appearance === ToastAppearance.INFO" />
          <WarningIcon v-else-if="appearance === ToastAppearance.WARNING" />
          <AlertIcon v-else-if="appearance === ToastAppearance.DANGER" />
          <CheckIcon v-else-if="appearance === ToastAppearance.SUCCESS" />
        </div>

        <slot name="title" />
      </ToastTitle>
      <ToastDescription class="flopui-toast-description">
        <slot name="description" />
      </ToastDescription>
      <ToastAction v-if="$slots.action && altText" class="flopui-toast-action" :alt-text="altText" as-child>
        <SimpleButton :color="buttonColor" :look="ButtonLook.OUTLINED" @click="emits('action')">
          <slot name="action" />
        </SimpleButton>
      </ToastAction>
      <ToastClose class="flopui-toast-close">
        <ClearIcon />
      </ToastClose>
      <!-- TODO: https://github.com/radix-vue/radix-vue/pull/1119 -->
      <div
        class="flopui-toast-remaining"
        :data-remaining="remaining"
        :style="{ width: `${(remaining / (duration || 6500)) * 100}%` }"
      />
    </template>
  </ToastRoot>
</template>

<style>
.flopui-toast {
  display: grid;
  padding: 1rem;
  align-items: center;
  grid-template-columns: auto max-content;
  grid-template-areas:
    'title action'
    'description action';

  background-color: var(--color-background-primary);
  border-radius: var(--border-radius);
  box-shadow: var(--shadow-strong);

  position: relative;
  overflow: hidden;
}

.flopui-toast:focus {
  outline: 2px var(--color-outline) solid;
}

.flopui-toast[data-state='open'] {
  animation: flopui-toast-slide-in 0.2s cubic-bezier(0.16, 1, 0.3, 1);
}
[dir='rtl'] .flopui-toast[data-state='open'] {
  animation: flopui-toast-slide-in-rtl 0.2s cubic-bezier(0.16, 1, 0.3, 1);
}
.flopui-toast[data-state='closed'] {
  animation: flopui-fade-out 0.15s ease-in;
}

.flopui-toast[data-swipe='move'] {
  cursor: grabbing;
  transform: translateX(var(--radix-toast-swipe-move-x));
}
.flopui-toast[data-swipe='cancel'] {
  transform: translateX(0);
  transition: transform 0.1s ease-out;
}
.flopui-toast[data-swipe='end'] {
  animation: flopui-toast-swipe-out 0.15s ease-out;
}
[dir='rtl'] .flopui-toast[data-swipe='end'] {
  animation: flopui-toast-swipe-out-rtl 0.15s ease-out;
}

.flopui-toast-title {
  grid-area: title;
  margin-bottom: 0.25rem;
  font-weight: 500;
  font-size: 1rem;

  display: flex;
  align-items: center;
  gap: 0.5rem;
}

.flopui-toast-icon {
  width: 1.25rem;
  height: 1.25rem;
}

.flopui-toast.info .flopui-toast-icon {
  color: var(--color-text-accent);
}
.flopui-toast.warning .flopui-toast-icon {
  color: var(--color-text-warning);
}
.flopui-toast.danger .flopui-toast-icon {
  color: var(--color-text-danger);
}
.flopui-toast.success .flopui-toast-icon {
  color: var(--color-text-success);
}

.flopui-toast-description {
  grid-area: description;
  color: var(--color-text-secondary);
  font-size: 0.875rem;
  line-height: 1.3;
  margin: 0;
}

.flopui-toast-action {
  grid-area: action;
  display: inline-flex;
  border-radius: var(--border-radius-small);
  padding: 0 0.625rem;
  height: 1.5rem;
  margin: 0 1rem;
}

.flopui-toast-close {
  all: unset;
  box-sizing: border-box;

  display: flex;
  align-items: center;
  justify-content: center;

  position: absolute;
  top: 0.25rem;
  right: 0.25rem;

  width: 1.25rem;
  height: 1.25rem;
  padding: 0.25rem;
  border-radius: 50%;
  color: var(--color-text-muted);
  cursor: pointer;
}

[dir='rtl'] .flopui-toast-close {
  right: unset;
  left: 0.25rem;
}

@media (prefers-reduced-motion: no-preference) {
  .flopui-toast-close {
    transition:
      background-color 0.2s,
      color 0.2s;
  }
}

.flopui-toast-close:hover {
  color: var(--color-text-primary);
  background-color: var(--color-background-danger-light);
}

.flopui-toast-close:focus-visible {
  outline: 2px var(--color-background-danger) solid;
}

.flopui-toast-remaining {
  position: absolute;
  bottom: 0;
  left: 0;
  height: 0.25rem;
  background-color: var(--color-background-accent);
}
[dir='rtl'] .flopui-toast-remaining {
  right: 0;
}

.flopui-toast.warning .flopui-toast-remaining {
  background: var(--color-background-warning);
}
.flopui-toast.danger .flopui-toast-remaining {
  background: var(--color-background-danger);
}
.flopui-toast.success .flopui-toast-remaining {
  background: var(--color-background-success);
}

.flopui-toast[data-state='closed'] .flopui-toast-remaining {
  display: none;
}

@keyframes flopui-toast-slide-in {
  from {
    transform: translateX(calc(100% + var(--viewport-padding)));
  }
}
@keyframes flopui-toast-slide-in-rtl {
  from {
    transform: translateX(calc(-100% - var(--viewport-padding)));
  }
}

@keyframes flopui-toast-swipe-out {
  from {
    transform: translateX(var(--radix-toast-swipe-end-x));
  }

  to {
    transform: translateX(calc(100% + var(--viewport-padding)));
  }
}
@keyframes flopui-toast-swipe-out-rtl {
  from {
    transform: translateX(var(--radix-toast-swipe-end-x));
  }

  to {
    transform: translateX(calc(-100% - var(--viewport-padding)));
  }
}
</style>
