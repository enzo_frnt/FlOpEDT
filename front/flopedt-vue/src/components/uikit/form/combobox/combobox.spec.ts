import { it, expect } from 'vitest'
import { filter } from './combobox'

it('Matches string without being case sensitive', () => {
  expect(filter(['Meow', 'ROAR'], 'me')).toEqual(['Meow'])
})

it('Matches string without considering accents', () => {
  expect(filter(['Méow', 'ROAR'], 'Me')).toEqual(['Méow'])
})
