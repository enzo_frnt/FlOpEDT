import type { ComboboxValue } from '../input/ComboboxInput.vue'
import { Entity } from '@/ts/entities/base/entity'
import { toRaw } from 'vue'

function norm(str: string) {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
}

export function filter(values: ComboboxValue[], search: string): ComboboxValue[] {
  let warned = false
  const normalized = values.map((val): string[] => {
    if (val instanceof Entity) {
      return [norm(val.toFormDisplayValue()), ...val.getFormAliases().map(norm)]
    }

    if (typeof val === 'object') {
      if (import.meta.env.DEV) {
        if (!warned) {
          console.warn('[Combobox] Attempted to filter a combobox with object values without a filter function!')
          warned = true
        }
      }

      return []
    }

    return [norm('' + val)]
  })

  search = norm(search)
  return values.filter((_, i) => {
    const val = normalized[i]
    if (!val.length) return true

    return !!val.find((v) => v.includes(search))
  })
}

export function display(value: ComboboxValue) {
  const rawValue = toRaw(value)
  console.log(value, rawValue)
  if (rawValue instanceof Entity) return rawValue.toFormDisplayValue()

  // eslint-disable-next-line @typescript-eslint/restrict-plus-operands, @typescript-eslint/no-base-to-string -- Intentional
  return '' + rawValue
}
