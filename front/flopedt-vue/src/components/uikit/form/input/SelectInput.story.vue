<script setup lang="ts">
import SelectInput from './SelectInput.vue'
import SelectItem from '../select/SelectItem.vue'
import SelectGroup from '../select/SelectGroup.vue'
import SelectLabel from '../select/SelectLabel.vue'
import SelectSeparator from '../select/SelectSeparator.vue'

const options = [
  { value: 'apple', name: 'Apple' },
  { value: 'banana', name: 'Banana' },
  { value: 'blueberry', name: 'Blueberry' },
  { value: 'grapes', name: 'Grapes' },
  { value: 'pineapple', name: 'Pineapple' },
]

const vegetables = [
  { value: 'aubergine', name: 'Aubergine' },
  { value: 'broccoli', name: 'Broccoli' },
  { value: 'carrot', name: 'Carrot' },
  { value: 'courgette', name: 'Courgette' },
  { value: 'leek', name: 'Leek' },
]

function initState() {
  return { value: 'blueberry', disabled: false }
}

function initStateRepro() {
  return { value1: undefined, value2: undefined }
}
</script>

<template>
  <Story title="form/input/SelectInput">
    <Variant title="Basic usage" :init-state="initState">
      <template #default="{ state }">
        <SelectInput v-model="state.value" :disabled="state.disabled">
          <SelectItem v-for="option in options" :key="option.value" :value="option.value">
            {{ option.name }}
          </SelectItem>
        </SelectInput>
      </template>
    </Variant>
    <Variant title="Advanced usage" :init-state="initState">
      <template #default="{ state }">
        <SelectInput v-model="state.value" :disabled="state.disabled">
          <SelectLabel>Fruits</SelectLabel>
          <SelectGroup>
            <SelectItem v-for="option in options" :key="option.value" :value="option.value">
              {{ option.name }}
            </SelectItem>
          </SelectGroup>
          <SelectSeparator />
          <SelectLabel>Vegetables</SelectLabel>
          <SelectGroup>
            <SelectItem
              v-for="option in vegetables"
              :key="option.value"
              :value="option.value"
              :disabled="option.value === 'courgette'"
            >
              {{ option.name }}
            </SelectItem>
          </SelectGroup>
        </SelectInput>
      </template>
    </Variant>
    <Variant title="Item aligned" :init-state="initState">
      <template #default="{ state }">
        <div class="extra-padding">
          <SelectInput v-model="state.value" :disabled="state.disabled" item-aligned>
            <SelectGroup>
              <SelectLabel>Fruits</SelectLabel>
              <SelectItem v-for="option in options" :key="option.value" :value="option.value">
                {{ option.name }}
              </SelectItem>
            </SelectGroup>
            <SelectSeparator />
            <SelectGroup>
              <SelectLabel>Vegetables</SelectLabel>
              <SelectItem
                v-for="option in vegetables"
                :key="option.value"
                :value="option.value"
                :disabled="option.value === 'courgette'"
              >
                {{ option.name }}
              </SelectItem>
            </SelectGroup>
          </SelectInput>
        </div>
      </template>
    </Variant>

    <Variant title="With lots of options" :init-state="initStateRepro" icon="carbon:chemistry">
      <template #default="{ state }">
        <p>This isn't really a demo but rather a stress test to see how it performs with lots of entries.</p>
        <p>
          Also a useful reproduction case to
          <a href="https://github.com/radix-vue/radix-vue/issues/1040" target="_blank">radix-vue#1040</a>
          so we can track whether the bug occurs again or not.
        </p>
        <h2 class="repro-header">Basic case</h2>
        <SelectInput v-model="state.value1" placeholder="Pick your favorite number...">
          <SelectItem v-for="i in 1000" :key="i" :value="`${i}`" />
        </SelectInput>

        <h2 class="repro-header">Item aligned</h2>
        <SelectInput v-model="state.value2" placeholder="Pick your favorite number..." item-aligned>
          <SelectItem v-for="i in 1000" :key="i" :value="`${i}`" />
        </SelectInput>
      </template>
    </Variant>
  </Story>
</template>

<style scoped>
.extra-padding {
  display: flex;
  padding: 1.5rem;
  height: calc(100vh - 1rem);
  align-items: center;
}

.repro-header {
  font-size: 1.25rem;
  margin-top: 2rem;
  margin-bottom: 0.5rem;
  font-weight: 500;
}
</style>

<docs lang="md">
An input allowing the user to pick one of the proposed values.

There are two different positioning modes: the default one with the listbox below the select trigger, and the
"item aligned" position, where the listbox is placed relative to the active item, similarly to MacOS select menus.

This component does not support autocomplete. If you want it, you should use [ComboboxInput](./ComboboxInput.story.vue)
instead. If you want to be able to select multiple values, you should use [ComboboxTagsInput](./ComboboxTagsInput.story.vue).

For general use in forms you generally want to use [SelectField](../SelectField.story.vue) instead.

### SelectItem
Import path: `@/components/uikit/form/select/SelectItem.vue`

Values the input can take are represented by `SelectItem` components passed as its descendants.

### SelectGroup
Import path: `@/components/uikit/form/select/SelectGroup.vue`

Groups a bunch of items together. Creates an element with WAI-ARIA `role=group`. **The underlying implementation
expects exactly one `SelectLabel` to be present** in order to label the group. While dangling IDs are not explicitly
prohibited by the WAI-ARIA specification, the behavior of user agents may vary. Consider an unlabelled group as an
accessibility non-compliance. See [WAI-ARIA 1.2 8.6.1.](https://www.w3.org/TR/wai-aria-1.2/#mapping_additional_relations_error_processing)

It is possible to nest groups, but this is generally not recommended as it complexifies the layout.

See also: [WCAG 2.0 technique ARIA17](https://www.w3.org/TR/WCAG20-TECHS/ARIA17)

### SelectLabel
Import path: `@/components/uikit/form/select/SelectLabel.vue`

Used to label groups of select items. They must **only** appear in `SelectGroup`s, and there must be **only one**
label per group.

### SelectSeparator
Import path: `@/components/uikit/form/select/SelectSeparator.vue`

Horizontal separator to divide the listbox in multiple sections. Does not carry any semantic value for assistive
technologies.

### Accessibility
In WAI-ARIA documentation, "combobox" may refer to both classic select inputs without support for text input
("select-only combobox", or "non-editable combobox") or combobox with support for a text input ("editable combobox").
This component is a <u>select-only combobox</u>. For an editable combobox see [ComboboxInput](./ComboboxInput.story.vue).

---
See also: https://www.radix-vue.com/components/select.html
</docs>
