<script setup lang="ts">
import { ref } from 'vue'
import Toast, { ToastAppearance } from './Toast.vue'
import PublishableToast, { type PublishableToastInstance } from './PublishableToast.vue'

import SimpleButton from './button/SimpleButton.vue'

import CookieIcon from '~mingcute/food/cookie_fill.svg'

const toastRef = ref<PublishableToastInstance>()

function initState() {
  return {
    open: false,
  }
}

function initStateIcons() {
  return {
    info: false,
    warn: false,
    danger: false,
    success: false,
    custom: false,
  }
}

function onAction() {
  alert('You clicked!')
}
</script>

<template>
  <Story>
    <Variant title="Basic usage" :init-state="initState">
      <template #default="{ state }">
        <SimpleButton @click="state.open = true">Click me</SimpleButton>

        <Toast v-model:open="state.open">
          <template #title>Button has been clicked</template>
          <template #description>Here is the toasty toast you ordered!</template>
        </Toast>
      </template>
    </Variant>

    <Variant title="With an appearance or icon" :init-state="initStateIcons">
      <template #default="{ state }">
        <div :style="{ display: 'flex', flexWrap: 'wrap', gap: '0.5rem' }">
          <SimpleButton @click="state.info = true">Open info toast</SimpleButton>
          <SimpleButton @click="state.warn = true">Open warn toast</SimpleButton>
          <SimpleButton @click="state.danger = true">Open danger toast</SimpleButton>
          <SimpleButton @click="state.success = true">Open success toast</SimpleButton>
          <SimpleButton @click="state.custom = true">Open toast with icon</SimpleButton>
        </div>

        <Toast v-model:open="state.info" :appearance="ToastAppearance.INFO">
          <template #title>I'm an info toast!</template>
          <template #description>And that's a generic toast message.</template>
        </Toast>
        <Toast v-model:open="state.warn" :appearance="ToastAppearance.WARNING">
          <template #title>I'm a warning toast!</template>
          <template #description>And that's a generic toast message.</template>
        </Toast>
        <Toast v-model:open="state.danger" :appearance="ToastAppearance.DANGER">
          <template #title>I'm a danger toast!</template>
          <template #description>And that's a generic toast message.</template>
        </Toast>
        <Toast v-model:open="state.success" :appearance="ToastAppearance.SUCCESS">
          <template #title>I'm a success toast!</template>
          <template #description>And that's a generic toast message.</template>
        </Toast>
        <Toast v-model:open="state.custom">
          <template #icon><CookieIcon /></template>
          <template #title>It's cookie time!</template>
          <template #description>Nom nom nom!</template>
        </Toast>
      </template>
    </Variant>

    <Variant title="With an action button" :init-state="initState">
      <template #default="{ state }">
        <SimpleButton @click="state.open = true">Add to calendar</SimpleButton>

        <Toast v-model:open="state.open" alt-text="Goto schedule to undo" @action="onAction">
          <template #title>Button has been clicked</template>
          <template #description>Here is the toasty toast you ordered!</template>
          <template #action>Undo</template>
        </Toast>
      </template>
    </Variant>

    <Variant title="Using an imperative API">
      <SimpleButton @click="toastRef?.publish()">Click me</SimpleButton>

      <PublishableToast ref="toastRef">
        <template #title>Button has been clicked</template>
        <template #description>Here is the toasty toast you ordered!</template>
      </PublishableToast>

      <template #controls />
    </Variant>
  </Story>
</template>

<docs lang="md">
Displays a temporary message to the user. Can offer an [action](#actions) button.

Toasts are tricky to use with regards to accessibility. Only use them to convey low-importance messages that could be
safely ignored, such as the confirmation a task has been completed successfully. The text should be short and straight
to the point, while still being compliant with [WCAG 2.2 Guideline 3.1. Readable](https://www.w3.org/TR/WCAG22/#readable).
See [WCAG 2.2 & WAI-ARIA 1.2](#wcag-2-2-wai-aria-1-2) section below.

Toasts should only be used as a response to a user-action, and in some rare occasions as the result of a background
task, such as a long running job status update. If the attention of the user is absolutely required, use
an [AlertDialog](./dialog/AlertDialog.story.vue) instead.

Toasts can be closed by gesture (swiping them out of viewport), or via the close button at the top.

### Toast duration
The implementation stops the timer when hovering or focusing the region via the `F8` hotkey. The duration must not be
too low; the user must have reasonable time to understand a toast showed up and react accordingly even if the message
is temporary.

To prevent the toast from disappearing after a set period of time, set the duration to `Infinity`.

### Appearance
It is possible to change the appearance of the toast. Note that this is purely aesthetic and does not convey any extra
information. The icon is always hidden to screen readers.

It is possible to combine both a custom appearance and a custom icon, to get for example a warning toast with a traffic
cone as an icon, if that's what you want.

The color of the action button will also be changed according to the set appearance.

### Action
The ability to offer an action must **not** be considered a viable way to receive user input; the action should be
provided for convenience and **must** come with an alternative action (`alt-text`).

The alternative action should direct users to a permanent place where they can perform the proposed action. It is also
possible to implement a custom hotkey logic and inform the users they can perform the action using it. In that case,
the toast duration **must** be increased to at least 10 seconds, and the sensibility should be `foreground` to
immediately announce the toast.

### Sensibility
The default sensibility of toasts is `foreground`, which means they will be pushed as an `assertive` live region and
will cause an interruption in the user's current task, immediately announcing the toast.

This is not ideal but acceptable for toasts that are the result of a user-input (such as submitting a form; on success
the form closes and the toast announces the success of the operation).

If the toast is the result of a background operation, such as a long-running job the user triggered a long time ago,
then it must have a `background` sensibility.

### WCAG 2.2 & WAI-ARIA 1.2
The following concepts from the WCAG 2.2 and WAI-ARIA 1.2 specifications are involved when using this component:

- [WCAG 2.2 - SC 2.2.1. Timing Adjustable (A)](https://www.w3.org/TR/WCAG22/#timing-adjustable)
  - By itself, this component **does not guarantee** compliance with this success criterion. See below.
- [WCAG 2.2 - SC 2.2.3. No Timing (AAA)](https://www.w3.org/TR/WCAG22/#no-timing)
- [WCAG 2.2 - SC 2.2.4. Interruptions (AAA)](https://www.w3.org/TR/WCAG22/#interruptions)
  - `polite` waits for graceful interruption, but are not a guarantee the interruption will be postponed satisfactorily.
- [WAI-ARIA 1.2 `status` role](https://www.w3.org/TR/wai-aria/#status)
- [WAI-ARIA 1.2 `aria-live` property](https://www.w3.org/TR/wai-aria/#aria-live)

#### Compliance with WCAG 2.2 Success Criterion 2.2.1.
Toasts messages have long been a concern with regards to compliance with this guideline. While the other relevant
guidelines are taken care of by the underlying implementation, the presence of a timer is causing troubles.

Version 2.2 of the WCAG added clarification in its [WCAG 2.2 Understanding Docs](https://www.w3.org/WAI/WCAG22/Understanding/timing-adjustable.html)
regarding toast messages:

> Content that operates on a timer does not need to be time adjustable if there is an alternative that does not rely on
> a timer. For example, a web application such as an email client provides notification of new email arriving with a
> temporary message (such as a 'toast' message) in the lower right-hand side of the interface, and the message
> disappears after 5 seconds. Users are able to identify the arrival of email through other means, such as viewing the
> Inbox, so the disappearance of the message does not set a time limit on the their ability to determine if new mail
> has arrived. If the user has no other means of discovering the same information (or performing the same function),
> then each message would need to meet this Success Criterion in order to provide users with sufficient time to access
> the information.

The application-level usage must ensure the duration for which toasts are configurable to meet SC 2.2.1..
The WCAG 2.2 Techniques proposes implementing one of these techniques for sufficient compliance:
- [Technique G180: Providing the user with a means to set the time limit to 10 times the default time limit](https://www.w3.org/WAI/WCAG22/Techniques/general/G180)
- [Technique G198: Providing a way for the user to turn the time limit off](https://www.w3.org/WAI/WCAG22/Techniques/general/G198)

---
See also: https://www.radix-vue.com/components/toast.html
</docs>
