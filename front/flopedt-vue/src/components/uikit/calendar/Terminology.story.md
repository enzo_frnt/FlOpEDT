---
title: calendar/Terminology
icon: carbon:spell-check
---

# Calendar Terminology Guide
As flop! may deal with a lot of types of calendar, it's important to know what specific type of calendar is being
mentioned unambiguously.

This terminology guide is here to help harmonize use a given term to mention a given type of calendar and streamline
documentation and communication efforts.

## Terms
- **Date Calendar**: A simple, compact calendar which displays a single month at a time in form of a 7x6
  grid where each line is a single week. Days do not carry any information and are no bigger than a button.
  - See the [DateCalendar](./DateCalendar.story.vue) component

- **Range Calendar**: A simple, compact calendar which displays a single month at a time in form of a 7x6
  grid where each line is a single week. Days do not carry any information and are no bigger than a button.
  - See the [RangeCalendar](./RangeCalendar.story.vue) component

- **Schedule Calendar**: A calendar with multiple (or a single) columns. Displays events scheduled for a given day
  based on the time they will happen. May contain more complex structure, such as sub-column-based groups to display
  multiple POVs at a time.
  - Example: [QCalendarDay](https://qcalendar.netlify.app/developing/qcalendarday-week/week-slot-day-body) 

- **Grid Schedule Calendar**: Displays a month in form of a 7x6 grid where each line is a single week. Days are
  (relatively) large cases that can contain a list of events scheduled for that day. Not recommended for displaying 
  large amounts of data per day.
  - Example: [QCalendarMonth](https://qcalendar.netlify.app/developing/qcalendarmonth/month-slot-week) 

- **Resource Calendar**: A grid showing the usage of a list of resources and their availability. Each row represents
  a resource, and columns represents time. Note that the existence of "columns" is useful for dividing the space
  in discrete increments visually, but does not necessarily represent an indivisible unit of time.
  - Examples: [QCalendarResource](https://qcalendar.netlify.app/developing/qcalendarresource/resource-slot-resource-intervals) (single day)
  - [QCalendarScheduler](https://qcalendar.netlify.app/developing/qcalendarscheduler/scheduler-slot-resource-days) (week)
