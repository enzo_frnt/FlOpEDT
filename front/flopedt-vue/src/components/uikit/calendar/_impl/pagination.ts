import { type DateValue, endOfMonth, endOfWeek, startOfMonth, startOfWeek } from '@internationalized/date'
import { isBetweenInclusive } from 'radix-vue/date'

type PaginationFn = (date: DateValue) => DateValue
export default function pagination(
  prevPage: PaginationFn | undefined,
  nextPage: PaginationFn | undefined,
  locale: string,
  isWeek: boolean
) {
  if (isWeek) {
    if (!prevPage) {
      prevPage = (d) => {
        const weekStart = startOfWeek(d, locale)
        // @ts-expect-error -- See https://github.com/adobe/react-spectrum/pull/6749
        const weekEnd = endOfWeek(d, locale)
        const prevMonth = startOfMonth(d).subtract({ days: 1 })
        if (isBetweenInclusive(prevMonth, weekStart, weekEnd)) return prevMonth

        return weekStart.subtract({ weeks: 1 })
      }
    }
    if (!nextPage)
      nextPage = (d) => {
        const weekStart = startOfWeek(d, locale)
        // @ts-expect-error -- See https://github.com/adobe/react-spectrum/pull/6749
        const weekEnd = endOfWeek(d, locale)
        const nextMonth = endOfMonth(d).add({ days: 1 })
        if (isBetweenInclusive(nextMonth, weekStart, weekEnd)) return nextMonth

        return weekStart.add({ weeks: 1 })
      }
  }

  return {
    prevPage,
    nextPage,
  }
}
