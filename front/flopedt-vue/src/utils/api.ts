import {
  AvailabilityBack,
  Course,
  Department,
  ModuleAPI,
  RoomAPI,
  ScheduledCourse,
  StartTime,
  TimeSettingBack,
  UserAPI,
  Period,
  CourseType,
  CourseUpdateData,
  CourseCreateData,
  TrainingPeriod,
  Dependency,
  DependencyCreateData,
  DependencyUpdateData,
} from '@/ts/type'

import { buildUrl, dateToString, durationDjangoToMinutes } from '@/helpers'
import { TimeSetting } from '@/stores/declarations'
import { parseTime, parseTimestamp } from '@quasar/quasar-ui-qcalendar'

const API_ENDPOINT = '/fr/api/'

const urls = {
  getScheduledcourses: 'v1/base/courses/scheduled_courses',
  getRooms: 'v1/base/courses/rooms',
  getTimeSettings: 'base/timesettings',
  roomreservation: 'roomreservations/reservation',
  roomreservationtype: 'roomreservations/reservationtype',
  reservationperiodicity: 'roomreservations/reservationperiodicity',
  reservationperiodicitytype: 'roomreservations/reservationperiodicitytype',
  reservationperiodicitybyweek: 'roomreservations/reservationperiodicitybyweek',
  reservationperiodicityeachmonthsamedate: 'roomreservations/reservationperiodicityeachmonthsamedate',
  reservationperiodicitybymonth: 'roomreservations/reservationperiodicitybymonth',
  reservationperiodicitybymonthxchoice: 'roomreservations/reservationperiodicitybymonthxchoice',
  courses: 'v1/base/courses/courses',
  scheduledcourses: 'v1/base/courses/scheduledcourses',
  coursetypes: 'courses/type',
  coursemodules: 'courses/module',
  users: 'user/users',
  getTutors: 'v1/people/tutors',
  booleanroomattributes: 'rooms/booleanattributes',
  numericroomattributes: 'rooms/numericattributes',
  booleanroomattributevalues: 'rooms/booleanattributevalues',
  numericroomattributevalues: 'rooms/numericattributevalues',
  timingPeriods: 'v1/base/timing/periods',
  getGroups: 'v1/base/groups/structural_groups',
  getTransversalGroups: 'v1/base/groups/transversal_groups',
  getModules: 'v1/base/courses/module',
  getTrainingPeriods: 'base/trainingperiods',
  dependencies: 'v1/base/courses/dependencies',
  getAvailability: 'v1/availability/user',
  getStartTimes: 'v1/constraint/base/course_start_time',
}

export function getCookie(name: string) {
  if (!document.cookie) {
    return null
  }
  const xsrfCookies = document.cookie
    .split(';')
    .map((c) => c.trim())
    .filter((c) => c.startsWith(name + '='))

  if (xsrfCookies.length === 0) {
    return null
  }
  return decodeURIComponent(xsrfCookies[0].split('=')[1])
}

const csrfToken = getCookie('csrftoken')

console.log('csrfToken retrieved: ', csrfToken)

interface FlopAPI {
  getScheduledCourses(from?: Date, to?: Date, department_id?: number, tutor?: number): Promise<Array<ScheduledCourse>>

  getModules(): Promise<ModuleAPI[]>

  getTutors(id?: number): Promise<Array<UserAPI>>

  getDependencies(): Promise<Dependency[]>

  getAllRooms(department?: Department): Promise<Array<RoomAPI>>

  getRoomById(id: number): Promise<RoomAPI | undefined>

  getAvailabilities(userId: number, from: Date, to: Date): Promise<Array<AvailabilityBack>>

  getTimeSettings(): Promise<TimeSetting[]>

  getStartTimes(deptId?: number): Promise<StartTime[]>

  getNewPeriods(): Promise<Array<Period>>

  getTrainingPeriods(departmentId?: number): Promise<Array<TrainingPeriod>>

  fetch: {
    /** @deprecated */
    courses(params: { week?: number; year?: number; department?: string }): Promise<Array<Course>>
    /** @deprecated */
    courseTypes(params: { department: string }): Promise<Array<CourseType>>
    /** @deprecated */
    courseModules(params: { department: string }): Promise<Array<ModuleAPI>>
  }
  delete: {
    /** @deprecated */
    deleteCourse(id: number): Promise<unknown>
    deleteDependency(id: number): Promise<unknown>
  }
  patch: {
    /** @deprecated */
    patchCourse(course: CourseUpdateData): Promise<Course>
    patchDependency(dependency: DependencyUpdateData): Promise<Dependency>
  }
  post: {
    /** @deprecated */
    postCourse(course: CourseCreateData): Promise<Course>
    postDependency(dependency: DependencyCreateData): Promise<Dependency>
  }
}

export const api: FlopAPI = {
  async getScheduledCourses(
    from?: Date,
    to?: Date,
    department_id?: number,
    tutor?: number
  ): Promise<Array<ScheduledCourse>> {
    const scheduledCourses: Array<ScheduledCourse> = []
    const context = new Map<string, string | number | undefined | null>([['dept_id', department_id]])
    if (from) {
      context.set('from_date', dateToString(from))
    }
    if (to) {
      context.set('to_date', dateToString(to))
    }
    if (tutor !== -1) {
      context.set('tutor_name', tutor)
    }
    const finalUrl = buildUrl(API_ENDPOINT + urls.getScheduledcourses + '/', context)
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        if (!response.ok) {
          throw Error('Error : ' + response.status)
        }
        await response
          .json()
          .then(
            (
              data: {
                id: number
                room_id: number
                start_time: string
                end_time: string
                course_id: number
                tutor_id: number
                module_id: number
                train_prog_id: number
                number: number
                course_type_id: number
                supp_tutors_ids: number[]
                group_ids: number[]
              }[]
            ) => {
              data.forEach(
                (d: {
                  id: number
                  room_id: number
                  start_time: string
                  end_time: string
                  course_id: number
                  tutor_id: number
                  module_id: number
                  train_prog_id: number
                  number: number
                  course_type_id: number
                  supp_tutors_ids: number[]
                  group_ids: number[]
                }) => {
                  const sc: ScheduledCourse = {
                    id: d.id,
                    roomId: d.room_id,
                    start_time: parseTimestamp(d.start_time),
                    end_time: parseTimestamp(d.end_time),
                    courseId: d.course_id,
                    tutor: d.tutor_id,
                    id_visio: -1,
                    moduleId: d.module_id,
                    trainProgId: d.train_prog_id,
                    groupIds: [],
                    suppTutorsIds: [],
                    no: d.number,
                    courseTypeId: d.course_type_id,
                  }
                  d.supp_tutors_ids.forEach((sti: number) => {
                    sc.suppTutorsIds.push(sti)
                  })
                  d.group_ids.forEach((gId: number) => {
                    sc.groupIds.push(gId)
                  })
                  scheduledCourses.push(sc)
                }
              )
            }
          )
          .catch((error: Error) => console.log('Error : ' + error.message))
      })
      .catch((error: Error) => {
        console.log(error.message)
      })
    return scheduledCourses
  },
  async getDependencies(): Promise<Dependency[]> {
    let dependencies: Array<Dependency> = []
    const finalUrl: string = API_ENDPOINT + urls.dependencies

    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    }).then(async (response) => {
      if (!response.ok) {
        throw Error('Error : ' + response.status)
      }
      await response.json().then((data: Dependency[]) => {
        dependencies = data
      })
    })

    return dependencies
  },
  async getModules(): Promise<Array<ModuleAPI>> {
    let modules: Array<ModuleAPI> = []
    const finalUrl: string = API_ENDPOINT + urls.getModules
    // if (department_id) finalUrl += '/?dept_id=' + department_id
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        if (!response.ok) {
          throw Error('Error : ' + response.status)
        }
        await response
          .json()
          .then((data: ModuleAPI[]) => {
            modules = data
          })
          .catch((error: Error) => console.log('Error : ' + error.message))
      })
      .catch((error: Error) => {
        console.log(error.message)
      })
    return modules
  },
  async getTutors(id?: number): Promise<Array<UserAPI>> {
    const tutors: Array<UserAPI> = []
    let finalUrl: string = API_ENDPOINT + urls.getTutors
    if (id) finalUrl += '/' + id
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        if (!response.ok) {
          throw Error('Error : ' + response.status)
        }
        await response
          .json()
          .then((data: UserAPI[] | UserAPI) => {
            if (id) tutors.push(data as UserAPI)
            else if (Array.isArray(data)) {
              data.forEach((d: UserAPI) => tutors.push(d))
            }
          })
          .catch((error: Error) => console.log('Error : ' + error.message))
      })
      .catch((error: Error) => {
        console.log(error.message)
      })
    return tutors
  },
  async getAllRooms(department?: Department): Promise<Array<RoomAPI>> {
    let rooms: Array<RoomAPI> = []
    let finalUrl = API_ENDPOINT + urls.getRooms
    if (department) finalUrl += '/?dept=' + department?.abbrev
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        if (!response.ok) {
          return Promise.reject('Erreur : ' + response.status + ': ' + response.statusText)
        }
        await response
          .json()
          .then((data: RoomAPI[]) => {
            rooms = data
          })
          .catch((error: Error) => {
            return Promise.reject(error.message)
          })
      })
      .catch((error: Error) => {
        console.log(error.message)
      })
    return rooms
  },
  async getRoomById(id: number): Promise<RoomAPI | undefined> {
    let room: RoomAPI | undefined
    if (id) {
      await fetch(API_ENDPOINT + urls.getRooms + '/' + id, {
        method: 'GET',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
      })
        .then(async (response) => {
          if (!response.ok) {
            return Promise.reject('Erreur : ' + response.status + ': ' + response.statusText)
          }
          await response
            .json()
            .then((data: RoomAPI) => {
              room = data
            })
            .catch((error: Error) => {
              return Promise.reject(error.message)
            })
        })
        .catch((error: Error) => {
          console.log(error.message)
        })
    }
    return room
  },
  async getAvailabilities(userId: number, from: Date, to: Date): Promise<Array<AvailabilityBack>> {
    const availabilities: AvailabilityBack[] = []
    await fetch(
      API_ENDPOINT +
        urls.getAvailability +
        '/?user_id=' +
        userId +
        '&from_date=' +
        dateToString(from) +
        '&to_date=' +
        dateToString(to),
      {
        method: 'GET',
        credentials: 'same-origin',
        headers: { 'Content-Type': 'application/json' },
      }
    )
      .then(async (response) => {
        if (!response.ok) {
          return Promise.reject('Erreur : ' + response.status + ': ' + response.statusText)
        }
        await response
          .json()
          .then(
            (
              data: {
                subject_type: string
                start_time: string
                duration: string
                userId: number
                value: number
              }[]
            ) => {
              data.forEach((avail) => {
                availabilities.push({
                  av_type: avail.subject_type,
                  start_time: parseTimestamp(avail.start_time),
                  duration: avail.duration,
                  dataId: userId,
                  value: avail.value,
                })
              })
            }
          )
          .catch((error: Error) => {
            return Promise.reject(error.message)
          })
      })
      .catch((error: Error) => {
        console.log(error.message)
      })
    return availabilities
  },
  async getTimeSettings(): Promise<TimeSetting[]> {
    const timeSettings: TimeSetting[] = []
    await fetch(API_ENDPOINT + urls.getTimeSettings, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    }).then(async (response) => {
      if (!response.ok) {
        return Promise.reject('Erreur : ' + response.status + ': ' + response.statusText)
      }
      await response.json().then((data: TimeSettingBack[]) => {
        data.forEach((timeSettingBack: TimeSettingBack) => {
          timeSettings.push({
            id: timeSettingBack.id,
            dayStartTime: parseTime(timeSettingBack.day_start_time),
            dayEndTime: parseTime(timeSettingBack.day_end_time),
            morningEndTime: parseTime(timeSettingBack.morning_end_time),
            afternoonStartTime: parseTime(timeSettingBack.afternoon_start_time),
            days: timeSettingBack.days,
            departmentId: timeSettingBack.department,
          })
        })
      })
    })
    return timeSettings
  },
  async getStartTimes(deptId?: number): Promise<StartTime[]> {
    const startTimes: StartTime[] = []
    let finalUrl = API_ENDPOINT + urls.getStartTimes
    if (deptId) finalUrl += '/?department_id=' + deptId
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    }).then(async (response) => {
      if (!response.ok) {
        return Promise.reject('Erreur : ' + response.status + ': ' + response.statusText)
      }
      await response.json().then(
        (
          data: {
            id: number
            department_id: number
            duration: string
            allowed_start_times: string[]
          }[]
        ) => {
          data.forEach((allowedStartTime) => {
            startTimes.push({
              id: allowedStartTime.id,
              departmentId: allowedStartTime.department_id,
              duration: durationDjangoToMinutes(allowedStartTime.duration),
              allowedStartTimes: allowedStartTime.allowed_start_times.map((ast: string) =>
                durationDjangoToMinutes(ast)
              ),
            })
          })
        }
      )
    })
    return startTimes
  },
  async getTrainingPeriods(departmentId?: number): Promise<TrainingPeriod[]> {
    let trainingPeriods: Array<TrainingPeriod> = []
    let finalUrl: string = API_ENDPOINT + urls.getTrainingPeriods
    if (departmentId) finalUrl += '/?dept=' + departmentId
    await fetch(finalUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' },
    }).then(async (response) => {
      if (!response.ok) {
        throw Error('Error : ' + response.status)
      }
      await response.json().then((data: TrainingPeriod[]) => {
        trainingPeriods = data
      })
    })
    return trainingPeriods
  },
  getNewPeriods() {
    return fetcher2(urls.timingPeriods)
  },
  fetch: {
    /** @deprecated */
    courses(params: { week?: number; year?: number; department?: string }) {
      return fetcher2(urls.courses, params, [['department', 'dept']])
    },
    /** @deprecated */
    courseTypes(params: { department: string }) {
      return fetcher2(urls.coursetypes, params, [['department', 'dept']])
    },
    /** @deprecated */
    courseModules(params: { department: string }) {
      return fetcher2(urls.coursemodules, params, [['department', 'dept']])
    },
  },
  delete: {
    deleteCourse(id: number): Promise<unknown> {
      return deleteData(urls.courses, id)
    },
    deleteDependency(id: number): Promise<unknown> {
      return deleteData(urls.dependencies, id)
    },
  },
  patch: {
    patchCourse(course): Promise<Course> {
      if (course.id === undefined) {
        const error = `Error : course have no ID`
        return Promise.reject(new Error(error))
      }
      return patchData(urls.courses, course.id, course)
    },
    patchDependency(dependency): Promise<Dependency> {
      if (dependency.id === undefined) {
        const error = `Error : dependency have no ID`
        return Promise.reject(new Error(error))
      }
      return patchData(urls.dependencies, dependency.id, dependency)
    },
  },
  post: {
    postCourse(course): Promise<Course> {
      return postData(urls.courses, course)
    },
    postDependency(dependency): Promise<Dependency> {
      return postData(urls.dependencies, dependency)
    },
  },
}

/* OLD CODE */
/* eslint-disable */

/** @deprecated */
async function fetchData(url: string, params: { [k: string]: any }) {
  const providedParams = params
  console.log('providedParams: ', providedParams)
  params = Object.assign(
    {
      method: 'GET',
      credentials: 'same-origin',
    },
    params
  )

  params.headers = Object.assign(
    {
      'Content-Type': 'application/json',
    },
    params.headers
  )

  let args = ''

  if (providedParams) {
    args = Object.keys(providedParams)
      .map((p) => p + '=' + providedParams[p])
      .join('&')
  }
  const finalUrl = `${API_ENDPOINT}${url}/?${args}`
  console.log(`Fetching ${finalUrl}...`)

  const response = await fetch(finalUrl, params)
  const json = (await response.json()) || {}
  if (!response.ok) {
    const errorMessage = json.error || `${response.status}`
    throw new Error(errorMessage)
  }
  return json
}

/**
 * Accepts an object and returns a new object with undefined values removed.
 * The object keys can be renamed using the renameList parameter.
 * @param obj The object to filter
 * @param renameList The rename list as an array of pair of strings as [oldName, newName]
 * @deprecated
 */
function filterObject(obj: { [key: string]: any }, renameList?: Array<[oldName: string, newName: string]>) {
  let filtered = Object.entries(obj).filter((entry) => entry[1])
  if (renameList) {
    filtered = filtered.map((entry: [string, any]) => {
      const toRename = renameList.find((renamePair) => renamePair[0] === entry[0])
      const keyName = toRename ? toRename[1] : entry[0]
      return [keyName, entry[1]]
    })
  }
  return Object.fromEntries(filtered)
}

/** @deprecated */
const fetcher2 = (url: string, params?: object, renameList?: Array<[string, string]>) =>
  fetchData(url, params ? filterObject(params, renameList) : {})

/** @deprecated */
async function sendData<T>(method: string, url: string, optional: { data?: unknown; id?: number }): Promise<T> {
  if (!['PUT', 'POST', 'DELETE', 'PATCH'].includes(method)) {
    return Promise.reject('Method must be either PUT, POST, PATCH, or DELETE')
  }

  // Setup headers
  const requestHeaders: HeadersInit = new Headers()
  requestHeaders.set('Content-Type', 'application/json')
  if (csrfToken) {
    requestHeaders.set('X-CSRFToken', csrfToken)
  }
  // Setup request
  const requestInit: RequestInit = {
    method: method,
    headers: requestHeaders,
  }
  if (optional.data) {
    requestInit.body = JSON.stringify(optional.data)
  }

  // Create url
  let endUrl = '/'
  if (optional.id) {
    endUrl = `/${optional.id}/`
  }
  const finalUrl = `${API_ENDPOINT}${url}${endUrl}`
  console.log(`Updating ${finalUrl}...`)

  // Wait for the response
  return await fetch(finalUrl, requestInit)
    .then(async (response) => {
      if (!response.ok) {
        const data = await response.json()
        const error = data.detail || `Error ${response.status}: ${response.statusText}`
        return Promise.reject(new Error(error))
      }
      return response.status !== 204 ? response.json() : null
    })
    .catch((reason) => {
      return Promise.reject(reason)
    })
}

/** @deprecated */
async function postData<T>(url: string, data: unknown): Promise<T> {
  const optional: { [key: string]: unknown } = {
    data: data,
  }
  return await sendData('POST', url, optional)
}

/** @deprecated */
async function patchData<T>(url: string, id: number, data: unknown): Promise<T> {
  const optional: { [key: string]: unknown } = {
    id: id,
    data: data,
  }
  return await sendData('PATCH', url, optional)
}


/** @deprecated */
function deleteData(url: string, id: number) {
  const optional: { [key: string]: unknown } = {
    id: id,
  }
  return sendData('DELETE', url, optional)
}
