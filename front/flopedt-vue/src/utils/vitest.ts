/* eslint-disable vue/one-component-per-file */

import type { EntitySet, ReadonlyEntitySet } from '@/ts/entities/base/entityset.ts'
import type { Entity } from '@/ts/entities/base/entity.ts'

import { createApp, h, watch } from 'vue'
import { type Router, createRouter, RouterView, createMemoryHistory } from 'vue-router'
import { createDeferred } from '@/utils/deferred.ts'

// Quick sanity check, just in case.
function _ensureVitest() {
  if (import.meta.env.TEST !== 'true') {
    throw new TypeError('This function cannot be called outside of Vitest tests!')
  }
}

export function withVueSetup<T>(setup: () => T) {
  _ensureVitest()

  let result: T

  const app = createApp({
    setup() {
      result = setup()
      return () => {}
    },
  })

  app.mount(document.createElement('div'))
  return [result!, app] as const
}

export async function withRouterSetup<T>(fn: () => T) {
  _ensureVitest()

  const deferred = createDeferred<T>()
  const router = createRouter({
    history: createMemoryHistory(),
    routes: [
      {
        path: '/:pathMatch(.*)',
        component: {
          setup() {
            deferred.resolve(fn())
            return () => {}
          },
        },
      },
    ],
  })

  const app = createApp({ render: () => h(RouterView) })
  app.use(router)

  app.mount(document.createElement('div'))
  const result = await deferred.promise

  return [result, router, app] as const
}

export function waitForNavigation(router: Router) {
  _ensureVitest()

  const deferred = createDeferred()
  watch(router.currentRoute, () => deferred.resolve(), { once: true })
  return deferred.promise
}

export function toWritableEntitySet<T extends Entity>(val: ReadonlyEntitySet<T>): EntitySet<T> {
  _ensureVitest()

  // Doing this is always safe since ReadonlyEntitySet only exists in the type system.
  return val as EntitySet<T>
}
