import { describe, it, expectTypeOf } from 'vitest'
import { SerializableTo } from '@/utils/types'

describe('SerializableTo', () => {
  type TestEntity = {
    id: number
    some_value: boolean
  }

  const basic = {
    id: 1,
    some_value: true,
  }

  class Identifier {
    constructor(public id: number) {}

    toJSON() {
      return this.id
    }
  }

  class Entity {
    constructor(
      public id: number,
      public someValue: boolean
    ) {}

    toJSON() {
      return {
        id: this.id,
        some_value: this.someValue,
      }
    }
  }

  class EntityIdentifier {
    constructor(
      public id: Identifier,
      public someValue: boolean
    ) {}

    toJSON() {
      return {
        id: this.id,
        some_value: this.someValue,
      }
    }
  }

  const goodPlainEntity = {
    id: 1,
    someValue: true,
    toJSON() {
      return { id: this.id, some_value: this.someValue }
    },
  }

  const badPlainEntity = {
    id: 1,
    some_value: true,
    toJSON() {
      return { bad: true }
    },
  }

  type FnSerializable = (v: SerializableTo<TestEntity>) => void

  it('accepts an entity that strictly conforms to the requested type', () => {
    expectTypeOf<FnSerializable>().toBeCallableWith(basic)
  })

  it('accepts an entity that serializes to the desired shape via toJSON', () => {
    const entity: Entity = null as unknown as Entity
    expectTypeOf<FnSerializable>().toBeCallableWith(entity)
  })

  it('understands and accepts nested toJSON serialization logic', () => {
    const entityIdentifier: EntityIdentifier = null as unknown as EntityIdentifier
    expectTypeOf<FnSerializable>().toBeCallableWith(entityIdentifier)
  })

  it('handles toJSON on plain objects', () => {
    expectTypeOf<FnSerializable>().toBeCallableWith(goodPlainEntity)
  })

  it('does not accept objects that conform to the requested type but have a bad toJSON', () => {
    // @ts-expect-error -- Expected for the purposes of this test
    expectTypeOf<FnSerializable>().toBeCallableWith(badPlainEntity)
  })
})
