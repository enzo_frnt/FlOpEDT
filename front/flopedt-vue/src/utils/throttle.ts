export function throttle<T extends unknown[]>(fn: (...args: T) => void, delay: number): (...args: T) => void {
  let pending = false
  let passedArgs: T | null
  return (...args: T) => {
    passedArgs = args
    if (!pending) {
      pending = true
      fn(...args)
      setTimeout(() => {
        pending = false
        if (passedArgs) {
          fn(...passedArgs)
          passedArgs = null
        }
      }, delay)
    } else {
      passedArgs = args
    }
  }
}
