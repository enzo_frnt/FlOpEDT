// https://github.com/cyyynthia/pronoundb.org/blob/79aae8af/packages/extension/src/utils/deferred.ts
// Adapted to have reject and comply with stricter type requirements

export type Deferred<T = void, E = Error> = {
  promise: Promise<T>
  resolve: (res: T) => void
  reject: (err: E) => void
}

export function createDeferred<T = void, E = Error>(): Deferred<T, E> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Whatever you say eslint lalalalalalala
  const deferred: any = {}

  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access -- Whatever you say eslint lalalalalalala
  deferred.promise = new Promise((resolve, reject) => void Object.assign(deferred, { resolve, reject }))

  return deferred as Deferred<T, E>
}
