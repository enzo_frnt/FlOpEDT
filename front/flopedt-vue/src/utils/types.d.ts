type Primitive = string | number | bigint | boolean | symbol | null | undefined

type AnyObject = Record<PropertyKey, unknown>

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyFunction = (...args: any[]) => any // unknown doesn't cut it: https://stackoverflow.com/a/62136603

// Helper to "force" TS to compute a given type's actual value
export type Pretty<T extends AnyObject> = { [TKey in keyof T]: T[TKey] extends AnyObject ? Pretty<T[TKey]> : T[TKey] }

// Helper to merge types following what JS would do for { ...T1, ...T2 }
export type Merge<T1 extends AnyObject, T2 extends AnyObject> =
  T2 extends Record<string, never> ? T1 : Omit<T1, keyof T2> & T2

// Helper to get the properties of a class
export type ClassProperties<TEntity> = Pretty<{
  [TKey in keyof TEntity as TEntity[TKey] extends AnyFunction ? never : TKey]: TEntity[TKey]
}>

// Helper type to accept anything that serializes to the desired shape. Useful for API types.
export type SerializableToResult<T> = T extends AnyObject
  ? Pretty<{ [TKey in keyof T]: SerializableTo<T[TKey]> }>
  : T extends ReadonlyArray<infer E>
    ? ReadonlyArray<SerializableTo<E>>
    : T extends Array<infer E>
      ? Array<SerializableTo<E>>
      : T

export type SerializableTo<T> =
  | (T extends Primitive ? T : SerializableToResult<T> & { toJSON?: never })
  | { toJSON(): SerializableToResult<T> }
