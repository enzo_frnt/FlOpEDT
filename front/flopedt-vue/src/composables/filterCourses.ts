import type { Course } from '@/ts/type'
import type { MaybeRefOrGetter } from 'vue'
import { computed, toRef, toValue } from 'vue'

/** Available filter types */
export enum FilterType {
  MODULE = 'module',
  GROUP = 'group',
  TEACHER = 'teacher',
  PERIOD = 'period',
  ROOM_TYPE = 'room_type',
  COURSE_TYPE = 'course_type',
}

/** A generic entity representation for the filters */
export interface FilterEntity {
  /** ID of the selected entity */
  id: number
  /** Name of the entity */
  name: string
  /** Aliases of the entity */
  alias?: string[]
  /** Color associated with the entity, if applicable */
  color?: { bg: string; fg: string }
}

export interface SelectedFilter {
  /** Whether the filter is in exclusion mode or not */
  exclude: boolean
  /** Type of filter */
  filter: FilterType
  /** ID of the selected entities */
  entities: number[]
}

export type FilteredCourseNode = {
  leaf: false
  filter: FilterType
  entity: FilterEntity | null
  children: FilteredCourses
  totalTime: { hours: number; minutes: number }
}
export type FilteredCourseLeaf = { leaf: true; children: Course[]; totalTime: { hours: number; minutes: number } }
export type FilteredCourses = FilteredCourseNode[] | [FilteredCourseLeaf]

type FilterEntityRegistry = {
  [FilterType.MODULE]: Map<number, FilterEntity>
  [FilterType.GROUP]: Map<number, FilterEntity>
  [FilterType.TEACHER]: Map<number, FilterEntity>
  [FilterType.PERIOD]: Map<number, FilterEntity>
  [FilterType.ROOM_TYPE]: Map<number, FilterEntity>
  [FilterType.COURSE_TYPE]: Map<number, FilterEntity>
}

const FILTER_TO_PROPERTY = (<const>{
  [FilterType.MODULE]: 'module',
  [FilterType.GROUP]: 'groups',
  [FilterType.TEACHER]: 'tutor',
  [FilterType.PERIOD]: 'period',
  [FilterType.ROOM_TYPE]: 'room_type',
  [FilterType.COURSE_TYPE]: 'type',
}) satisfies Record<FilterType, keyof Course>

type FlopEntity =
  | Course['module']
  | Course['groups'][number]
  | Exclude<Course['tutor'], null>
  | Exclude<Course['period'], null>
  | Course['room_type']
  | Course['type']

function createEntity(id: number, entity: FlopEntity): FilterEntity {
  if ('username' in entity) {
    // Tutor
    return { id, name: entity.username, alias: [`${entity.first_name} ${entity.last_name}`] }
  }

  if ('abbrev' in entity) {
    // Module
    return {
      id,
      name: entity.abbrev,
      alias: [entity.name],
      color: { bg: entity.display.color_bg, fg: entity.display.color_txt },
    }
  }

  if ('name' in entity) {
    if ('department' in entity) {
      // Course type
      return { id, name: entity.name, color: { bg: `var(--color-${entity.name})`, fg: `var(--color-text)` } }
    }

    if ('train_prog' in entity) {
      // Group
      return { id, name: entity.train_prog.abbrev + '-' + entity.name }
    }

    // Group, Room Type, Period
    return { id, name: entity.name }
  } /* v8 ignore start */

  // This branch is theoretically "impossible" unless a bug occurs.
  // The type of entity is never, and it'll never be covered by code coverage

  // @ts-expect-error -- See above comment.
  // eslint-disable-next-line -- See above comment.
  return { id, name: entity.id.toString(), alias: [] }

  /* v8 ignore end */
}

/**
 * Sort the courses array by week, then by module, then by teacher, and then by course type.
 * If a teacher is logged in, their courses are hoisted at the top of the list.
 * @internal
 *
 * @param coursesList - The courses' list fetched by the API.
 * @param teacherId - ID of the logged in teacher, if logged in.
 * @returns The sorted courses.
 */
function defaultCourseSort(coursesList: Course[], teacherId: number | undefined | null) {
  return [...coursesList].sort((course1, course2) => {
    let result = 0
    // Sort by teacher, only if the logged-in user is a teacher
    if (teacherId) result = sortByHoistedTeacher(course1, course2, teacherId)
    if (!result) result = sortByPeriod(course1, course2)
    if (!result) result = sortByModule(course1, course2)
    if (!result) result = sortByTeacher(course1, course2)
    if (!result) result = sortByCourseType(course1, course2)
    return result
  })
}

type Grouped = { entity: FilterEntity | null; items: Course[] }
function doGroupEntity(
  course: Course,
  filter: SelectedFilter,
  registry: FilterEntityRegistry,
  map: Map<unknown, Grouped>,
  entity: FlopEntity | null
) {
  let filterEntity: FilterEntity | null = null
  if (entity) {
    filterEntity = createEntity(entity.id, entity)
    registry[filter.filter].set(entity.id, filterEntity)

    if (filter.entities.length && filter.exclude === filter.entities.includes(entity.id)) {
      return
    }
  } else if (filter.filter === FilterType.TEACHER || filter.filter === FilterType.PERIOD) {
    // handle null teacher or period case
    filterEntity = registry[filter.filter].get(-1) || { id: -1, name: 'Non défini' }
    if (filter.entities.length && filter.exclude === filter.entities.includes(-1)) {
      return
    }
  }

  const el = map.get(entity?.id)
  if (!el) {
    map.set(entity?.id, {
      entity: entity ? createEntity(entity.id, entity) : filterEntity,
      items: [course],
    })
  } else {
    el.items.push(course)
  }
}

function groupByProperty(courses: Course[], filter: SelectedFilter, registry: FilterEntityRegistry): Grouped[] {
  const property = FILTER_TO_PROPERTY[filter.filter]

  const map = new Map<unknown, Grouped>()
  // C-style loop for performance reasons
  for (let i = 0; i < courses.length; i++) {
    const course = courses[i]
    const entityId = course[property]
    if (Array.isArray(entityId)) {
      for (const e of entityId) {
        doGroupEntity(course, filter, registry, map, e)
      }
    } else {
      doGroupEntity(course, filter, registry, map, entityId)
    }
  }

  return Array.from(map.values())
}

function getTotalTime(courses: Course[]): { hours: number; minutes: number } {
  let secondsSum = 0

  for (const course of courses) {
    const parts = course.duration.split(':')
    const hours = parseInt(parts[0], 10)
    const minutes = parseInt(parts[1], 10)
    const seconds = parseInt(parts[2], 10)

    secondsSum += hours * 3600 + minutes * 60 + seconds
  }

  const totalHours = Math.floor(secondsSum / 3600)
  secondsSum %= 3600
  const totalMinutes = Math.floor(secondsSum / 60)

  return { hours: totalHours, minutes: totalMinutes }
}

function getChildrenTotalTime(children: FilteredCourses) {
  let totalSeconds = 0

  // Iterate over each child
  for (const child of children) {
    const { hours, minutes } = child.totalTime

    totalSeconds += hours * 3600 + minutes * 60
  }

  const totalHours = Math.floor(totalSeconds / 3600)
  totalSeconds %= 3600
  const totalMinutes = Math.floor(totalSeconds / 60)

  return { hours: totalHours, minutes: totalMinutes }
}

/**
 * Performs filtering (and grouping) recursively on the provided courses
 *
 * @param courses The courses to filter
 * @param filters The filters to apply
 * @param registry The entity registry to store all the entities that are relevant to the filtered set
 * @returns The filtered and grouped courses
 */
function filterCourses(courses: Course[], filters: SelectedFilter[], registry: FilterEntityRegistry): FilteredCourses {
  if (!filters.length) return [{ leaf: true, children: courses, totalTime: getTotalTime(courses) }]

  const [filter, ...remaining] = filters
  const grouped = groupByProperty(courses, filter, registry)

  // Conditionally add null teacher entity if there are courses with null teachers
  if (filter.filter === FilterType.TEACHER) {
    const hasNullTeacher = courses.some((course) => !course.tutor)
    if (hasNullTeacher) {
      registry[FilterType.TEACHER].set(-1, { id: -1, name: 'Non défini' })
    }
  }

  // Conditionally add null period entity if there are courses with null teachers
  if (filter.filter === FilterType.PERIOD) {
    const hasNullPeriod = courses.some((course) => !course.period)
    if (hasNullPeriod) {
      registry[FilterType.PERIOD].set(-1, { id: -1, name: 'Non défini' })
    }
  }

  const groupedCourses: FilteredCourseNode[] = []

  // C-style loop for performance reasons
  for (let i = 0; i < grouped.length; i++) {
    const { entity, items } = grouped[i]
    const children = filterCourses(items, remaining, registry)

    if (!Array.isArray(children) || children.length) {
      groupedCourses.push({
        leaf: false,
        filter: filter.filter,
        entity: entity,
        children: children,
        totalTime: getChildrenTotalTime(children),
      })
    }
  }

  return groupedCourses
}

export function useFilteredCourses(
  courses: MaybeRefOrGetter<Course[]>,
  teacherId: MaybeRefOrGetter<number | undefined | null>,
  filters: MaybeRefOrGetter<SelectedFilter[]>,
  periods?: MaybeRefOrGetter<number[] | undefined>
) {
  const sorted = computed<Course[]>(() => {
    const coursesValue = toValue(courses)
    const teacherIdValue = toValue(teacherId)
    return defaultCourseSort(coursesValue, teacherIdValue)
  })

  const filtered = computed(() => {
    const entities: FilterEntityRegistry = {
      [FilterType.MODULE]: new Map(),
      [FilterType.GROUP]: new Map(),
      [FilterType.TEACHER]: new Map(),
      [FilterType.PERIOD]: new Map(),
      [FilterType.ROOM_TYPE]: new Map(),
      [FilterType.COURSE_TYPE]: new Map(),
    }

    const periodsValue = toValue(periods)
    let coursesValue = toValue(sorted)
    const filtersValue = toValue(filters)

    // Apply training period global filter on courses
    if (periodsValue)
      coursesValue = coursesValue.filter((course) => !course.period || periodsValue.includes(course.period.id))

    return { entities, filtered: filterCourses(coursesValue, filtersValue, entities) }
  })

  return {
    sorted,
    filtered: toRef(() => filtered.value.filtered),
    entities: toRef(() => ({
      modules: Array.from(filtered.value.entities[FilterType.MODULE].values()).sort(sortById),
      groups: Array.from(filtered.value.entities[FilterType.GROUP].values()).sort(sortById),
      teachers: Array.from(filtered.value.entities[FilterType.TEACHER].values()).sort(sortById),
      periods: Array.from(filtered.value.entities[FilterType.PERIOD].values()).sort(sortById),
      roomTypes: Array.from(filtered.value.entities[FilterType.ROOM_TYPE].values()).sort(sortById),
      courseTypes: Array.from(filtered.value.entities[FilterType.COURSE_TYPE].values()).sort(sortById),
    })),
  }
}

/**
 * Check if 1,2 or none of the course's id are the same as the teacherId
 * @internal
 *
 * @param course1 - First course
 * @param course2 - Second course
 * @param tutorId - Tutor logged id
 *
 * @return 0 : the all have (or don't have) the same id as the tutor
 *         1 : id1 is correct and id2 not
 *        -1 : id1 is incorrect and id2 correct
 */
function sortByHoistedTeacher(course1: Course, course2: Course, tutorId: number): number {
  const id1 = course1.tutor?.id
  const id2 = course2.tutor?.id

  if (id1 === id2) return 0
  if (id1 === undefined) return 1
  if (id2 === undefined) return -1
  if (id1 === tutorId) return -1
  if (id2 === tutorId) return 1
  return 0
}

/**
 * Sort by teacher ID
 * @internal
 *
 * @return An int to know which teacher have the smallest ID
 */
function sortByTeacher(course1: Course, course2: Course): number {
  const id1 = course1.tutor?.id
  const id2 = course2.tutor?.id

  if (id1 === id2) return 0
  if (id1 === undefined) return 1
  if (id2 === undefined) return -1
  return id1 - id2
}

/**
 * Check which course begin first
 * @internal
 *
 * @return An int to know which day come first
 */
function sortByPeriod(course1: Course, course2: Course): number {
  const day1 = +(course1.period?.start_date.replaceAll('-', '') ?? 0)
  const day2 = +(course2.period?.start_date.replaceAll('-', '') ?? 0)
  return day1 - day2
}

/**
 * Check which Module is first in the list
 * @internal
 *
 * @return An int to know which module have the smallest ID
 */
function sortByModule(course1: Course, course2: Course): number {
  return course1.module.id - course2.module.id
}

/**
 * Check which course type comes first
 * @internal
 *
 * @return An int to know which course have the smallest ID
 */
function sortByCourseType(course1: Course, course2: Course): number {
  return course1.type.id - course2.type.id
}

type HasId = { id: number }
function sortById(a: HasId, b: HasId) {
  return a.id - b.id
}
