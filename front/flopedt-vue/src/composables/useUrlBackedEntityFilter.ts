import type { Entity } from '@/ts/entities/base/entity'

import { type ComputedRef, computed, watch, watchEffect, shallowReactive } from 'vue'
import { Identifier } from '@/ts/entities/base/identifier'
import useUrlBackedArray from './useUrlBackedArray.ts'
import { isEqual } from 'lodash-es'

// Avoids bringing in the full CrudStore type; makes things a lot easier >:)
// It also allows passing in unrelated types, such as the Groups store (union of structural and transversal)
type Store<T extends Entity> = {
  isLoading: boolean
  getEntitiesByIds: (ids: ComputedRef<Identifier[]>) => ComputedRef<T[]>
}

export default function useUrlBackedEntityFilter<T extends Entity>(
  store: Store<T>,
  key: string,
  filter?: (v: T, idx: number, arr: T[]) => unknown
) {
  const selected = useUrlBackedArray(key)
  const ids = computed(() => selected.value.map((id) => new Identifier(+id)))

  // This code is here to watch the value of the IDs, and automatically update them in the URL when they change.
  // IDs may change when they go from temporary IDs to permanent IDs. Ideally, the URL should be updated.
  const cleanups: Array<() => void> = []
  watchEffect(() => {
    cleanups.forEach((fn) => fn())

    for (let i = 0; i < ids.value.length; i++) {
      const id = ids.value[i]
      cleanups.push(watch(id.ref, () => (selected.value[i] = id.toString())))
    }
  })

  const matchedEntities = store.getEntitiesByIds(ids)
  const entities = computed({
    get: () => shallowReactive(filter ? matchedEntities.value.filter(filter) : matchedEntities.value),
    set: (v) => (selected.value = v.map((e) => e.id.toString())),
  })

  // Synchronize entities
  watch(
    () => entities.value,
    () => {
      const newIds = entities.value.map((e) => e.id.toString())
      if (!store.isLoading && !isEqual(newIds, selected.value)) selected.value = newIds
    },
    { deep: true, immediate: true }
  )

  return entities
}
