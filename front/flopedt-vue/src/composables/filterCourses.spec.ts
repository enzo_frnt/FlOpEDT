import type { Course } from '@/ts/type'
import { describe, expect, it } from 'vitest'
import { FilterType, useFilteredCourses } from './filterCourses'

it('sorts courses using a default ordering', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { sorted } = useFilteredCourses(arrays[0], null, [])
  expect(sorted.value.map((c) => c.id)).toEqual(arrays[1].map((c) => c.id))
})

it('hoists the connected tutor in the default order', () => {
  const arrays = defineTestsCoursesArrays(true)
  const { sorted } = useFilteredCourses(arrays[0], -1, [])
  expect(sorted.value.map((c) => c.id)).toEqual(arrays[1].map((c) => c.id))
})

it('groups based on the specified filters', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.MODULE, exclude: false, entities: [] },
  ])

  expect(filtered.value.length).toBe(3)
  for (const group of filtered.value) {
    // Tested this way to trigger type inference
    if (group.leaf) expect.unreachable('Expected node, got leaf!')

    expect(group.children.length).toBe(1)
    // Tested this way to trigger type inference
    if (!group.children[0].leaf) expect.unreachable('Expected leaf, got node!')

    for (const course of group.children[0].children) {
      expect(course.module.id).toBe(group.entity?.id)
    }
  }
})

it('groups deeply based on the specified filters', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.MODULE, exclude: false, entities: [] },
    { filter: FilterType.COURSE_TYPE, exclude: false, entities: [] },
  ])

  expect(filtered.value.length).toBe(3)

  // Tested this way to trigger type inference
  if (filtered.value[0].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].children.length).toBe(2)
  // Tested this way to trigger type inference
  if (filtered.value[0].children[0].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].children[0].children.length).toBe(1)
  expect(filtered.value[0].children[0].children[0].leaf).toBe(true)
})

it('only keeps courses matching the filter', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.MODULE, exclude: false, entities: [2] },
  ])

  expect(filtered.value.length).toBe(1)
  // Tested this way to trigger type inference
  if (filtered.value[0].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].children[0].leaf).toBe(true)
  expect(filtered.value[0].children[0].children.length).toBe(2)
})

it('removes courses matching the filter', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.MODULE, exclude: true, entities: [2] },
  ])

  expect(filtered.value.length).toBe(2)
  // Tested this way to trigger type inference
  if (filtered.value[0].leaf) expect.unreachable('Expected node, got leaf!')
  if (filtered.value[1].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].children[0].leaf).toBe(true)
  expect(filtered.value[1].children[0].leaf).toBe(true)
  expect(filtered.value[0].children[0].children.length).toBe(6)
  expect(filtered.value[1].children[0].children.length).toBe(2)
})

it('splits courses with multiple groups when filtering per group', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.GROUP, exclude: false, entities: [1, 2] },
  ])

  expect(filtered.value.length).toBe(2)
  // Tested this way to trigger type inference
  if (filtered.value[0].leaf) expect.unreachable('Expected node, got leaf!')
  if (filtered.value[1].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].children[0].leaf).toBe(true)
  expect(filtered.value[1].children[0].leaf).toBe(true)
  expect(filtered.value[0].children[0].children.length).toBe(3)
  expect(filtered.value[1].children[0].children.length).toBe(2)

  expect(filtered.value[0].children[0].children).toContain(arrays[0][2])
  expect(filtered.value[1].children[0].children).toContain(arrays[0][2])
})

it('handles cases where there are no entities associated to a course for a given property', () => {
  const arrays = defineTestsCoursesArrays(false)
  const { filtered } = useFilteredCourses(arrays[0], null, [
    { filter: FilterType.TEACHER, exclude: false, entities: [] },
  ])

  expect(filtered.value.length).toBe(4)
  // Tested this way to trigger type inference
  if (filtered.value[0].leaf) expect.unreachable('Expected node, got leaf!')
  if (filtered.value[1].leaf) expect.unreachable('Expected node, got leaf!')
  if (filtered.value[2].leaf) expect.unreachable('Expected node, got leaf!')
  if (filtered.value[3].leaf) expect.unreachable('Expected node, got leaf!')

  expect(filtered.value[0].entity?.id).toBe(-2)
  expect(filtered.value[1].entity?.id).toBe(-1)
  expect(filtered.value[2].entity?.id).toBe(1)
  expect(filtered.value[3].entity?.id).toBe(-1)
})

describe('Entity extraction', () => {
  type Dept = Course['type']['department']
  type Module = Course['module']
  type Group = Course['groups'][number]
  type Tutor = Exclude<Course['tutor'], null>
  type Period = Course['period']
  type RoomType = Course['room_type']
  type CourseType = Course['type']

  const FAKE_DEPT: Dept = { id: 31, name: 'Haute-Garonne', abbrev: 'HG' } // i'm so funny please laugh ;w;
  const TEST_MODULE_1: Module = { id: 1, abbrev: 'T1', name: 'Test 1', display: { color_bg: '', color_txt: '' } }
  const TEST_MODULE_2: Module = { id: 2, abbrev: 'T2', name: 'Test 2', display: { color_bg: '', color_txt: '' } }
  const TEST_GROUP_1: Group = {
    id: 3,
    name: 'GRP1',
    train_prog: { id: 15, abbrev: 'Competitive sleeping' },
    is_structural: true,
  }
  const TEST_GROUP_2: Group = {
    id: 4,
    name: 'GRP2',
    train_prog: { id: 15, abbrev: 'Competitive sleeping' },
    is_structural: true,
  }
  const TEST_GROUP_3: Group = {
    id: 5,
    name: 'GRP3',
    train_prog: { id: 15, abbrev: 'Competitive sleeping' },
    is_structural: true,
  }
  const TEST_GROUP_4: Group = {
    id: 6,
    name: 'GRP4',
    train_prog: { id: 15, abbrev: 'Competitive sleeping' },
    is_structural: true,
  }
  const TEST_TUTOR_1: Tutor = { id: 7, username: 'TUT1', first_name: 'Lara', last_name: 'Clette' }
  const TEST_TUTOR_2: Tutor = { id: 8, username: 'TUT2', first_name: 'Yves', last_name: 'Rogne' }
  const TEST_PERIOD_1: Period = { id: 9, start_date: '2024-01-01', end_date: '2024-12-31', name: 'Y2024' }
  const TEST_PERIOD_2: Period = { id: 10, start_date: '2025-01-01', end_date: '2025-12-31', name: 'Y2025' }
  const TEST_ROOM_1: RoomType = { id: 11, name: 'Backrooms' }
  const TEST_ROOM_2: RoomType = { id: 12, name: 'Moon base' }
  const TEST_TYPE_1: CourseType = { id: 13, name: 'CM', department: FAKE_DEPT }
  const TEST_TYPE_2: CourseType = { id: 14, name: 'TD', department: FAKE_DEPT }

  it('extracts entities from courses', () => {
    const course = makeCourse(
      TEST_MODULE_1,
      [TEST_GROUP_2, TEST_GROUP_4],
      TEST_TUTOR_1,
      TEST_PERIOD_2,
      TEST_ROOM_1,
      TEST_TYPE_1
    )

    const { entities } = useFilteredCourses([course], null, [
      { filter: FilterType.MODULE, exclude: false, entities: [] },
      { filter: FilterType.GROUP, exclude: false, entities: [] },
      { filter: FilterType.TEACHER, exclude: false, entities: [] },
      { filter: FilterType.PERIOD, exclude: false, entities: [] },
      { filter: FilterType.ROOM_TYPE, exclude: false, entities: [] },
      { filter: FilterType.COURSE_TYPE, exclude: false, entities: [] },
    ])

    expect(entities.value.modules).toHaveLength(1)
    expect(entities.value.modules[0].id).toBe(TEST_MODULE_1.id)

    expect(entities.value.groups).toHaveLength(2)
    expect(entities.value.groups[0].id).toBe(TEST_GROUP_2.id)
    expect(entities.value.groups[1].id).toBe(TEST_GROUP_4.id)

    expect(entities.value.teachers).toHaveLength(1)
    expect(entities.value.teachers[0].id).toBe(TEST_TUTOR_1.id)

    expect(entities.value.periods).toHaveLength(1)
    expect(entities.value.periods[0].id).toBe(TEST_PERIOD_2.id)

    expect(entities.value.roomTypes).toHaveLength(1)
    expect(entities.value.roomTypes[0].id).toBe(TEST_ROOM_1.id)

    expect(entities.value.courseTypes).toHaveLength(1)
    expect(entities.value.courseTypes[0].id).toBe(TEST_TYPE_1.id)
  })

  it('does not include duplicated entries', () => {
    const course1 = makeCourse(
      TEST_MODULE_1,
      [TEST_GROUP_2, TEST_GROUP_4],
      TEST_TUTOR_1,
      TEST_PERIOD_2,
      TEST_ROOM_1,
      TEST_TYPE_1
    )
    const course2 = makeCourse(
      TEST_MODULE_1,
      [TEST_GROUP_2, TEST_GROUP_3],
      TEST_TUTOR_2,
      TEST_PERIOD_2,
      TEST_ROOM_1,
      TEST_TYPE_2
    )

    const { entities } = useFilteredCourses([course1, course2], null, [
      { filter: FilterType.GROUP, exclude: false, entities: [] },
      { filter: FilterType.TEACHER, exclude: false, entities: [] },
    ])

    expect(entities.value.groups).toHaveLength(3)
    expect(entities.value.teachers).toHaveLength(2)

    expect(entities.value.groups[0].id).not.toBe(entities.value.groups[1].id)
    expect(entities.value.groups[1].id).not.toBe(entities.value.groups[2].id)
    expect(entities.value.teachers[0].id).not.toBe(entities.value.teachers[1].id)
  })

  it('handles null cases gracefully', () => {
    const course = makeCourse(
      TEST_MODULE_2,
      [TEST_GROUP_1, TEST_GROUP_4],
      null,
      TEST_PERIOD_1,
      TEST_ROOM_2,
      TEST_TYPE_1
    )

    const { entities } = useFilteredCourses([course], null, [
      { filter: FilterType.TEACHER, exclude: false, entities: [] },
    ])

    expect(entities.value.teachers).toHaveLength(1)
  })

  it('does filter entities that are unnecessary due to filtering', () => {
    const course1 = makeCourse(TEST_MODULE_1, [], TEST_TUTOR_1, TEST_PERIOD_1, TEST_ROOM_1, TEST_TYPE_1)

    const course2 = makeCourse(TEST_MODULE_2, [], TEST_TUTOR_2, TEST_PERIOD_1, TEST_ROOM_1, TEST_TYPE_1)

    const { entities } = useFilteredCourses([course1, course2], null, [
      { filter: FilterType.MODULE, exclude: false, entities: [TEST_MODULE_2.id] },
      { filter: FilterType.TEACHER, exclude: false, entities: [] },
    ])

    expect(entities.value.modules).toHaveLength(2)
    expect(entities.value.teachers).toHaveLength(1)

    expect(entities.value.modules[0].id).toBe(TEST_MODULE_1.id)
    expect(entities.value.modules[1].id).toBe(TEST_MODULE_2.id)
    expect(entities.value.teachers[0].id).toBe(TEST_TUTOR_2.id)
  })
})

/** Test utils **/

function defineTestsCoursesArrays(isUserConnected: boolean): Course[][] {
  // Define a courses test array (len = 10)
  const a0 = makeCourseFromBasicData(1, 0, [1], new Date('2023-05-05'), -1, 0)
  const a1 = makeCourseFromBasicData(2, 1, [1, 2], new Date('2023-05-05'), -1, 0)
  const a2 = makeCourseFromBasicData(3, -1, [0, 3], new Date('2023-10-10'), null, 2)
  const a3 = makeCourseFromBasicData(4, 0, [4, 5], new Date('2024-01-05'), -1, 0)
  const a4 = makeCourseFromBasicData(5, 2, [0, 5], new Date('2024-05-05'), -1, 1)

  const a5 = makeCourseFromBasicData(6, 0, [4], new Date('2023-05-05'), -2, 0)
  const a6 = makeCourseFromBasicData(7, 1, [1, 3], new Date('2023-05-05'), 1, 0)
  const a7 = makeCourseFromBasicData(8, -1, [2, 0], new Date('2023-10-10'), 1, 2)
  const a8 = makeCourseFromBasicData(9, 0, [5, 8], new Date('2024-01-05'), 1, 0)
  const a9 = makeCourseFromBasicData(10, 2, [9, 8, 7], new Date('2024-05-05'), null, 1)

  const testArray = [a2, a5, a1, a3, a4, a9, a0, a6, a8, a7]
  const desiredArray = isUserConnected
    ? [a0, a1, a3, a4, a5, a6, a7, a8, a2, a9]
    : [a5, a0, a1, a6, a7, a2, a3, a8, a4, a9]

  return [testArray, desiredArray]
}

export function makeCourseFromBasicData(
  idCourse: number,
  idType: number,
  idsGroups: number[],
  periodStart: Date,
  tutorId: number | null,
  idM: number
): Course {
  return makeCourse(
    {
      id: idM,
      name: 'Flop Web',
      abbrev: 'FW',
      display: {
        color_bg: '#008000',
        color_txt: '#000000',
      },
    },
    idsGroups.map((id) => ({ id: id, train_prog: { id: -1, abbrev: '' }, name: `GRP-${id}`, is_structural: true })),
    typeof tutorId === 'number'
      ? {
          id: tutorId,
          username: 'PB',
          first_name: 'P',
          last_name: 'B',
        }
      : null,
    {
      id: 1,
      name: 'Test period',
      start_date: periodStart.toISOString().slice(0, 10),
      end_date: new Date(periodStart.getTime() + 30 * 24 * 3600e3).toISOString().slice(0, 10),
    },
    {
      id: 20,
      name: 'B',
    },
    {
      department: {
        name: 'Test department',
        abbrev: 'TEST',
        id: 1,
      },
      name: 'Test type',
      id: idType,
    }
  )
}

function makeCourse(
  module: Course['module'],
  groups: Course['groups'],
  tutor: Course['tutor'],
  period: Course['period'],
  roomType: Course['room_type'],
  courseType: Course['type']
): Course {
  // noinspection JSDeprecatedSymbols
  return {
    id: -1,
    no: 0,
    supp_tutors: [],
    modulesupp: { abbrev: '' },
    pay_module: { abbrev: '' },
    is_graded: false,
    duration: '00:02:00',

    module: module,
    groups: groups,
    tutor: tutor,
    period: period,
    room_type: roomType,
    type: courseType,
  }
}
