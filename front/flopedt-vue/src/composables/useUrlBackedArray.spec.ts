import { expect, it } from 'vitest'

import { waitForNavigation, withRouterSetup } from '@/utils/vitest'
import useUrlBackedArray from './useUrlBackedArray.ts'

it('should give an empty array when query parameter is empty', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))

  await router.push('/schedule')
  expect(store.value).toStrictEqual([])
})

it('should give an array with a single entry with there is only one value', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))

  await router.push('/schedule?test=meow')
  expect(store.value).toStrictEqual(['meow'])
})

it('should give back all entities when there are multiple ones', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))

  await router.push('/schedule?test=meow&test=nya')
  expect(store.value).toStrictEqual(['meow', 'nya'])
})

it('should update the URL when updating the data', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))
  await router.push('/schedule?test=meow&test=nya')

  store.value.push('miaou')
  await waitForNavigation(router)

  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=meow&test=nya&test=miaou')
})

it('should update the URL when overwriting the data', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))
  await router.push('/schedule?test=meow&test=nya')

  store.value = ['roar']
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=roar')

  store.value.push('miaou')
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=roar&test=miaou')
})

it('should restore previous state when going back in history', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))
  await router.push('/schedule?test=meow&test=nya')

  store.value.push('miaou')
  await waitForNavigation(router)

  router.back()
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=meow&test=nya')

  router.forward()
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=meow&test=nya&test=miaou')
})

it('should preserve unrelated query parameters when updating query params', async () => {
  const [store, router] = await withRouterSetup(() => useUrlBackedArray('test'))
  await router.push('/schedule?test=meow&test=nya&param1=true&param2=false')

  store.value.push('miaou')
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?test=meow&test=nya&test=miaou&param1=true&param2=false')
})
