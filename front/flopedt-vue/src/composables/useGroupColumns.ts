import type { Group, TransversalGroup } from '@/ts/entities/group.ts'
import type { StructuralGroupTreeNode } from '@/stores/departments/groups/structuralGroups.ts'
import { type MaybeRefOrGetter, computed, toValue } from 'vue'

export type Column = { id: string | number; name: string }
export type ColumnSpan = { start: number; end: number }

export function collapseColumns(columns: ColumnSpan[]): ColumnSpan[] {
  // toSorted exists, but it's quite modern so let's not rely on that for browser compat
  columns = [...columns.sort((a, b) => a.start - b.start)]
  const collapsed: ColumnSpan[] = []
  for (const column of columns) {
    const last = collapsed[collapsed.length - 1]
    if (last && last.end >= column.start) {
      if (column.end > last.end) last.end = column.end
    } else {
      collapsed.push({ ...column })
    }
  }

  return collapsed
}

export function useGroupColumns(
  structuralGroupsTree: MaybeRefOrGetter<StructuralGroupTreeNode[]>,
  structuralGroupsDescendantsMap: MaybeRefOrGetter<Map<number, Set<number>>>,
  transversalGroups: MaybeRefOrGetter<TransversalGroup[]>,
  selectedGroups: MaybeRefOrGetter<Group[]>
) {
  const expandedSelectedGroupsId = computed(() => {
    const descendantsMap = toValue(structuralGroupsDescendantsMap)
    const selectedGroupsValue = toValue(selectedGroups)

    // If no group is selected, consider them all selected.
    const groups = selectedGroupsValue.length
      ? selectedGroupsValue
      : [...toValue(structuralGroupsTree), ...toValue(transversalGroups)]

    // Split the groups apart
    const selectedStructural = groups.filter((g) => g.isStructural())
    const selectedTransversal = groups.filter((g) => g.isTransversal())

    const selected = new Set<number>()
    function processStructural(structuralId: number) {
      selected.add(structuralId)
      const descendants = descendantsMap.get(structuralId)
      if (descendants) descendants.forEach((d) => selected.add(d))
    }

    // Push all groups to the set
    selectedStructural.forEach((g) => processStructural(g.id.valueOf()))
    for (const transversal of selectedTransversal) {
      selected.add(transversal.id.valueOf())
      transversal.conflictingGroups.forEach(processStructural)
    }

    return selected
  })

  // The tree is assumed to be sorted. This is the tested behavior of the structural groups store
  return computed(() => {
    const columns: Column[] = []
    const groupsColumnMap = new Map<number, ColumnSpan[]>()

    // Recursive function that computes the columns a group spans across
    function traverseGroup(group: StructuralGroupTreeNode): ColumnSpan[] {
      if (!group.children) {
        if (!expandedSelectedGroupsId.value.has(group.id.valueOf())) {
          // Group not selected, skip.
          return []
        }

        // We use the group ID to make a unique identifier.
        // It must not be considered a guarantee! Column IDs must be considered opaque.
        columns.push({ id: group.id.valueOf(), name: group.name })

        const result = [{ start: columns.length - 1, end: columns.length }]
        groupsColumnMap.set(group.id.valueOf(), result)
        return result
      }

      const result = group.children.flatMap((g) => traverseGroup(g))
      if (!result.length) return [] // Group not selected most likely.

      const collapsed = collapseColumns(result)
      groupsColumnMap.set(group.id.valueOf(), collapsed)
      return collapsed
    }

    // Traverse groups
    toValue(structuralGroupsTree).forEach((g) => traverseGroup(g))

    // Map transversal groups to the structural columns
    for (const transversalGroup of toValue(transversalGroups)) {
      // Group is ignored; continue
      if (!expandedSelectedGroupsId.value.has(transversalGroup.id.valueOf())) continue

      const columns: ColumnSpan[] = []
      for (const structural of transversalGroup.conflictingGroups) {
        columns.push(...(groupsColumnMap.get(structural) /* v8 ignore next */ ?? []))
      }

      const collapsedColumns = collapseColumns(columns)
      groupsColumnMap.set(transversalGroup.id.valueOf(), collapsedColumns)
    }

    return {
      columns,
      groupsColumnMap,
    }
  })
}
