import { beforeEach, expect, it } from 'vitest'

import { watchEffect } from 'vue'
import { useRoute } from 'vue-router'
import { setActivePinia, createPinia } from 'pinia'

import { waitForNavigation, withRouterSetup } from '@/utils/vitest.ts'
import { CrudStore, defineCrudStore } from '@/stores/defineCrudStore.ts'
import { Entity } from '@/ts/entities/base/entity.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'

import useUrlBackedEntityFilter from './useUrlBackedEntityFilter.ts'

class TestEntity extends Entity<null> {
  constructor(id: Identifier) {
    super(id)
  }

  toJSON() {
    return null
  }
}

let useTestStore: () => CrudStore<{ Entity: TestEntity; CreateData: TestEntity }>
beforeEach(() => {
  setActivePinia(createPinia())

  useTestStore = defineCrudStore({
    name: 'test-store',
    fetchAll: () =>
      Promise.resolve([
        new TestEntity(new Identifier(1)),
        new TestEntity(new Identifier(2)),
        new TestEntity(new Identifier(3)),
        new TestEntity(new Identifier(4)),
        new TestEntity(new Identifier(5)),
      ]),
    fetchOne: () => Promise.reject(),
    create: () => Promise.reject(),
    update: () => Promise.reject(),
    remove: () => Promise.reject(),
  })
})

it('should filter entities based on the current URL query parameter', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=3')

  expect(entities.value).toHaveLength(2)
  expect(entities.value[0].id.valueOf()).toBe(1)
  expect(entities.value[1].id.valueOf()).toBe(3)
})

it('should dynamically update the entities when URL changes', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=3')
  expect(entities.value).toHaveLength(2)

  await router.push('/schedule?e=2')
  expect(entities.value).toHaveLength(1)
  expect(entities.value[0].id.valueOf()).toBe(2)
})

it('should update the URL when entities are added to the array', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=3')
  expect(entities.value).toHaveLength(2)

  entities.value.push(store.entities.get(5)!)
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1&e=3&e=5')
})

it('should update the URL when selected entities are overwritten', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=3')
  expect(entities.value).toHaveLength(2)

  entities.value = [store.entities.get(4)!]
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=4')

  entities.value.push(store.entities.get(5)!)
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=4&e=5')
})

it('should get rid of IDs that do not point to a valid entity', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=9')
  expect(entities.value).toHaveLength(1)

  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1')
})

it('should not get rid of IDs if the store is currently loading', async () => {
  const store = useTestStore()

  const [entities, router] = await withRouterSetup(() => {
    expect(store.entities.size).toBe(0)
    const route = useRoute()
    watchEffect(() => {
      if (route.path !== '/') void store.fetchAll()
    })

    return useUrlBackedEntityFilter(store, 'e')
  })

  await router.push('/schedule?e=1&e=4&e=9')
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1&e=4&e=9')
  expect(entities.value).toHaveLength(0)

  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1&e=4')
  expect(entities.value).toHaveLength(2)
})

it('should update the URL whenever the ID of an entity changes', async () => {
  // Reset ID
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e'))
  await router.push('/schedule?e=1&e=3')
  expect(entities.value).toHaveLength(2)

  entities.value[0].id.ref.value = 999
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=999&e=3')

  await router.push('/schedule?e=1&e=3')
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1&e=3')
  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=999&e=3')
})

it('should apply the filter on the entities matching and discard irrelevant entities', async () => {
  const store = useTestStore()
  await store.fetchAll()

  const [entities, router] = await withRouterSetup(() => useUrlBackedEntityFilter(store, 'e', (e) => e.id.equals(1)))
  await router.push('/schedule?e=1&e=3')
  expect(entities.value).toHaveLength(1)

  await waitForNavigation(router)
  expect(router.currentRoute.value.fullPath).toBe('/schedule?e=1')
})
