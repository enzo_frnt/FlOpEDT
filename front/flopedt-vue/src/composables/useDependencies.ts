import { useI18n } from 'vue-i18n'
import { computed, inject, Ref, ref } from 'vue'
import { Connection } from '@vue-flow/core'
import { useDependencyStore } from '@/stores/timetable/dependency'
import { useSelectedCourseStore } from '@/stores/timetable/selectedCourse'
import { useCourseStore } from '@/stores/timetable/course'
import { layoutAllGraphs, layoutGroups } from '@/components/management/dependencies/useLayout'
import { PseudoGlobalDependency } from '@/components/management/dependencies/tempGlobalDependencies'
import { Course, Dependency, DependencyCreateData, DependencyUpdateData } from '@/ts/type'
import { CreationMode } from '@/components/management/dependencies/TempDependencyEdge.vue'
import { ColorMode } from '@/components/management/courses/CourseItems.vue'
import { useGroupsStore } from '@/stores/departments/groups'
import { useDepartmentsStore } from '@/stores/departments'
import { useStructuralGroupsStore } from '@/stores/departments/groups/structuralGroups'
export interface CustomNodeData {
  colorMode: ColorMode
  course?: Course
  courses?: Course[]
  selected?: boolean
  inGroups?: boolean
}

export interface CustomNode {
  id: string
  label: string
  type: string
  position: { x: number; y: number }
  class: string
  data: CustomNodeData
}

export interface CustomEdgeData {
  global?: boolean
  text?: string
  complex?: boolean
  graphStatus?: GraphStatus
}

export interface CustomEdge {
  id: string
  source: string
  target: string
  data?: CustomEdgeData
  style: object
  type: string
  markerEnd: string
}

export interface DependencyGraph {
  id: number
  nodes: CustomNode[]
  edges: CustomEdge[]
  graphNumber?: number
  status: GraphStatus
  max_days?: number
}

export enum GraphStatus {
  CYCLIC = 'cyclic',
  DAYS_ISSUE = 'days_issue',
  VALID = 'valid',
}

export function useDependencies() {
  const i18n = useI18n()

  const department = useDepartmentsStore()
  const structuralGroupsStore = useStructuralGroupsStore()
  const { descendantsMap } = structuralGroupsStore.getDepartmentGroups(department.current.id)

  const dependencyStore = useDependencyStore()
  const selectedCourseStore = useSelectedCourseStore()
  const courseStore = useCourseStore()
  const groupsStore = useGroupsStore()

  const globalDependencies = inject('globalDependencies') as Ref<PseudoGlobalDependency[]>

  const logicColorMode = ref<ColorMode>(ColorMode.COURSE_TYPE)

  const graphs = ref<DependencyGraph[]>([])
  const courseGroupNodes = ref<CustomNode[]>([])
  const tempEdges = ref<CustomEdge[]>([])

  const groupId = ref<number>(0)
  const graphId = ref<number>(0)

  function inGroups(courseId: number): boolean {
    return courseGroupNodes.value.some((node) => node.data.courses?.some((course) => course.id === courseId))
  }

  function createGroup() {
    const groupCourses: Course[] = []

    for (const selectedCourse of selectedCourseStore.selectedCourses) {
      if (!inGroups(selectedCourse.id)) groupCourses.push(selectedCourse)
    }

    if (groupCourses.length === 0) return

    courseGroupNodes.value.push({
      id: 'course-group-' + ++groupId.value,
      label: 'course-group-' + groupId.value,
      type: 'course-group',
      position: { x: 0, y: 0 },
      class: 'light',
      data: {
        courses: groupCourses,
        colorMode: logicColorMode.value,
      },
    })

    courseGroupNodes.value = layoutGroups(courseGroupNodes.value)

    reload()

    groupCourses.forEach((course: Course) => {
      const courseNode: CustomNode | undefined = getCourseNode(course.id)

      if (courseNode) courseNode.data.inGroups = true
    })
  }

  function removeCourseGroup(courseGroupId: string): CustomNode | null {
    const courseGroupIndex = courseGroupNodes.value.findIndex((courseGroup) => courseGroup.id === courseGroupId)
    const [removedCourseGroup] = courseGroupNodes.value.splice(courseGroupIndex, 1)

    reload()

    return removedCourseGroup
  }

  function inGraphs(givenGraphs: DependencyGraph[], courseId: number): boolean {
    return givenGraphs.some((graph) => graph.nodes.some((node) => parseInt(node.id) === courseId))
  }

  function generateCourseNode(course: Course): CustomNode {
    return {
      id: course.id.toString(),
      label: course.type.name,
      type: 'course',
      position: { x: 0, y: 0 },
      class: 'light',
      data: {
        course,
        colorMode: logicColorMode.value,
        selected: selectedCourseStore.selectedCourses.some((selectedCourse) => course.id === selectedCourse.id),
        inGroups: inGroups(course.id),
      },
    } as CustomNode
  }

  function generateEdgeFromGlobal(id: string, source: string, target: string, dayGap: number): CustomEdge {
    return {
      id: id,
      source: source,
      target: target,
      type: 'dependency',
      data: {
        text: dayGap.toString(),
        global: true,
      },
      style: { strokeWidth: '2px', stroke: 'currentColor' },
    } as CustomEdge
  }

  function generateEdge(dependency: Dependency): CustomEdge {
    return {
      id: dependency.id.toString(),
      source: dependency.course1.toString(),
      target: dependency.course2.toString(),
      type: 'dependency',
      data: {
        text: dependency.successive ? i18n.t('management.dependencies.successive') : dependency.day_gap.toString(),
        global: false,
      },
      style: { strokeWidth: '2px', stroke: 'currentColor' },
    } as CustomEdge
  }

  function relatedGlobalDependency(
    course: Course,
    globalDependency: PseudoGlobalDependency,
    isCourse1: boolean
  ): boolean {
    // Check if the course is in the excepted_courses list
    const exceptedCourses = globalDependency.parameters.find((param) => param.name === 'excepted_courses')!.id_list

    if (exceptedCourses.includes(course.id)) {
      return false
    }

    const courseTypeId = isCourse1
      ? globalDependency.parameters.find((param) => param.name === 'course1_type')?.id_list[0]
      : globalDependency.parameters.find((param) => param.name === 'course2_type')?.id_list[0]
    const courseTutorId = isCourse1
      ? globalDependency.parameters.find((param) => param.name === 'course1_tutor')?.id_list[0]
      : globalDependency.parameters.find((param) => param.name === 'course2_tutor')?.id_list[0]

    if (courseTypeId && course.type.id !== courseTypeId) {
      return false
    }

    if (courseTutorId && course.tutor?.id !== courseTutorId) {
      return false
    }

    // Check modules
    const moduleIds = globalDependency.parameters.find((param) => param.name === 'modules')!.id_list
    if (moduleIds.length > 0 && !moduleIds.includes(course.module.id)) {
      return false
    }

    if (!course.period) {
      return false
    }

    // Check periods
    const periodIds = globalDependency.parameters.find((param) => param.name === 'periods')!.id_list
    return !(periodIds.length > 0 && !periodIds.includes(course.period.id))
  }

  function getCourseGroups(course: Course) {
    // FIXME: temporary fix; Course will be crudstorified too, most probably
    return groupsStore.getGroupsByIds(course.groups.map((group) => group.id))
  }

  function generateDependencyGraph(course: Course): DependencyGraph {
    const graph: DependencyGraph = {
      id: graphId.value++,
      nodes: [],
      edges: [],
      status: GraphStatus.VALID,
    }

    const exploredNodes = new Set<number>() // Track explored nodes
    const nodesToExplore = new Set<number>() // Track nodes to explore later

    function addCourseNode(courseId: number) {
      if (exploredNodes.has(courseId)) return
      exploredNodes.add(courseId)

      const course: Course | undefined = courseStore.courses.find((course) => course.id === courseId)
      if (!course) {
        console.log('Course ' + courseId + ' not found.')
        return
      }

      const node: CustomNode = generateCourseNode(course)
      if (!graph.nodes.find((n) => n.id === node.id)) {
        graph.nodes.push(node)
      }

      for (const globalDependency of globalDependencies.value) {
        if (globalDependency.is_active) {
          // Process as course1 in global dependency
          if (relatedGlobalDependency(course, globalDependency, true)) {
            const course2Type = globalDependency.parameters.find((param) => param.name === 'course2_type')?.id_list[0]
            const course2TutorId = globalDependency.parameters.find((param) => param.name === 'course2_tutor')
              ?.id_list[0]

            const relatedCourses = courseStore.courses.filter(
              (c) =>
                relatedGlobalDependency(c, globalDependency, false) &&
                c.type.id === course2Type &&
                (course2TutorId === undefined || c.tutor?.id === course2TutorId) &&
                c.module.id === course.module.id &&
                c.period &&
                course.period &&
                c.period.id === course.period.id &&
                getCourseGroups(c).value.some((group) =>
                  getCourseGroups(course).value.some((g) => areSameBranchGroups(group.id.valueOf(), g.id.valueOf()))
                )
            )

            for (const relatedCourse of relatedCourses) {
              const edgeId = `${course.id}-${relatedCourse.id}`
              if (!graph.edges.find((edge) => edge.id === edgeId)) {
                graph.edges.push(
                  generateEdgeFromGlobal(
                    edgeId,
                    course.id.toString(),
                    relatedCourse.id.toString(),
                    globalDependency.parameters.find((param) => param.name === 'day_gap')?.id_list[0] || 0
                  )
                )
              }
              // Add to nodes to explore
              nodesToExplore.add(relatedCourse.id)
            }
          }

          // Process as course2 in global dependency
          if (relatedGlobalDependency(course, globalDependency, false)) {
            const course1Type = globalDependency.parameters.find((param) => param.name === 'course1_type')?.id_list[0]
            const course1TutorId = globalDependency.parameters.find((param) => param.name === 'course1_tutor')
              ?.id_list[0]

            const relatedCourses = courseStore.courses.filter(
              (c) =>
                relatedGlobalDependency(c, globalDependency, true) &&
                c.type.id === course1Type &&
                (course1TutorId === undefined || c.tutor?.id === course1TutorId) &&
                c.module.id === course.module.id &&
                c.period &&
                course.period &&
                c.period.id === course.period.id &&
                getCourseGroups(c).value.some((group) =>
                  getCourseGroups(course).value.some((g) => areSameBranchGroups(group.id.valueOf(), g.id.valueOf()))
                )
            )

            for (const relatedCourse of relatedCourses) {
              const edgeId = `${relatedCourse.id}-${course.id}`
              if (!graph.edges.find((edge) => edge.id === edgeId)) {
                graph.edges.push(
                  generateEdgeFromGlobal(
                    edgeId,
                    relatedCourse.id.toString(),
                    course.id.toString(),
                    globalDependency.parameters.find((param) => param.name === 'day_gap')?.id_list[0] || 0
                  )
                )
              }
              // Add to nodes to explore
              nodesToExplore.add(relatedCourse.id)
            }
          }
        }
      }

      // Process normal dependencies
      const dependencies: Dependency[] = dependencyStore.dependencies.filter(
        (dependency) => dependency.course1 === course.id || dependency.course2 === course.id
      )

      for (const dependency of dependencies) {
        const sourceId = dependency.course1.toString()
        const targetId = dependency.course2.toString()

        if (
          !graph.edges.find(
            (edge) =>
              (edge.source === sourceId && edge.target === targetId) ||
              (edge.source === targetId && edge.target === sourceId)
          )
        ) {
          graph.edges.push(generateEdge(dependency))
          refreshGraphStatus()
        }

        const dependentCourseId = dependency.course1 === course.id ? dependency.course2 : dependency.course1
        nodesToExplore.add(dependentCourseId)
      }
    }

    // Start with the initial course
    addCourseNode(course.id)

    // Add nodes in the nodesToExplore set
    while (nodesToExplore.size > 0) {
      const nextId: number | undefined = nodesToExplore.values().next().value as number
      if (nextId !== undefined) {
        nodesToExplore.delete(nextId)
        addCourseNode(nextId)
      }
    }

    return graph
  }

  function graphIsCyclic(graph: DependencyGraph): boolean {
    const { nodes, edges } = graph

    const visited: boolean[] = new Array(nodes.length).fill(false) as boolean[]
    const recStack: boolean[] = new Array(nodes.length).fill(false) as boolean[]

    function isCyclicRec(i: number, visited: boolean[], recStack: boolean[]): boolean {
      if (recStack[i]) return true
      if (visited[i]) return false

      visited[i] = true
      recStack[i] = true

      const children: number[] = edges
        .filter((edge) => nodes[i] && edge.source === nodes[i].id)
        .map((edge) => nodes.findIndex((node) => node.id === edge.target))

      for (let childNum = 0; childNum < children.length; childNum++) {
        if (isCyclicRec(children[childNum], visited, recStack)) return true
      }

      recStack[i] = false

      return false
    }

    for (let i = 0; i < nodes.length; i++) {
      if (isCyclicRec(i, visited, recStack)) return true
    }

    return false
  }

  // This function replicates max weight search for DAGs (Directed acyclic graphs) using topological search
  function getMaxGraphDays(graph: DependencyGraph): number {
    const { nodes, edges } = graph
    const nodeCount = nodes.length
    let errorDetected: boolean = false

    // Creates the array to keep all in-degree values of each node, meaning the number of edges where the node is the target
    const inDegree = new Array(nodeCount).fill(0)

    // Creates the adjList to keep for each node its target neighbours and the weight of their edge
    const adjList = Array.from({ length: nodeCount }, () => [] as { target: number; weight: number }[])

    // Build adjacency list and calculate in-degrees for each node
    edges.forEach((edge) => {
      // Fetch source and target ids
      const sourceIndex = nodes.findIndex((node) => node.id === edge.source)
      const targetIndex = nodes.findIndex((node) => node.id === edge.target)

      if (sourceIndex === -1 || targetIndex === -1 || !edge.data || edge.data.text === undefined) {
        errorDetected = true
        return
      }

      // Calculate weight based on edge data text... could be changed for the value to be directly in the edge...
      const weight = edge.data.text === 'successive' ? 0 : parseInt(edge.data.text, 10)

      // Update array with edges data
      adjList[sourceIndex].push({ target: targetIndex, weight })
      inDegree[targetIndex]++
    })

    if (errorDetected) return -1

    // Topological sort based on Khan's algorithm
    const topologicalSort: number[] = []
    const queue: number[] = []

    // Get all source nodes in a queue as starters
    for (let i = 0; i < nodeCount; i++) {
      if (inDegree[i] === 0) queue.push(i)
    }

    while (queue.length > 0) {
      // pop first node and add to topologicalSort
      const node = queue.shift()!
      topologicalSort.push(node)

      // "deletes" first layer of nodes and lower all connected nodes in-degree, take 0 ones as sources
      adjList[node].forEach(({ target }) => {
        inDegree[target]--
        if (inDegree[target] === 0) queue.push(target)
      })
    }

    // Initialize distances array that keeps max distance associated to each node
    const distances: number[] = new Array(nodeCount).fill(-Infinity) as number[]

    // Initialize source nodes distances to 0
    topologicalSort.forEach((node) => {
      if (distances[node] === -Infinity) {
        distances[node] = 0
      }
    })

    // Calculate longest path
    topologicalSort.forEach((node) => {
      adjList[node].forEach(({ target, weight }) => {
        if (distances[node] + weight > distances[target]) {
          distances[target] = distances[node] + weight
        }
      })
    })

    // The maximum value in distances array is the answer
    return Math.max(...distances)
  }

  function getPeriodGap(periodStart: string, periodEnd: string): number {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24

    // Parse the input dates
    const startDate = new Date(periodStart)
    const endDate = new Date(periodEnd)

    // Discard the time and time-zone information.
    const utc1 = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate())
    const utc2 = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate())

    return Math.floor((utc2 - utc1) / _MS_PER_DAY) - 1
  }

  function getGraphStatus(graph: DependencyGraph): { status: GraphStatus; max_days_error?: number } {
    let maxPeriodGap = null

    const graphFirstNode: CustomNode = graph.nodes[0]
    const graphFirstCourse: Course | undefined = getNodeCourse(graphFirstNode.id)

    if (graphFirstCourse && graphFirstCourse.period) {
      maxPeriodGap = getPeriodGap(graphFirstCourse.period.start_date, graphFirstCourse.period.end_date)
    }

    if (graphIsCyclic(graph)) return { status: GraphStatus.CYCLIC }

    const maxGraphDays = getMaxGraphDays(graph)

    if (maxPeriodGap && (maxGraphDays === -1 || maxGraphDays > maxPeriodGap))
      return { status: GraphStatus.DAYS_ISSUE, max_days_error: maxGraphDays }

    return { status: GraphStatus.VALID }
  }

  function refreshGraphStatus(): void {
    graphs.value.forEach((graph) => {
      const graphStatusInfo = getGraphStatus(graph)
      graph.status = graphStatusInfo.status
      if (graphStatusInfo.max_days_error) graph.max_days = graphStatusInfo.max_days_error
      graph.edges.forEach((edge) => {
        const graphStatus = graph.status
        edge.data = {
          ...edge.data,
          graphStatus,
        } as CustomEdgeData
      })
    })
  }

  function generateGraphs(): void {
    const newGraphs: DependencyGraph[] = []
    const selectedCourses: Course[] = selectedCourseStore.selectedCourses

    for (const course of selectedCourses) {
      if (!inGraphs(newGraphs, course.id)) {
        const newGraph: DependencyGraph = generateDependencyGraph(course)
        newGraphs.push(newGraph)
      }
    }

    graphs.value = newGraphs

    refreshGraphStatus()
  }

  function createTempDependency(source: string, target: string, complex: boolean): void {
    tempEdges.value.push({
      id: 'temp-' + source + '-' + target,
      source: source,
      target: target,
      type: 'temp-dependency',
      data: {
        complex: complex,
      },
      style: { strokeWidth: '2px', stroke: 'currentColor' },
      markerEnd: 'dependency-arrow-closed',
    })
  }

  function removeTempDependency(tempDependencyId: string): CustomEdge {
    const edgeIndex = tempEdges.value.findIndex((tempEdge) => tempEdge.id === tempDependencyId)
    const [removedEdge] = tempEdges.value.splice(edgeIndex, 1)

    return removedEdge
  }

  function validDependency(sourceCourseId: number, targetCourseId: number): boolean {
    const sourceCourse: Course | undefined = courseStore.courses.find((course) => course.id === sourceCourseId)
    const targetCourse: Course | undefined = courseStore.courses.find((course) => course.id === targetCourseId)

    if (!sourceCourse) {
      console.log('Course ' + sourceCourseId + " doesn't exist.")
      return false
    }

    if (!targetCourse) {
      console.log('Course ' + targetCourseId + " doesn't exist.")
      return false
    }

    if (sourceCourse.period?.id !== targetCourse.period?.id) {
      console.log('Courses ' + sourceCourse.id + ' and ' + targetCourse.id + ' periods are different or undefined.')
      return false
    }

    if (
      dependencyStore.dependencies.find(
        (dependency) => dependency.course1 === sourceCourseId && dependency.course2 === targetCourseId
      )
    ) {
      console.log('Dependency already exists.')
      return false
    }

    if (
      tempEdges.value.find(
        (tempEdge) => parseInt(tempEdge.source) === sourceCourseId && parseInt(tempEdge.target) === targetCourseId
      )
    ) {
      console.log('Temp dependency already exists.')
      return false
    }

    return true
  }

  async function handleConnection(connection: Connection) {
    // treats group -> group connections
    if (connection.source.includes('group') && connection.target.includes('group')) {
      const sourceGroupNode: CustomNode | undefined = courseGroupNodes.value.find(
        (node) => node.id === connection.source
      )

      const targetGroupNode: CustomNode | undefined = courseGroupNodes.value.find(
        (node) => node.id === connection.target
      )

      if (!sourceGroupNode) {
        console.log('Course group ' + connection.source + " doesn't exist.")
        return
      }

      if (!targetGroupNode) {
        console.log('Course group ' + connection.target + " doesn't exist.")
        return
      }

      createTempDependency(connection.source, connection.target, true)

      return
    }

    // treats group -> course connections
    if (connection.source.includes('group')) {
      const courseGroupNode: CustomNode | undefined = courseGroupNodes.value.find(
        (node) => node.id === connection.source
      )

      if (!courseGroupNode) {
        console.log('Course group ' + connection.source + " doesn't exist.")
        return
      }

      createTempDependency(connection.source, connection.target, false)

      return
    }

    // treats course -> group connections
    if (connection.target.includes('group')) {
      const courseGroupNode: CustomNode | undefined = courseGroupNodes.value.find(
        (node) => node.id === connection.target
      )

      if (!courseGroupNode) {
        console.log('Course group ' + connection.target + " doesn't exist.")
        return
      }

      createTempDependency(connection.source, connection.target, false)

      return
    }

    // treats course -> course connections by directly creating the dependency
    if (validDependency(parseInt(connection.source), parseInt(connection.target))) {
      const createdDependency = await createDependency(
        false,
        0,
        parseInt(connection.source),
        parseInt(connection.target)
      )

      if (createdDependency) {
        const node1 = nodes.value.find((node) => node.id === connection.source)!
        const node2 = nodes.value.find((node) => node.id === connection.target)!

        const nodeGraph1: DependencyGraph | undefined = getNodeGraph(node1.id)
        const nodeGraph2: DependencyGraph | undefined = getNodeGraph(node2.id)

        if (!nodeGraph1 || !nodeGraph2) {
          console.log("Couldn't fetch related graphs")
          return
        }

        if (nodeGraph1.id !== nodeGraph2.id) {
          reload()
        }

        // Add the edge to the graph
        nodeGraph1.edges.push(generateEdge(createdDependency))
        refreshGraphStatus()
      }
    }
  }

  function getCourseNode(courseId: number): CustomNode | undefined {
    const nodeGraph: DependencyGraph | undefined = getNodeGraph(courseId.toString())

    if (nodeGraph) {
      return nodeGraph.nodes.find((node: CustomNode) => node.id === courseId.toString())
    }

    return undefined
  }

  function getNodeCourse(nodeId: string): Course | undefined {
    return courseStore.courses.find((course) => course.id === parseInt(nodeId))
  }

  function getNodeGraph(nodeId: string): DependencyGraph | undefined {
    return graphs.value.find((graph) => graph.nodes.some((graphNode) => graphNode.id === nodeId))
  }

  function getEdgeGraph(edgeId: string): DependencyGraph | undefined {
    return graphs.value.find((graph) => graph.edges.some((graphEdge) => graphEdge.id === edgeId))
  }

  async function cutEdge(edgeId: number): Promise<void> {
    const dependency: Dependency | undefined = dependencyStore.dependencies.find((dep) => dep.id === edgeId)
    const relatedGraph: DependencyGraph | undefined = getEdgeGraph(edgeId.toString())

    if (dependency && relatedGraph) {
      await dependencyStore.deleteDependency(dependency)
      console.log('Dependency ' + dependency.id + ' deleted successfully.')

      relatedGraph.edges.splice(
        relatedGraph.edges.findIndex((edge) => parseInt(edge.id) === edgeId),
        1
      )

      // Collects related nodes
      const sourceNode: CustomNode | undefined = relatedGraph.nodes.find(
        (node: CustomNode) => parseInt(node.id) === dependency.course1
      )

      const targetNode: CustomNode | undefined = relatedGraph.nodes.find(
        (node: CustomNode) => parseInt(node.id) === dependency.course2
      )

      if (sourceNode) {
        if (
          !sourceNode.data.selected &&
          !relatedGraph.edges.find((edge: CustomEdge) => edge.source === sourceNode.id || edge.target === sourceNode.id)
        ) {
          relatedGraph.nodes.splice(
            relatedGraph.nodes.findIndex((node) => node.id === sourceNode.id),
            1
          )
        }
      }

      if (targetNode) {
        if (
          !targetNode.data.selected &&
          !relatedGraph.edges.find((edge: CustomEdge) => edge.source === targetNode.id || edge.target === targetNode.id)
        ) {
          relatedGraph.nodes.splice(
            relatedGraph.nodes.findIndex((node) => node.id === targetNode.id),
            1
          )
        }
      }

      refreshGraphStatus()
    }
  }

  async function createDependency(
    successive: boolean,
    days: number,
    source: number,
    target: number
  ): Promise<Dependency | null> {
    if (validDependency(source, target)) {
      const dependencyData: DependencyCreateData = {
        successive: successive,
        day_gap: days,
        course1: source,
        course2: target,
      }

      try {
        const createdDependency = await dependencyStore.postDependency(dependencyData)

        console.log(
          `Created new dependency between ${dependencyData.course1} and ${dependencyData.course2}: ${createdDependency.id}`
        )

        return createdDependency
      } catch (error) {
        console.error('Error creating dependency:', error)
        throw error
      }
    }

    return null
  }

  function areSameBranchGroups(groupId1: number, groupId2: number) {
    return (
      groupId1 === groupId2 ||
      !descendantsMap.value.get(groupId1)?.has(groupId2) ||
      !descendantsMap.value.get(groupId2)?.has(groupId1)
    )
    // return groupsStore.collectDescendantLeafNodeIds(checkedGroup.id.valueOf()).includes(group.id.valueOf())
  }

  function getPeriodsMap(
    sourceCourses: Course[],
    targetCourses: Course[]
  ): Map<number, { sources: Course[]; targets: Course[] }> {
    const periodsMap = new Map<number, { sources: Course[]; targets: Course[] }>()

    // Populate periodsMap with source courses
    sourceCourses.forEach((course) => {
      const periodId = course.period?.id
      if (periodId) {
        if (!periodsMap.has(periodId)) {
          periodsMap.set(periodId, { sources: [], targets: [] })
        }
        periodsMap.get(periodId)?.sources.push(course)
      }
    })

    // Populate periodsMap with target courses
    targetCourses.forEach((course) => {
      const periodId = course.period?.id
      if (periodId) {
        if (!periodsMap.has(periodId)) {
          periodsMap.set(periodId, { sources: [], targets: [] })
        }
        periodsMap.get(periodId)?.targets.push(course)
      }
    })

    return periodsMap
  }

  async function addDependencyFromTemp(
    tempDependencyId: string,
    successive: boolean,
    days: number,
    mode?: CreationMode
  ): Promise<void> {
    const edge: CustomEdge = removeTempDependency(tempDependencyId)

    const node1 = nodes.value.find((node) => node.id === edge.source)
    const node2 = nodes.value.find((node) => node.id === edge.target)

    if (!node1 || !node2) {
      console.log('Nodes not found')
      return
    }

    if (node1.type === 'course' && node2.type === 'course') {
      const existingEdge: CustomEdge | undefined = edges.value.find(
        (edge) => edge.source === node1.id && edge.target === node2.id
      )

      // modifies the existing edge by patching dependency
      if (existingEdge) {
        const existingDependency = dependencyStore.dependencies.find(
          (dependency) => dependency.id === parseInt(existingEdge.id)
        )

        if (existingDependency) {
          const updateDependencyData: DependencyUpdateData = {
            id: existingDependency.id,
            successive: successive,
            day_gap: days,
          }

          const modifiedDependency = await dependencyStore.updateDependency(updateDependencyData)

          if (modifiedDependency) {
            const modifiedEdge = generateEdge(modifiedDependency)

            let relatedGraph: DependencyGraph | undefined = undefined
            let edgeIndex: number = -1

            for (const graph of graphs.value) {
              edgeIndex = graph.edges.findIndex((edge) => edge.id === existingEdge.id)
              if (edgeIndex !== -1) {
                relatedGraph = graph
                break
              }
            }

            relatedGraph!.edges[edgeIndex] = modifiedEdge

            edgeMountKey.value = 1 - edgeMountKey.value
            refreshGraphStatus()
          }
        }
      }
      // creates dependency completely, useful if you decide to cut the edge while having a tempDependency
      else {
        const createdDependency = await createDependency(successive, days, parseInt(edge.source), parseInt(edge.target))

        if (createdDependency) {
          const nodeGraph1: DependencyGraph | undefined = getNodeGraph(node1.id)
          const nodeGraph2: DependencyGraph | undefined = getNodeGraph(node2.id)

          if (!nodeGraph1 || !nodeGraph2) {
            console.log("Couldn't fetch related graphs")
            return
          }

          if (nodeGraph1.id !== nodeGraph2.id) {
            reload()
          }

          // Add the edge to the graph
          nodeGraph1.edges.push(generateEdge(createdDependency))
          refreshGraphStatus()
        }
      }
    } else if (node1.type === 'course-group' && node2.type === 'course' && node1.data.courses) {
      for (const course of node1.data.courses) {
        await createDependency(successive, days, course.id, parseInt(edge.target))
      }
      reload()
    } else if (node1.type === 'course' && node2.type === 'course-group' && node2.data.courses) {
      for (const course of node2.data.courses) {
        await createDependency(successive, days, parseInt(edge.source), course.id)
      }
      reload()
    } else if (node1.data.courses && node2.data.courses) {
      const periodsMap = getPeriodsMap(node1.data.courses, node2.data.courses)

      // Treats each period one by one
      for (const periodGroup of periodsMap.values()) {
        // Create dependencies only if there are both source and target courses in the current period
        if (periodGroup.sources.length && periodGroup.targets.length) {
          for (const sourceCourse of periodGroup.sources) {
            for (const targetCourse of periodGroup.targets) {
              if (mode === CreationMode.SMART) {
                // Keeps only same module courses
                if (sourceCourse.module.id === targetCourse.module.id) {
                  // Collects complete source groups from the store
                  const sourceCourseGroups = sourceCourse.groups.map((group) =>
                    groupsStore.groups.find((completeGroup) => group.id === completeGroup.id.valueOf())
                  )
                  // Collects complete target groups from the store
                  const targetCourseGroups = targetCourse.groups.map((group) =>
                    groupsStore.groups.find((completeGroup) => group.id === completeGroup.id.valueOf())
                  )

                  // Creates the dependency only if the courses have common groups, or if any groups have a parent child relationship
                  if (
                    sourceCourseGroups.some(
                      (group) =>
                        group &&
                        targetCourseGroups.some(
                          (targetCourseGroup) =>
                            targetCourseGroup &&
                            (targetCourseGroup.id === group?.id ||
                              areSameBranchGroups(group.id.valueOf(), targetCourseGroup.id.valueOf()))
                        )
                    )
                  ) {
                    await createDependency(successive, days, sourceCourse.id, targetCourse.id)
                  }
                }
              } else {
                await createDependency(successive, days, sourceCourse.id, targetCourse.id)
              }
            }
          }
        }
      }
      reload()
    }
    refreshGraphStatus()
  }

  function reload(): void {
    generateGraphs()
    graphs.value = layoutAllGraphs(graphs.value)
  }

  // small trick to remount edge on dependency update to bypass label alignment problems
  const edgeMountKey = ref<number>(0)

  const edges: Ref<CustomEdge[]> = computed(() => [...tempEdges.value, ...graphs.value.flatMap((graph) => graph.edges)])
  const nodes: Ref<CustomNode[]> = computed(() => [
    ...courseGroupNodes.value,
    ...graphs.value.flatMap((graph) => graph.nodes),
  ])
  const erroredGraphs: Ref<DependencyGraph[]> = computed(() =>
    graphs.value.filter((graph) => graph.status !== GraphStatus.VALID)
  )

  return {
    createGroup,
    removeCourseGroup,
    inGroups,
    createTempDependency,
    removeTempDependency,
    handleConnection,
    cutEdge,
    addDependencyFromTemp,
    reload,
    logicColorMode,
    graphs,
    courseGroupNodes,
    edgeMountKey,
    edges,
    nodes,
    erroredGraphs,
  }
}
