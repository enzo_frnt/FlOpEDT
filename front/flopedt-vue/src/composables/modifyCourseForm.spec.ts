import type { Course, CourseCreateData } from '@/ts/type'
import { useCourseStore } from '@/stores/timetable/course'
import { describe, expect, it, beforeEach } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'

// TODO: make API mocks to make it work - or drop them and rely on E2E tests instead
describe.skip('Modify Course Store', () => {
  beforeEach(() => {
    // creates a fresh pinia and makes it active,
    // so it's automatically picked up by any useStore() call
    // without having to pass it to it: `useStore(pinia)`
    setActivePinia(createPinia())
  })

  it('Create a course', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    const courseExemple = courseStore.courses[0]
    expect(courseExemple).not.toBeUndefined()

    const courseToPost = {
      ...toFlatCouse(courseExemple),
      supp_tutors: [{ id: 148 }],
      duration: '01:30:00',
    }

    const ret = await courseStore.postCourse(courseToPost)
    expect(ret).not.toBeNull()
    expect(ret.type).toStrictEqual(courseToPost.type_id)
  })

  it('Create multiple courses', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    const coursesExemple = [courseStore.courses[1], courseStore.courses[2], courseStore.courses[3]]
    expect(coursesExemple).not.toContain(undefined)

    const coursesToPost = coursesExemple.map((course) => ({
      ...toFlatCouse(course),
      supp_tutors: [{ id: 148 }],
      duration: '01:30:00',
    }))

    const ret = []
    for (let i = 0; i < coursesToPost.length; i++) {
      const newCourse = await courseStore.postCourse(coursesToPost[i])
      ret.push(newCourse)
    }

    expect(ret).toHaveLength(3)
    expect(ret).not.toContain(null)

    expect(ret[0].type).toStrictEqual(coursesToPost[0].type_id)
    expect(ret[1].type).toStrictEqual(coursesToPost[1].type_id)
    expect(ret[2].type).toStrictEqual(coursesToPost[2].type_id)
  })

  // Now we have 4 tests courses with IDs from
  it('Modify a course', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    // Get a course
    const courseExample: Course | undefined = courseStore.courses.find((course) => course.id === 1)
    expect(courseExample).not.toBeUndefined()

    // Modify it
    const courseToModify = { id: courseExample!.id, type: courseExample!.type.id, duration: '03:00:00' }

    // Patch the modification
    const ret = await courseStore.updateCourse(courseToModify)
    expect(ret).not.toBeNull()
    expect(ret.duration).toBe('03:00:00')
  })

  it('Modify multiple courses', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    const coursesExamples = getMultipleCourses()
    expect(coursesExamples).not.toContain(undefined)

    const coursesToModify = coursesExamples.map((course) => ({
      id: course.id,
      duration: '06:54:32',
    }))

    const ret = []
    for (let i = 0; i < coursesToModify.length; i++) {
      const newCourse = await courseStore.updateCourse(coursesToModify[i])
      ret.push(newCourse)
    }

    expect(ret).toHaveLength(3)
    expect(ret).not.toContain(null)

    expect(ret[0].duration).toBe('06:54:32')
    expect(ret[1].duration).toBe('06:54:32')
    expect(ret[2].duration).toBe('06:54:32')
  })

  it('Delete a course', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    const courseToDelete = courseStore.courses.find((course) => course.id === 1)
    expect(courseToDelete).not.toBeUndefined()

    await courseStore.deleteCourse(courseToDelete!)
    expect(courseStore.courses).not.toContain(courseToDelete)
  })

  it('Delete multiple courses', async () => {
    const courseStore = useCourseStore()
    await courseStore.fetchAllCourses('INFO')

    const coursesToDelete = getMultipleCourses()
    expect(coursesToDelete).not.toContain(undefined)

    const ret = []
    for (let i = 0; i < coursesToDelete.length; i++) {
      const newCourse = await courseStore.deleteCourse(coursesToDelete[i])
      ret.push(newCourse)
    }

    expect(ret).toHaveLength(3)
    expect(ret[0]).toBe(true)
    expect(ret[1]).toBe(true)
    expect(ret[2]).toBe(true)
    expect(courseStore.courses).not.toContain(coursesToDelete[0])
    expect(courseStore.courses).not.toContain(coursesToDelete[1])
    expect(courseStore.courses).not.toContain(coursesToDelete[2])
  })
})

function getMultipleCourses(): Course[] {
  const courseStore = useCourseStore()
  const c1: Course = courseStore.courses.find((course) => course.id === 2)!
  const c2: Course = courseStore.courses.find((course) => course.id === 3)!
  const c3: Course = courseStore.courses.find((course) => course.id === 4)!
  return [c1, c2, c3]
}

function toFlatCouse(course: Course): CourseCreateData & { id?: number } {
  return {
    id: course.id,
    type_id: course.type.id,
    room_type_id: course.room_type.id,
    period_id: course.period?.id ?? void 0,
    group_ids: course.groups.map((g) => g.id),
    tutor_id: course.tutor?.id ?? void 0,
    supp_tutor_ids: course.supp_tutors.map((t) => t.id),
    module_id: course.module.id,
    is_graded: course.is_graded,
    duration: course.duration,
    modulesupp_id: course.module.id, // TODO: fix this
    pay_module_id: course.module.id, // TODO: fix this
  }
}
