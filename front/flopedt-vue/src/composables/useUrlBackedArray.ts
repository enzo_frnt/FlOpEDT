import { type Ref, watch, ref } from 'vue'
import { useRoute, useRouter } from 'vue-router'
import { isEqual } from 'lodash-es'

function toArray<T>(a: T | (T | null)[] | null | undefined): T[] {
  // ;eslint-disable-next-line eqeqeq -- Shorter way to check for both null and undefined.
  return a == null ? [] : Array.isArray(a) ? a.filter((v) => v !== null) : [a]
}

export default function useUrlBackedArray(key: string): Ref<string[]> {
  const router = useRouter()
  const route = useRoute()

  const data = ref<string[]>([])

  function pushQuery(v?: string[]) {
    void router.push({
      query: {
        ...route.query,
        [key]: v ?? data.value,
      },
    })
  }

  watch(
    route,
    () => {
      const newValue = toArray(route.query[key])
      if (!isEqual(newValue, data.value)) data.value = newValue
    },
    { deep: true, immediate: true }
  )

  watch(
    data,
    () => {
      const oldValue = toArray(route.query[key])
      if (!isEqual(data.value, oldValue)) pushQuery()
    },
    { deep: true }
  )

  return data
}
