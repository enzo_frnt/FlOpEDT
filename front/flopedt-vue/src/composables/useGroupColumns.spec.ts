import { describe, it, expect, beforeEach } from 'vitest'
import { collapseColumns, useGroupColumns } from '@/composables/useGroupColumns.ts'
import { StructuralGroup, TransversalGroup } from '@/ts/entities/group.ts'
import { Identifier } from '@/ts/entities/base/identifier.ts'
import { StructuralGroupTreeNode } from '@/stores/departments/groups/structuralGroups.ts'

const groupsMap = new Map<number, StructuralGroupTreeNode>()
const transversalGroupsMap = new Map<number, TransversalGroup>()
const descendantsMap = new Map<number, Set<number>>()
beforeEach(() => {
  groupsMap.clear()
  descendantsMap.clear()
})

function updateDescendants(group: number, child: number) {
  for (const [g, desc] of descendantsMap) {
    if (g === group || desc.has(group)) {
      desc.add(child)
    }
  }
}

function createStGroup(id: number, parent: number | null) {
  const group = new StructuralGroup(
    new Identifier(id),
    `S${id}`,
    `FLOP-T${id}`,
    0,
    0,
    0,
    1,
    parent,
    false
  ) as StructuralGroupTreeNode
  groupsMap.set(id, group)
  descendantsMap.set(id, new Set())

  if (parent) {
    const parentGroup = groupsMap.get(parent)!
    if (!parentGroup.children) parentGroup.children = []
    parentGroup.children.push(group)

    updateDescendants(parent, id)
  }

  return group
}

function createTrGroup(id: number, structuralIds: number[]) {
  const group = new TransversalGroup(new Identifier(id), `T${id}`, `FLOP-T${id}`, 0, 0, 0, 1, structuralIds, [])
  transversalGroupsMap.set(id, group)
  return group
}

function createBasicGroupTree() {
  const root = createStGroup(1, null)
  createStGroup(2, 1)
  createStGroup(3, 1)
  createStGroup(4, 2)
  createStGroup(5, 2)
  createStGroup(6, 3)
  createStGroup(7, 3)
  return [root]
}

describe('useGroupsColumn', () => {
  it('should create columns for group trees', () => {
    const tree = createBasicGroupTree()
    const expectedColumns = [
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      { id: expect.anything(), name: 'S4' },
      { id: expect.anything(), name: 'S5' },
      { id: expect.anything(), name: 'S6' },
      { id: expect.anything(), name: 'S7' },
      /* eslint-enable @typescript-eslint/no-unsafe-assignment */
    ]

    const expectedGroupsColumnMap = new Map([
      [1, [{ start: 0, end: 4 }]],
      [2, [{ start: 0, end: 2 }]],
      [3, [{ start: 2, end: 4 }]],
      [4, [{ start: 0, end: 1 }]],
      [5, [{ start: 1, end: 2 }]],
      [6, [{ start: 2, end: 3 }]],
      [7, [{ start: 3, end: 4 }]],
    ])

    const result = useGroupColumns(tree, descendantsMap, [], []).value
    expect(result.columns).toEqual(expectedColumns)
    expect(result.groupsColumnMap).toEqual(expectedGroupsColumnMap)
    expect(result.columns.map((c) => c.id)).toEqual([...new Set(result.columns.map((c) => c.id))])
  })

  it('should not consider groups that are not selected to build the columns', () => {
    const tree = createBasicGroupTree()
    const selected = [groupsMap.get(3)!, groupsMap.get(4)!]
    const expectedColumns = [
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      { id: expect.anything(), name: 'S4' },
      { id: expect.anything(), name: 'S6' },
      { id: expect.anything(), name: 'S7' },
      /* eslint-enable @typescript-eslint/no-unsafe-assignment */
    ]

    const expectedGroupsColumnMap = new Map([
      [1, [{ start: 0, end: 3 }]],
      [2, [{ start: 0, end: 1 }]],
      [3, [{ start: 1, end: 3 }]],
      [4, [{ start: 0, end: 1 }]],
      [6, [{ start: 1, end: 2 }]],
      [7, [{ start: 2, end: 3 }]],
    ])

    const result = useGroupColumns(tree, descendantsMap, [], selected).value
    expect(result.columns).toEqual(expectedColumns)
    expect(result.groupsColumnMap).toEqual(expectedGroupsColumnMap)
    expect(result.columns.map((c) => c.id)).toEqual([...new Set(result.columns.map((c) => c.id))])
  })

  it('should not include information about groups who are not selected and do not have a descendant selected', () => {
    const tree = createBasicGroupTree()
    const selected = [groupsMap.get(3)!]
    const expectedColumns = [
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      { id: expect.anything(), name: 'S6' },
      { id: expect.anything(), name: 'S7' },
      /* eslint-enable @typescript-eslint/no-unsafe-assignment */
    ]

    const expectedGroupsColumnMap = new Map([
      [1, [{ start: 0, end: 2 }]],
      [3, [{ start: 0, end: 2 }]],
      [6, [{ start: 0, end: 1 }]],
      [7, [{ start: 1, end: 2 }]],
    ])

    const result = useGroupColumns(tree, descendantsMap, [], selected).value
    expect(result.columns).toEqual(expectedColumns)
    expect(result.groupsColumnMap).toEqual(expectedGroupsColumnMap)
    expect(result.columns.map((c) => c.id)).toEqual([...new Set(result.columns.map((c) => c.id))])
  })

  it('should put transversal groups in the columns of the structural groups they are conflicting with', () => {
    const tree = createBasicGroupTree()
    const transversal = [createTrGroup(100, [4, 5]), createTrGroup(101, [4, 7]), createTrGroup(102, [4, 3])]
    const expectedColumns = [
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      { id: expect.anything(), name: 'S4' },
      { id: expect.anything(), name: 'S5' },
      { id: expect.anything(), name: 'S6' },
      { id: expect.anything(), name: 'S7' },
      /* eslint-enable @typescript-eslint/no-unsafe-assignment */
    ]

    const expectedGroupsColumnMap = new Map([
      [1, [{ start: 0, end: 4 }]],
      [2, [{ start: 0, end: 2 }]],
      [3, [{ start: 2, end: 4 }]],
      [4, [{ start: 0, end: 1 }]],
      [5, [{ start: 1, end: 2 }]],
      [6, [{ start: 2, end: 3 }]],
      [7, [{ start: 3, end: 4 }]],
      [100, [{ start: 0, end: 2 }]],
      [
        101,
        [
          { start: 0, end: 1 },
          { start: 3, end: 4 },
        ],
      ],
      [
        102,
        [
          { start: 0, end: 1 },
          { start: 2, end: 4 },
        ],
      ],
    ])

    const result = useGroupColumns(tree, descendantsMap, transversal, []).value
    expect(result.columns).toEqual(expectedColumns)
    expect(result.groupsColumnMap).toEqual(expectedGroupsColumnMap)
  })

  it('should handle transversal groups being selected but not the relevant structural groups directly', () => {
    const tree = createBasicGroupTree()
    const transversal = [createTrGroup(100, [4, 5]), createTrGroup(101, [4, 7]), createTrGroup(102, [4, 3])]
    const selected = [transversalGroupsMap.get(101)!, groupsMap.get(5)!]
    const expectedColumns = [
      /* eslint-disable @typescript-eslint/no-unsafe-assignment */
      { id: expect.anything(), name: 'S4' },
      { id: expect.anything(), name: 'S5' },
      { id: expect.anything(), name: 'S7' },
      /* eslint-enable @typescript-eslint/no-unsafe-assignment */
    ]

    const expectedGroupsColumnMap = new Map([
      [1, [{ start: 0, end: 3 }]],
      [2, [{ start: 0, end: 2 }]],
      [3, [{ start: 2, end: 3 }]],
      [4, [{ start: 0, end: 1 }]],
      [5, [{ start: 1, end: 2 }]],
      [7, [{ start: 2, end: 3 }]],
      [
        101,
        [
          { start: 0, end: 1 },
          { start: 2, end: 3 },
        ],
      ],
    ])

    const result = useGroupColumns(tree, descendantsMap, transversal, selected).value
    expect(result.columns).toEqual(expectedColumns)
    expect(result.groupsColumnMap).toEqual(expectedGroupsColumnMap)
    expect(result.columns.map((c) => c.id)).toEqual([...new Set(result.columns.map((c) => c.id))])
  })
})

describe('collapseColumns', () => {
  it('should collapse columns that are next to each other as one big column', () => {
    const test = [
      { start: 0, end: 1 },
      { start: 1, end: 2 },
      { start: 3, end: 5 },
      { start: 4, end: 6 },
    ]

    expect(collapseColumns(test)).toEqual([
      { start: 0, end: 2 },
      { start: 3, end: 6 },
    ])
  })

  it('should handle columns not being ordered', () => {
    const test = [
      { start: 4, end: 6 },
      { start: 0, end: 1 },
      { start: 3, end: 5 },
      { start: 1, end: 2 },
    ]

    expect(collapseColumns(test)).toEqual([
      { start: 0, end: 2 },
      { start: 3, end: 6 },
    ])
  })

  it('should get rid of columns "swallowed" by a larger column', () => {
    const test = [
      { start: 0, end: 3 },
      { start: 1, end: 2 },
    ]

    expect(collapseColumns(test)).toEqual([{ start: 0, end: 3 }])
  })
})
