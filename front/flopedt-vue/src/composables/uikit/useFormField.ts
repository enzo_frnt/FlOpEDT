// This composable is meant for internal use within the UIKit
// You shouldn't need to use it in application-level components

import { type ComponentPublicInstance, type ModelRef, type Ref, watch, ref, computed } from 'vue'
import { useI18n } from 'vue-i18n'

type ElementWithValidation = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement

type ValidatableInstance = Omit<ComponentPublicInstance, '$el'> & { $el: ElementWithValidation }

function isValidatorTarget(ref: Ref<ComponentPublicInstance | undefined>): ref is Ref<ValidatableInstance> {
  return !!(
    ref.value &&
    (ref.value.$el instanceof HTMLInputElement ||
      ref.value.$el instanceof HTMLSelectElement ||
      ref.value.$el instanceof HTMLTextAreaElement)
  )
}

export function useFormField<T>(
  value: ModelRef<T>,
  userError: Ref<string | undefined>,
  validator: Ref<((value: T) => string | null | undefined) | undefined>,
  disableAutoClean?: boolean
) {
  const i18n = useI18n()
  const inputRef = ref<ComponentPublicInstance>()

  const controlledUserError = ref(userError.value)
  const internalError = ref<string>()

  const error = computed(() => (internalError.value ? i18n.t(internalError.value) : controlledUserError.value))

  function clearErrors() {
    controlledUserError.value = undefined
    internalError.value = undefined
  }

  function performValidation() {
    if (isValidatorTarget(inputRef)) {
      inputRef.value.$el.checkValidity()
      if (!inputRef.value.$el.validity.valid) {
        for (const k in inputRef.value.$el.validity) {
          if (k === 'valid' || k === 'customError') continue

          if (inputRef.value.$el.validity[k as keyof ValidityState]) {
            internalError.value = `uikit.form.validation.native.${k}`
            return
          }
        }
      }
    }

    if (validator.value) {
      const validationError = validator.value(value.value)
      if (validationError) {
        internalError.value = validationError
      }
    }
  }

  if (!disableAutoClean) {
    // Clear errors when input value changes
    watch(value, clearErrors)
  }

  // Display error again when it changes
  watch(userError, () => {
    controlledUserError.value = userError.value
    internalError.value = undefined
  })

  // Set error message
  watch(error, () => {
    if (isValidatorTarget(inputRef)) {
      inputRef.value.$el.setCustomValidity(error.value ? error.value : '')
    }
  })

  return {
    inputRef,
    error,
    performValidation,
    clearErrors,
  }
}
