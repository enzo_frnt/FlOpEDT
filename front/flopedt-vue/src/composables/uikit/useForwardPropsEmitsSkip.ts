// Like Radix Vue's useForwardPropsEmits, with the possibility to exclude certain keys and models.

import { type MaybeRefOrGetter, camelize, computed, toHandlerKey, toValue, DefineProps } from 'vue'
import { useEmitAsProps, useForwardProps } from 'radix-vue'

type KeysOf<T extends Record<string, unknown>> = (T extends Record<string, unknown> ? keyof T : never) | `@${string}`

export function useForwardPropsEmitsSkip<T extends Record<string, unknown>, N extends string>(
  props: MaybeRefOrGetter<DefineProps<T, never>> | null,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Can't use anything else here
  emits: ((name: N, ...args: any[]) => void) | null,
  skip?: MaybeRefOrGetter<Array<KeysOf<T>>>,
  models?: MaybeRefOrGetter<string[]>
) {
  const forwardProps = props ? useForwardProps(props) : null
  const forwardEmits = emits ? useEmitAsProps(emits) : null

  return computed(() => {
    const skipValue = toValue(skip)
    const modelsValue = toValue(models)
    const res: Record<string, unknown> = {}

    if (forwardProps) Object.assign(res, forwardProps.value)
    if (forwardEmits) Object.assign(res, forwardEmits)

    if (skipValue) {
      for (const s of skipValue) {
        const key = s.startsWith('@') ? toHandlerKey(camelize(s.slice(1))) : s
        delete res[key]
      }
    }

    delete res.modelValue
    delete res[toHandlerKey(camelize('update:modelValue'))]

    if (modelsValue) {
      for (const m of modelsValue) {
        delete res[m]
        delete res[toHandlerKey(camelize(`update:${m}`))]
      }
    }

    return res
  })
}
