import { defineConfig } from '@playwright/test'
import { fileURLToPath } from 'url'

export default defineConfig({
  // Look for test files in the "e2e" directory, relative to this configuration file.
  testDir: 'e2e',

  // Run all tests in parallel.
  fullyParallel: true,

  // Fail the build on CI if you accidentally left test.only in the source code.
  forbidOnly: !!process.env.CI,

  // Retry on CI only.
  retries: process.env.CI ? 2 : 0,

  // Opt out of parallel tests on CI.
  workers: process.env.CI ? 1 : undefined,

  // Reporter to use
  reporter: [
    ['html', { open: 'never', outputFolder: './.reports/playwright' }],
    ['junit', { outputFile: './.reports/playwright/junit.xml' }],
  ],

  use: {
    baseURL: 'http://localhost:4173',
    trace: 'on-first-retry',
  },
  webServer: [
    {
      command: 'yarn run preview',
      url: 'http://localhost:4173',
      reuseExistingServer: !process.env.CI,
    },
    {
      cwd: fileURLToPath(new URL('../../back/flopedt-django', import.meta.url)),
      command: 'poetry run python3 manage.py runserver',
      url: 'http://localhost:8000',
      reuseExistingServer: !process.env.CI,
    },
  ],
})
