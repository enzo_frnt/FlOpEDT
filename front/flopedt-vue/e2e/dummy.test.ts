import { test, expect } from '@playwright/test'

test('it works', async ({ page }) => {
  await page.goto('/')
  expect(await page.title()).toBe('flop!')
})
